# Kitten Web Application

These are web routes served by all Kitten applications.

They are namespaced by purpose:

  - 💕 = Small Web Protocol routes
  - 🐱 = Kitten-specific features (libraries, etc.)

## 💕 Small Web Protocol routes

Namespaced under the 💕 path, the Small Web Protocol routes include:

  - __hello/<token>/__: Initial generation of secret and identity for Small Web place owner.
  - __id__: Serves the ID (ed25519 public key) of the Small Web place owner. Used by other Small Web places to send end-to-end encrypted messages as well as to verify signatures on public messages.
  - __sign-in__: Client-side implementation of sign-in via public-key encryption.
  - __sign-out__: Client-side sign-out.
  - 🔒__settings__: Authenticated route for Small Web place settings.
  - 🔒__settings/domain__: Authenticated route for Small Web place owner to manage their relationship with their [Domain](https://codeberg.org/domain/app) host.

## 🐱 Kitten routes

  - __images__: Images used by the various Kitten routes, error messages, etc.
  - __library__: Kitten libraries that are used by Kitten apps (HTMX, Alpine.JS, Water, etc.) as well as the full set of highlight.js styles used for syntax highlighting.
