/**
  Progress indicator component.
*/

import { html } from '@small-web/kitten'

export default ({
  title='Please wait…',
  height='1lh'
}={}) => html`
  <svg
    class='ProgressIndicator'
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
  >
    <title>${title}</title>
    <g fill="none" stroke-width="1.5" stroke="currentColor">
      <path stroke-linecap="round" stroke-linejoin="round" d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
    </g>
    <g transform="translate(5 5) scale(0.6)">
      <g>
        <rect x="11" y="1" width="2" height="5" opacity=".14"/>
        <rect x="11" y="1" width="2" height="5" transform="rotate(30 12 12)" opacity=".29"/>
        <rect x="11" y="1" width="2" height="5" transform="rotate(60 12 12)" opacity=".43"/>
        <rect x="11" y="1" width="2" height="5" transform="rotate(90 12 12)" opacity=".57"/>
        <rect x="11" y="1" width="2" height="5" transform="rotate(120 12 12)" opacity=".71"/>
        <rect x="11" y="1" width="2" height="5" transform="rotate(150 12 12)" opacity=".86"/>
        <rect x="11" y="1" width="2" height="5" transform="rotate(180 12 12)"/>
        <animateTransform attributeName="transform" type="rotate" calcMode="discrete" dur="0.75s" values="0 12 12;30 12 12;60 12 12;90 12 12;120 12 12;150 12 12;180 12 12;210 12 12;240 12 12;270 12 12;300 12 12;330 12 12;360 12 12" repeatCount="indefinite"/>
      </g>
    </g>
    <style>
      .ProgressIndicator {
        display: inline-block;
        height: ${height};
        vertical-align: top;
        margin-right: 0.5ch;
      }
    </style>
  </svg>
`
