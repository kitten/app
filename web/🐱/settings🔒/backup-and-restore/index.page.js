/**
  Settings: Backup and restore page.
*/

import fs from 'node:fs/promises'
import path from 'node:path'
import { extract } from 'tar'
import kitten, { Upload } from '@small-web/kitten'

import Settings from '../Settings.layout.js'
import ProgressIndicator from '../components/ProgressIndicator.component.js'

export default function () {
  return kitten.html`
    <${Settings} activeSection='backup-and-restore'>
      <markdown>
        ## 📦 Backup and restore

        ### Backup

        This will download a full backup of your data, including:

        - [Internal database (&lowbar;db)](/🐱/settings/_db/)
        - [App database (db)](/🐱/settings/db/)
        - [Uploads](/🐱/settings/uploads/)

        <form method='GET' action='/💕/data/backup/'>
          <button type='submit'>Download backup</button>
        </form>

        ### Restore

        This will restore your Small Web place from a full backup of your data. That includes your internal database (so your identity, secret, etc.) as well as the app database and uploads (if any).
      </markdown>

      <form
        class='danger'
        hx-post='/💕/data/restore/'
        hx-encoding='multipart/form-data'
        hx-confirm='Are you sure you want to replace all current app data (database, uploads, etc.) with the uploaded backup file?'
        hx-trigger='click from:button[name="startRestore"]'
        hx-swap='none'
      >
        <label for='dataBackupFile'>Data backup file (.kitten.data.tar.gz)</label>
        <!--
          Can’t limit the file input to files with the .kitten.data.tar.gz
          extension as Safari can only handle one level in the extension.
          See: https://bugs.webkit.org/show_bug.cgi?id=273203
          (Thank you Timothy Hatcher for filing the bug based on my fediverse rant.)
        -->
        <div style='display: flex;'>
          <input
            id='dataBackupFile'
            name='dataBackupFile'
            type='file'
            accept='.gz'
            required
          ></input>
          <button
            name='startRestore'
            connect
          >Restore</button>
        </div>
        <div id='restore-status' />
      </form>
    </>
  `
}

const RestoreStatusError = ({SLOT}) => kitten.html`
  <div
    id='restore-status'
    class='error'
  >${SLOT}</div>
`

const RestoreStatusUpdate = ({SLOT}) => kitten.html`
  <div id='restore-status' class='progress'>
    <${ProgressIndicator} title='Restoring data…'/>
    ${SLOT}
    <style>
      .progress {
        background-color: var(--background-body);
        font-weight: bold;
      }
    </style>
  </div>
`

export function onConnect ({ page, request }) {
  // Since the event dataUploadedForRestore event is dispatched on
  // the session and since the person might have multiple tabs open, we need
  // to flag that we’re the page that’s listening.
  let waitingForDatabaseUpload = false
  
  page.on('startRestore', () => {
    waitingForDatabaseUpload = true
    page.send(kitten.html`<${RestoreStatusUpdate}>Uploading backup to restore from…</>`)
  })

  const restoreData = async (/** @type {Upload} */ upload) => {
    if (upload == undefined) {
      page.send(kitten.html`<${RestoreStatusError}>❌ Upload not found.</>`)
      return
    }

    if (!waitingForDatabaseUpload) {
      // Some other tab is waiting for this event, not us, ignore it.
      console.debug('dataUploadedForRestore event is not for this tab, ignoring.')
      return
    }
    // Reset the flag as we’re now handling the restore.
    waitingForDatabaseUpload = false

    // Show status indicator.
    page.send(kitten.html`<${RestoreStatusUpdate}>Restoring from backup…</>`)

    const appDataDirectory = globalThis.kitten.paths.APP_DATA_DIRECTORY

    // Since the uploaded backup file is, itself, part of the data, we need to
    // move it out of the data directory (as we’re going to nuke the data directory).
    const kittenTempDirectory = globalThis.kitten.paths.KITTEN_SAME_DEVICE_TEMP_DIRECTORY
    const backupInTemporaryFolder = path.join(kittenTempDirectory, 'backup.tar.gz')

    try {
      // Note: rename will replace an existing file, if there is one, so
      // we don’t have to check separately for that.
      await fs.mkdir(kittenTempDirectory, { recursive: true })
      await fs.rename(upload.filePath, backupInTemporaryFolder)

      // Delete current data.
      await fs.rm(appDataDirectory, { recursive: true, force: true })

      // Re-create data directory.
      await fs.mkdir(appDataDirectory, { recursive: true })
  
      // Decompress backed-up data.
      await extract({
        cwd: appDataDirectory,
        file: backupInTemporaryFolder
      })
    } catch (error) {
      console.error(error)
      page.send(kitten.html`<${RestoreStatusError}><p>❌ Could not restore backup.</p><pre><code>${error.message}</code></pre></>`)
      return
    }

    // Send refresh header and restart the server so the
    // restored data is loaded into memory.
    // TODO: Handle this via polling from client-side so it is precise.
    page.send(kitten.html`<${RestoreStatusUpdate}>
      <meta id='deploy-refresh' http-equiv='refresh' content='5;url=/🐱/settings/' />
      Restore complete, waiting for server restart…
    </>`)

    // Restart the server.
    process.exit(99 /* code for restart request */)
  }
  request.session.removeListener('dataUploadedForRestore', restoreData)
  request.session.addListener('dataUploadedForRestore', restoreData)
}
