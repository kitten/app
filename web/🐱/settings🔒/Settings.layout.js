import kitten, { html } from '@small-web/kitten'

import Footer from './Footer.component.js'
import Navigation from './Navigation.fragment.js'
// @ts-ignore CSS module.
import Styles from './Settings.fragment.css'

export default function ({ SLOT, activeSection = '', ...props }) {
  
  // @ts-ignore missing types TODO: Add _db.settings to @small-web/kitten.
  if (kitten._db.settings.id === undefined) {
    // An identity has not been created yet. Do not allow access.
    const forbiddenError = new Error()
    // @ts-ignore code does exist on Error instances.
    forbiddenError.code = 403
    forbiddenError.message = 'Forbidden'
    throw forbiddenError
  }

  return html`
    <div id='application'>
      <if ${!process.env.PRODUCTION}>
        <then>
          <p class='DevelopmentMode' ...${props}>🚧 Running in <strong>development</strong> mode.</p>
        </then>
      </if>

      <header id='settings-title'>
        <h1><img alt='Cute little grey kitten head icon' src='/🐱/images/kitten.svg'> Settings</h1>
      </header>

      <page title='💕 Small Web Place Settings' htmx htmx-websocket water syntax-highlighter>

      <${Navigation} activeSection=${activeSection} />

      <main id='content' ...${props}>
        ${SLOT}
      </main>

      <${Footer} />
      <${Styles} />
    </div>
  `
}
