import kitten from '@small-web/kitten'

import Settings from '../../Settings.layout.js'
import Pages from './Pages.fragment.js'

// @ts-ignore CSS module.
import Styles from './index.fragment.css'

/**
  Shows current state of live pages in Kitten’s memory.
*/
export default class PagesPage extends kitten.Page {
  html () {
    return kitten.html`
      <${Settings} activeSection='state/pages'>
        <markdown>
          ## 📃 State: pages

          Currently-active live pages.
        </markdown>

        <${Pages.connectedTo(this)} />

        <${Styles} />
      </>
    `
  }
}
