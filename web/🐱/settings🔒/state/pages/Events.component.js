import util from 'node:util'
import kitten from '@small-web/kitten'

/**
  Shows list of events for a live page.
*/
export default class Events extends kitten.Component {
  placeHolderIsVisible = true

  html () {
    return kitten.html`
      <tr>
        <th>Events</th>
        <td>
          <table class='eventsTable'>
            <thead>
              <tr>
                <th>Name</th>
                <th>Data</th>
              </tr>
            </thead>
            <tbody id='tbody-${this.id}'>
              <tr id='placeholder-${this.id}'>
                <td>None yet.</td>
                <td>None yet.</td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    `
  }

  onConnect () {
    this.addEventHandler(kitten.events, `kittenPageEvent-${this.data.page.id}`, this.showPageEvent)
  }

  /**
    Stream new page event details to top of the events list,
    hiding the placeholder if necessary.

    @param {{
      name: string,
      data: Object<string, any>
    }} event
  */
  showPageEvent (event) {
    // Hide placeholder if necessary.
    if (this.placeHolderIsVisible) {
      this.page.send(kitten.html`
        <tr id='placeholder-${this.id}' swap-target='delete'>
      `)
      this.placeHolderIsVisible = false
    }

    // Send the new event row with event details.
  this.page.send(
    kitten.html`
      <tr>
        <td>${event.name}</td>
        <td>${util.inspect(event.data)}</td>
      </tr>
    `,
    { asFirstChildOf: `#tbody-${this.id}`}
  )
  }
}
