import kitten from '@small-web/kitten'
import Page from './Page.component.js'

/**
  @typedef {import('@small-web/kitten').KittenPage} KittenPage
*/

/**
  Displays list of live pages in Kitten’s memory.
*/
export default class Pages extends kitten.Component {
  html() {
    // We could just display a “loading” message here
    // as we’re going to refresh the list at onConnect(),
    // but given how fast it all happens, we might was well
    // send the whole list instead of having an odd flash.
    const pages = Object.values(kitten.pages)

    return kitten.html`
      <ul id='pages' morph>
        ${pages.map(page => {
          return kitten.html`
            <${Page.connectedTo(this, { page })} />
          `
        })}
      </ul>
    `
  }

  onConnect () {
    // Send updated list of pages on connect so the old version
    // of this page can be removed. (The remove event
    // can come in before the connection is made.)
    this.update()

    // Display information about pages as they are added to Kitten’s memory.
    // (As people connect to them via their browsers.)
    this.addEventHandler(
      kitten.events,
      'kittenPageCreate',
      /**
        Show new page at top of pages list.

        @param {{ page: KittenPage }} event
      */
      function (event) {
        /** @type {Pages} */ (this).page.send(
          kitten.html`<${Page.connectedTo(this, { page: event.page })} />`,
          { asFirstChildOf: '#pages' }
        )
      }
    )
  }
}
