import kitten from '@small-web/kitten'
import Events from './Events.component.js'

/**
  Displays information about a specific live page in Kitten’s memory.
*/
export default class Page extends kitten.Component {
  html () {
    const page = this.data.page

    // All Kitten components have an automatically-generated UUID at `this.id`.
    return kitten.html`
      <li>
        <!--
          HTMl <details> controls must have  unique name if you want more than
          one to be open at any given time. Kitten ignores anything beyond a
          colon (:) in the element name when mapping element names on the client to
          event handler names on the server.
        -->
        <details
          name='detailToggle:${this.id}'
        >
          <summary>${page.request.url.replace('%F0%9F%92%95', '💕').replace('%F0%9F%90%B1', '🐱')}</summary>
          <table>
            <thead>
              <tr>
                <th>ID</th>
                <td>${page.id}</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Session</th>
                <td>
                  <if ${page.session === undefined}>
                    None
                  <else>
                    <a href='/🐱/settings/state/sessions/#${page.session.id}'>${page.session.id}</a>
                  </if>
                </td>
              </tr>
              <tr>
                <th>Sockets</th>
                <td>${page.sockets === undefined ? 'Not connected yet.' : page.sockets.length}</td>
              </tr>
              <${Events.connectedTo(this, { page: this.data.page })} />
            </tbody>
          </table>
        </details>
      </li>
    `
  }

  onConnect () {
    // Update page details when the page being displayed connects to its socket.
    this.addEventHandler(kitten.events, `kittenPageConnect-${this.data.page.id}`, function () {
      this.update()
    })
    
    // Remove pages from display when they’re deleted from Kitten’s memory.
    this.addEventHandler(kitten.events, `kittenPageDelete-${this.data.page.id}`, function () {
      this.remove()
    })
  }
}
