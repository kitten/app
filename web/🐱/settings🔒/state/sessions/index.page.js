import Settings from '../../Settings.layout.js'

const Sessions = () => {
  const sessions = Object.values(kitten._db.sessions)

  return kitten.html`
    <table id='sessions morph'>
      <thead>
        <td>Auth</td>
        <td>Age</td>
        <td>ID</td>
        <td>Events</td>
      </thead>
      ${sessions.map(session => {
        const date = new Date(session.createdAt)
        const ageInMinutes = (Date.now() - date.getTime())/1000/60
        const ageInMinutesFloored = Math.floor(ageInMinutes)
        const ageInMinutesLabel = ageInMinutesFloored === 1 ? 'min' : 'mins'
        const ageInHours = ageInMinutes/60
        const ageInHoursFloored = Math.floor(ageInHours)
        const ageInHoursLabel = ageInHoursFloored === 1 ? 'hour' : 'hours'

        const ageRough = ageInHours < 1 ? `~${ageInMinutesFloored} ${ageInMinutesLabel}` : `~${ageInHoursFloored} ${ageInHoursLabel}`
        const agePrecise = `Created ${date.toLocaleDateString()} at ${date.toLocaleTimeString()}`

        return kitten.html`
          <tr id='${session.id}'>
            <td>${session.authenticated ? '✅' : ''}</td>
            <td title='${agePrecise}'>${ageRough}</td>
            <td>${session.id}</td>
            <td>${session._eventsCount}</td>
          </tr>
      `})}
    </table>
  `
}

const timers = {}
export function onConnect ({ page }) {
  timers[page.id] = setInterval(() => {
    page.send(kitten.html`<${Sessions} />`)
  }, 1000)
}

export function onDisconnect ({ page }) {
  if (timers[page.id] !== undefined) {
    clearInterval(timers[page.id])
  } else {
    console.warn('Could not find timer for page', page.id)
  }
}

export default async function () {

  return kitten.html`
    <${Settings} activeSection='state/sessions'>
      <markdown>
        ## 🙏 State: sessions

        Active sessions are listed below.
      </markdown>

      <${Sessions} />

      <style>
        table { table-layout: auto; }
        tr:target {
          border: 2px solid var(--accessible-deep-pink);
        }
      </style>
    </>
  `
}
