import Settings from '../../Settings.layout.js'

const Requests = () => kitten.html`
  <ol id='requests' morph>
    ${kitten.requests.map(request => kitten.html`<li>${request.replace('%F0%9F%92%95', '💕').replace('%F0%9F%90%B1', '🐱')}</li>`)}
  </ol>
`

const timers = {}
export function onConnect ({ page }) {
  timers[page.id] = setInterval(() => {
    page.send(kitten.html`<${Requests} />`)
  }, 250)
}

export function onDisconnect ({ page }) {
  if (timers[page.id] !== undefined) {
    clearInterval(timers[page.id])
  } else {
    console.warn('Could not find timer for page', page.id)
  }
}

export default async function () {

  return kitten.html`
    <${Settings} activeSection='state/requests'>
      <markdown>
        ## 🙏 State: requests

        Last 25 requests, latest at top.
      </markdown>

      <${Requests} />

      <style>
        #requests {
          list-style-type: none;
          padding: 0;
          margin: 0;
        }

        #requests li {
          margin-bottom: 0.5em;
        }

        #requests li:nth-of-type(2n) {
          background-color: var(--background-alt)
        }
      </style>
    </>
  `
}
