import systemInformation from 'systeminformation'
import Settings from '../../Settings.layout.js'

const Sessions = () => kitten.html`
  <a href='/🐱/settings/state/sessions/'>
    <div id='sessions' class='stat' morph>
      <h3>Sessions</h3>
      <div>${Object.keys(kitten._db.sessions).length}</div>
    </div>
  </a>
`

const Pages = () => kitten.html`
  <div id='pages' class='stat' morph>
    <h3>Pages</h3>
    <div>${Object.keys(kitten.pages).length}</div>
  </div>
`

const Events = () => kitten.html`
  <div id='events' class='stat' morph>
    <h3>Events</h3>
    <div>${Object.keys(kitten.events._events).length}</div>
  </div>
`
const Listeners = () => {
  const events = Object.entries(globalThis.kitten.events._events)
  const listenerCount = events.length === 0 ? 0 : events.reduce((previous, current) => previous += current[1].length, 0)

  return kitten.html`
    <div id='listeners' class='stat' morph>
      <h3>Listeners</h3>
      <div>${listenerCount}</div>
    </div>
  `
}

let processes = null
let processError = null
let cpu = null
let load = null

const updateProcessState = async () => {
  try {
    processes = await systemInformation.processes()
    cpu = await systemInformation.cpu()
    load = await systemInformation.currentLoad()
  } catch (error) {
    processes = null
    processError = error
  }
}
await updateProcessState()
setInterval(updateProcessState, 1000)

const MachineInformation = () => {
  const processorLabel = cpu.processors > 1 ? 'processors' : 'processor'

  return kitten.html`
    <div id='machine-information'>
      <h3>Machine</h3>
      <table>
        <tr>
          <th>Load</th>
          <td>${load.currentLoad.toFixed(2)}% (current), ${load.avgLoad.toFixed(2)}% (average)</td>
        </tr>
        <tr>
          <th>Processes</th>
          <td>${processes.all} (total), ${processes.running} (running), ${processes.blocked} (blocked), ${processes.sleeping} (sleeping)</td>
        </tr>
        <tr>
          <th>CPU Type</th>
          <td>${cpu.manufacturer} ${cpu.brand} ${cpu.speed}GHz (min ${cpu.speedMin}GHz, max ${cpu.speedMax}GHz)</td>
        </tr>
        <tr>
          <th>CPU Count</th>
          <td>${cpu.processors} ${processorLabel} (${cpu.cores} cores: ${cpu.physicalCores} physical, ${cpu.performanceCores} performance, ${cpu.efficiencyCores} efficiency)</td>
        </tr>
        <tr>
          <th>Virtualised?</th>
          <td>${cpu.virtualization ? 'Yes' : 'No'}</td>
        </tr>
      </table>
    </div>
  `
}

const SystemInformation = () => {
  if (processes === null) {
    return kitten.html`
      <div id='system-information' morph>
        <h3>Error</h3>
        <p>Could not get system information: ${processError.message}</p>
      </div>
    `
  }

  // Kitten processes
  const kittenProcessManagerProcesses = processes.list.filter(process => process.params.includes('kitten-process-manager.js'))
  const kittenProcesses = kittenProcessManagerProcesses.filter(
    process => kittenProcessManagerProcesses.find(otherProcess => otherProcess.pid === process.parentPid)?.name === 'node'
  )
  const kittenDaemonProcesses = processes.list.filter(process => process.params.includes('kitten-bundle.js'))

  if (kittenDaemonProcesses.length > 1) {
    // There should only be one Kitten daemon process running on any given
    // machine so something has clearly gone wrong here.
    return kitten.html`
      <div id='system-information' morph>
        <h3>Error</h3>
        <p>There should only be one Kitten daemon process per server but found ${kittenDaemonProcesses.length} processes:</p>
        <ul>
          ${kittenDaemonProcesses.map(kittenDaemonProcess => kitten.html`
            <li>[${kittenDaemonProcess.pid}] ${kittenDaemonProcess.command} ${kittenDaemonProcess.params}</li>
          `)}
        </ul>
      </div>
    `
  }

  const showProcessState = (process, custom = {}) => {
    return kitten.html`
      <div id='system-information' morph>
        <h3>Process</h3>
        <table>
          <tr>
            <th>Mode</th>
            <td>${custom.mode}</td>
          </tr>
          <tr>
            <th>State</th>
            <td>${process.state}</td>
          </tr>
          <tr>
            <th>CPU</th>
            <td>${process.cpu.toFixed(2)}% (user: ${process.cpuu.toFixed(2)}%, system: ${process.cpus.toFixed(2)}%)</td>
          </tr>
          <tr>
            <th>Memory</th>
            <td>${process.mem}%</td>
          </tr>
        </table>
      </div>
    `
  }
  // const showProcessDetails = process => {
  //   const dateTime = process.started.split(' ')
  //   print(`  ${chalk.bold('Owner     ')} ${process.user}`)
  //   print(`  ${chalk.bold('Started   ')} ${dateTime[0]} at ${dateTime[1]}`) 
  //   print(`  ${chalk.bold('ID        ')} ${process.pid}`)
  //   print(`  ${chalk.bold('Parent ID ')} ${process.parentPid}`)
  // }

  // What sort of process are we? (Interactive or deamon?)
  let currentProcess
  let currentProcessIsInteractive = false
  currentProcess = kittenProcessManagerProcesses.find(process => process.pid === globalThis.process.pid)
  if (currentProcess !== undefined) {
    currentProcessIsInteractive = true
  } else {
    currentProcess = kittenDaemonProcesses.find(process => process.pid === globalThis.process.pid)
    if (currentProcess === undefined) {
      return kitten.html`
        <div id='system-information' morph>
          <h3>Error</h3>
          <p>Cannot find current process.</p>
        </div>
      `
    }
  }

  // Current server statistics.
  const custom = {
    mode: (currentProcessIsInteractive ? 'interactive (development)' : 'daemon (run via systemd)')
  }
  if (!currentProcessIsInteractive) {
    let processPath = currentProcess.params.split('--working-directory=')[1]
    processPath = processPath.slice(0, processPath.indexOf(' '))
    processPath = '…' + processPath.slice(processPath.indexOf('/deployments'))
    custom.processPath = processPath
  }
  return showProcessState(currentProcess, custom)
}

const timers = {}
export function onConnect ({ page }) {
  timers[page.id] = setInterval(() => {
    page.send(kitten.html`<${Sessions} />`)
    page.send(kitten.html`<${Pages} />`)
    page.send(kitten.html`<${Events} />`)
    page.send(kitten.html`<${Listeners} />`)
    page.send(kitten.html`<${SystemInformation} />`)
    page.send(kitten.html`<${MachineInformation} />`)
  }, 1000)
}

export function onDisconnect ({ page }) {
  if (timers[page.id] !== undefined) {
    clearInterval(timers[page.id])
  } else {
    console.warn('Could not find timer for page', page.id)
  }
}

export default async function () {

  return kitten.html`
    <${Settings} activeSection='state/overview'>
      <markdown>
        ## 📈 State: overview

        This section shows you information about the current state of your server.
      </markdown>

      <div class='stats'>
        <${Sessions} />
        <${Pages} />
        <${Events} />
        <${Listeners} />
      </div>

      <${SystemInformation} />
      <${MachineInformation} />

      <style>
        table {
          table-layout: auto;
        }

        .stats {
          display: flex;
          column-gap: 1.5em;
          text-align: center;
        }

        a {
          color: inherit;
        }

        a:hover {
          text-decoration: none;
        }

        .stat {
          background-color: var(--background-alt);
          border-radius: 0.5em;
          padding: 0.5em;

          h3 {
            color: var(--background-alt);
            background: var(--text-main);
            margin: -0.5rem;
            padding: 0.25rem 0.5rem;
            border-radius: 0.5rem 0.5rem 0 0;
          }

          div {
            font-size: 4em;
          }
        }
      </style>
    </>
  `
}
