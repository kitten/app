/**
  Settings: Identity page.
*/
import { html } from '@small-web/kitten'
import Settings from '../Settings.layout.js'

export default function () {
  return html`
    <${Settings} activeSection='identity' x-data={}>
      <page alpinejs>
      <markdown>
        ## 🆔 Identity

        This is your public identity for ${kitten.domain} (ed25519 public key).

        <div class='public'>
          <input readonly type='input' value ='${kitten._db.settings.id.ed25519.asString}'>
          <button
            @click='await navigator.clipboard.writeText("${kitten._db.settings.id.ed25519.asString}"); $el.innerText="Copied!"'
          >Copy</button>
        </div>

        ### SSH

        This is your SSH public key.

        <div class='public'>
          <input readonly type='input' value ='${kitten._db.settings.id.ssh.asString}'>
          <button
            @click='await navigator.clipboard.writeText("${kitten._db.settings.id.ssh.asString}"); $el.innerText="Copied!"'
          >Copy</button>
        </div>

      </markdown>
      <style>
        .public {
          display: flex;
        }
        button {
          min-width: 12ch;
        }
      </style>
    </>
  `
}
