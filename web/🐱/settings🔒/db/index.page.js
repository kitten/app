/**
  Settings: App Database (db)
*/

import kitten from '@small-web/kitten'
import Settings from '../Settings.layout.js'
// @ts-ignore CSS module.
import Styles from '../databases.fragment.css'

export default function () {
  const tables = Object.keys(kitten.db)

  return kitten.html`
    <${Settings} activeSection='db'>
      <h2>🗄️ App database (db)</h2>

      <if ${tables.length > 0}>
        <then>
          <p>This is your app’s database.</p>
          ${tables.map(tableName => kitten.html`
            <h4>${tableName}</h4>
            ${[globalThis.kitten.md.render(
`
  \`\`\`json
  ${JSON.stringify(kitten.db[tableName], null, 2)}
  \`\`\`
`)]}
          `)}
        <${Styles} />
        </then>
        <else>
          <p>There are no tables in your database yet.</p>
        </else>
      </if>
    </>
  `
}
