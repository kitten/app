/**
  Settings: Domain
*/

import fs from 'node:fs/promises'
import dns from 'node:dns/promises'
import { execSync } from 'node:child_process'
import kitten, { html } from '@small-web/kitten'
import Settings from '../Settings.layout.js'
import ProgressIndicator from '../components/ProgressIndicator.component.js'

let externalIp

const kittenCommandDomainOptionRegExp = /(?<=\s)(--domain=)[^\s]+?\.[^\s]+?(?=\s)/

const DomainToken = ({ visible = false } = {}) => {
  // @ts-ignore TODO: Add settings object to @small-web/kitten
  const domainToken = kitten._db.settings.domainToken

  return kitten.html`
    <div id='domain-token-fragment'>
      <input id='domain-token' type='${visible ? 'input' : 'password'}' value='${domainToken}'>
      <button name='toggleDomainTokenVisibility' data='{ visible: ${visible} }' connect>${visible ? 'Hide' : 'Show'}</button>
      <button
        @click='await navigator.clipboard.writeText("${domainToken}"); $el.innerText="Copied!"'
      >Copy</button>
    </div>
  `
}

const UpdateButton = ({ disabled = true } = {}) => kitten.html`
  <button id='update-button' ${disabled && 'disabled'}>Update</button>
`

const DomainUpdateStatusError = ({SLOT}) => kitten.html`
  <div
    id='domain-update-status'
    class='error'
    morph
  >
    <p>❌ ${SLOT}</p>
  </div>
`

const DomainUpdateStatus = ({SLOT}) => kitten.html`
  <div
    id='domain-update-status'
    class='progress'
    morph
  >
    <${ProgressIndicator} title='Updating domain…'/>
    ${SLOT}
    <style>
      .progress {
        background-color: var(--background-body);
        font-weight: bold;
      }
    </style>
  </div>
`

const Domain = ({ externalIp = false} = {}) => {
  return kitten.html`
    <form
      id='domain-fragment'
      name='updateDomain'
      connect
    >
      <if ${process.env.PRODUCTION}>
        <then>
          <markdown>
            This is the domain your place is currently accessible from. You can update it at any time to migrate to a new domain.

            ### Instructions

            Before you update your domain, make sure you __update the DNS settings for your new domain__ on your domain registrar’s web site.
          </markdown>
    
          <if ${externalIp !== false}>
            <then>
              <markdown>
                Specifically, you must add a Type __A record__ for your domain with its value set to the following IP address: __${externalIp}__.
                
                > 💡Once you’ve added the A record, it will take time for the change to propagate through the global DNS system. You can check if the change has propagated by running the following command [code]host -t a <your domain>[code] in your terminal.
              </markdown>
            </then>
            <else>
              <p>Warning: Could not find external IP.</p>
            </else>
          </if>
        </then>
      </if>
      <label for='domain'>Domain</label>
      <input
        id='domain'
        name='domain'
        type='input'
        value='${kitten.domain}'
        ${process.env.PRODUCTION ? '' : 'disabled'}
        connect
        trigger='input delay:30ms'
      >
      <if ${process.env.PRODUCTION}>
        <then>
          <${UpdateButton} />
          <div id='domain-update-status' morph></div>
          <div id='domain-update-refresh'></div>
        </then>
        <else>
          <markdown>
            > 💡Use the [code]--domain[code] flag to set the domain when running in development mode. In addition to localhost, Kitten has support for place1.localhost - place4.localhost for testing Small Web apps (peer-to-peer web apps) locally.

            ### Deployment
    
            Here are some ways you can deploy your site when you’re ready to.

            __Using Domain:__

            - Use an existing Small Web host like [small-web.org](https://small-web.org).
            - Run your own instance of [Domain](https://codeberg.org/domain/app) and deploy using that.

            __On any computer that runs Linux[^1]:__
    
            - Deploy your site to any server (like a tiny single-board computer or your own VPS server somewhere) [using Kitten’s \`deploy\` command](https://kitten.small-web.org/reference/#deploying-a-kitten-application) and point your domain’s DNS to its IP address manually.

            For more details, please see the [Deploying a Kitten applicaton](https://kitten.small-web.org/reference/#deploying-a-kitten-application) section of the Kitten Reference guide.

            [^1]: With systemd (this is not some political statement; it’s just easier for us to support with limited resources). We recommend Alma Linux as that’s what we deploy with at small-web.org but it should work with any Linux with systemd.
          </markdown>
        </else>
      </if>
      <style>
        .checkbox {
          margin: 1em 0;

          label {
            font-weight: normal;
          }
        }

        .error {
          color: red;
        }

        #domain-update-status {
          margin-top: 1em;
        }
      </style>
    </form>
  `
}

function showError(error = null, page, message) {
  if (error !== null) {
    console.error(error)
  }
  page.send(kitten.html`<${DomainUpdateStatusError}>${message}</>`)
}

// This is not meant to be exhaustive; only to include common mistakes
// where someone might try to enter the protocol prefix or a full URL
// with a path, etc. 
const invalidCharacters = /[\s,:=!@#%\?\\\/]/
const isAtLeastSecondLevel = /\.../

export function onConnect({ page }) {
  page.on('toggleDomainTokenVisibility', data => {
    page.send(kitten.html`<${DomainToken} visible=${!data.visible} />`)
  })

  page.on('domain', data => {
    const disabled = data.domain === ''
      || !isAtLeastSecondLevel.test(data.domain)
      || data.domain === kitten.domain
      || invalidCharacters.test(data.domain) 
    page.send(kitten.html`<${UpdateButton} disabled=${disabled} />`)
  })

  page.on('updateDomain', async data => {
    // Show progress.
    page.send(kitten.html`<${DomainUpdateStatus}>Checking new domain has propagated…</>`)

    const newDomain = data.domain

    let newDomainIp
    try {
      newDomainIp = (await dns.lookup(newDomain, { family: 4})).address
    } catch (error) {
      console.error(error)
      return showError(error, page, 'Could not resolve domain. You might have to wait for it to finish propagating.')
    }

    if (newDomainIp !== externalIp) {
      // The domain does not point here.
      return showError(null, page, `That domain points to ${newDomain}, not ${externalIp}. If you’ve updated your DNS settings, it might just take a little longer to propagate based on its time-to-live setting (TTL). If so, please wait a little while and try again.`)
    }

    // OK, this place is reachable by the new domain.

    // Update the domain.
    page.send(kitten.html`<${DomainUpdateStatus}>Updating domain…</>`)

    const systemdUnitFilePath = globalThis.kitten.paths.KITTEN_SYSTEMD_UNIT_PATH

    // To update the domain, we simply update the value passed to the --domain
    // command-line option when kitten is started in the systemd unit file.
    let systemdUnit
    try {
     systemdUnit = await fs.readFile(systemdUnitFilePath, 'utf-8')
    } catch (error) {
      return showError(error, page, 'Could not read systemd unit file')
    }

    // Update the domain in the systemd unit configuration.
    systemdUnit = systemdUnit.replace(kittenCommandDomainOptionRegExp, `$1${newDomain}`)

    // Save the updated systemd unit file.
    try {
      await fs.writeFile(systemdUnitFilePath, systemdUnit, 'utf-8')
    } catch (error) {
      return showError(error, page, 'Could not update systemd unit file.')
    }

    // Since the domain has changed, Kitten will look for the data in
    // a path that contains the new domain. Since app-specific data might
    // contain the old path (e.g., copies of file upload paths kept in an
    // app’s database), we do not want to rename the old directory. Instead,
    // we will keep it there and create a symbolic link to it.
    //
    // This also means that we do not have to worry about race conditions
    // or closing off connections during a rename/move operation.
    //
    // The performance cost of using a symbolic link is negligible as it
    // is cached after first use.
    const oldDomain = new RegExp(`(?<=\\.)${kitten.domain.replace('.', '\\.')}(?=\\.)`)
    const currentDataDirectory = globalThis.kitten.paths.APP_DATA_DIRECTORY
    const newDataDirectory = currentDataDirectory.replace(oldDomain, newDomain)

    try {
      await fs.symlink(currentDataDirectory, newDataDirectory)
    } catch (error) {
      if (error.code === 'EEXIST') {
        // The symbolic link already exists. This is likely because the site was
        // pointing to this domain in the past. This is not really an error, so
        // we can ignore is. Let’s still issue a warning anyway in case this is
        // an edge case we haven’t considered that can lead to something bad.
        console.warn(`Symbolic link from ${newDataDirectory} to ${currentDataDirectory} already exists. This is likely because the new domain was pointing to this place at some point in the past.`)
      } else {
        return showError(error, page, 'Could not create symlink for data directory.')
      }
    }

    // Updating the systemd unit file is not enough, we must also ask
    // systemd to reload it so that it can be used on restart
    // (otherwise it will only be used on reboot).
    execSync('systemctl --user daemon-reload', { env: process.env })

    // OK, it’s all good; update progress.
    page.send(kitten.html`<${DomainUpdateStatus}>Updated domain; waiting for restart…</>`)

    // Ask client to refresh in five seconds.
    // TODO: Implement client-side polling instead as this is imprecise.
   page.send(kitten.html`<meta id='domain-update-refresh' http-equiv='refresh' content='5;url=https://${newDomain}/🐱/settings/domain/' />`)

    // Request server restart.
    setTimeout(() => process.exit(99 /* system exit code: restart request */), 1000)
  })
}

export default async function () {
  // @ts-ignore TODO: Add settings object to @small-web/kitten
  const smallWebHostDomain = kitten._db.settings.smallWebHostDomain
  try {
    externalIp = (await dns.lookup(kitten.domain, { family: 4 })).address
  } catch (error) {
    console.error(error)
    externalIp = false
  }

  return html`
    <${Settings} activeSection='domain'>
      <page alpine>
      <h2>🏠 Domain</h2>
      <${Domain} externalIp=${externalIp}/>
      <if ${smallWebHostDomain !== undefined}>
        <then>
          <p><strong>Your Small Web place is hosted by <a href='https://${smallWebHostDomain}'>${smallWebHostDomain}</a>.</strong></p>
  
          <p>You can manage your hosting account with them from here.</p>

          <label for='domain-token'>Domain token:</label>
          <${DomainToken} />
        </then>
        <else>
          <markdown>
          </markdown>
        </else>
      </if>
      <style>
        label { font-weight: 600; }
        button { width: 12ch; }
      </style>
    </>
  `
}
