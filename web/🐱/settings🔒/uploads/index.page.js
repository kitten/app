/**
  Settings: Uploads page.
*/

import kitten from '@small-web/kitten'
import Settings from '../Settings.layout.js'

export default function () {
  return kitten.html`
    <${Settings} activeSection='uploads'>
      <markdown>
        ## 📂 Uploads

        These are files that have been uploaded to your Small Web place.
      </markdown>

      <if ${globalThis.kitten._db.uploads.length() > 0}>
        <then>
          <div id='uploads-list'>
            <ul>
              ${kitten._db.uploads.all().map(upload => kitten.html`
                <li>
                  <a href='${upload.resourcePath}'>
                    <if ${
                      upload.fileName.endsWith('.jpg')
                      || upload.fileName.endsWith('.jpeg')
                      || upload.fileName.endsWith('.png')
                      || upload.fileName.endsWith('.svg')
                    }>
                      <img
                        class='uploadThumbnail'
                        src='${upload.resourcePath}'
                        alt='Thumbnail of ${upload.fileName}'
                      >
                    </if>
                    ${upload.fileName}
                  </a>
                </li>
              `)}
            </ul>
            <style>
              #uploads-list {
                img {
                  max-width: 2em;
                }
              }
            </style>
          </div>
        </then>
        <else>
          <p><strong>Nothing yet.</strong></p>
        </else>
      </if>
    </>
  `
}
