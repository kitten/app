/**
  Redirect index on /settings to the first item in
  the Settings navigation (Kitten settings).
*/

/**
  @param {{ response: import('@small-web/kitten').KittenResponse }} props
*/
export default function ({ response }) {
  return response.get('/🐱/settings/kitten/')
}
