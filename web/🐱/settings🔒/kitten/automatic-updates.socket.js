import kitten from '@small-web/kitten'
import { CheckNowButton, AutomaticUpdates } from './AutomaticUpdates.fragment.js'

/**
  Handle automatic upgrades settings and checks.

  // @param TODO: Add socket type info to kitten/globals.
*/
export default ({socket}) => {
  socket.addEventListener('message', async event => {
    const data = JSON.parse(event.data)

    switch (data.HEADERS['HX-Trigger-Name']) {
      case 'checkNow':
        socket.send(kitten.html`<${CheckNowButton} type='checking' />`)

        await globalThis.kitten.automaticUpdates.checkForUpdates()

        // If we reach here, we haven’t restarted and so there
        // were no updates. Let the person know and then reset the
        // button to its normal state after a brief delay.
        socket.send(kitten.html`<${CheckNowButton} type='no-updates' />`)
        setTimeout(() => socket.send(kitten.html`<${CheckNowButton} />`), 1500)
      break

      case 'updateDuration':
        globalThis.kitten.automaticUpdates.interval = Number(data.updateDuration)

        socket.send(kitten.html`
          <${AutomaticUpdates} />`
        )
      break
    }
  })
}
