/**
  The automatic updates section in Settings.
*/

import kitten from '@small-web/kitten'
import SvgSpinner from './SvgSpinner.component.js'

const buttonLabels = {
  'default': 'Check now',
  'checking': 'Checking…',
  'upgrading': 'Upgrading…',
  'no-updates': 'No updates available.',
  'restarting': 'Restarting…'
}

export function CheckNowButton ({type = 'default'}) {
  const isStatusUpdate = type === 'checking' || type === 'no-updates' || type === 'upgrading' || type === 'restarting'
  return kitten.html`
    <button
      id='check-now-button'
      name='checkNow'
      ws-send
      hx-oob-swap='outerHTML'
      ${isStatusUpdate ? 'disabled' : ''}
    >
      <if ${isStatusUpdate}>
        <if ${type !== 'no-updates'}>
          <${SvgSpinner}/>
        </if>
      </if>
      ${buttonLabels[type]}
    </button>
  `
}

// TODO: Refactor; copied from src/AutomaticUpdates.js
const hoursInMilliseconds = hours => hours * 60 /* minutes */ * 60 /* seconds */ * 1000 /* milliseconds */

export function AutomaticUpdates ({message}) {
  // Note: this is a Kitten class and will not be available at runtime
  // but we can still use it for type checking during development via JSDoc.
  const automaticUpdates = /** @type { import('../../../../src/AutomaticUpdates.js').default } */ (globalThis.kitten.automaticUpdates)

  if (automaticUpdates === undefined) {
    return kitten.html``
  }

  const options = [
    {label: 'once a day', value: hoursInMilliseconds(24)},
    {label: 'twice a day', value: hoursInMilliseconds(12)},
    {label: 'three times a day', value: hoursInMilliseconds(8)},
    {label: 'four times a day', value: hoursInMilliseconds(6)},
    {label: 'every hour', value: hoursInMilliseconds(1)},
    {label: 'never', value: -1}
  ]
  const optionMap = {'1': 0, '2':1, '3':2, '4':3, '24':4, '-24':5}
  const selectedOption = options[optionMap[(24/automaticUpdates.intervalInHours).toString()]]

  return kitten.html`
    <if ${automaticUpdates !== undefined}>
      <then>
        <div id='automaticUpdates'>
          <markdown>
            ## 🪄 Automatic updates
          </markdown>

          <if ${!automaticUpdates.appRepository.usesKittenVersioning}>
            <then>
              <markdown>
                __App does not use Kitten versioning so automatic Kitten and app updates are turned off.__
    
                💡 _To use automatic versioning, create tags in your app’s git repository in the form &lt;kitten api version&gt;.&lt;app version&gt; like 1.1, 1.2, 2.1, etc._ 

                You can still manually update your Kitten version from here and manually update the deployed app from the [App Settings](/🐱/settings/app/).

                🚧 If you do start publishing Kitten API tags, automatic updates will automatically commence.
              </markdown>
            </then>
            <else>
              <form
                hx-ext='ws'
                ws-connect='/🐱/settings/automatic-updates.socket'
                hx-trigger='change, submit'
              >
                <p>
                  Check for updates
                    <select
                      name='updateDuration'
                      ws-send
                    >
                      ${options.map(option => globalThis.kitten.html`
                        <option value='${option.value}' ${option === selectedOption ? 'selected' : ''}>${option.label}</option>
                      `)}
                    </select>
                </p>

                <if ${automaticUpdates?.running}>
                  <then>
                    <p><strong>Next check ${automaticUpdates?.timeToNextCheckPretty()}.</strong></p>

                    <div class='update'>
                      <p>Kitten automatically updates itself and the installed app when compatible updates are available.</p>
                      <${CheckNowButton} />
                    </div>
                  </then>
                </if>
              </form>
            </else>
          </if>

          <style>
            #automaticUpdates {
              & select {
                display: inline-block;
                margin-left: 0.5em;
                margin-right: 0.5em;
              }
            }
          </style>
        </div>
      </then>
    </if>
  `
}

export default AutomaticUpdates

