import kitten, { html } from '@small-web/kitten'
// @ts-ignore CSS module.
import Styles from './WelcomeMessage.fragment.css'

export default function WelcomeMessage () {
  return html`
    <section id='welcome-message' morph>
      <if ${!kitten._db.settings.hideWelcomeMessage}>
        <then>
          <markdown>
            ## 👋🤓 Hello!

            Here you will find technical details about your Small Web place (such as your cryptographic identity) as well as configuration options.

            _You should not need this page for the day-to-day operation of your Small Web place but it is here in case something goes wrong or if you want to tinker with it._

            <button name='hideWelcomeMessage' connect>Hide</a>
          </markdown>
          <${Styles} />
        </then>
        <else>
          <style>
            #welcome-message { display: none; }
          </style>
        </else>
      </if>
    </section>
  `
}

WelcomeMessage.onConnect = function ({ page }) {
  page.on('hideWelcomeMessage', () => {
    kitten._db.settings.hideWelcomeMessage = true
    page.send(html`<${WelcomeMessage} />`)
  })
} 

