/**
  Kitten Settings: Kitten details
*/

import kitten, { html } from '@small-web/kitten'

import Settings from '../Settings.layout.js'

import ManualUpdate from './ManualUpdate.fragment.js'
import AutomaticUpdates from './AutomaticUpdates.fragment.js'
import WelcomeMessage from './WelcomeMessage.fragment.js'

export function onConnect ({ page }) {
  ManualUpdate.onConnect({ page })
  // AutomaticUpdates.onConnect({ page })
  WelcomeMessage.onConnect({ page })
}

export default async function () {
    const kittenPackage = /** @type {import('../../../../src/lib/KittenPackage.js').default} */ (globalThis.kitten.package)

    // Get latest package information from kittens.small-web.org.
    let versionInformationExists = false
    try {
      await kittenPackage.update()
      versionInformationExists = true
    } catch (error) {
      // Could not update Kitten package (likely an issue with the Internet connection).
      // Handle it gracefully.
      console.error('Could not load version information. Is your Internet connection working?')
    }

    return html`
      <${Settings} activeSection='kitten'>
        <${WelcomeMessage} />

        <markdown>
          ## 🐱 Kitten
        </markdown>


        <if ${versionInformationExists}>
          <then>
            <${kitten.version.Component} />
          </then>
          <else>
            <markdown>
              __❌ Could not load version information.__
    
              _Is your Intenet connection working?_
            </markdown>
          </else>
        </if>

        <${ManualUpdate} />
        <${AutomaticUpdates} />
      </>
    `
}
