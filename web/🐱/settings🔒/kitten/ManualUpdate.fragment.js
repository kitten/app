/**
  Manual Update (upgrade/downgrade) component and related sub-components,
  including the UpdateButton component for use in htmx updates.
*/

import kitten from '@small-web/kitten'
import SvgSpinner from './SvgSpinner.component.js'

const kittenPackage = globalThis.kitten.package

/**
  Returns a formatted date string.

  @param {Date} date
*/
function formattedDate(date) {
  // TODO: remove redundancy – code here is from Version.js.
  const year = date.getUTCFullYear()
  const month = date.getUTCMonth() + 1
  const day = date.getUTCDate()

  const paddedMonth = month.toString().padStart(2, '0')
  const paddedDay = day.toString().padStart(2, '0')

  const hour = date.getUTCHours()
  const minute = date.getUTCMinutes()
  const second = date.getUTCSeconds()

  const paddedHour = hour.toString().padStart(2, '0')
  const paddedMinute = minute.toString().padStart(2, '0')
  const paddedSecond = second.toString().padStart(2, '0')

  const formattedDate = `${year}/${paddedMonth}/${paddedDay}`
  const formattedTime = `${paddedHour}:${paddedMinute}:${paddedSecond}`

  const zodiac = ['', 'Capricorn', 'Aquarius', 'Pisces', 'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn']
  const lastDay = [0, 19, 18, 20, 20, 21, 21, 22, 22, 21, 22, 21, 20, 19]
  const starSign = (day > lastDay[month]) ? zodiac[month + 1] : zodiac[month]

  return `${formattedDate} at ${formattedTime} UTC (${starSign})`
}

export function UpdateDetails ({type = 'upgrade'}) {
  return type === 'upgrade'
    ? kitten.markdown`
        __📦 Upgrade available__

        __There is a [newer version of Kitten](https://kittens.small-web.org/)__ (<a href='https://kittens.small-web.org/uploads/${kittenPackage.latestRecommendedVersion?.upload.id}/download'>${kittenPackage.latestRecommendedVersion?.exactVersion}</a>) available that was born on ${formattedDate(new Date(kittenPackage.latestRecommendedVersion?._buildDate))}.
    `
    : kitten.markdown`
        __You’re running a development version that’s more recent than the [latest released Kitten](https://kittens.small-web.org/)__ (<a href='https://kittens.small-web.org/uploads/${kittenPackage.latestRecommendedVersion?.upload.id}/download'>${kittenPackage.latestRecommendedVersion?.exactVersion}</a>) that was born on ${formattedDate(new Date(kittenPackage.latestRecommendedVersion?._buildDate))}.
    `
}

const buttonLabels = {
  'upgrade': 'Upgrade Kitten',
  'downgrade': 'Downgrade Kitten to release version',
  'updating': 'Updating…',
  'restarting': 'Restarting…'
}

export function UpdateButton ({type = 'upgrade'}) {
  const isStatusUpdate = type === 'updating' || type === 'restarting'
  return kitten.html`
    <button
      id='updateButton'
      name='manualUpdate'
      connect
      morph
      ${isStatusUpdate ? 'disabled' : ''}
    >
      <if ${isStatusUpdate}>
        <${SvgSpinner}/>
      </if>
      ${buttonLabels[type]}
    </button>
  `
}

export function UpdateAvailable ({type = 'upgrade'}) {
  return kitten.html`
    <form
      class='update'
      confirm='Proceed to ${type} your Kitten version?'
    >
      <${UpdateDetails} type=${type}/>
      <${UpdateButton} type=${type}/>
    </form>
  `
}

export function ManualUpdate () {
  return kitten.html`
    <if ${kittenPackage.isLatestReleaseVersion}>
      <then>
        <p class='runningLatest'>✅ You’re running <a href='https://kittens.small-web.org/'>the latest release version</a> of Kitten.</p>
        <br>
      </then>
    </if>

    <if ${kittenPackage.canBeUpgraded}>
      <${UpdateAvailable} type='upgrade' />
    </if>

    <if ${kittenPackage.isMoreRecentThanReleaseVersion}>
      <${UpdateAvailable} type='downgrade' />
    </if>

    <div id='manual-update-status' />
    <div id='manual-update-refresh' />
  `
}

const ManualUpdateError = ({SLOT}) => kitten.html`
  <div
    id='manual-update-status'
    class='error'
  >${SLOT}</div>
`

ManualUpdate.onConnect = function ({ page }) {
  page.on('manualUpdate', data => {
    page.send(kitten.html`<${UpdateButton} type='updating' />`)

    // Carry out upgrade.
    try {
      globalThis.kitten.package.upgrade()
    } catch (error) {
      page.send(kitten.html`<${ManualUpdateError}>Kitten update failed: ${error}.</>`)
      return
    }

    page.send(kitten.html`<${UpdateButton} type='restarting' />`)

    // We’re going to ask for a server restart. Refresh the page after a wait. 
    // TODO: Carry out client-side polling here instead so it is precise.
    page.send(kitten.html`
      <meta id='manual-update-refresh' http-equiv='refresh' content='5;url=/🐱/settings/kitten/' />
    `)

    // Exit the process with a restart request.
    setTimeout(() => process.exit(99 /* system exit code: restart request */), 1000)
  })
}

export default ManualUpdate
