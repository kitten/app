/**
  Kitten Settings: 
*/

import { html } from '@small-web/kitten'
import Settings from '../Settings.layout.js'

export default function () {
  return html`
    <${Settings} activeSection='evergreen-web'>
      <markdown>
        ## 🌲 Evergreen Web

        Did you have a site here before?

        You can preserve it and keep existing links to it on the Web from breaking by redirecting pages that cannot be found here to the older version of this place that is now hosted the domain set below.
      </markdown>

      <${EvergreenWebForm} />
  
      <p class='snug'>Not sure what to enter here? Please see <a href='https://4042307.org'>4042307.org</a></p>
    </>
  `
}

export function onConnect ({ page }) {
  page.on('evergreenWebUrl', data => {
    if (typeof data.evergreenWebUrl === 'string') {
      kitten._db.settings.evergreenWebUrl = data.evergreenWebUrl
      page.send(html`<${EvergreenWebForm} />`)
    }
  })
}

/**
  Evergreen Web Form fragment. 

  @typedef {string|array<string>} KittenHTML
  @returns {KittenHTML}
*/
function EvergreenWebForm () {
  const evergreenWebUrl = kitten._db.settings.evergreenWebUrl === undefined ? '' : kitten._db.settings.evergreenWebUrl

  // We only support https urls.
  const urlWithoutProtocol = evergreenWebUrl.replace('https://', '').replace('http://', '')
  const httpsUrl = `https://${urlWithoutProtocol}`

  let error = ''
  try {
    new URL(httpsUrl)
  } catch (e) {
    if (e.code === 'ERR_INVALID_URL') {
      error = `Invalid URL`
    } else {
      error = e
    }
  }

  // An empty URL is valid (it means we don’t perform redirection).
  if (evergreenWebUrl === '') {
    error = ''
  }

  return kitten.html`
    <div
      id='evergreenWeb'
      morph='config:{ignoreActive:true}'
    >
      <page alpinejs>
      <label for='evergreenWebUrl'>Redirect missing pages to the following domain:</label>
      <div
        class='evergreenWeb'
        x-data='{evergreenWebUrl: "${[evergreenWebUrl]}"}'
      >
        <input
          type='input'
          id='evergreenWebUrl'
          name='evergreenWebUrl'
          x-model='evergreenWebUrl'
          placeholder='Do not redirect missing pages.'
          hx-trigger='flush, keyup changed delay:500ms'
          connect
        >
        <button
          @click.prevent='
            evergreenWebUrl=""
            // Give the binding a chance to update the input before
            // triggering the htmx call to persist the value.
            $nextTick(() => htmx.trigger("#evergreenWebUrl", "flush"))
          '
        >Clear</button>
      </div>
      <if ${error !== ''}>
        <div class='error'>
          <p>Error: ${error}</p>
        </div>
      </if>

      <style>
        #evergreenWeb {
          .error {
            background-color: red;
            color: white;
          }
          .evergreenWeb {
            display: flex;
          }
          label {
            font-weight: 600;
          }
        }
      </style>
    </div>
  `
}
