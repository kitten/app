/**
  Evergreen Web URL entry form.

  See https://4042307.org
*/
import kitten from '@small-web/kitten'

export default () => {
  const evergreenWebUrl = kitten._db.settings.evergreenWebUrl === undefined ? '' : kitten._db.settings.evergreenWebUrl

  // We only support https urls.
  const urlWithoutProtocol = evergreenWebUrl.replace('https://', '').replace('http://', '')
  const httpsUrl = `https://${urlWithoutProtocol}`

  let error = ''
  try {
    new URL(httpsUrl)
  } catch (e) {
    if (e.code === 'ERR_INVALID_URL') {
      error = `Invalid URL`
    } else {
      error = e
    }
  }

  // An empty URL is valid (it means we don’t perform redirection).
  if (evergreenWebUrl === '') {
    error = ''
  }

  return kitten.html`
    <div id='evergreenWeb' hx-swap-oob='true'>
      <label for='evergreenWebUrl'>Redirect pages not found here to the older version of this place that’s now hosted at:</label>
      <div class='evergreenWeb' x-data='{evergreenWebUrl: "${[evergreenWebUrl]}"}'>
        <input
          type='input'
          id='evergreenWebUrl'
          name='evergreenWebUrl'
          x-model='evergreenWebUrl'
          placeholder='Do not redirect missing pages.'
          hx-post='persist-evergreen-web-url'
          hx-trigger='flush, keyup changed delay:500ms'
        >
        <button
          @click.prevent='
            evergreenWebUrl=""
            // Give the binding a chance to update the input before
            // triggering the htmx call to persist the value.
            $nextTick(() => htmx.trigger("#evergreenWebUrl", "flush"))
          '
        >Clear</button>
      </div>
      <if ${error !== ''}>
        <div class='error'>
          <p>Error: ${error}</p>
        </div>
      </if>

      <style>
        #evergreenWebForm {
          .error {
            background-color: red;
            color: white;
          }
        }
      </style>
    </div>
  `
}
