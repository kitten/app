import { html } from '@small-web/kitten'

// @ts-ignore CSS module.
import Styles from './Footer.fragment.css'

export default () => html`
  <footer class='Footer'>
    <p>This is a Small Web place powered by <a href='https://codeberg.org/kitten/app'>Kitten</a>.</p>
    <p>Copyright &copy; 2021-present, <a href='https://ar.al'>Aral Balkan</a>, <a href='https://small-tech.org'>Small Technology Foundation</a>. All Rights Reserved.</p>
    <p>Released with 💕 under the <a href='https://www.gnu.org/licenses/agpl-3.0.en.html'>AGPL Version 3.0 license.</a></p>
    <p id='funding-link'>Like this? <a href='https://small-tech.org/fund-us'>Fund us!</a></p>
    <${Styles} />
  </footer>
`
