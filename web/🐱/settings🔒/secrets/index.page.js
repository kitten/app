/**
  Settings: security page.
*/

import kitten, { html } from '@small-web/kitten'
import Settings from '../Settings.layout.js'

const SSH_KEY_HIDDEN = "•".repeat(35)+"\\n\\n"+"•".repeat(70)+"\\n"+"•".repeat(70)+"\\n"+"•".repeat(70)+"\\n"+"•".repeat(70)+"\\n"+"•".repeat(70)+"\\n"+"•".repeat(33)
const sshKeyHidden = SSH_KEY_HIDDEN

export default function () {

  return html`
    <${Settings} activeSection='secrets'>
      <page alpinejs>
      <markdown>
        ## 🤫 Secrets
        
        These secrets are generated in your browser. They are not sent to or stored on your server. Please do not share these with anyone. 
    
        ### ed25519 private key
    
        This is the secret that protects your main public identity (ed25519 private key). You should keep your ed25519 private key in your password manager.
      </markdown>

      <div class='secret' x-data='{showSecret:false}'>
        <input
          readonly
          x-init='$nextTick(() => {
            $el.value = localStorage.getItem("secret")
          })'
          :type='showSecret ? "input" : "password"'
        >
        <button
          @click='showSecret = !showSecret'
          x-text='showSecret ? "Hide" : "Show"'
        ></button>
        <button
          @click='await navigator.clipboard.writeText(localStorage.getItem("secret")); $el.innerText="Copied!"'
        >Copy</button>
      </div>

      <markdown>
        ### ssh private key

        If you add this to the ~/.ssh directory on your computer, alongside your public ssh key, you can ssh into your server.

        _(Each time your OpenSSH private key is derived from your ed25519 private key, it will be a little different – by 12 characters, to be precise. [This is normal](https://carlosbecker.com/posts/ssh-marshal-private-key/) and the keys are all equivalent.)_
      </markdown>

      <div class='secret' x-data='{ key: "${sshKeyHidden}" }'>
        <input
          readonly
          type='input'
          x-model='key'
        >
        <button
          @click='key = key === "${SSH_KEY_HIDDEN}" ? globalThis.openSshKey : "${SSH_KEY_HIDDEN}"'
          x-text='key === "${SSH_KEY_HIDDEN}" ? "Show" : "Hide"'
        ></button>
        <button
          @click='await navigator.clipboard.writeText(globalThis.openSshKey); $el.innerText="Copied!"'
        >Copy</button>
      </div>
      <script type='module'>
        import { secretToEmojiString, emojiStringToSecret, sign, verify, bytesToHex, hexToBytes, deriveKeysFromSecretKeyForDomain } from '/🐱/library/crypto-1.js'

        // We have to do this here as Safari will not allow the clipboard to be
        // written to if we call a function from within in a module.
        const secret = emojiStringToSecret(localStorage.getItem("secret"))
        const secrets = await deriveKeysFromSecretKeyForDomain(secret, "${kitten.domain}")
        globalThis.openSshKey = secrets.private.ssh.asString
      </script>
    </>
  `
}
