import { secretToEmojiString, emojiStringToSecret, sign, verify, bytesToHex, hexToBytes, deriveKeysFromSecretKeyForDomain } from '/🐱/library/crypto-1.js'

globalThis.secretKeys = async function (emojiString, domain) {
  const secret = emojiStringToSecret(emojiString)
  const secrets = await deriveKeysFromSecretKeyForDomain(secret, domain)
  return secrets
}
