/**
  Manual Update (upgrade/downgrade) component and related sub-components,
  including the UpdateButton component for use in htmx updates.

  TODO: Refactor: based on ManualUpdate.component.js
*/

import kitten from '@small-web/kitten'
import SvgSpinner from './SvgSpinner.component.js'

const buttonLabels = {
  'upgrade': 'Upgrade the app',
  'downgrade': 'Downgrade the app',
  'updating': 'Updating…',
  'restarting': 'Restarting…'
}

export function UpdateButton ({type = 'upgrade', small = false}) {
  const isStatusUpdate = type === 'updating' || type === 'restarting'
  return kitten.html`
    <button
      id='updateButton'
      ws-send
      @htmx:ws-before-send='if (!window.confirm("Proceed to ${type} the running app?")) $event.preventDefault()'
      hx-oob-swap='outerHTML'
      ${isStatusUpdate ? 'disabled' : ''}
      ${small ? 'class="smallButton"' : ''}
    >
      <if ${isStatusUpdate}>
        <${SvgSpinner}/>
      </if>
      ${buttonLabels[type]}
    </button>
  `
}
