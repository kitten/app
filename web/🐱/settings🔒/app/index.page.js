/**
  Settings: App
*/

import kitten, { html } from '@small-web/kitten'
import Settings from '../Settings.layout.js'
import ProgressIndicator from '../components/ProgressIndicator.component.js'
// @ts-ignore CSS module.
import Styles from './index.fragment.css'

const LocalPath = () => {
  let fullBasePath = process.env.basePath || 'Unknown.'
  const kittenFolderIndex = fullBasePath.indexOf('/kitten/')
  const prettyBasePath = kittenFolderIndex !== -1 ? fullBasePath.slice(kittenFolderIndex) : fullBasePath
  const truncated = prettyBasePath !== fullBasePath
  const truncationIndicator = truncated ? '…' : ''
  const titleIfTruncated = truncated ? ` title=${fullBasePath}` : ''

  return kitten.html`
    <dt>Local path</dt>
    <dd><abbr ${titleIfTruncated}>${truncationIndicator}${prettyBasePath}</abbr> <button class='smallButton' @click='await navigator.clipboard.writeText("${fullBasePath}"); $el.innerText="Copied!"' href='file://${fullBasePath}'>Copy</button></dd>
  `
}

const DeployError = ({SLOT}) => kitten.html`
  <div
    id='deploy-status'
    class='error'
  >${SLOT}</div>
`

const ManualUpdateError = ({SLOT}) => kitten.html`
  <div
    id='manual-update-status'
    class='error'
  >${SLOT}</div>
`
const WebhookSecret = ({changed=false}) => { 
  // @ts-ignore TODO: Add settings object to JSDoc for internal database.
  const webhookSecret = kitten._db.settings.webhookSecret

  return kitten.html`
    <div id='webhook-secret'>
      <dt>Webhook secret</dt>
      <dd>
        <if ${changed}>
          <p class='important'>Your webhook secret has changed. Please update it in your Git remote hosts. Until you do, existing webhooks will fail.</p>
        </if>
        <div class='secret' x-data='{showSecret:false}'>
          <input
            readonly
            value='${webhookSecret}'
            :type='showSecret ? "input" : "password"'
          >
          <button
            @click='showSecret = !showSecret'
            x-text='showSecret ? "Hide" : "Show"'
          ></button>
          <button
            @click='await navigator.clipboard.writeText("${webhookSecret}"); $el.innerText="Copied!"'
          >Copy</button>
          <button
            name='changeSecret'
            connect
            confirm='Are you sure you want to change your webhook secret? (Existing webhooks will fail until you update your Git remote hosts with your new secret.)'
          >Change</button>
        </div>
      </dd>
    </div>
  `
}

export function onConnect ({page}) {
  page.on('changeSecret', function () {
    globalThis.kitten._db.settings.webhookSecret = globalThis.kitten.crypto.random32ByteTokenInHex()
    page.send(kitten.html`<${WebhookSecret} changed=true />`)
  })

  page.on('manualUpdate', async function () {
    // Show status indicator.
    page.send(kitten.html`
      <div id='manual-update-status' class='progress'>
        <${ProgressIndicator} title='Manually updating app to latest commit…'/>
        Manually updating app to latest commit…
      </div>
    `)

    const appRepository = /** @type {import('../../../../src/AppRepository.js').default} */ (globalThis.kitten.appRepository)
    try {
      await appRepository.upgradeAppToLatestAvailableCommit()
    } catch (error) {
      page.send(kitten.html`<${ManualUpdateError}>Git operation failed: ${error}.</>`)
      return
    }

    // Update progress: app has been updated; waiting for server restart.
    page.send(kitten.html`
      <div id='manual-update-status' class='progress'>
        <${ProgressIndicator} title='Done! Waiting for server restart…'/>
          Done! Waiting for server restart…
      </div>
    `)

    // We are about to request a server restart so the latest
    // version of the app starts getting served. Before we do, we
    // send a refresh request to the front-end to make a best-effort
    // attempt to refresh the page after that happens.
    //
    // TODO: To get a better estimate, actually time the server-start
    // and use that plus a second or two. It might still be that a
    // performance regression could cause the page to refresh before
    // the server is ready… so actually the only foolproof way to do
    // this is to poll from the server and only refresh the page when
    // the server is back up. That’s what should ultimately be implemented.
    page.send(kitten.html`
      <meta id='manual-update-refresh' http-equiv='refresh' content='5;url=/🐱/settings/app/' />
    `)

    // Exit the process with a restart request.
    setTimeout(() => process.exit(99 /* system exit code: restart request */), 1000)
  })

  page.on('deploy', async function (/** @type {any} */ data) {
    // Show status indicator.
    page.send(kitten.html`
      <div id='deploy-status' class='progress'>
        <${ProgressIndicator} title='Deploying new app…'/>
        Deploying new app…
      </div>
    `)

    if (!data.gitRepositoryUrl) {
      page.send(kitten.html`<${DeployError}>Git repository URL not found.</>`)
      return
    }
      
    if (typeof data.gitRepositoryUrl !== 'string') {
      page.send(kitten.html`<${DeployError}>Git repository URL is not a string.</>`)
      return
    }

    /** @type {string} */
    const gitRepositoryUrl = data.gitRepositoryUrl

    // @ts-ignore Internal Kitten database property.
    const domainToken = kitten._db.settings.domainToken

    if (!gitRepositoryUrl.startsWith('https://')) {
      page.send(kitten.html`<${DeployError}>Git repository URL must begin with <em>https</em>.</>`)
      return
    }

    if (!gitRepositoryUrl.endsWith('.git')) {
      page.send(kitten.html`<${DeployError}>Git repository URL does not end with <em>.git</em>.</>`)
      return
    }

    const options = {
      'domain-token': domainToken,
      context: 'web',
      progress: (/** @type {string} */ message) => {
        if (message === 'starting-kitten-service') {
          // OK, the redeployment went well. Since the server is restarting,
          // instruct the client to hit the page for generating the new
          // password in a few seconds.
          page.send(kitten.html`
            <meta id='deploy-refresh' http-equiv='refresh' content='5;url=/💕/hello/${domainToken}' />
          `)
        }
      }
    }

    // @ts-ignore Internal Kitten database property.
    const smallWebHostDomain = kitten._db.settings.smallWebHostDomain

    // Also copy the Small Web Host domain if one exists.
    if (smallWebHostDomain !== undefined) {
      options['small-web-host-domain'] = smallWebHostDomain
    }

    try {
      await globalThis.kitten.deploy(gitRepositoryUrl, options)

      // Note that because the new deployment will result in a server restart
      // execution will never reach this point so we don’t have a final
      // status communication here. Instead, we complete the process
      // in the progress handler, above, when the starting-kitten-service
      // event is received.
    } catch (error) {
      page.send(kitten.html`<${DeployError}>${[error]}</>`)
    }
  })
}

export default async function () {
  const appRepository = /** @type {import('../../../../src/AppRepository.js').default} */ (globalThis.kitten.appRepository)
  if (!appRepository === undefined) {
    await appRepository.update()
  }
  const webhookUrl = `https://${kitten.domain}/💕/webhook/`

  return html`
    <${Settings} activeSection='app' x-data={}>
      <page alpinejs>

      <markdown>
        ## 🧶 App
      </markdown>
  
      <if ${appRepository === undefined}>
        <then>
          <!-- App is being served locally, show local path to app. -->
          <dl>
            <${LocalPath} />
          </dl>
        </then>
        <else>
          <!-- App was deployed from a git repository. -->
          <dl class='nameValueList'>
            <dt>Version</dt>
            <dd><${appRepository?.CurrentAppVersionComponent()} /></dd>
            <${LocalPath} />

            <markdown>
              ### Webhook

              You can set up a webhook on your remote Git source code repository that updates the site whenever you perform a certain git action (e.g., push to main).

              e.g., Instructions for [Codeberg](https://codeberg.org):
            
              1. Enter the _Webhook URL_ from below into the _Target URL_ field.
              2. Set the _HTTP method_ to [code]POST[code].
              3. Set the _POST content type_ to [code]application/x-www-form-urlencoded[code].
              4. Leave the _Secret_ field empty.
              5. Set _Trigger on_ to [code]Push events[code]
              6. Set Branch filter to [code]main[code] (or set to a separate branch you want to deploy from)
              7. Enter the _Webhook secret_ from below into the _Authorization header_ field.
              8. Make sure the _Active_ checkbox is checked.

              Press the _Add webhook_ button. Once the webhook has been successfully added, your site should update whenever you push to main.
            </markdown>

            <dt>Webhook URL</dt>
            <dd>${webhookUrl} <button class='smallButton' @click='await navigator.clipboard.writeText("${webhookUrl}"); $el.innerText="Copied!"'>Copy</button></dd>
            <${WebhookSecret} />
          </dl>

          <if ${appRepository?.canBeUpgraded}>
            <then>
              <div class='update'>
                <form
                  hx-ext='ws'
                  ws-connect='/🐱/settings/app-upgrade.socket'
                >
                  <markdown>
                    📦 __App upgrade available__

                    A new version of the app _(version ${appRepository?.latestCompatibleVersion})_ is available to install.
                  </markdown>
                  <input
                    type='hidden'
                    name='version'
                    value='${appRepository?.latestCompatibleVersion}'
                  >
                  <${appRepository?.UpdateButtonComponent()}
                    version='${appRepository?.latestCompatibleVersion}'
                  />
                </form>
              </div>

              <if ${appRepository?.hasNewerApiVersion}>
                <then><br></then>
              </if>
            </then>
          </if>

          <if ${appRepository?.hasNewerApiVersion}>
            <then>
              <div class='update'>
                <markdown>
                  🗞 __Latest available app version requires Kitten upgrade__

                  The latest available version of the app _(version ${appRepository?.latestVersion})_ requires a newer version of Kitten _(version ${appRepository?.latestApiVersion}.x)_ that’s compatible with Kitten API version ${appRepository?.latestApiVersion} and can only be installed after Kitten has been upgraded.
                </markdown>
              </div>
            </then>
          </if>

          <${appRepository?.AllAvailableVersionsComponent()} />

          <if ${appRepository?.manualUpdateAvailable}>
            <then>
              <div class='danger'>
                <div id='manual-update-refresh' />
                <markdown>
                  ### Update app

                  There is a newer version of the current app (a newer commit) available on the git repository:

                  <a href='${appRepository?.originRemoteUrl?.replace('.git', `/compare/${appRepository?.currentVersion}..${appRepository?.latestAvailableCommit}`)}' target='_blank'>View changes current (${appRepository?.currentVersion}) and latest (${appRepository?.latestAvailableCommit}) versions.</a>
                </markdown>
    
                <if ${kitten.version.apiVersion === 0}>
                  <then>
                    <markdown>
                      Normally, upgrading to the latest commit instead of a versioned tag can be dangerous as the latest commit may not be compatible with the API Version of Kitten you have installed but __Kitten is currently in pre-release at API Version 0 so the API is not stable and API version guarantees do not exist.__
                    </markdown>
                  </then>
                  <else>
                    <markdown>
                      If you are going to update to this commit, please make sure that it is compatible with the current Kitten API Version (${kitten.version.apiVersion})
                    </markdown>
                  </else>
                </if>

                <button
                  name='manualUpdate'
                  connect
                  confirm='Are you sure you want to manually update deployed app to latest git commit?'
                >Update to latest commit</button>

                <div id='manual-update-status' />

              </div>
            </then>
          </if>

          <!-- Fix this. -->
          <br>

          <div class='danger'>
            <div id='deploy-refresh' />
            <markdown>
              ### Deploy new app

              You can replace the current app by deploying a new one. __This will replace the current app.__

              <label for='gitRepositoryUrl'>Git URL of app:</label>
              <form
                name='deploy'
                connect
                confirm='Are you sure you want to replace the currently deployed app with a new one?'
              >
                <input
                  id='gitRepositoryUrl'
                  name='gitRepositoryUrl'
                  type='url'
                  placeholder='https://git.host/path/to/repository.git'
                  required
                />
                <button
                  type='submit'
                >Deploy</button>
              </form>
            </markdown>
            <div id='deploy-status' />
          </div>
        </else>
      </if>

      <${Styles} />
    </>
  `
}
