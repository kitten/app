/**
  Settings: Internal Database (_db)
*/

import kitten from '@small-web/kitten'
import Settings from '../Settings.layout.js'
// @ts-ignore CSS module.
import Styles from '../databases.fragment.css'

export default function () {
  const tables = Object.keys(kitten._db)

  return kitten.html`
    <${Settings} activeSection='_db'>
      <h2>🗄️ Internal database (_db)</h2>

      <if ${tables.length > 0}>
        <then>
          <p>This is Kitten’s internal database.</p>
          ${tables.map(tableName => kitten.html`
            <h4>${tableName}</h4>
            ${[globalThis.kitten.md.render(
`
  \`\`\`json
  ${JSON.stringify(kitten._db[tableName], null, 2)}
  \`\`\`
`)]}
          `)}
        <${Styles} />
        </then>
        <else>
          <p>There are no tables in Kitten’s internal database (this should not happen).</p>
        </else>
      </if>
    </>
  `
}
