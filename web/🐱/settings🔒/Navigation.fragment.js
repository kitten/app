import { html } from '@small-web/kitten'
// @ts-ignore CSS module.
import Styles from './Navigation.fragment.css'

export default ({ activeSection }) => {
  function isActive (section) {
    return section === activeSection ? 'active' : ''
  }

  const href = fragment => [`href=/🐱/settings/${fragment}/`]

  return html`
    <nav class='Navigation'>
      <div id='skip'>
        <a href='#content'>Skip to main content.</a>
      </div>
      <ul>
        <li>
          <section>
            <h2>General</h2>
            <ul class='sub-nav'>
              <li><a class=${isActive('kitten')} ${href('kitten')}>🐱 Kitten</a></li>
              <li><a class=${isActive('domain')} ${href('domain')}>🏠 Domain</li>
              <li><a class=${isActive('app')} ${href('app')}>🧶 App</a></li>
              <li><a class=${isActive('evergreen-web')} ${href('evergreen-web')}>🌲 Evergreen Web</a></li>
            </ul>
          </section>
        </li>
        <li>
          <section>
            <h2>Security</h2>
            <ul class='sub-nav'>
              <li><a class=${isActive('identity')} ${href('identity')}>🆔 Identity</a></li>
              <li><a class=${isActive('secrets')} ${href('secrets')}>🤫 Secrets</a></li>
            </ul>
          </section>
        </li>
        <li>
          <section>
            <h2>State</h2>
            <ul class='sub-nav'>
              <li><a class=${isActive('state/overview')} ${href('state/overview')}>📈 Overview</a></li>
              <li><a class=${isActive('state/sessions')} ${href('state/sessions')}>🕰️ Sessions</a></li>
              <li><a class=${isActive('state/pages')} ${href('state/pages')}>📃 Pages</a></li>
              <li><a class=${isActive('state/requests')} ${href('state/requests')}>🙏 Requests</a></li>
            </ul>
          </section>
        </li>
        <li>
          <section>
            <h2>Data</h2>
            <ul class='sub-nav'>
              <li><a class=${isActive('backup-and-restore')} ${href('backup-and-restore')}>📦 Backup and restore</a></li>
              <li><a class=${isActive('uploads')} ${href('uploads')}>📂 Uploads</a></li>
              <li><a class=${isActive('db')} ${href('db')}>🗄️ App Database (db)</a></li>
              <li><a class=${isActive('_db')} ${href('_db')}>🗄️ Internal Database (_db)</a></li>

            </ul>
          </section>
        </li>
      </ul>
      <${Styles} />
    </nav>
  `
}
