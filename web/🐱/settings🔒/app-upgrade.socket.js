import kitten from '@small-web/kitten'

/**
  Update (upgrade or downgrade) Kitten to latest compatible release version.

  Will result in a restart of the server.

  // @param TODO: Add socket type info to kitten/globals.
*/
export default ({socket}) => {
  socket.addEventListener('message', async event => {
    const data = JSON.parse(event.data)

    // If the button that triggered us includes Small in its id,
    // we make sure the button we send back has it too.
    const small = data.HEADERS['HX-Trigger'].includes('Small')

    const version = data.version
    
    // Inform person that the update has started.
    setStatus({socket, version, small, status: 'updating'})

    // Actually carry out the update.
    // TODO: Handle errors.
    await globalThis.kitten.appRepository.updateAppToVersion(version)
    
    // Inform person that the server will be restarting.
    setStatus({socket, version, small, status: 'restarting'})

    // If we are running in production, we must exit manually.
    if (process.env.PRODUCTION) {
      process.exit(99 /* restart request */)
    }
  })
}


function setStatus({socket, version, small, status}) {
  socket.send(kitten.html`
    <${globalThis.kitten.appRepository.UpdateButtonComponent()}
      type='${status}'
      version='${version}'
      ${small ? 'small' : ''}
    />
  `)
}
