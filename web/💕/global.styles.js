export default kitten.css`
  :root {
    --accent-colour: deeppink;
    --light-violet: rgba(238, 130, 238, 0.25);
  }

  * {
    box-sizing: border-box;
    accent-color: darkviolet;
  }

  *:focus {
    outline: 2px solid violet;
  }

  *::selection {
    background-color: var(--light-violet);
  }

  body {
    font-family: sans-serif;
    max-width: 640px;
    margin-left: auto;
    margin-right: auto;
  }

  [inert] > * {
    opacity: 0.25;
  }

  li {
    margin-bottom: 0.5em;
  }

  a {
    text-decoration: none;
    border-bottom: 2px solid deepskyblue;
  }

  a:hover {
    text-decoration: none;
    border-bottom: 3px solid deepskyblue;
  }
`
