export default function ({response}) {
  response.setHeader('Content-Type', 'text/plain; charset=utf-8')
  response.end(kitten._db.settings.id.ssh.asString)
}
