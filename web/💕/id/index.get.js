export default function ({response}) {
  response.setHeader('Content-Type', 'text/plain; charset=utf-8')
  if (kitten._db.settings.id === undefined) {
    return response.notFound()
  }

  const id = kitten._db.settings.id.ed25519.asString
  response.end(id)
}
