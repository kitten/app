/**
  Receives backup of Kitten app data to restore.
*/
export default function ({ request }) {
  // The event is broadcast on the session id.
  request.session.emit('dataUploadedForRestore', request.uploads[0])
}
