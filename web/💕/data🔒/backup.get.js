import * as tar from 'tar'

/**
  Returns a tar.gz of all data for current app.

  This includes:

    • Internal database (_db)
    • App database (db)
    • Uploads folder
    • REPL history

  @return {Promise<void>}
*/
// TODO: Set the response type properly.
export default function ({ response }) {
  // Have to return a Promise here, can’t just stream to
  // the response, as we’re using a GET route. It might
  // make sense to implement a raw route in Kitten that
  // doesn’t have any expectations and is meant to be
  // streamed to.
  return new Promise((resolve, reject) => {
    response.statusCode = 200
    response.setHeader('Content-Type', 'application/x-tar-gz')
    response.setHeader('Content-Disposition', `attachment; filename="${globalThis.kitten.domain}.kitten.data.tar.gz"`)
    const stream = tar.create({
      gzip: true,
      follow: true,
      cwd: globalThis.kitten.paths.APP_DATA_DIRECTORY
    }, ['_db', 'db', 'uploads', 'repl'])
    stream.on('error', error => {
      console.error(error)
      response.error(error)
      reject(error)
    })
    stream.on('end', () => {
      response.end()
      resolve()
    })
    stream.pipe(response)
  })
}
