/**
  Basic route that returns 200 Success.

  To be used to test if Kitten has been successfully installed during deployment
  even if the app itself (e.g., the index route) returns an error due to a bug with
  the app itself.
*/
export default function ({ response }) {
  response.withCode(200)
}
