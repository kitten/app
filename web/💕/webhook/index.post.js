/**
  Webhook to be used by remote git repositories to signal
  that a new version of the app is available and the current
  deployment should be upgraded to it.

  Expects the Webhook secret either in the `Authorization` header
  or in a field called `secret` in the body of a POST request.
*/

export default async function ({ request, response }) {
  const hasSecretInBody = request.body !== undefined && request.body.secret !== undefined
  const hasAuthorisationHeader = request.headers !== undefined && request.headers.authorization !== undefined
  if (!hasSecretInBody && !hasAuthorisationHeader) {
    return response.badRequest()
  }

  const secret = hasSecretInBody ? request.body.secret : request.headers.authorization

  if (globalThis.kitten._db.settings.webhookSecret !== secret) {
    return response.forbidden()
  }

  // OK, the sender of the upgrade request is authorised. Let’s
  // upgrade to the latest commit.
  const appRepository = /** @type {import('../../../src/AppRepository.js').default} */ (globalThis.kitten.appRepository)
  await appRepository.update()
  try {
    await appRepository.upgradeAppToLatestAvailableCommit()
  } catch (error) {
    console.error('Webhook: update app to latest git commit in main failed.', error)
    return
  }

  // All good.
  response.ok()

  // App has been updated. Restart for changes to reflect.
  process.exit(99 /* system exit code for restart request */)
}
