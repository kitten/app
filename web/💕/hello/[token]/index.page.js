/**
  Identity and secret creation page.
  
  (The welcome page of a Small Web place that, the first time
  it’s run, displays the kittenmoji-encoded secret of the place’s owner.)

  Copyright ⓒ 2023-present Aral Balkan, Small Technology Foundation.
  Released under AGPL version 3.0.
*/

import kitten from '@small-web/kitten'
import globalStyles from '../../global.styles.js'

export function onConnect ({ page, request }) {
  page.on('generateSecret', data => {
    const domainToken = request.params.token

    if (data.type === 'keys') {
      //
      // Security checks.
      //
      
      // Only persist identity if it doesn’t already exist.
      if (kitten._db.settings.id !== undefined) {
        // Identity already exists, do not overwrite.
        const errorMessage = 'Error: Kitten identity already exists. Will not overwrite.'
        console.error(errorMessage)
        page.send(kitten.html`
          <div id='server-response'>${errorMessage}</div>
        `)
        return
      }

      // Ensure the domain token is correct.
      if (kitten._db.settings.domainToken !== domainToken) {
        // Domain token is incorrect, do not create identity.
        const errorMessage = 'Error: Incorrect domain token. Not generating identity. If you got the error message after creating a new Small Web place at a Domain host, please talk to your domain host about this.'
        console.error(errorMessage)
        page.send(kitten.html`
          <div id='server-response'>${errorMessage}</div>
        `)
        return
      }

      // Security checks have passed.

      // Persist keys.
      const keys = data.keys
      kitten._db.settings.id = keys.public

      // Update client.
      page.send(kitten.html`
        <div id='server-response'>
          <markdown>
            Afterwards, here are some pages you can visit:

            - [Home](/)
            - [Settings](/🐱/settings)

            __Have fun and welcome to the Small Web!__
          </markdown>
        </div>
      `)
    } else {
      // Unexpected data type.
      console.warn(`hello/default.socket: Unexpected data type received, ignoring.`, data)
    }
  })
}

export default ({ request }) => {
  const domain = globalThis.kitten.domain
  const idExists = kitten._db.settings.id !== undefined
  const domainToken = kitten._db.settings.domainToken
  const pathToken = request.params.token

  // Ensure the path token matches the domain token before allowing access to the page.
  if (domainToken !== pathToken) {
    const forbiddenError = new Error()
    // @ts-ignore dynamic property
    forbiddenError.code = 403
    forbiddenError.message = 'Forbidden'
    throw forbiddenError
  }

  return kitten.html`
    <img id='kitten-sitting' src='/🐱/images/kitten-sitting.svg' alt=''>
    <style>
      #kitten-sitting {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 24ch;
      }
    </style>
    <if ${idExists}>
      <then>
        <page syntax-highlighter css>
        <markdown>
          # Welcome back to your Small Web place!

          Here are some pages you can visit:

          - [Home](/)
          - [Settings](/🐱/settings)

          __Security note:__ If you don’t remember visiting this page before, it might be that your secret has been compromised. If so, we’d recommend contacting your Small Web host if you have one and perhaps considering using a different one.

          (If you didn’t save your secret in your password manager the first time you were here, there is no way to retrieve it now[^1] and you will have to contact your Small Web host to recreate your place from scratch.)

          [^1]: If you’re a developer and you’ve lost your secret, you can have Kitten recreate your identity by deleting the internal [code]_db[code] database. From your project’s directory, run:
              [code shell]
              kitten _db delete
              [code] 
        </markdown>
      </then>
      <else>
        <page alpinejs css>
        <!--
          We trigger a WebSocket send the moment Kitten’s crypto library becomes
          available but delay it until after the secret has been generated (at
          which point we issue the request manually).
          
          Secret generation is a very fast process so we don’t need an additional
          progress indicator. (If we find that this is unacceptably slow on certain
          machines, we can always revisit this decision.)

          Note: We use the pathToken here instead of the domain token we get
          from the database to practice defensive coding. Even if the check
          that forbids access to this page should fail due to a regression, we
          won’t be accidentally exposing the correct domain token.
        -->
        <main
          x-data
          connect
          name='generateSecret'
          hx-trigger='kitten:crypto:ready from:body'
          @htmx:ws-config-send.prevent='
            generateSecret($event, $refs.secret, $refs.copySecretButtonPlaceholder)
          '
        >
          <div id='toast'>Copied!</div>
          <h1>Hello!</h2>
          <p><strong>Welcome to your new Small Web place!</strong></p>
          <p>This is your secret:</p>
          <div class='secret'>
            <span id='secret' x-ref='secret'>••••••••••••••••••••••••••••</span>
          </div>
          <div x-ref='copySecretButtonPlaceholder'></div>
          <style>
            .secret {
              font-size: 1.5em;
              text-align: center;
              margin-top: 1em;
              border: 1px solid deeppink;
              border-radius: 1em;
              padding: 1em;
            }
            #secret {
              word-break: break-all;
            }
            #copy-secret-button {
              display: block;
              border: 1px solid white;
              border-radius: 0.5em;
              background-color: deeppink;
              padding: 0.5em;
              color: white;
              font-weight: bold;
              margin: 1em auto;
              font-size: 1.25em;
            }
            #copy-secret-button:hover {
              background: white;
              color: deeppink;
              border: 1px solid deeppink;
            }
            #toast.show { opacity: 1; top: 1em; }
            #toast {
              opacity: 0;
              top: -3em;
              position: fixed;
              transition: all 0.7s ease-out;
              left: 50%;
              transform: translate(calc(-50% + 12ch), 0); /* to right of the kitten */
              border-radius: 1em;
              background-color: var(--background-alt);
              padding: 1em;
              font-weight: bold;
            }
          </style>

          <p><strong>Important:</strong> Please do not leave this page without saving your secret in your password manager.</p>
    
          <p>You will need your secret to sign into your Small Web place and to communicate with your domain host. It is the most important part of your Small Web identity.</p>

          <div id='server-response'></div>
        </main>
        <content for='HEAD'>
          <script type='module' async>
            import { createKeys, secretToEmojiString } from '/🐱/library/crypto-1.js'

            async function generateSecret(event, secretView, copySecretButtonPlaceholder) {
              const keys = await createKeys('${domain}')
              const secret = keys.private.ed25519.asString

              // Important: the server must not have the secret key. That is only
              // for the person who owns this Small Web place to know.
              // (So we only keep the keys we want the server to know about and persist.)
              /**
                @typedef {{
                  private?: Object,
                  public: {
                    ed25519: { asBytes?: Uint8Array },
                    ssh: { asBytes?: Uint8Array }
                  }
                }} DeletableKeys 
              */
              delete(/** @type DeletableKeys */ (keys).private)
              delete(/** @type DeletableKeys */ (keys).public.ed25519.asBytes)
              delete(/** @type DeletableKeys */ (keys).public.ssh.asBytes)

              secretView.innerHTML = secret
              copySecretButtonPlaceholder.outerHTML = \`
                <button id='copy-secret-button' @click="copyToClipboard('\${secret}')">Copy your secret</button>
              \`

              event.detail.socketWrapper.send(
                JSON.stringify({
                  HEADERS: {
                    'HX-Trigger-Name': 'generateSecret'
                  },
                  type: 'keys',
                  keys
                }),
                event.detail.elt /* elt = element */
              )
            }
            globalThis.generateSecret = generateSecret

            const copyToClipboard = async text => {
              await navigator.clipboard.writeText(text)
              const toast = document.getElementById('toast')
              toast.className = 'show'
              setTimeout(() => toast.className = '', 1000)
            }
            globalThis.copyToClipboard = copyToClipboard
          </script>
        </content>
        <content for='AFTER_LIBRARIES'>
          <script>
            htmx.logger = function(elt, event, data) {
              if(console) {
                console.info(event, elt, data)
              }
            }

            // Since we are mixing an IIFE script with src (htmx),
            // with an inline ESM script with async the loading order
            // cannot be predicted (and differs between browser). So
            // we wait for the cryptography function to become
            // available before triggering the htmx call.
            const checkForCryptoReady = setInterval(() => {
              if (globalThis.generateSecret !== undefined) {
                clearInterval(checkForCryptoReady)
                document.body.dispatchEvent(new Event('kitten:crypto:ready'))
              }
            }, 100)
        </script>
      </content>
      </else>
    </if>
    ${[globalStyles]}
  `
}
