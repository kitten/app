import indexStyles from './index.styles.js'

export default function ({request, response}) {
  const idExists = kitten._db.settings.id !== undefined
  
  // Check if the person is already signed in and redirect them if
  // they are. (They may have typed the URL in manually or gone back
  // to it or something.)
  if (request.session.authenticated) {
    response.statusCode = 303
    const redirectUrl = request.session.redirectToAfterSignIn == undefined ? '/%F0%9F%92%95/settings/' : request.session.redirectToAfterSignIn
    return response.redirect(redirectUrl)
  }
  
  // If the page to redirect to hasn’t already been set by the authenticated
  // route middleware and there is a referrer (and it’s not the sign in page itself)
  // set it as the page we want to return the person to after sign in.
  // (We also don’t forward the person back to identity creation page if that’s
  // where they were before hitting the sign-in page.)
  if (request.session.redirectToAfterSignIn == undefined) {
    const referrer = /** @type {string} */ (request.headers.referer)
    if (referrer !== undefined && !referrer.includes('/%F0%9F%92%95/hello/') && !referrer.endsWith('/%F0%9F%92%95/sign-in/')) {
      request.session.redirectToAfterSignIn = kitten.utils.decodeFilePath(referrer)
    }
  }

  // Create a random challenge that the person has to sign with their private key
  // in order to sign in.
  request.session.challenge = kitten.crypto.random32ByteTokenInHex()

  const indexOfDotInDomain = kitten.domain.indexOf('.')

  const signInFormTitle = indexOfDotInDomain === -1 ? kitten.domain : kitten.domain.slice(0, indexOfDotInDomain) + (kitten.port === 443 ? '' : ` (:${kitten.port})`)
  
  return kitten.html`
    <page alpinejs css>

    <div x-data='{ javaScriptAvailable: false }'>
      <div id='javaScriptRequiredForPrivacyReasons' x-cloak x-show='javaScriptAvailable' x-init='javaScriptAvailable = true'>
        <img id='kitten-sleeping' src='/🐱/images/kitten-sleeping.svg' alt=''>
        <main id='content'>
          <h1><a href='/'>${signInFormTitle}</a></h1>
          <h2>Sign in</h2>
          <if ${idExists}>
            <then>
              <p>Use your password manager to enter your secret.</p>

              <section id='authenticationFailure' class='hide'>
                <h3>❌ Sorry, the secret is wrong.</h3>
              </section>

              <form method='POST' @submit.prevent='signIn($el, $refs.secret.value)'>
                <label for='secret'>Secret</label>
                <input
                  id='secret'
                  name='secret'
                  type='password'
                  required
                  x-ref='secret'
                />
                <input name='signature' type='hidden' value=''>
                <button type='submit'>Enter</button>
              </form>
            </then>
            <else>
              <p>You cannot sign in since your identity and secret have not been created yet.</p>
              <p>Please follow the link provided by your Small Web host during sign up or in your terminal during development to generate your identity and secret.</p>
            </else>
          </if>
        </main>
      </div>
      <div id='javaScriptOff' x-show='!javaScriptAvailable'>
        <img id='kitten-paw' src='/🐱/images/kitten-paw.svg' alt=''>
        <markdown>
          # Sign-in requires JavaScript
          
          ## What should I do?
          
          __Please turn JavaScript on in your browser.__

          If you’re using a browser extension like [JavaScript Toggle On and Off](https://addons.mozilla.org/en-US/firefox/addon/javascript-toggler/), please make sure that it’s toggled on.

          ## Why?
          
          __To protect your privacy.__

          Your secret is yours and yours alone. You should only enter and store it in places that you (and you alone) own and control.

          While your browser runs on your machine and is in your control, your server might be hosted by someone else. So your server never knows or stores your secret.

          If you’re interested in the technical details, signing in makes use of [public-key cryptography](https://ar.al/2024/06/24/small-web-computer-science-colloquium-at-university-of-groningen/).

          ## But isn’t all JavaScript evil?

          No.

          Like all code, it depends on who owns and controls it and what it does.

          This is a [Small Web](https://ar.al/2024/06/24/small-web-computer-science-colloquium-at-university-of-groningen/) site powered by [Kitten](https://kitten.small-web.org).

          All the JavaScript here is free and open.
 
          You own and control this site and all of the code that powers it works to protect your interests and your interests alone.
        </markdown>
      </div>
    </div>
    <script>const challenge='${request.session.challenge}'</script>
    <if ${idExists}>
      <script type='module' src='./index.js' />
    </if>
    ${[indexStyles]}
  `
}
