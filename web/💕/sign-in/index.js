import { emojiStringToSecret, sign, verify, bytesToHex, hexToBytes } from '/🐱/library/crypto-1.js'

const publicKey = hexToBytes(await(await fetch('/💕/id/')).text())
const errorMessage = document.getElementById('authenticationFailure')

function showError() {
  errorMessage.classList.remove('hide')
  errorMessage.classList.add('show')
}

function hideError() {
  errorMessage.classList.add('hide')
  errorMessage.classList.remove('show')
}

globalThis.signIn = async function (form, secretEmojiString) {
  hideError()

  const secret = emojiStringToSecret(secretEmojiString)
  
  // Check the general shape of the secret before going any further.
  const secretIsTheCorrectLength = secret.length === 32
  const secretIsNotAllZeros = secret.join('') !== '0'.repeat(32)
  const secretIsTheCorrectShape = secretIsTheCorrectLength && secretIsNotAllZeros

  if (secretIsTheCorrectShape) {
    // OK, secret is the correct shape. Now let’s sign the challenge
    // and verify the signature client-side before sending it off
    // to the server.
    const signature = await sign(challenge, secret)
  
    const verified = await verify(signature, challenge, publicKey)
    if (verified) {
      // OK, we know the person should be authorised.
      // Let’s save it in local storage for the time being so
      // it can be used by any authenticated page to encrypt data.
      // (TODO: Should this be session storage instead? Although
      // that‘s not synced to our understanding of a session so it might
      // be more confusing/unpredictable than not. Likely best if we
      // keep it in localStorage and expire it on sign out.)
      localStorage.setItem('secret', secretEmojiString)
      
      // Let’s send the signature to the server too so it knows also.
      // (And remove the secret so we don’t send that to the server.)
      const signatureAsHexString = bytesToHex(signature)
      form.elements.secret.value = ''
      form.elements.signature.value = signatureAsHexString 
      form.submit()
    } else {
      // We couldn’t verify the signature client-side so no need to send it to the server.
      showError()
    }
  } else {
    // The secret isn’t even the right shape. No need to go any further.
    showError()
  }
}
