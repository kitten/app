import globalStyles from '../global.styles.js'

const indexStyles = globalStyles + kitten.css`
  @media (prefers-color-scheme: dark) {
    :root {
      --accent-colour: var(--border);
    }
  }
  
  main {
    max-width: 50ch;
    padding: 1em;
    border: 1em solid var(--accent-colour);
    border-radius: 1em;
    margin-left: auto;
    margin-right: auto;
  }

  h1 {
    background-color: var(--accent-colour);
    color: white;
    margin-left: -0.75em;
    margin-right: -0.75em;
    margin-top: -0.75em;
    padding-top: 0.5em;
    padding-bottom: 0.25em;
    padding-left: 1em;
    padding-right: 1em;
    font-size: 2rem;
    text-align: center;
  }

  h1 a, h1 a:hover {
    color: white;
    border-bottom: none;
  }

  form {
    margin-top: 2em;
  }

  form label, form input, form button, form select, form textarea {
    font-size: 1.25em;
    display: block;
  }

  form label {
    font-weight: 500;
  }

  form input, form select, form textarea {
    margin: 0.5em 0 1.5em 0;
    width: 100%;
    padding: 0.25em;
  }

  /* Note: there is currently no way to actually set the colour of a selected item in a select box. */
  option:checked, option:hover, option:active, option:focus {
    background: linear-gradient(var(--light-violet), var(--light-violet)) !important;
  }

  form button {
    display: block;
    border-radius: 0.5em;
    width: 50%;
    padding: 0.5em;
    margin: 0.75em auto;
  }

  /*
  form button:hover, form button:focus {
    background-color: var(--light-violet);
    color: darkviolet;
    border-color: darkviolet;
  }

  form button:active {
    background-color: violet;
    color: white;
  }
  */

  /* Alpine.js x-cloak attribute support */
  [x-cloak] { display: none !important; }

  #kitten-sleeping {
    width: 100%;
    max-width: 30ch;
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: -4.8em;
  }

  #javaScriptOff h1 {
    padding-top: 1em,
    padding-bottom: 1em
  }

  #kitten-paw {
    width: 8em;
    display: block;
    margin-bottom: 4em;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
  }

  #javaScriptRequiredForPrivacyReasons {
    min-width: 260px;
  }

  .show {
    display: block;
  }

  .hide {
    display: none;
  }
`
export default indexStyles
