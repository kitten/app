////////////////////////////////////////////////////////////////////////////////
//
// Kitten: /sign-in (POST)
//
// Authenticates provided signature (the 32-byte random challenge we sent
// earlier from the server that the person has signed using their ed25519
// private key).
//
////////////////////////////////////////////////////////////////////////////////

// Verifies ed25519 signatures.
import { verify } from '@noble/ed25519'

// General utilities for converting between bytes and hex.
import { hexToBytes } from '@noble/hashes/utils'

const lowercaseHexadecimalStringRegExp = /^[a-f0-9]+$/

export default async function ({request, response}) {
  const signature = request.body.signature
  if (!signature) {
    return response.forbidden()
  }

  // Verify the shape of signature before going any further.
  // (We expect a 128-bit ed25519 signature in all-lowercase
  // hexademical string format.)
  const signatureIsTheRightLength = signature.length === 128
  const signatureIsLowercaseHexadecimalString = lowercaseHexadecimalStringRegExp.exec(signature)
  const signatureIsTheRightShape = signatureIsTheRightLength && signatureIsLowercaseHexadecimalString

  if (!signatureIsTheRightShape) {
    return response.forbidden()
  }

  // Edge case: not sure what exactly is causing it but I’ve seen hexToBytes(), below,
  // being called with undefined. Which causes a server error. Let’s try to catch it
  // and debug it next time it happens.
  const _publicKey = kitten._db.settings.id.ed25519.asString
  if (_publicKey === undefined) {
    console.trace('Public key in database is undefined. This should not happen.')
    return response.forbidden()
  }

  const challenge = request.session.challenge
  const publicKey = hexToBytes(_publicKey)
  const verified = await verify(signature, challenge, publicKey)

  if (!verified) {
    // The web interface should only ever send us verified signatures.
    // Since this is not verified, we know it didn’t come from the web
    // interface (or other valid interface). So we just write a forbidden response.
    // (As this is likely a hack attempt.)
    return response.forbidden()
  }

  // Authentication succeeded.
  request.session.authenticated = true
  let urlToRedirectTo = /** @type {string?} */ (request.session.redirectToAfterSignIn)
  if (urlToRedirectTo == undefined || urlToRedirectTo.endsWith('.socket') || urlToRedirectTo.includes('/💕/hello/')) {
    urlToRedirectTo = '/🐱/settings/'
  }
  request.session.redirectToAfterSignIn = null
  response.get(urlToRedirectTo)
}
