export default function ({request, response}) {
  request.session.authenticated = false
  if (request.session.redirectToAfterSignIn != undefined && !request.session.redirectToAfterSignIn.startsWith('/💕/hello/')) {
    response.get(request.session.redirectToAfterSignIn)
  } else {
    request.session.redirectToAfterSignIn = '/'
    response.get('/💕/sign-in/')
  }
}
