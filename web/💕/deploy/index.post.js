/**
  Deploys a new app to the Small Web server.

  For use with cryptographically-secure domain token as provided in the route.
*/
export default async function ({ request, response }) {
  if (
    !request.body ||
    !request.body.gitHttpsCloneUrl ||
    !request.body.domainToken ||
    !request.body.domain
  ) {
    return response.badRequest()
  }

  const {gitHttpsCloneUrl, domainToken, domain} = request.body

  // Security check. Ensure domain token is correct.
  if (globalThis.kitten._db.settings.domainToken !== domainToken) {
    return response.forbidden()
  }

  if (!gitHttpsCloneUrl.startsWith('https://')) {
    return response.badRequest('Git repository URL must begin with https.')
  }

  if (!gitHttpsCloneUrl.endsWith('.git')) {
    return response.badRequest('Git repository URL must end with .git.')
  }

  const options = {
    domain,
    context: 'web',
    'domain-token': domainToken,
    progress: (/** @type {string} */ message) => {
      if (message === 'starting-kitten-service') {
        // OK, the redeployment went well. Since the server is restarting,
        // this is the last chance we have to communicate status to the
        // Small Web Host.
        response.ok()
      }
    }
  }

  // Note that the Small Web Host Domain cannot be changed via this method
  // and is set to the value already specified during initial deployment time
  // (if any). This is to prevent some other Small Web Host from gaining
  // control of a Small Web place if they somehow learn the domain token.
  
  // @ts-ignore Internal Kitten database property.
  const smallWebHostDomain = kitten._db.settings.smallWebHostDomain

  // Also copy the Small Web Host domain if one exists.
  if (smallWebHostDomain !== undefined) {
    console.info('🚚 /💕/deploy/ – actual app is being deployed by domain')
    options['small-web-host-domain'] = smallWebHostDomain
    globalThis.kitten.__db.state.isBeingDeployedByDomain = true
  }

  try {
    await globalThis.kitten.deploy(gitHttpsCloneUrl, options)

    // Note that because the new deployment will result in a server restart
    // execution will never reach this point so we don’t have a final
    // status communication here. Instead, we complete the process
    // in the progress handler, above, when the starting-kitten-service
    // event is received.
  } catch (error) {
    globalThis.kitten.__db.state.isBeingDeployedByDomain = false
    response.error(error)
  }
}
