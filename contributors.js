// Compile a list of the libraries used and the people
// responsible for them so we know whose work has gone
// into making Kitten possible.

import fs from 'node:fs'
import glob from 'fast-glob'

const packageFilePaths = await glob('node_modules/**/package.json')

let i = 0
const packages = {}
const contributors = {}
const numberOfContributions = {}
const licenses = {}

function addContributorDetailsFrom(stringOrObject) {
  /** @type {(string|null)} */
  let contributorName = null

  /** @type {(string|null)} */
  let contributorEmail = null

  /** @type {(string|null)} */
  let contributorUrl = null
  
  if (stringOrObject) {
    if (typeof stringOrObject === 'object') {
      /** @type {{name: string, email: string, url: string}} */
      const contributor = stringOrObject

      if (contributor.name) contributorName = contributor.name
      if (contributor.email) contributorEmail = contributor.email
      if (contributor.url) contributorUrl = contributor.url
    } else if (typeof stringOrObject === 'string') {
      /** @type {string} */ 
      const contributor = stringOrObject

      contributorName = contributor.match(/^[^(<]+/)
      contributorEmail = contributor.match(/<(.+?)>/)
      contributorUrl = contributor.match(/\((.+?)\)/)

      if (contributorName !== null) {
        contributorName = contributorName[0].replace(/\s+?$/, '')
      }
      if (contributorEmail !== null) contributorEmail = contributorEmail[1]
      if (contributorUrl !== null) contributorUrl = contributorUrl[1]
    } else {
      throw new Error(`Unexpected contributor data type found (${typeof stringOrObject})`, stringOrObject)
    }
  }

  // People put all sorts of things in package.json fields :)
  // (Try to clean things up a bit an encourage good practices.)
  if (contributorUrl) {
    if (contributorUrl.startsWith('http://')) contributorUrl = contributorUrl.replace('http://', 'https://') // Secure links only, please (it’s 2023).
    if (!contributorUrl.startsWith('https://')) contributorUrl = `https://${contributorUrl}`
  }

  if (contributorName !== null) {   
    if (contributors[contributorName] === undefined) {
      contributors[contributorName] = {}
      numberOfContributions[contributorName] = 0
    }

    numberOfContributions[contributorName]++

    /** @type {{name: string, email: string, url: string}} */
    const contributor = contributors[contributorName]

    contributor.name = contributorName
    if (contributorEmail !== null) contributor.email = contributorEmail
    if (contributorUrl !== null) contributor.url = contributorUrl
  }
}

packageFilePaths.forEach(packageFilePath => {
  if (!packageFilePath.includes('resolve/test') && !packageFilePath.includes('tape/test')) {
    i++
    const packageName = packageFilePath.match(/\/([^/]*?)\/package\.json$/)[1]
    if (packages[packageName] === undefined) {

      /** @type {{author: (string|object), contributors: array}} */
      const pkg = JSON.parse(fs.readFileSync(packageFilePath, 'utf-8'))

      packages[packageName] = pkg

      if (pkg.author) addContributorDetailsFrom(pkg.author)
      if (Array.isArray(pkg.contributors)) pkg.contributors.forEach(contributor => addContributorDetailsFrom(contributor))

      if (pkg.license) {
        let license
        if (typeof pkg.license === 'string') {
          license = pkg.license
        }
        else if (pkg.license.type !== undefined) {
          license = pkg.license.type
        }
        else {
          console.warn(`!!! Package ${pkg.name} has invalid license property`, pkg.license)
          license = 'Invalid'
        }

        if (licenses[license] === undefined) {
          licenses[license] = 0
        }
        licenses[license]++
      }
    }
  }
})

const featuredContributors = fs.readFileSync('./featured-contributors.md', 'utf-8')

const project = JSON.parse(fs.readFileSync('./package.json', 'utf-8'))

const alphabetically = (a, b) => a.toLowerCase().localeCompare(b.toLowerCase())
const descendingByValue = (a, b) => b[1] - a[1]

const packageNames = Object.keys(packages).sort(alphabetically)
const contributorNames = Object.keys(contributors).sort(alphabetically)
const licensesSortedByPopularity = Object.entries(licenses).sort(descendingByValue)

const pluralise = (amount, singular, plural) => amount === 1 ? singular : plural

const contributorsMarkdown = `
## Credits

Kitten is authored by [${project.author.name}](${project.author.url}) with the contributions of the people featured below as well as the [${contributorNames.length} people, organisations, and teams](#package-contributors) that helped create the [${packageNames.length} packages](#packages) that make Kitten possible.

${featuredContributors}

### Package contributors

${contributorNames.map(contributorName => {
  /** @type {{ url: string, name:string, email: string }} */
  const contributor = contributors[contributorName]

  return '- '
    + (contributor.url ? '[' : '')
    + contributor.name
    + (contributor.url ? `](${contributor.url})` : '')
    + (numberOfContributions[contributor.name] > 1 ? ` – ${numberOfContributions[contributor.name]} packages` : '')
}).join('\n')}

### Packages

${packageNames.map(packageName => {
  const pkg = packages[packageName]
  return '- '
    + '['
    + pkg.name
    + `](https://npmjs.org/${pkg.name})`
    + (pkg.description ? `: ${pkg.description}` : '')
    + (pkg.license && typeof pkg.license === 'string' ? ` (${pkg.license} license)` : '') 
}).join('\n')}

### Licenses

${licensesSortedByPopularity.map(license => {
  return `- ${license[0].replace(/\((.*?)\)/, '$1')} (${license[1]} ${pluralise(license[1], 'package', 'packages')})`
}).join('\n')}
`

fs.writeFileSync('CONTRIBUTORS.md', contributorsMarkdown, 'utf-8')
