import fs from 'node:fs'
import path from 'node:path'
import assert from 'node:assert/strict'

import test from 'tape'
import { withoutWhitespace } from './helpers/index.js'

import * as utils from '../src/Utils.js'

test('basepath', t => {
  const relativePathToEmptyProject = 'tests/fixtures/emptyProject'
  const absolutePathToEmptyProject = path.resolve(relativePathToEmptyProject)

  const relativePathToProjectWithSrcFolder = 'tests/fixtures/emptyProjectWithSrcFolder'
  const absolutePathToProjectWithSrcFolder = path.resolve(relativePathToProjectWithSrcFolder)
  
  // Since these are empty folders, they will not be held in the 
  // git repository. So we ensure they exist and create them if they do not
  // our test doesn’t fail erroneously.
  if (!fs.existsSync(absolutePathToEmptyProject)) {
    fs.mkdirSync(absolutePathToEmptyProject, {recursive: true})
  }
  
  if (!fs.existsSync(absolutePathToProjectWithSrcFolder)) {
    fs.mkdirSync(path.join(absolutePathToProjectWithSrcFolder, 'src'), {recursive: true})
  }
  
  const relativeNonExistentPath = 'this-path-does-not-exist'
  const absoluteNonExistentPath = path.resolve(relativeNonExistentPath)

  // This is what’s passed in via the --working-directory option by the kitten command.
  const defaultWorkingDirectory = process.cwd()

  const basePath = utils.setBasePath(defaultWorkingDirectory, relativePathToEmptyProject)

  t.equal(process.cwd(), basePath, 'process.cwd is set base path of empty project')

  const basePathWithSourceFolder = utils.setBasePath(defaultWorkingDirectory, relativePathToProjectWithSrcFolder)

  t.equal(process.cwd(), basePathWithSourceFolder, 'process.cwd is set to base path of project with src folder')

  t.equal(basePath, absolutePathToEmptyProject, 'base path to empty project is correct')
  t.equal(basePathWithSourceFolder, path.join(absolutePathToProjectWithSrcFolder, 'src'), 'base path to project with src folder is correct')
  t.throws(
    () => utils.setBasePath(defaultWorkingDirectory, relativeNonExistentPath),
    error => {
      assert(error.message.includes(`${absoluteNonExistentPath} – path does not exist`))
      return true
    },
    'Non-existent paths throw.'
  )
  
  // TODO: Also test non-default working directory.
  // TODO: Also test that process.env.basePath is set.

  t.end()
})

test ('class name from route', t => {
  t.equal(
    utils.classNameFromRoutePattern('/some_route/with/underscores-and-hyphens:and-a-parameter/or:two'), 'SomeRouteWithUnderscoresAndHyphensAndAParameterOrTwoPage'
  )
  t.equal(utils.classNameFromRoutePattern('/'), 'IndexPage')
  t.end()
})

test ('routePatternFromFilePath', t => {
  const basePath = process.env.basePath

  // Note: not including .socket.js here because that’s the odd one out where
  // ===== we don’t currently remove the extension (due to the current limitation
  // with WebSocket routes sharing the same path as http routes).
  const supportedExtensions = ['get', 'head', 'patch', 'options', 'connect', 'delete', 'trace', 'post', 'put', 'page'] 

  // Some manual tests of actual routes in the Domain app (https://github.com/small-tech/domain).
  const expectations = [
    [path.join(basePath, 'index_[property1]_and_[property2].page.js'), '/:property1/and/:property2/'],
    [path.join(basePath, 'index_with_[property1]_and_[property2].page.js'), '/with/:property1/and/:property2/'],
    [path.join(basePath, 'admin/index_[password].socket.js'), '/admin/:password.socket'],
    [path.join(basePath, 'domain/available_[domain].get.js'), '/domain/available/:domain/'],
    [path.join(basePath, 'private_[token]_[domain].get.js'), '/private/:token/:domain/'],
    [path.join(basePath, 'hello_[name]index.page.js'), '/hello/:name/'],
    [path.join(basePath, 'goodbye_[name].page.js'), '/goodbye/:name/'],
    [path.join(basePath, 'happy-birthday_[name]_[age].page.js'), '/happy-birthday/:name/:age/'],
    [path.join(basePath, 'hi_[optional-name].page.js'), '/hi/:name?/'],
    [path.join(basePath, 'bye_[name]_[some-last-words].page.js'), '/bye/:name/:some-last-words/'],
    
    
  ]

  for (const supportedExtension of supportedExtensions) {
    expectations.push([
      path.join(basePath, 'a', 'route-with', `this_[property].${supportedExtension}.js`),
      `/a/route-with/this/:property/`
    ])
  }

  for (const expectation of expectations) {
    t.equal(utils.routePatternFromFilePath(expectation[0], basePath), expectation[1], expectation[0])
  }

  t.end()
})

// test ('kittenAppPath', t => {
//   // Note: this doesn’t test the app path when run from the Kitten bundle or 
//   // the Kitten launch script from source. It only tests it when run from this
//   // file in the unit tests.
//   t.equal(utils.kittenAppPath, path.resolve('.') + '/')
//   t.end()
// })
