import path from 'path'
import { fileURLToPath, URL } from 'url'
import process from 'process'

import test from 'tape'

// .rejects() and .doesNotReject() mixins taken from tape-promise:
// https://github.com/jprichardson/tape-promise/blob/master/index.babel.js#L25
function registerNewAssertions (Test) {
  Test.prototype.rejects = function (promise, expected, message = 'should reject', extra) {

    if (typeof promise === 'function') promise = promise()

    return promise
      .then(() => {
        this.throws(() => {}, expected, message, extra)
      })
      .catch(err => {
        this.throws(() => { throw err }, expected, message, extra)
      })
      .then(() => {}) // resolve on failure to not stop execution (assertion is still failing)
  }

  Test.prototype.doesNotReject = function (promise, expected, message = 'should resolve', extra) {
    if (typeof promise === 'function') promise = promise()
    return promise
      .then(() => {
        this.doesNotThrow(() => {}, expected, message, extra)
      })
      .catch(err => {
        this.doesNotThrow(() => { throw err }, expected, message, extra)
      })
      .then(() => {}) // resolve on failure to not stop execution (assertion is still failing)
  }
}
// @ts-ignore Private property.
registerNewAssertions(test.Test)

import Files, { FilesByExtensionCategoryType } from '../src/Files.js'

const __dirname = fileURLToPath(new URL('.', import.meta.url))
const fixturesPath = path.join(process.cwd(), 'examples', 'routing')

test('files', async t => {
  t.throws (() => new Files('.'), 'Attempting to run private constructor should throw.')
  
  const files = await Files.new(fixturesPath)

  await /** @type {test.Test & {rejects: function}} */ (t).rejects (async () => await files.initialise(), 'Attempting to call initialise() directly should throw.')
  
  const f = files.byExtensionCategoryType
  const pages = f.frontendRoutes['page.js']
  const md = f.staticRoutes.md
  t.equals(pages.length, 5, 'there are five pages in front-end routes')
  t.assert(/** @type {string} */ (pages[0]).endsWith('bye_[name]_[some-last-words].page.js'))
  t.assert(/** @type {string} */ (pages[1]).endsWith('goodbye_[name].page.js'))
  t.assert(/** @type {string} */ (pages[2]).endsWith('happy-birthday_[name]_[age].page.js'))
  t.assert(/** @type {string} */ (pages[3]).endsWith('hi_[optional-name].page.js'))
  t.assert(/** @type {string} */ (pages[4]).endsWith('hello/[name]/index.page.js'))
  t.isEquivalent(f.frontendRoutes['page.js'], f.dynamicRoutes['page.js'])
  t.isEquivalent(f.dynamicRoutes['page.js'], f.allRoutes['page.js'])
  t.equals(md.length, 1)
  t.assert(/** @type {string} */ (md[0]).endsWith('README.md'))
  files.close()
})

test('chokidar error handling', async t => {
  process.env.basePath = '/'
  await /** @type {test.Test & {rejects: function}} */ (t).rejects(async () => await Files.new(), /Cannot serve root/, 'attempting to watch root should reject')
})
