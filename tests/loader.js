import os from 'os'
import test from 'tape'
import { resolve } from '../src/processes/loader.js'
import asyncForEach from '@small-tech/jsdb/lib/async-foreach.js'

// FIXME: Some of these directories are hardcoded to work only on my (Aral’s)
// machine (e.g., the relative paths to domain, etc.)
// The scaffolding for any Domain-related test should either be recreated here or 
// moved to Domain itself.

const homeDirectory = os.homedir()

test('resolve', async t => {
  // The context conditions passed during resolution.
  const conditions = [ 'node', 'import', 'node-addons' ]

  // Paths/URLs used in the tests.
  // Gathered from a run of Domain (https://github.com/small-tech/domain).
  const kittenBaseUrl = `file://${homeDirectory}/.local/share/small-tech.org/kitten/app`
  const appBaseUrl = `file://${homeDirectory}/Projects/small-web/domain`
  const kittenBundleUrl = `${kittenBaseUrl}/kitten-bundle.js`
  const adminIndexPagePath = `${homeDirectory}/Projects/small-web/domain/admin/index.page.js`
  const adminIndexPageFileUrl = `file://${adminIndexPagePath}`
  const placesComponentFileUrl = `${appBaseUrl}/admin/places/Index.component.js`
  const paymentBaseFileUrl = `file://${homeDirectory}/Projects/small-web/domain/admin/setup/payment`

  const defaultResolve = 'expect-default-resolve'

  const resolutions = [
    // Kitten bundle itself.
    {
      specifier: kittenBundleUrl,
      parentURL: undefined,
      resolvesTo: defaultResolve
    },

    // An internal node package.
    {
      specifier: 'module',
      parentURL: kittenBundleUrl,
      resolvesTo: defaultResolve
    },

    // A page from within the app/site being served.
    {
      specifier: adminIndexPagePath,
      parentURL: `${appBaseUrl}/`,
      resolvesTo: {
        url: `file://${homeDirectory}/Projects/small-web/domain/admin/index.page.js`,
        format: 'module',
        shortCircuit: true
      }
    },

    // An internal Node package imported by the app/site being served.
    {
      specifier: 'node:buffer',
      parentURL: adminIndexPageFileUrl,
      resolvesTo: defaultResolve
    },

    // A third-party regular Node module imported from a component in the app/site.
    {
      specifier: '@small-tech/spinners',
      parentURL: placesComponentFileUrl,
      resolvesTo: defaultResolve
    },

    // A component imported from another component in the app/site using a relative path (component extension).
    {
      specifier: './Tokens.component.js',
      parentURL: `${paymentBaseFileUrl}/Index.component.js`,
      resolvesTo: {
        url: `file://${homeDirectory}/Projects/small-web/domain/admin/setup/payment/Tokens.component.js`,
        format: 'module',
        shortCircuit: true
      }
    },

    // Loading one module from another with a relative specifier in the app/site.
    {
      specifier: './Util.js',
      parentURL: `${appBaseUrl}/library/JSDB/DataProxy.js`,
      resolvesTo: defaultResolve
    },

    // A regular module imported from a component using a relative app in the app/site.
    {
      specifier: '../../library/Constants.js',
      parentUrl: `${appBaseUrl}/admin/setup/PSL.component.js`,
      resolvesTo: defaultResolve
    }
  ]

  t.plan(resolutions.length)

  await asyncForEach(resolutions, async resolution => {
    let defaultResolveFunction
    if (resolution.resolvesTo === defaultResolve) {
      defaultResolveFunction = () => {
        t.pass(`defaultResolve called as expected for specifier ${resolution.specifier}`)
      }
    } else {
      defaultResolveFunction = () => {
        t.fail(`defaultResolve unexpectedly called for specifier ${resolution.specifier}`)
      }
    }
    const resolved = await resolve(
      resolution.specifier,
      { 
        parentURL: resolution.parentURL,
        conditions
      }, // (context)
      defaultResolveFunction
    )

    if (resolution.resolvesTo !== defaultResolve) {
      t.deepEqual(resolution.resolvesTo, resolved, `Custom resolution as expected for specifier ${resolution.specifier}`)
    }
  })
})
