/**
  End-to-end tests: Draw Together.

  Live at: https://draw-together.small-web.org

  Tests:

    - Streaming HTML.
*/
import { Kitten } from '../regression/helpers/Kitten.js'
import { test, expect } from '@playwright/test'

class DrawTogetherPage {
  get buttons () {
    return this.page.getByRole('button')
  }

  /**
    @param {{
      row: number,
      column: number
    }} parameterObject
  */
  buttonAt({ row, column }) {
    return this.page.getByLabel(new RegExp(`^pixel-${row}-${column}\$`))
  }

  /**
      @param {{
        browser: import('@playwright/test').Browser 
      }} parameterObject
  */
  async initialise ({ browser }) {
    const context = await browser.newContext()
    this.page = await context.newPage()

    await this.page.goto('https://localhost/')
    await expect(this.page).toHaveTitle(/Draw Together/)
    await expect(this.buttons).toHaveCount(400)
    // Make sure all buttons start out white.
    for (let row = 0; row < 20; row++) {
      for (let column = 0; column < 20; column++) {
        await expect(this.buttonAt({ row, column })).toHaveCSS('background-color', 'rgb(255, 255, 255)')
      }
    }
    return this
  }
}

const kitten = new Kitten({
  path: 'examples/streaming-html/draw-together'
})

test.describe('Draw Together', () => {
  // Start Kitten.
  test.beforeAll(async () => {
    await kitten.start()
  })

  // Run tests.
  test('Works as expected', async ({ browser }) => {
    const page1 = await (new DrawTogetherPage()).initialise({ browser })
    const page2 = await (new DrawTogetherPage()).initialise({ browser })

    // Toggle a “pixel” on the first page.
    const page1Button = page1.buttonAt({ row: 9, column: 9 })
    await page1Button.click()
    await expect(page1Button).toHaveCSS('background-color', 'rgb(0, 0, 0)')

    // Expect the change on page 1 to be reflected on page 2.
    await expect(page2.buttonAt({ row: 9, column: 9})).toHaveCSS('background-color', 'rgb(0, 0, 0)')
  })

  // Stop Kitten.
  test.afterAll(async () => {
    await kitten.stop()
  })
})
