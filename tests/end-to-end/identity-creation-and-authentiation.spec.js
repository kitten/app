/**
  End-to-end tests: identity creation and authentication.

  Tests:

    - Sessions
    - Client-side secret creation
    - Identity creation and persistence
    - Authenticated routes
    - Sign in
    - Sign out
*/
import { Kitten } from '../regression/helpers/Kitten.js'
import { test, expect } from '@playwright/test'

let kitten

test.describe('Identity creation and authentication', () => {
  // Start Kitten.
  test.beforeAll(async () => {
    kitten = await Kitten.serverRunningAt({
      path: 'examples/authentication',
      deleteData: true,
      debug: false
    })
  })

  // Run tests.
  test('Works as expected', async ({ page, browser, context }) => {
    // Attempt to access the private route before generating your identity/secret.
    // const browserName = browser.browserType().name()
    await page.goto('https://localhost/private')

    // Since we have not created our secret yet, we should be forwarded
    // to the Kitten sign-in page informing us of this.
    await expect(page.locator('#content > h2')).toHaveText('Sign in')
    await expect(page.locator('#content > p:first-of-type')).toContainText('your identity and secret have not been created yet')

    // At this point we expect one session/cookie.
    const cookies = await context.cookies()
    expect (cookies.length).toBe(1)
    const cookie = cookies[0]
    expect (cookie.name).toBe('sessionId')
    expect (cookie.domain).toBe('localhost')
    expect (cookie.path).toBe('/')
    expect (new Date(cookie.expires * 1000) > new Date(), 'expiry date should be in the future').toBeTruthy()
    expect (cookie.secure).toBe(true)
    expect (cookie.sameSite).toBe('Strict')

    // At this point, the session should be persisted and
    // set as unauthenticated.
    const sessionsPreAuthentication = kitten.sessions()
    const unauthenticatedSession = sessionsPreAuthentication[cookie.value]
    // FIXME: This very rarely (and seemingly randomly) can fail
    // with kitten.sessions returning an empty object (the table’s root
    // object is empty. It doesn’t seem to be a timing issue as waiting and
    // retrying doesn’t work – waited up to 20 seconds to see. Also, it doesn’t
    // seem to be a sync/async-related issue as it occurs regardless of which
    // is used to read in the table. I’m stumped at the moment but keeping the
    // logging statements in, commented out, in case they come in handy down
    // the line.)
    expect (unauthenticatedSession !== undefined).toBe(true)
    expect (unauthenticatedSession.authenticated).toBe(false)
    expect (unauthenticatedSession.redirectToAfterSignIn).toBe('/private/')

    // Generate identity.
    // @ts-ignore kitten.identityUrl might be null but that’s an error we want the test to catch.
    const identityUrl = kitten.identityUrl
    await page.goto(identityUrl)

    // Ideally, we’d wait for the htmx:wsOpen event on the body as that’ what
    // the page itself does before generating the secret (this is to workaround
    // a random timing bug in Safari where kicking things off in the `load` event
    // doesn’t always guarantee that the module script has executed – even if
    // set to `async`). Given Playwright doesn’t have the ability to easily wait
    // for custom events¹, and the suggested convoluted workarounds² don’t seem
    // to work in this case, we simply wait for the text to not be the placeholder.
    // (This is somewhat of a brittle test as it will fail if we ever change
    // the placeholder without updating this test.)
    // 
    // ¹ See: https://github.com/microsoft/playwright/issues/15440
    // ² Based on: https://stackoverflow.com/a/77312913

    // Make sure secret is properly generated.
    // await page.screenshot({ path: `secret-${browserName}-1.png` })
    const secretDisplay = page.locator('#secret')
    await expect(secretDisplay).not.toHaveText('••••••••••••••••••••••••••••')
    const secret = await secretDisplay.textContent()

    // await page.screenshot({ path: `secret-${browserName}-2.png` })
    // Test copy your secret button (actually the toast message).
    // Clipboard testing is not really supported yet¹ so we at
    // least test for the toast message.
    //
    // ¹ https://github.com/microsoft/playwright/issues/13097
    // 
    // Can’t test that either, apparently, since Chromium, at least,
    // isn’t fast enough on my machine to catch the class change within
    // one second.
    // 
    // Disabling all these tests for the time being.
    // 
    // await context.grantPermissions(['clipboard-read', 'clipboard-write'])
    // const toast = page.locator('#toast')
    // await expect(toast).not.toHaveClass('show')
    // const copySecretButton = page.locator('#copy-secret-button')
    // await copySecretButton.click()
    // await expect(toast).toHaveClass('show')
    // Wait for toast to be hidden (happens after 1000ms).
    // await page.waitForTimeout(1500)
    // await expect(toast).not.toHaveClass('show')
    
    // const secretInClipboard = await page.evaluate('navigator.clipboard.readText()')
    // expect (secretInClipboard).toEqual(secret)

    // Now that we’ve generated the secret, let’s go back and
    // test the sign-in page.
    await page.goto('https://localhost/private')
    await expect(page).toHaveURL('https://localhost/💕/sign-in/')
    const secretInput = page.locator('#secret')
    const submitButton = page.locator('button[type=submit]')
    // @ts-ignore If secret is null, we want this to fail.
    // await page.screenshot({ path: `secret-${browserName}-3.png` })
    await secretInput.fill(secret)
    // await page.screenshot({ path: `secret-${browserName}-4.png` })
    await submitButton.click()
    await page.waitForLoadState()

    // Ensure we’re successfully signed in and viewing the private page.
    await expect(page).toHaveURL('https://localhost/private/')
    await expect(page.locator('h1')).toHaveText('Private')

    // Ensure state of the session reflects our authenticated state.
    // NB. Kitten only uses HTTPS so is not open to session fixation attacks.
    // (Which is why we wouldn’t gain anything by recreating the session after sign in.)
    const sessionsPostAuthentication = kitten.sessions()
    const authenticatedSession = sessionsPostAuthentication[cookie.value]
    expect (authenticatedSession !== undefined)
    expect (authenticatedSession.authenticated).toBe(true)
    expect (authenticatedSession.redirectToAfterSignIn).toBe(null)

    // Ensure different challenges are sent to the client each time.
    expect (unauthenticatedSession.challenge !== authenticatedSession.challenge)

    // Restart the server and ensure that all relevant data was
    // correctly persisted and that we are still signed in.
    await kitten.stop()
    kitten = await Kitten.serverRunningAt({
      path: 'examples/authentication',
      deleteData: false,
      debug: false
    })
    await page.goto('https://localhost/private/')
    await expect(page).toHaveURL('https://localhost/private/')
    await expect(page.locator('h1')).toHaveText('Private')

    // Test sign out.
    await page.goto('https://localhost/💕/sign-out/')
    await page.waitForLoadState()

    // The default redirection route after sign in is
    // Kitten’s Settings page.
    await expect(page).toHaveURL('https://localhost/💕/sign-in/')

    const sessionsPostSignOut = kitten.sessions()
    const signedOutSession = sessionsPostSignOut[cookie.value]
    expect (signedOutSession !== undefined)
    expect (signedOutSession.authenticated).toBe(false)

    // Load up the “hello” page again and make sure
    // that the secret is not re-generated and we get the welcome
    // back message that we expect.
    // Note: we use the stored identityUrl here since we restarted
    // the server and there won’t be one we can get from kitten.identityUrl.
    // @ts-ignore identityUrl might be null but that’s an error we want the test to catch.
    await page.goto(identityUrl)
    await expect (page.locator('#secret')).toHaveCount(0)
    await expect (page.locator('#welcome-back-to-your-small-web-place')).toHaveCount(1)
    await expect (page.getByText('Security note')).toBeVisible()

    // Let’s also test that loading up the “hello” route with the
    // wrong security token results in a 403 forbidden error.
    const forbiddenResponse = await page.goto('https://localhost/💕/hello/this-is-not-a-valid-one-time-security-token/')
    expect(forbiddenResponse?.status()).toBe(403)
  })

  // Stop Kitten.
  test.afterAll(async () => {
    await kitten.stop()
  })
})
