/**
  End-to-end tests: Peer-to-peer

  Uses the animated end-to-end Kitten chat example (Streaming HTML version) to test:

    - Diffie-Hellman key exchange via Curve25519 (X25519).
    - AES encryption/decryption.
    - Localhost domain aliases for local peer-to-peer testing (place1 and place2).
    - POST route.
*/

import { Kitten } from '../regression/helpers/Kitten.js'
import { test, expect } from '@playwright/test'

class Place {
  /** @type {Kitten} */ #kitten
  /** @type {string} */ #domain
  /** @type {number} */ #port
  /** @type {import('@playwright/test').BrowserContext} */ #context
  /** @type {import('@playwright/test').Page} */ #page
  /** @type {import('@playwright/test').Locator} */ #domainBox
  /** @type {import('@playwright/test').Locator} */ #messageBox
  /** @type {import('@playwright/test').Locator} */ #sendButton

  static messageCount = 0

  /**
    Create a place instance for testing the animated end-to-end
    encrypted Kitten chat example (streaming HTML version).
    
    @param {{
      domain: string,
      port: number,
      message: string
    }} parameterObject
  */
  constructor ({ domain, port, message }) {
    this.#domain = domain
    this.#port = port

    this.message = message
  }

  /**
    Returns the full domain including the port.
  */
  get domain () {
    return `${this.#domain}:${this.#port}`
  }

  async startKitten () {
    this.#kitten = await Kitten.serverRunningAt({
      path: 'examples/streaming-html/kitten-chat/animated-end-to-end-encrypted',
      domain: this.#domain,
      port: this.#port,
      deleteData: true,
      debug: false
    })
  }

  async stopKitten () {
    await this.#kitten.stop()
  }

  /**
    Creates a new context and page on the passed browser.

    @param {{
      browser: import('@playwright/test').Browser 
    }} parameterObject
  */
  async initialise ({ browser }) {
    this.#context = await browser.newContext()
    this.#page = await this.#context.newPage()
  }

  /**
    Create identity and return the secret for a given Kitten server.
  */
  async generateIdentityAndSignIn() {
    // Generate secret.
    // @ts-ignore kitten.identityUrl might be null
    const identityUrl = /** @type {string} */ (this.#kitten.identityUrl)
    await this.#page.goto(identityUrl)
    const secretDisplay = this.#page.locator('#secret')
    await expect(secretDisplay).not.toHaveText('••••••••••••••••••••••••••••')
    const secret = await secretDisplay.textContent()

    // Sign in.
    await this.#page.goto(`https://${this.domain}/private`)
    await expect(this.#page).toHaveURL(`https://${this.domain}/💕/sign-in/`)
    const secretInput = this.#page.locator('#secret')
    const submitButton = this.#page.locator('button[type=submit]')
    // @ts-ignore If secret is null, we want this to fail.
    await secretInput.fill(secret)
    await submitButton.click()
    await this.#page.waitForLoadState()

    // Ensure we’re successfully signed in and viewing the private page.
    await expect(this.#page).toHaveURL(`https://${this.domain}/private/`)

    // Save references to the interface elements.
    this.#messageBox = this.#page.getByLabel('Message:')
    this.#domainBox = this.#page.getByLabel('To:')
    this.#sendButton = this.#page.getByRole('button')
  }

  /**
    Sends a message to the specified place and verifies that
    the interface on both peers reflects that the message
    has been sucessfully sent and received.

    @param {{
      to: Place
    }} parameterObject
  */
  async sendMessage ({ to }) {
    // Send the message.
    await this.#domainBox.fill(to.domain)
    await this.#messageBox.fill(this.message)
    await this.#sendButton.click()

    // Update the global message count.
    Place.messageCount++

    // Expect domain box to remain populated
    // and message box to be cleared.
    await expect(this.#domainBox).toHaveValue(to.domain)
    await expect(this.#messageBox).toBeEmpty()

    const message = {
      message: this.message,
      from: this,
      to
    }

    // Verify message is received both locally and on the remote peer.
    await this.verifyMessageReceived(message)
    await to.verifyMessageReceived(message)
  }

  /**
    Verifies message has been received from the specified place.

    @param {{
      message: string,
      from: Place,
      to: Place
    }} parameterObject
  */
  async verifyMessageReceived ({ message, from, to }) {
    // Expect the messages placeholder to be hidden
    // and to see the sent message displayed locally.
    const currentMessageCount = Place.messageCount + 1
    const messages = this.#page.locator('#messages > li')
    await expect(messages).toHaveCount(currentMessageCount)
    const placeholderMessage = this.#page.locator('#messages > li:first-of-type')
    await expect(placeholderMessage).toHaveId('placeholder')
    await expect(placeholderMessage).toBeHidden()
    const sentMessage = this.#page.locator(`#messages > li:nth-of-type(${currentMessageCount})`)
    await expect(sentMessage).toHaveClass('Message')
    await expect(sentMessage).toContainText(from.domain)
    await expect(sentMessage).toContainText(to.domain)
    await expect(sentMessage).toContainText(message)
  }
}

const place1 = new Place ({
  domain: 'place1.localhost',
  port: 498,
  message: 'Hello there, second place, how goes?'
})

const place2 = new Place ({
  domain: 'place2.localhost',
  port: 499,
  message: 'All right, first place, thanks for asking!'
})

test.describe('Peer to peer', () => {
  // Start Kitten instances.
  test.beforeAll(async () => {
    await place1.startKitten()
    await place2.startKitten()
  })

  // Run tests.
  test('Works as expected', async ({ browser }) => {
    // Create different browser contexts and pages for the two peers on the browser instance.
    await place1.initialise({ browser })
    await place2.initialise({ browser })
    
    // Create identity, sign in, and go to chat page on both peers.
    await place1.generateIdentityAndSignIn()
    await place2.generateIdentityAndSignIn()
    
    // Send messages and verify receipt.
    await place1.sendMessage({ to: place2 })
    await place2.sendMessage({ to: place1 })
  })

  // Stop Kitten instances.
  test.afterAll(async () => {
    await place1.stopKitten()
    await place2.stopKitten()
  })
})
