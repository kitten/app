/**
  End-to-end tests: Kitten Kawaii.

  Live at: https://kitten-kawaii.small-web.org
*/
import fs from 'node:fs/promises'
import { Kitten } from '../regression/helpers/Kitten.js'
import { test, expect } from '@playwright/test'

const kitten = new Kitten({ path: 'examples/streaming-html/kitten-kawaii' })

test.describe('Kitten Kawaii', () => {
  // Start Kitten.
  test.beforeAll(async () => {
    await kitten.start()
  })

  // Run tests.
  test('Works as expected', async ({ browser }) => {
    // Create a Playwright context that allows downloads.
    const context = await browser.newContext({ acceptDownloads: true })
    const page = await context.newPage()

    // Initial load and state-based URL forwarding.
    await page.goto('https://localhost')
    await expect (page).toHaveURL('https://localhost/character/blissful/granny-smith-apple/cat/')
    await expect (page).toHaveTitle('Kitten Kawaii character: Blissful granny smith-apple cat')
    await expect (page.locator('#detail-tabs')).not.toBeVisible()
    const characterSvg = page.getByLabel('Cat character: Cute minimalist vector illustration of granny-smith-apple coloured cat, looking blissful.')
    await expect(characterSvg).toBeVisible()
    await expect(characterSvg).toBeInViewport()
    await expect(page.locator('#the-character > path:nth-child(3)')).toHaveAttribute('fill', '#a6e191')

    // Change the colour.
    const colours = page.locator('#colour > div')
    await expect (colours).toHaveCount(6)
    const mauveButton = colours.nth(4).getByRole('radio')
    await expect(mauveButton).toHaveCSS('background-color', 'rgb(215, 186, 255)')
    await mauveButton.click()
    await expect (page).toHaveURL('https://localhost/character/blissful/mauve/cat/')
    expect (page.getByLabel('Cat character: Cute minimalist vector illustration of mauve coloured cat, looking blissful.')).toBeDefined()
    await expect(page.locator('#the-character > path:nth-child(3)')).toHaveAttribute('fill', '#d7baff')

    // Change the mood.
    await expect(page.locator('#mood-labels > span')).toHaveCount(7)
    const mood = page.locator('#mood')
    await mood.fill('5')
    expect(page.getByLabel('Cat character: Cute minimalist vector illustration of mauve coloured cat, looking excited.')).toBeDefined()
    
    // Change the character.
    const characterSelector = page.getByLabel(/^Character$/)
    await expect (characterSelector.locator('option'), 'should have 16 characters').toHaveCount(16)
    await characterSelector.selectOption('ghost')
    await expect (page).toHaveURL('https://localhost/character/excited/mauve/ghost/')
    expect (page.getByLabel('Cat character: Cute minimalist vector illustration of mauve coloured ghost, looking excited.')).toBeDefined()

    // Test downloads.
    // FIXME: Change this ID to kebab-case for consistency.
    const [ download ] = await Promise.all([
      page.waitForEvent('download'),
      page.locator('#downloadLink').click()
    ])
    expect(download.suggestedFilename()).toBe('excited-mauve-ghost.svg')
    const downloadedSvg = await fs.readFile(await download.path(), 'utf-8')
    expect(downloadedSvg.startsWith(`<svg role="img" aria-label="Ghost character: Cute minimalist vector illustration of mauve coloured ghost, looking excited." xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 240 240" fill="none"><title>Ghost character</title><desc>Cute minimalist vector illustration of mauve coloured ghost, looking excited.</desc>`)).toBe(true)

    // Test details view.
    const detailsDrawer = page.locator('#details-drawer')
    const toggleDetailsButton = page.locator('#toggle-details')
    await expect(toggleDetailsButton).toHaveAttribute('aria-expanded', 'false')
    await expect(toggleDetailsButton).toHaveText('Show details')
    await expect(detailsDrawer, 'not to be showing').toHaveCSS('grid-template-rows', '0px')

    await toggleDetailsButton.click()

    await expect(toggleDetailsButton).toHaveAttribute('aria-expanded', 'true')
    await expect(toggleDetailsButton).toHaveText('Hide details')
    await expect(detailsDrawer, 'to be showing').not.toHaveCSS('grid-template-rows', '0px')

    await expect(page.locator('#code-tab')).toHaveAttribute('aria-selected', 'true')
    const runItLocallyTab = page.locator('#run-it-locally-tab')
    await expect(runItLocallyTab).toHaveAttribute('aria-selected', 'false')
    await runItLocallyTab.click()
    await expect(runItLocallyTab).toHaveAttribute('aria-selected', 'true')
  })

  // Stop Kitten.
  test.afterAll(async () => {
    await kitten.stop()
  })
})
