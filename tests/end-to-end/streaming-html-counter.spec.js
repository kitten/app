/**
  End-to-end tests: Streaming HTML counter.

  Tests:

    - Default web socket route creation.
    - Streaming HTML routing.
    - Automatic HTMX inclusion.
*/

import { Kitten } from '../regression/helpers/Kitten.js'
import { test, expect } from '@playwright/test'

let kitten

test.describe('Streaming HTML counter', () => {
  // Start Kitten.
  test.beforeAll(async () => {
    kitten = await Kitten.serverRunningAt({
      path: 'examples/streaming-html/counter',
      deleteData: true,
      debug: false
    })
  })

  // Run tests.
  test('Works as expected', async ({ page }) => {
    // Expect a title "to contain" a substring.
    await page.goto('https://localhost/')
    await expect(page).toHaveTitle(/Counter/)
    const counter = page.locator('#counter')
    await expect(counter).toHaveText('0')
    const minusButton = page.getByText('-')
    const plusButton = page.getByText('+')
    await plusButton.click()
    await expect(counter).toHaveText('1')
    await plusButton.click()
    await expect(counter).toHaveText('2')
    await minusButton.click()
    await expect(counter).toHaveText('1')
    await minusButton.click()
    await minusButton.click()
    await expect(counter).toHaveText('-1')
    await page.reload()
    await expect(counter, 'to persist').toHaveText('-1')
  })

  // Stop Kitten.
  test.afterAll(async () => {
    await kitten.stop()
  })
})
