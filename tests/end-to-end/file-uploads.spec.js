/**
  End-to-end tests: File Uploads.

  Tests:

    - File upload
    - File download
*/
import { text } from 'node:stream/consumers'
import { Readable } from 'node:stream'

import { Kitten } from '../regression/helpers/Kitten.js'
import { test, expect } from '@playwright/test'

// FIXME: Add TypeScript definitions to this module.
import fs from 'node:fs'
import path from 'node:path'
import os from 'node:os'
import syswideCas from '@small-tech/syswide-cas'


// Add Kitten’s local certificate authority (created by Auto Encrypt Localhost)
// to Node so that fetch doesn’t balk. We do this here so that it is only done once,
// the first time this module is loaded.
syswideCas.addCAs(path.join(os.homedir(), '.local/share/small-tech.org/kitten/tls/local/auto-encrypt-localhost-CA.pem'))

let kitten

test.describe('File uploads/downloads', () => {
  // Start Kitten.
  test.beforeAll(async () => {
    kitten = await Kitten.serverRunningAt({
      path: 'examples/file-uploads',
      deleteData: true,
      debug: false
    })
  })

  // Run tests.
  test('Works as expected', async ({ page }) => {
    // Expect a title "to contain" a substring.
    await page.goto('https://localhost/')
    await expect(page).toHaveTitle(/Image gallery/)

    // Ensure gallery is empty to begin with.
    const images = page.locator('li')
    expect(await images.count()).toBe(0)

    //
    // Test uploads.
    // 
    
    const imagesToUpload = [
      'happy-light-pink-ice-cream.svg',
      'lovestruck-mauve-cat.svg'
    ]

    const uploadPathRegExp = /^\/uploads\/.*?\//
    
    const fileUploadInput = page.locator('#image')
    const altTextInput = page.locator('#alt-text')
    const uploadButton = page.getByRole('button')

    const filePathFromFileName = fileName => `./tests/fixtures/${fileName}`
    const altTextFromFileName = fileName => fileName.replace('_', '').replace('.svg', '')

    const uploadImageAndVerifyUpload = async (fileName, index) => {
      // Set image to upload.
      const filePath = filePathFromFileName(fileName)
      await fileUploadInput.click()
      await fileUploadInput.setInputFiles(filePath)

      // Add alt text.
      const altText = altTextFromFileName(fileName)
      await altTextInput.fill(altText)

      // Press upload button.
      await uploadButton.click()

      // Ensure image appears in gallery.
      expect(await images.count()).toBe(index + 1)
      const imageInGallery = page.locator(`li:nth-of-type(${index + 1}) > img`)
      await expect(imageInGallery).toHaveAttribute('src', uploadPathRegExp)
      await expect(imageInGallery).toHaveAttribute('alt', altText)

      // Download image and ensure it matches one we uploaded.
      const downloadUrl = `https://localhost${await imageInGallery.getAttribute('src')}`
      const download = await fetch(downloadUrl)
      // @ts-ignore Node.js TypeScript types are not maintained by the Node.js project. It's done by the community in https://github.com/DefinitelyTyped/DefinitelyTyped issue.
      // https://github.com/nodejs/node/issues/53119
      const stream = Readable.fromWeb(download.body)
      const downloadedSvg = await text(stream)

      const originalSvg = fs.readFileSync(filePath, 'utf-8')
      expect(downloadedSvg === originalSvg).toBe(true)
    }

    // Upload all images and verify the uploads.
    let index = 0
    for await (const image of imagesToUpload) {
      await uploadImageAndVerifyUpload(image, index++)
    }
  })

  // Stop Kitten.
  test.afterAll(async () => {
    await kitten.stop()
  })
})
