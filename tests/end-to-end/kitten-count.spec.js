/**
  End-to-end tests: Kitten counter.

  Tests:

    - Initial render
*/
import { Kitten } from '../regression/helpers/Kitten.js'
import { test, expect } from '@playwright/test'

let kitten

test.describe('Kitten count', () => {
  // Start Kitten.
  test.beforeAll(async () => {
    kitten = await Kitten.serverRunningAt({
      path: 'examples/kitten-count',
      debug: false
    })
  })

  // Run tests.
  test('Works as expected', async ({ page }) => {
    // Expect a title "to contain" a substring.
    await page.goto('https://localhost/')
    await expect(page).toHaveTitle(/Kitten count/)
    await expect(page.getByRole('paragraph').first()).toHaveText('🐱️')
    await page.reload({ waitUntil: 'domcontentloaded' })
    await expect(page.getByRole('paragraph').first()).toHaveText('🐱️🐱️')
  })

  // Stop Kitten.
  test.afterAll(async () => {
    await kitten.stop()
  })
})
