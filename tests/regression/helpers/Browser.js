/**
  Basic browser emulation using JSDOM
  for testing via tape.
*/
import os from 'node:os'
import path from 'node:path'
import {JSDOM as DOM} from 'jsdom'

// FIXME: Add TypeScript definitions to this module.
import syswideCas from '@small-tech/syswide-cas'

// Add Kitten’s local certificate authority (created by Auto Encrypt Localhost)
// to Node so that fetch doesn’t balk. We do this here so that it is only done once,
// the first time this module is loaded.
syswideCas.addCAs(path.join(os.homedir(), '.local/share/small-tech.org/kitten/tls/local/auto-encrypt-localhost-CA.pem'))

/**
  Returns DOM for url, failing
  test t if there are any errors.

  @param {import('tape').Test} t 
  @param {string} url
*/
export async function getDom (t, url) {
  let response
  try {
    response = await fetch('https://localhost/')
  } catch (error) {
    t.fail('Fetch failed')
  }

  // @ts-ignore
  t.ok(response.ok, 'Response should succeed')

  // @ts-ignore
  const html = await response.text()
  return new DOM(html)
}

/**
  Returns HTML document for url, failing
  test t if there are any errors.

  @param {import('tape').Test} t 
  @param {string} url
*/
export async function getDocument (t, url) {
  const dom = await getDom(t, url)
  return dom.window.document
}

