/**
  Manages an external Kitten process for testing.
*/

import os from 'node:os'
import fs from 'node:fs/promises'
import vm from 'node:vm'
import path from 'node:path'
import { readFileSync } from 'node:fs'
import { SubProcess } from 'teen_process'

// So we can create Session instances when opening the sessions database. 
import { Session } from '../../../src/Sessions.js' 

// FIXME: Add TypeScript definitions to this module.
// @ts-ignore lack of TypeScript definitions in external module
import kill from 'tree-kill-promise'

import nodePath from 'node:path'

const ALL_FORWARD_SLASHES_REGEXP = /\//g
const IDENTITY_URL_REGEXP = /https:\/\/.*\/💕\/hello\/([a-f0-9]+?)\//

export class Kitten {
  #absolutePath
  #relativePath
  #port
  #domain
  #deleteData
  #debug
  stdout = ''
  stderr = ''
  
  /**
    Factory method (async): Creates Kitten instance and starts it.
    
    @param {{
      path: string,
      port?: number,
      domain?: string,
      deleteData?: boolean,
      debug?: boolean
    }} parameterObject -

    - `path`: relative path from Kitten app source folder (where tests are being run) to Kitten project being tested
    - `deleteData`: Whether or not to delete all data (the database folder) for the app before starting the server for the first time.
    - `debug`: Whether or not to log Kitten process output to console (default: false)
  */
  static async serverRunningAt (parameterObject) {
    const instance = new this(parameterObject)
    await instance.start()
    return instance
  }

  /**
    Configures Kitten instance.
    
    @param {{
      path: string,
      port?: number,
      domain?: string,
      deleteData?: boolean,
      debug?: boolean
    }} parameterObject -

    - `path`: relative path from Kitten app source folder (where tests are being run) to Kitten project being tested
    - `deleteData`: Whether or not to delete all data (the database folder) for the app before starting the server for the first time.
     `debug`: Whether or not to log Kitten process output to console (default: false)
  */
  constructor ({ path, port = 443, domain = 'localhost', deleteData = false, debug = false }) {
    this.#relativePath = path
    this.#absolutePath = nodePath.join(process.cwd(), path)
    this.#port = port
    this.#domain = domain
    this.#deleteData = deleteData
    this.#debug = debug

    if (this.#debug) {
      console.info(`🐱 New kitten (${path})`)
    }
  }

  /**
    The database folder path for this app.
  */
  get databaseFolderPath () {
    return `${os.homedir()}/.local/share/small-tech.org/kitten/data/${this.osPathToDatabaseName(process.cwd().slice(1))}.${this.osPathToDatabaseName(this.#relativePath)}.${this.#domain}.${this.#port}`
  }

  /**
    Parses and returns the current contents of a database table.

    Essentially does what the JSTable.load() method does in JSDB.
    If we export the JSTable class separately in a future JSDB version
    we can likely replace this with that.

    @param {string} databaseName - Name of database (folder).
    @param {string} tableName - Name of table (file name sans extension).
    @param {Array} [classes] - (Optional) Array of class references to be used to deserialise instances in the table.
  */
  databaseTable(databaseName, tableName, classes) {
    const tablePath = path.join(this.databaseFolderPath, databaseName, `${tableName}.js`)
    // Create the context (scope) for the data and add any
    // custom classes that have been passed to it so we can
    // deserialise them propery in the virtual machine.
    const contextObject = {}
    if (classes !== undefined) {
      // The classes are passed as an array for authoring convenience but
      // the they need to be added as properties to the VM’s context object.
      classes.forEach(klass => contextObject[klass.name] = klass)
    }
    const context = vm.createContext(contextObject)
    let table = readFileSync(tablePath, 'utf-8')
    // Create a local constant to hold the root data structure based on the
    // one exported in the first line of the table and populate it with
    // the initial/compacted data provided for it on that line.
    const matchLastNewline = /\n$/
    const indexOfLastNewlineOnFirstLine = matchLastNewline.exec(table)?.index
    if (indexOfLastNewlineOnFirstLine === undefined) throw new Error('Syntax error while loading database table')
    const initialData = table.substr(0, indexOfLastNewlineOnFirstLine).replace('export const ', '')

    // Remove the first line.
    table = table.substr(indexOfLastNewlineOnFirstLine)

    // Evaluate the data in a virtual machine.
    const initialScript = new vm.Script(initialData)
    initialScript.runInContext(context)

    const tableScript = new vm.Script(table)
    tableScript.runInContext(context)

    // Return the data (held in the _ variable in the JSTable)
    return context._
  }

  /**
    The current sessions for this app.
  */
  sessions () {
    return this.databaseTable('_db', 'sessions', [Session])
  }

  /**
    Replaces forward slashses with dots in passed path to return a
    correctly formatted chunk of the database name.

    @param {string} path
  */
  osPathToDatabaseName (path) {
    // @ts-ignore New ECMAScript feature
    // FIXME: Add correct TypeScript version config to project.
    return path.replaceAll(ALL_FORWARD_SLASHES_REGEXP, '.')
  }

  /**
    Deletes all data (the database folder) for the current app.
  */
  async deleteData() {
    if (this.#debug) {
      console.info(`🐱 Deleting data (${this.databaseFolderPath})`)
    }
    await fs.rm(this.databaseFolderPath, { recursive: true, force: true })
    if (this.#debug) {
      console.info(`🐱 Deleted data (${this.databaseFolderPath})`)
    }
  }

  /**
    Start the Kitten instance.
  */
  async start () {
    // If we’ve been asked to delete app data, do so before
    // we start the Kitten process (Kitten reads the databases as launch).
    if (this.#deleteData) {
      await this.deleteData()
    }

    // Start the process.
    const commandlineArguments = [this.#absolutePath]
    if (this.#port !== 443) {
      commandlineArguments.push(`--port=${this.#port}`)
    }
    if (this.#domain !== 'localhost') {
      commandlineArguments.push(`--domain=${this.#domain}`)
    }
    this.process = new SubProcess('kitten', commandlineArguments)

    // Pipe process output to console if asked for debug assistance.
    if (this.#debug) {
      this.process.on('stream-line',console.log.bind(console))
      console.info('Starting Kitten')
    }

    await this.process.start((stdout, stderr) => {
      this.stdout += stdout
      this.stderr += stderr
      return stdout.indexOf('Server is running and listening for connections…') !== -1
    })

    if (this.#debug) {
      console.info('Kitten started')
    }
  }

  /**
    Returns the identity URL (the page you go to to generate your identity if you
    haven’t already done so) or null if it has not been output during server start
    (which would mean that your identity has already been generated fro this
    Small Web place).
  */
  get identityUrl () {
    if (this.stdout == undefined) return null
    const match = this.stdout.match(IDENTITY_URL_REGEXP)
    return match === null ? match : match[0]
  }

  /**
    Stop the Kitten process.
  */
  async stop () {
    if (this.#debug) {
      console.info(`🐱 Stopping kitten (${this.#absolutePath})`)
    }

    if (this.process !== undefined) {
      await kill(this.process.pid)
    } else {
      throw new Error(`Kitten Process stop called but process does not exist. Are you sure you called start() first? Project path: ${this.#absolutePath}`)
    }

    if (this.#debug) {
      console.info(`🐱 Kitten stopped (${this.#absolutePath})`)
    }
  }

  /**
    Restart the Kitten process.

    Note that if you want a restart to delete data, you must specify it
    explicitly, regardless of whether or not `deleteData` was true when
    you originally created the server (e.g., via the factory method).
  */
  async restart ({ deleteData = false } = {}) {
    // Overwrite the original `deleteData` flag,
    // defaulting to false as we usually want to
    // test data persistence (default Kitten functionality)
    // between restarts.
    if (this.#debug) {
      console.info(`🐱 Kitten restarting (delete data? ${deleteData}, ${this.#absolutePath})`)
    }
    this.#deleteData = deleteData
    await this.stop()
    await this.start()
    if (this.#debug) {
      console.info(`🐱 Kitten restarted (delete data? ${this.#deleteData}, ${this.#absolutePath})`)
    }
  }
}

export default Kitten
