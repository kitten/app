import { Kitten } from './Kitten.js'

/**
  Generic base class to represent a Kitten place
  (an instance of the Kitten server) along with its
  own Playwright browser context and page for testing with.
*/
export class Place {
  /** @type {Kitten} */ #kitten
  /** @type {string} */ #path
  /** @type {string} */ #domain
  /** @type {number} */ #port
  /** @type {boolean} */ #deleteData
  /** @type {boolean} */ #debug
  /** @type {import('@playwright/test').BrowserContext} */ #context
  /** @type {import('@playwright/test').Page} */ #page

  /**
    Create a place instance for testing the animated end-to-end
    encrypted Kitten chat example (streaming HTML version).
    
    @param {{
      path: string,
      domain?: string,
      port?: number,
      deleteData?: boolean,
      debug?: boolean,
    }} parameterObject
  */
  constructor ({ path, domain = 'localhost', port = 443, deleteData = false, debug = false }) {
    this.#path = path
    this.#domain = domain
    this.#port = port
    this.#deleteData = deleteData
    this.#debug = debug
  }

  /**
    Returns the full domain including the port.
  */
  get domain () {
    return `${this.#domain}:${this.#port}`
  }

  /**
    Returns the Playwright context (throws if it hasn’t been created yet).
  */
  get context () {
    if (this.#context === undefined) {
      throw new Error('Playwright context does not exist, please call initialise() first.')
    }
    return this.#context
  }

  /**
    Returns the Playwright page (throws if it hasn’t been created yet).
  */
  get page () {
    if (this.#page === undefined) {
      throw new Error('Playwright page does not exist, please call initialise() first.')
    }
    return this.#page
  }

  async startKitten () {
    this.#kitten = await Kitten.serverRunningAt({
      path: this.#path,
      domain: this.#domain,
      port: this.#port,
      deleteData: this.#deleteData,
      debug: this.#debug
    })
  }

  async stopKitten () {
    await this.#kitten.stop()
  }

  /**
    Creates a new context and page on the passed browser.

    @param {{
      browser: import('@playwright/test').Browser 
    }} parameterObject
  */
  async initialise ({ browser }) {
    this.#context = await browser.newContext()
    this.#page = await this.#context.newPage()
  }
}

export default Place
