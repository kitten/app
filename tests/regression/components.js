import test from 'tape'
import { getDocument } from './helpers/Browser.js'
import { Kitten } from './helpers/Kitten.js'

test ('Components example', async t => {
  // Start Kitten server.
  const kitten = await Kitten.serverRunningAt({ path: 'examples/components' })

  const document = await getDocument(t, 'https://localhost/')

  // Title.
  t.equal(document.title, 'Components, etc.', 'Title is automatically set from H1')

  // Note: For Markdown, we don’t test the core markdown-it
  // functionality but we do test plugins.
  
  // Markdown: generated table of contents.
  const tableOfContents = document.getElementsByTagName('nav')[0]
  t.equal(tableOfContents?.className, 'table-of-contents', 'Table of contents has correct class')
  t.equal(tableOfContents?.children.length, 1, 'Table of contents has one second-level heading')
  const ol = tableOfContents?.children[0]
  t.equal(ol?.nodeName, 'OL', 'Second-level headings is an ordered list')
  t.equal(ol?.children.length, 1, 'Third-level headings are listed')
  const li = ol.children[0]
  t.equal(li?.children.length, 2, 'There are two children')
  t.equal(li?.children[0].nodeName, 'A', 'First child is a link')
  t.equal(li?.children[0].getAttribute('href'), '#markdown', 'H2 link is correct')
  const nestedOl = li?.children[1]
  t.equal(nestedOl.nodeName, 'OL', 'Second child is the third-level heading ordered list')
  t.equal(nestedOl.children.length, 7, 'There are seven third-level headings linked in the TOC')
  
  // HTML code block tabindex.
  t.equal(document.getElementsByTagName('code')[0]?.getAttribute('tabindex'), '0', 'tabindex=0 automatically added to first HTML code block')

  t.isNot(document.querySelector('pre > code.hljs.language-js'), null, 'JS code block with Highlight.js syntax highlighting is created as expected')

  // Markdown: Automatically-generated anchor slugs.
  const h3 = document.getElementById('make-sure-math-doesn-t-mess-it-up')
  t.equal(h3?.nodeName, 'H3', 'Node is H3 as expected')
  t.equal(h3?.innerHTML, 'Make sure math doesn’t mess it up.', 'Heading slugs are properly rendered')

  // Markdown: Footnotes.

  // Footnote references.
  const footnoteRefs = document.getElementsByClassName('footnote-ref')
  t.equal(footnoteRefs.length, 2, 'There are two footnote references')

  // First footnote reference.
  const footnoteRef = footnoteRefs[0]
  const linkInFootnote = footnoteRef?.children[0]
  t.equal(linkInFootnote?.nodeName, 'A', 'Link in footnote exists')
  t.equal(linkInFootnote?.getAttribute('href'), '#fn1', 'Footnote reference link is correct')
  t.equal(linkInFootnote?.id, 'fnref1', 'Footnote reference id is correct')
  t.equal(linkInFootnote?.innerHTML, '[1]', 'Footnote reference text is correct')

  // Footnotes section.
  const footnoteSeparators = document.getElementsByClassName('footnotes-sep')
  t.equal(footnoteSeparators.length, 1, 'There is one footnote separator')
  t.equal(footnoteSeparators[0]?.nodeName, 'HR', 'Footnote separator is an <hr>')

  // Footnote bodies.
  const footnotesSection = document.querySelector('section.footnotes')
  t.equal(footnotesSection?.children.length, 1, 'Footnotes section has one child node.')
  const footnotesOl = footnotesSection?.children[0]
  t.equal(footnotesOl?.nodeName, 'OL', 'Footnotes list is an <ol>')
  t.equal(footnotesOl?.children.length, 2, 'There are two footnote bodies.')

  t.equal(footnotesOl?.children[1]?.textContent, 'This is also a footnote. ↩︎', 'Second footnote’s body is as expected')
  const secondFootnoteLinkback = document.querySelector('a[href="#fnref2"].footnote-backref')
  t.equal(secondFootnoteLinkback?.textContent, '↩︎', 'Second footnote’s linkback link is as expected')
  
  // Component in markdown.

  const components = document.getElementsByClassName('component')
  t.equal(components.length, 8, 'There are eight components')
  t.ok(components[0]?.classList.contains('Addition'))
  t.ok(components[1]?.classList.contains('SubSub'))
  t.equal(components[0].children[1], components[1], 'First sub-sub component is correctly nested in first Addition component')

  // Other components.
  t.ok(components[2]?.classList.contains('Addition'))
  t.equal(components[2].children?.item(2)?.innerHTML, '1 + 2 = 3')
  t.ok(components[3]?.classList.contains('SubSub'))
  t.ok(components[4]?.classList.contains('Addition'))
  t.equal(components[4].children?.item(2)?.innerHTML, '2 + 2 = 4')
  t.ok(components[5]?.classList.contains('SubSub'))
  t.ok(components[6]?.classList.contains('Subtraction'))
  const randomSubtraction = components[6].children?.item(2)?.innerHTML;
  let matches
  if (randomSubtraction !== undefined) {
     matches = randomSubtraction.match(/^(\d*?) - (\d*?) = ([-]?\d*?)$/)
     if (matches !== null) {
       t.equal(Number(matches[1]) - Number(matches[2]), Number(matches[3]), 'Random subtraction is correct')
      } else {
        t.fail('Matches failed')
      }
  } else {
    t.fail('Subtraction component has unexpected contents')
  }
  t.ok(components[7]?.classList.contains('SubSub'))

  // Subscript, superscript, etc.
  const innerHtml = document.body.innerHTML
  t.ok(innerHtml.includes('<sub>2</sub>'), 'Subscript should exist')
  t.ok(innerHtml.includes('<sup>th</sup>'), 'Superscript should exist')
  t.ok(innerHtml.includes('™'), 'Trademark symbol should exist')
  t.ok(innerHtml.includes('©'), 'Copyright symbol should exist')
  t.ok(innerHtml.includes('<mark>marked text</mark>'), 'Marked text is rendered correctly')
  t.ok(innerHtml.includes('<ins>inserted text</ins>'), 'Inserted text is rendered correctly')
  t.ok(innerHtml.includes('<s>strike-through</s>'), 'Strikethrough text is rendered correctly')
  t.ok(innerHtml.includes('I do believe 4 &gt; 2 and is 2 &lt; 4? And what about &lt;&lt; and &gt;&gt; and &lt;&lt;&lt; and &gt;&gt;&gt;?'), 'Make sure math doesn’t mess it up (greater than/lesser than)')

  // Figures
  const figures = document.getElementsByTagName('figure')
  t.equal(figures.length, 1, 'There is one figure')
  const figure = figures.item(0)
  t.ok(figure, 'Figure exists')
  t.equal(figure?.children.length, 2, 'Figure has two children')
  const figureImg = figure?.children[0]
  const figureCaption = figure?.children[1]
  t.equal(figureImg?.getAttribute('src'), 'https://ar.al/images/aral-432.jpg', 'Figure img src is correct')
  t.equal(figureImg?.getAttribute('alt'), 'Black and white close-up of Aral, a white-passing man with short hair and stubble, looking off camera with a blurred background')
  t.equal(figureCaption?.innerHTML, 'Hey, look, it’s a-me, Mario! Umm… Aral.')
  
  // Stop Kitten server.
  await kitten.stop()
})
