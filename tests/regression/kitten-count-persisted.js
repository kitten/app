import test from 'tape'
import { getDocument } from './helpers/Browser.js'
import { Kitten } from './helpers/Kitten.js'

async function testNthDocumentLoad (t, n) {
  const document = await getDocument(t, 'https://localhost')
  t.equal(document.title, 'Kitten count', 'Title is set correctly from the H1')

  const paragraphs = document.getElementsByTagName('p')
  t.equal(paragraphs.length, 1, 'There is one paragraph tag')
  t.equal(paragraphs[0]?.innerHTML, '🐱️'.repeat(n), 'Expected number of kittens found')
}

test('kitten count (persisted)', async t => {
  const path = 'examples/persisted-kitten-count'

  // Start Kitten.
  let kitten = await Kitten.serverRunningAt({
    path,
    deleteData: true,
    debug: false
  })

  // Load the document twice and test that the general
  // structure and kitten count are as we expect them to be.
  await testNthDocumentLoad(t, 1)
  await testNthDocumentLoad(t, 2)

  // Restart Kitten.
  await kitten.restart()

  // Load the document twice more and test that the count
  // has persisted as expected.
  // 
  await testNthDocumentLoad(t, 3)
  await testNthDocumentLoad(t, 4)

  // Stop Kitten.
  await kitten.stop()

  t.end()
})
