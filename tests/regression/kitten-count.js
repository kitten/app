import test from 'tape'
import { getDocument } from './helpers/Browser.js'
import { Kitten } from './helpers/Kitten.js'

async function testNthDocumentLoad (t, n) {
  const document = await getDocument(t, 'https://localhost')
  t.equal(document.title, 'Kitten count', 'Title is set correctly from the H1')

  const paragraphs = document.getElementsByTagName('p')
  t.equal(paragraphs.length, 1, 'There is one paragraph tag')
  t.equal(paragraphs[0]?.innerHTML, '🐱️'.repeat(n), 'Expected number of kittens found')
}

function testFunction (path) {
  return async t => {
    // Start Kitten.
    const kitten = await Kitten.serverRunningAt({
      path,
      debug: false
    })

    // Load the document twice and test that the general
    // structure and kitten count are as we expect them to be.
    await testNthDocumentLoad(t, 1)
    await testNthDocumentLoad(t, 2)

    // Stop Kitten.
    await kitten.stop()
    t.end()
  }
}

test('kitten count', testFunction('examples/kitten-count'))
test('kitten count (markdown)', testFunction('examples/kitten-count-markdown'))
test('kitten count (typed)', testFunction('examples/kitten-count-typed'))
