// Updates Kitten version file.
import fs from 'node:fs'
import packageJson from './package.json' with { type: 'json' }
import { simpleGit } from 'simple-git'

/** @constant {SimpleGit} */
const git = simpleGit()

/** @constant {Array} Git log. */
const log = await git.log({maxCount: 1})

/** @constant {String} Kitten version. */
const gitHash = log.latest.hash.substring(0, 6)

const date = new Date()
const apiVersion = packageJson.version.split('.')[0]
const nodeVersion = packageJson.engines.node
const versionStamp = Number(`${
    date.getUTCFullYear()
  }${
    // getUTCMonth() returns a 0-based result; hence the increment.
    String(date.getUTCMonth() + 1).padStart(2, '0')
  }${
    String(date.getUTCDate()).padStart(2, '0')
  }${
    String(date.getUTCHours()).padStart(2, '0')
  }${
    String(date.getUTCMinutes()).padStart(2, '0')
  }${
    String(date.getUTCSeconds()).padStart(2, '0')
  }`)

const versionJson = `{
  "date": "${date.toISOString()}",
  "versionStamp": ${versionStamp},
  "gitHash": "${gitHash}",
  "apiVersion": ${apiVersion},
  "nodeVersion": "${nodeVersion}"
}`

fs.writeFileSync('dist/version.json', versionJson, 'utf-8')
