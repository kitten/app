
## Credits

Kitten is authored by [Aral Balkan](https://ar.al) with the contributions of the people featured below as well as the [377 people, organisations, and teams](#package-contributors) that helped create the [528 packages](#packages) that make Kitten possible.

### Special thanks to

This list is loosely based on the [All Contributors](https://allcontributors.org/) idea and curated by hand to say a personal thank-you to the people who have contributed to Kitten in substantial ways. 🐱💕

<table>
  <tr>
  <th></th><th>Name</th><th><a href='#contribution-key' title='Contribution emoji (jump to key)'>🔑</a></th><th>Details</th>
  </tr>
  <tr>
    <td><img class='avatar' src='https://codeberg.org/avatars/7c559f0dfaa0c30e04b9f7ccda5ec888' alt='Aral Balkan'></td>
    <td><a href='https://codeberg.org/aral'>Aral Balkan</a></td>
    <td>😻</td>
    <td><p> Chief meow.</p></td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://avatars.githubusercontent.com/u/300067?v=4' alt='A charcoal drawn triangular spiral with alternating white and black rings on a black background'>
    </td>
    <td><a href='https://github.com/dy'>Dmitry Ivanov</a></td>
    <td class='cemoji'><a href='https://github.com/dy/xhtm' title='Code'>💻</a></td>
    <td>
      <p><a href='https://codeberg.org/kitten/app/src/branch/main/src/lib/html.js'>Kitten’s template renderer</a> inlines and extends Dmitry’s <a href='https://github.com/dy/xhtm'>xhtm</a> library.</p>
      <p>Dmitry worked with me (Aral) over several days through <a href='https://github.com/dy/xhtm/issues?q=is%3Aissue+author%3Aaral'>more than a dozen issues</a> to improve xhtm and thus also Kitten’s HTML rendering. His help has been invaluable in ensuring one of Kitten’s core components works reliably.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://avatars.githubusercontent.com/u/105127?v=4' alt='A man with a beard looking at the camera, in front of an electronic drum set.'>
    </td>
    <td><a href='https://github.com/developit'>Jason Miller</a></td>
    <td class='cemoji'><a href='https://github.com/developit/htm' title='Code'>💻</a> </td>
    <td>
      <p>Jason’s <a href='https://github.com/developit/htm'>htm</a> is what Dmitry’s xhtm is based on and Jason’s <a href='https://github.com/developit/vhtml'>vhtml</a>, like xhtm, is now inlined into Kitten’s template renderer and extended with functionality like Kitten’s Markdown support.</p>
      <p>For eight months or so of its development, Kitten was using htm and vhtml verbatim as dependencies and could not have gotten to where it is today without them.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://avatars.githubusercontent.com/u/20725046?v=4' alt='Extreme close-up of an illustration of a yellow lion-like creature'>
    </td>
    <td><a href='https://github.com/Renerick'>Denis Palashevskii</a></td>
    <td class='cemoji'><a href='https://github.com/bigskysoftware/htmx/pull/2418/files' title='Code'>💻</a> </td>
    <td>
      <p>Denis was hugely helpful in tracking down and fixing <a href='https://discord.com/channels/725789699527933952/1046573806547910677/1219379104013488239'>a regression in HTMX 2 that was affecting the WebSocket extension</a> that Kitten uses for its Streaming HTML workflow, thereby helping us adopt HTMX 2 while still in pre-release.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://avatars.githubusercontent.com/u/65089?v=4' alt='A man with dark brown/black hair and glasses looking at the mirror while taking a selfie.'>
    </td>
    <td><a href='https://github.com/jeffturcotte'>Jeff Turcotte</a></td>
    <td class='cemoji'><a href='https://github.com/bigskysoftware/htmx/pull/2367' title='Code'>💻</a> </td>
    <td>
      <p>Jeff contributed <a href='https://github.com/bigskysoftware/htmx/pull/2367'>the Swap API normalisation code</a> to the HTMX 2 codebase, which we’re using in our fork of HTMX 2 in Kitten. This enables us to use <code>settle</code>, <code>swap</code> etc., attributes in out-of-band swaps (as used in Kitten’s Streaming HTML workflow). Jeff’s work, along with Denis’s, allowed us to adopt HTMX 2 in Kitten while it is still in pre-release.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://codeberg.org/avatars/4cf065a928bc23cca8222e3e3362932c' alt='A pencil drawing of a bee'>
    </td>
    <td><a href='https://codeberg.org/nonetoohappy'>Andrew Chou</a></td>
    <td class='cemoji'><a href='https://codeberg.org/kitten/app/issues/81' title='Ideas'>🤔</a></td>
    <td>
      <p>Thanks to Andrew’s idea, Kitten files have compound extensions (so what used to be <em>.page</em> is now <em>.page.js</em>, etc.)</p>
      <p>This means Kitten no longer needs any specialised tooling and Kitten files automatically get syntax highlighted in source code repositories. We were also able to remove the ES Module Loader, simplifying Kitten further.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://codeberg.org/avatars/b9e7feda9abb69d709c8c5459d9c24cf' alt='Close-up of a white man with brown hair wearing glasses standing in front of trees with a white building in the background.'>
    </td>
    <td><a href='https://codeberg.org/sexybiggetje'>Martijn de boer</a></td>
    <td class='cemoji'><a href='https://codeberg.org/kitten/app/issues/60' title='Bug report'>🐛</a></td>
    <td><p> Martijn’s bug report led to the implementation of cross-platform support for Kitten.</p></td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://codeberg.org/avatars/f12b79db02a257cf6bbd48e22e49c05d' alt='Generic avatar: A white square with geometric patterns in green inside it'>
    </td>
    <td><a href='https://codeberg.org/mabu'>mabu</a></td>
    <td class='cemoji'><a href='https://codeberg.org/kitten/app/issues/56' title='Bug report'>🐛</a></td>
    <td><p>mabu’s bug report led to an eventual rewrite of <a href='https://codeberg.org/small-tech/auto-encrypt-localhost'>Auto Encrypt Localhost</a> in 100% JavaScript.</p></td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://codeberg.org/avatars/15694406ba7ebd0fee1596fc8ed208ce' alt='Generic avatar: A purple square with pink squares and rectangles in it'>
    </td>
    <td class='cemoji'><a href='https://codeberg.org/sorenmat'>sorenmat</a></td>
    <td><a href='https://codeberg.org/kitten/app/commits/branch/main/search?q=author%3Asorenmat' title='Documentation'>📖</a></td>
    <td><p>Kitten’s first pull request 😽</p></td>
  </tr>
</table>

### Contribution Key

♿️ accessibility | 💬 answering questions | 🐛 bug reports | 💻 code | 📖 documentation | 💡 examples | 📋 event organisation | 🔍 finding funding | 🤔 ideas | 🚇 infrastructure (hosting, etc.) | 🚧 maintenance | 🧑‍🏫mentoring | 📣 promotion |  👀 review | 🛡️ security | 🌍 translation | ⚠️ testing | ✅ tutorials | 📢 talks | 📹 videos 



### Package contributors

- [Adam Baldwin](https://evilpacket.net)
- Adam Kirkton – 2 packages
- Alan Plum
- [Alberto Schiabel](https://github.com/jkomyno)
- [Aleksey V Zapparov](https://www.ixti.net/)
- Alex Bell
- Alex Indigo
- Alexander Savin
- Alexander Shtuchkin
- Alexander Solovyov – 2 packages
- Alexei – 2 packages
- Alexey – 2 packages
- Alexey Raspopov
- [Alvis HT Tang](https://github.com/alvis)
- [Amila Welihinda](https://amilajack.com)
- Amir Mikhak
- André Cruz
- [Andre Polykanine](https://github.com/oire)
- Andrea Giammarchi – 3 packages
- Andrew – 2 packages
- Andrew Eisenberg
- [Andrew Makarov](https://github.com/r3nya)
- Andrew Rhyne
- Andrew Waterman
- Andrey Sitnik – 2 packages
- [Anna Henningsen](https://github.com/addaleax)
- [Antonio Laguna](https://antonio.laguna.es)
- Apostrophe Technologies, Inc.
- Appium Contributors
- [Aral Balkan](https://ar.al) – 9 packages
- Aranđel Šarenac
- [Ari Porad](https://ariporad.com)
- Aseem Kishore
- [Aurélio A. Heckert](https://softwarelivre.org/aurium)
- azu – 2 packages
- Ben Coe – 7 packages
- Ben Noordhuis
- [Benjamin E. Coe](https://twitter.com/benjamincoe)
- [Benjamin Thomas](https://github.com/bentomas)
- [Benjamin Toueg](https://github.com/btoueg)
- Blaine Bublitz
- [Blake Miner](https://www.blakeminer.com/)
- [Bogdan Chadkin](https://github.com/TrySound) – 3 packages
- bradleymeck
- Brian Donovan
- Brian White – 3 packages
- [Brian Woodward](https://twitter.com/doowb) – 3 packages
- Caleb Porzio
- Caolan McMahon
- Capriza Inc. – defunct; domain down –
- [Chad Walker](https://github.com/chad3814)
- [Charles Samborski](https://demurgos.net) – 2 packages
- Chase Douglas – 2 packages
- [Chigozirim C.](https://github.com/smac89)
- Chris J. Shull
- Chris Montgomery – 2 packages
- Chris Truter – 2 packages
- Christoph Dorn
- Clint Ruoho
- Colin Casey
- Conrad Irwin – 2 packages
- Corey Farrell – 2 packages
- [Dan MacTough](https://yabfog.com/)
- Daniel Espeset – 2 packages
- Daniel Fischer
- [Daniel Perez](https://tuvistavie.com)
- Daniel Stockman
- [Daniele Belardi](https://github.com/dnlup)
- [Danilo Sampaio](https://localhost:8080)
- Dav Glass
- Dave Longley
- David Björklund
- David Clark
- David Glasser – 4 packages
- David I. Lehn
- [David Junger](https://github.com/touffy)
- David Sveningsson – 2 packages
- [Deividas Bakanas](https://github.com/DeividasBakanas)
- [Denis Malinochkin](https://mrmlnc.com)
- [Devon Govett](https://badassjs.com)
- Diego Perini
- [Digital Bazaar, Inc.](https://digitalbazaar.com/) – 2 packages
- djchie – 2 packages
- [Dmitry Semigradsky](https://github.com/Semigradsky)
- [Domenic Denicola](https://domenic.me/) – 6 packages
- [Dominic Tarr](https://dominictarr.com) – 4 packages
- Douglas Christopher Wilson – 26 packages
- Duncan Beevers – 2 packages
- [Dustin Diaz](https://dustindiaz.com)
- DY
- Eddy Bruël – 2 packages
- EditorConfig Team
- Edmond Meinfelder
- [Edo Rivai](https://edo.rivai.nl)
- Eemeli Aro
- Egor Rogov
- Einar Lielmanis
- [Einar Otto Stangvik](https://2x.io)
- Elan Shankar
- [Elan Shanker](https://github.com/es128) – 3 packages
- Elijah Insua
- Enrico Marino
- [Ethan Arrowood](https://github.com/ethan-arrowood)
- [Eugene Sharygin](https://github.com/eush77)
- [Eugene Y. Q. Shen](https://github.com/eyqs)
- [Evan Hahn](https://evanhahn.com)
- Evan Wallace – 2 packages
- Evan You – 2 packages
- Evgeny Poberezkin – 3 packages
- [ExE Boss](https://github.com/ExE-Boss)
- Fabio Zendhi Nagao
- [Fabrício Matté](https://ultcombo.js.org/) – 2 packages
- [Fede Ramirez](https://2fd.github.io)
- Federico Romero
- Fedor Indutny – 4 packages
- Felix Becker
- Felix Boehm – 6 packages
- [Felix Geisendörfer](https://debuggable.com/) – 3 packages
- Felix Gnass – 2 packages
- Felix Hanley
- [Feross Aboukhadijeh](https://feross.org) – 6 packages
- Filip Skokan
- Flow Team – 2 packages
- Gabriel Eisbruch
- Gary Court
- Gary Katsevman
- Gary Ye – 2 packages
- [George Stagas](https://stagas.com/)
- George Zahariev – 4 packages
- Gian Marco Gherardi
- Gidi Meir Morris
- Gilad Peleg – 2 packages
- GitHub Inc. – 3 packages
- [Gulp Team](https://gulpjs.com/)
- [Guy Ellis](https://www.guyellisrocks.com/)
- [Hannes Magnusson](https://github.com/Hannes-Magnusson-CK)
- Harutyun Amirjanyan
- Hawken Rives – 2 packages
- Heather Arthur – 3 packages
- [hemanth.hm](https://h3manth.com)
- [Hong Xu](https://topbug.net)
- [Huáng Jùnliàng](https://github.com/JLHwung)
- [Hugh Kennedy](https://twitter.com/hughskennedy) – 3 packages
- [Huw](https://github.com/hoo29)
- Ian Livingstone
- [Ilia Baryshnikov](https://github.com/qwelias)
- [Ilya Shaisultanov](https://github.com/diversario)
- [Isaac Cambron](https://isaaccambron.com)
- [Isaac Z. Schlueter](https://blog.izs.me/) – 17 packages
- [Iskren Ivov Chernev](https://github.com/ichernev)
- ITW Creative Works
- [Ivan Nikulin](https://github.com/inikulin) – 3 packages
- Ivan Sagalaev
- Ivan Starkov
- J. Ryan Stinnett – 2 packages
- Jack Herrington – 2 packages
- Jake Verbaten – 4 packages
- [James Halliday](https://substack.net) – 13 packages
- [James Talmage](https://github.com/jamestalmage) – 2 packages
- James Wyatt Cready
- Jamie Wong – 2 packages
- Jan T. Sott
- [Jared Wray](https://jaredwray.com) – 2 packages
- Javier Blanco
- [Jed Mao](https://github.com/jedmao/)
- Jed Watson
- [Jens Taylor](https://github.com/homebrewing)
- [Jeremiah Senkpiel](https://searchbeam.jit.su)
- Jeremy Hull
- Jeremy Stashewsky
- Jérôme Desboeufs – 2 packages
- Jmeas Smith – 2 packages
- [Joe Hildebrand](https://twitter.com/hildjj) – 2 packages
- Joel Feenstra
- Johannes Ewald – 2 packages
- John Gozde – 2 packages
- John-David Dalton – 4 packages
- [Jon Schlinkert](https://twitter.com/jonschlinkert) – 18 packages
- [Jonas Liljegren](https://jonas.liljegren.org/)
- [Jonathan Ong](https://jongleberry.com) – 17 packages
- Jonathan Stewmon
- [Jordan Harband](https://ljharb.codes) – 92 packages
- Jordan Tucker
- Joris van der Wel – 2 packages
- Josh Goebel – 2 packages
- JoshuaKGoldberg
- [Julian Gruber](https://juliangruber.com) – 3 packages
- [Junxiao Shi](https://github.com/yoursunny)
- Justin Ridgewell – 2 packages
- kael
- Kat Marchán
- Kelly Campbell – 2 packages
- [Kelvin Jin](https://github.com/kjin)
- Kevin Beaty
- [Kevin Mårtensson](https://github.com/kevva) – 2 packages
- Khaled Al-Ansari – 2 packages
- Kiko Beats – 2 packages
- Klaus Hartl
- [Klaus Meinhardt](https://github.com/ajafff)
- Kognise
- Kris Reeves – 2 packages
- Krishnan Anantheswaran – 3 packages
- Krzysztof Jan Modras – 2 packages
- [Kuba Juszczyk](https://github.com/ku8ar)
- [Kyle E. Mitchell](https://kemitchell.com) – 3 packages
- [Kyle Uehlein](https://github.com/kuehlein)
- Lalit Kapoor
- [Lauri Rooden](https://github.com/litejs/natural-compare-lite)
- Lea Verou
- [Lee Byron](https://leebyron.com/)
- Li Xuanji
- Liam Newman
- linsir
- [Linus Unnebäck](https://github.com/LinusU)
- [Lishude](https://github.com/islishude)
- Louis-Dominique Dubeau – 2 packages
- [Luke Edwards](https://lukeed.com) – 9 packages
- [Marcin Kopacz](https://github.com/chyzwar)
- Marcos Cáceres
- [Mariusz Wiktorczyk](https://github.com/mwiktorczyk)
- Mark Stacey
- [Martin Grenfell](https://got-ravings.blogspot.com)
- [Martin Kolárik](https://kolarik.sk)
- Masaki Komagata
- [Mathias Bynens](https://mathiasbynens.be/) – 6 packages
- [Matt Davies](https://github.com/mattpauldavies)
- Matt DesLauriers
- [Matt Johnson](https://codeofmatt.com)
- Matt Searle
- Matt-Esch – 2 packages
- [Matteo Collina](https://github.com/mcollina) – 4 packages
- [Matthew Aitken](https://github.com/KhafraDev)
- Matthew Dunsdon
- Max Nanasy
- Megan Holmes
- Meryn Stol
- Michael de Libero 
- Michael Ficarra – 2 packages
- Michael Mclaughlin
- Michael Z Goddard – 2 packages
- Microsoft Corp.
- Microsoft Corporation – 3 packages
- [Microsoft TypeScript](https://github.com/Microsoft)
- Miguel Roncancio
- Mihai Bazon – 2 packages
- Mike Atkins
- [Mikeal Rogers](https://www.mikealrogers.com/) – 3 packages
- Miles Elam – 2 packages
- Miroslav Bajtoš
- [Mohsen Azimi](https://github.com/mohsen1)
- Nadav Fischer – domain non-functional –
- [Nagao, Fabio Zendhi](https://github.com/nagaozen/)
- [Nathan MacInnes](https://macinn.es/)
- [Nathan Rajlich](https://n8.io/) – 6 packages
- [Nathan Zadoks](https://github.com/nathan7)
- Nicholas C. Zakas – 5 packages
- Nicholas C. Zaks
- [Nick Baugh](https://niftylettuce.com/)
- Nick Fitzgerald – 2 packages
- Nicolas Lalevée – 2 packages
- [Nikita Galkin](https://github.com/galkin)
- [Nikita Skovoroda](https://github.com/ChALkeR)
- Nikita Vasilyev
- Nochum Sossonko
- [NodeJS Contributors](https://github.com/NodeJS)
- Oleg Efimov
- Olivier Melcher – 2 packages
- [Olsten Larck](https://i.am.charlike.online) – 2 packages
- [Orlin Georgiev](https://github.com/orling)
- [Osman Nuri Okumuş](https://onokumus.com)
- [Parambir Singh](https://github.com/parambirs)
- [Paul Miller](https://paulmillr.com) – 9 packages
- [Paul Vorbach](https://paul.vorba.ch/)
- [Pavel Lang](https://github.com/langpavel)
- [Peter Bright](https://github.com/drpizza)
- [Peteris Krumins](https://www.catonmat.net)
- [Petka Antonov](https://github.com/petkaantonov/)
- Philipp Dunkel
- [Piotr Błażejewicz](https://github.com/peterblazejewicz)
- [Qix](https://github.com/qix-)
- Ram Damera
- [Ramesh Nair](https://www.hiddentao.com/)
- Randolf J
- Raynos – 13 packages
- [Rebecca Turner](https://re-becca.org)
- Rémi Berson – 4 packages
- Rich Harris – 2 packages
- Richie Bendall
- Rifat Nabi
- [Robert Kieffer](https://github.com/broofa) – 2 packages
- [Robert Nagy](https://github.com/ronag)
- [Róbert Oroszi](https://github.com/oroce)
- [Rocky Meza](https://rockymeza.com)
- Romain Beauxis – 2 packages
- Roman Shtylman
- [Rouven Weßling](https://www.rouvenwessling.de) – 3 packages
- [Roy Riojas](https://royriojas.com)
- [Ruy Adorno](https://ruyadorno.com)
- Ryan Seddon – 2 packages
- Saad Rashid – 2 packages
- Sam Thompson
- [Samuel Ainsworth](https://github.com/samuela)
- Sang Dang
- Scott Corgan
- [Sebastian Hildebrandt](https://plus-innovations.com)
- Sebastian Landwehr
- Sebastian Mayr – 3 packages
- [Sebastian Silbermann](https://github.com/eps1lon)
- shinnn
- [Shinnosuke Watanabe](https://github.com/shinnn)
- Shivan Kaul Sahib
- Simon Lydell – 3 packages
- [Sindre Sorhus](https://sindresorhus.com) – 63 packages
- Stan
- Stefan Penner
- Stefan Siegl
- Stephan Meijer
- Stephen Crane – 2 packages
- Stephen Hess
- Steve King – 6 packages
- [Steven Vachon](https://svachon.com)
- Suhas Karanth
- [Szymon Marczak](https://github.com/szmarczak)
- T. Jameson Little
- [Terkel Gjervig](https://terkel.com) – 2 packages
- [Thanik Bhongbhibhat](https://github.com/bhongy)
- [The Babel Team](https://babel.dev/team) – 3 packages
- The Linux Foundation
- Théo FIDRY
- thisconnect
- [Thomas den Hollander](https://github.com/ThomasdenH)
- Thomas Parisot – 2 packages
- [Thorsten Lorenz](https://thlorenz.com) – 2 packages
- [Tian You](https://blog.axqd.net/)
- Tim Channell
- Tim Oxley
- [Tim Wood](https://timwoodcreates.com/)
- Timo Tijhof – 2 packages
- Timothy Gu
- [TJ Holowaychuk](https://tjholowaychuk.com) – 5 packages
- Tobias Koppers – 2 packages
- [Tobiasz Cudnik](https://github.com/TobiaszCudnik)
- [Todd Kennedy](https://tck.io)
- [Todd Wolfson](https://twolfson.com/) – 3 packages
- [Tom Byrer](https://github.com/tomByrer)
- [Tomas Della Vedova](https://github.com/delvedor)
- [Toru Nagashima](https://github.com/mysticatea) – 3 packages
- [Trey Hunner](https://treyhunner.com)
- [Trivikram Kamat](https://github.com/trivikr)
- [Troy Goode](https://github.com/troygoode/) – 3 packages
- [Tyler Akins](https://rumkin.com)
- usrbincc – 2 packages
- [v1rtl](https://v1rtl.site)
- [Val](https://val.codejam.info) – 2 packages
- Valentin 7rulnik Semirulnik
- [Victor Perin](https://github.com/victorperin)
- Vital Batmanov
- [Vitaly Puzrin](https://github.com/puzrin)
- Vittorio Gambaletta
- Vladimir Jimenez
- Vladimir Zapparov
- [Vsevolod Strukchinsky](https://github.com/floatdrop)
- [w1nk](https://github.com/w1nk)
- [wafuwafu13](https://github.com/wafuwafu13)
- [Waldemar Reusch](https://github.com/lordvlad)
- Wes Todd
- [Wilco Bakker](https://github.com/WilcoBakker)
- Will Harney
- [William Hilton](https://wmhilton.com/)
- [Wolfgang Faust](https://www.linestarve.com)
- [Woong Jun](https://code.woong.org/)
- [wwwy3y3](https://github.com/wwwy3y3)
- Xavier Damman – 2 packages
- [Yargs Contributors](https://github.com/yargs/yargs/graphs/contributors)
- Yehezkiel Syamsuhadi – 2 packages
- [Yongsheng Zhang](https://github.com/ZYSzys)
- [Zach Hale](https://zachhale.com)

### Packages

- [abbrev](https://npmjs.org/abbrev): Like ruby's abbrev module, but in js (ISC license)
- [accepts](https://npmjs.org/accepts): Higher-level content negotiation (MIT license)
- [acorn](https://npmjs.org/acorn): ECMAScript parser (MIT license)
- [acorn-jsx](https://npmjs.org/acorn-jsx): Modern, fast React.js JSX parser (MIT license)
- [acorn-walk](https://npmjs.org/acorn-walk): ECMAScript (ESTree) AST walker (MIT license)
- [agent-base](https://npmjs.org/agent-base): Turn a function into an `http.Agent` instance (MIT license)
- [ajv](https://npmjs.org/ajv): Another JSON Schema Validator (MIT license)
- [alpinejs](https://npmjs.org/alpinejs): The rugged, minimal JavaScript framework (MIT license)
- [ansi-regex](https://npmjs.org/ansi-regex): Regular expression for matching ANSI escape codes (MIT license)
- [ansi-styles](https://npmjs.org/ansi-styles): ANSI escape codes for styling strings in the terminal (MIT license)
- [any-promise](https://npmjs.org/any-promise): Resolve any installed ES6 compatible promise (MIT license)
- [argparse](https://npmjs.org/argparse): CLI arguments parser. Native port of python's argparse. (Python-2.0 license)
- [array-buffer-byte-length](https://npmjs.org/array-buffer-byte-length): Get the byte length of an ArrayBuffer, even in engines without a `.byteLength` method. (MIT license)
- [array-union](https://npmjs.org/array-union): Create an array of unique values, in order, from the input arrays (MIT license)
- [array.prototype.every](https://npmjs.org/array.prototype.every): An ES5 spec-compliant `Array.prototype.every` shim/polyfill/replacement that works as far down as ES3. (MIT license)
- [arraybuffer.prototype.slice](https://npmjs.org/arraybuffer.prototype.slice): ES spec-compliant shim for ArrayBuffer.prototype.slice (MIT license)
- [asn1.js](https://npmjs.org/asn1.js): ASN.1 encoder and decoder (MIT license)
- [asn1.js-rfc2560](https://npmjs.org/asn1.js-rfc2560): RFC2560 structures for asn1.js (MIT license)
- [asn1.js-rfc5280](https://npmjs.org/asn1.js-rfc5280): RFC5280 extension structures for asn1.js (MIT license)
- [async](https://npmjs.org/async): Higher-order functions and common patterns for asynchronous code (MIT license)
- [asynckit](https://npmjs.org/asynckit): Minimal async jobs utility library, with streams support (MIT license)
- [@small-tech/attribute-parser](https://npmjs.org/@small-tech/attribute-parser): Parses the attributes in a string containing a single tag. (AGPL-3.0-or-later license)
- [@small-tech/auto-encrypt](https://npmjs.org/@small-tech/auto-encrypt): Automatically provisions and renews Let’s Encrypt TLS certificates on Node.js https servers (including Kitten, Polka, Express.js, etc.) (AGPL-3.0-or-later license)
- [@small-tech/auto-encrypt-localhost](https://npmjs.org/@small-tech/auto-encrypt-localhost): Automatically provisions and installs locally-trusted TLS certificates for Node.js https servers in 100% JavaScript. (AGPL-3.0 license)
- [available-typed-arrays](https://npmjs.org/available-typed-arrays): Returns an array of Typed Array names that are available in the current environment (MIT license)
- [balanced-match](https://npmjs.org/balanced-match): Match balanced character pairs, like "{" and "}" (MIT license)
- [@scure/base](https://npmjs.org/@scure/base): Secure, audited & 0-dep implementation of bech32, base64, base58, base32 & base16 (MIT license)
- [base64-js](https://npmjs.org/base64-js): Base64 encoding/decoding in pure JS (MIT license)
- [bent](https://npmjs.org/bent): Functional HTTP client for Node.js w/ async/await. (Apache-2.0 license)
- [@sidvind/better-ajv-errors](https://npmjs.org/@sidvind/better-ajv-errors): JSON Schema validation for Human (Apache-2.0 license)
- [bl](https://npmjs.org/bl): Buffer List: collect buffers and access with a standard readable Buffer interface, streamable too! (MIT license)
- [bluebird](https://npmjs.org/bluebird): Full featured Promises/A+ implementation with exceptionally good performance (MIT license)
- [bn.js](https://npmjs.org/bn.js): Big number implementation in pure javascript (MIT license)
- [body](https://npmjs.org/body): Body parsing
- [body-parser](https://npmjs.org/body-parser): Node.js body parsing middleware (MIT license)
- [brace-expansion](https://npmjs.org/brace-expansion): Brace expansion as known from sh/bash (MIT license)
- [braces](https://npmjs.org/braces): Bash-like brace expansion, implemented in JavaScript. Safer than other brace expansion libs, with complete support for the Bash 4.3 braces specification, without sacrificing speed. (MIT license)
- [undefined](https://npmjs.org/undefined)
- [buffer](https://npmjs.org/buffer): Node.js Buffer API, for the browser (MIT license)
- [buffer-from](https://npmjs.org/buffer-from) (MIT license)
- [bundle-name](https://npmjs.org/bundle-name): Get bundle name from a bundle identifier (macOS): `com.apple.Safari` → `Safari` (MIT license)
- [busboy](https://npmjs.org/busboy): A streaming parser for HTML form data for node.js
- [bytes](https://npmjs.org/bytes): Utility to parse a string bytes to bytes and vice-versa (MIT license)
- [bytesish](https://npmjs.org/bytesish): Cross-Platform Binary API ((Apache-2.0 AND MIT) license)
- [c8](https://npmjs.org/c8): output coverage reports using Node.js' built in coverage (ISC license)
- [call-bind](https://npmjs.org/call-bind): Robustly `.call.bind()` a function (MIT license)
- [callsites](https://npmjs.org/callsites): Get callsites from the V8 stack trace API (MIT license)
- [caseless](https://npmjs.org/caseless): Caseless object set/get/has, very useful when working with HTTP headers. (Apache-2.0 license)
- [chalk](https://npmjs.org/chalk): Terminal string styling done right (MIT license)
- [char-regex](https://npmjs.org/char-regex): A regex to match any full character, considering weird character ranges. (MIT license)
- [undefined](https://npmjs.org/undefined)
- [cli-cursor](https://npmjs.org/cli-cursor): Toggle the CLI cursor (MIT license)
- [cli-highlight](https://npmjs.org/cli-highlight): Syntax highlighting in your terminal (ISC license)
- [cli-spinners](https://npmjs.org/cli-spinners): Spinners for use in the terminal (MIT license)
- [cliui](https://npmjs.org/cliui): easily create complex multi-column command-line-interfaces (ISC license)
- [clone](https://npmjs.org/clone): deep cloning of objects and arrays (MIT license)
- [@babel/code-frame](https://npmjs.org/@babel/code-frame): Generate errors that contain a code frame that point to source locations. (MIT license)
- [color-convert](https://npmjs.org/color-convert): Plain color conversion functions (MIT license)
- [color-name](https://npmjs.org/color-name): A list of color names and its values (MIT license)
- [combined-stream](https://npmjs.org/combined-stream): A stream that emits multiple other streams one after another. (MIT license)
- [commander](https://npmjs.org/commander): the complete solution for node.js command-line programs (MIT license)
- [undefined](https://npmjs.org/undefined)
- [concat-map](https://npmjs.org/concat-map): concatenative mapdashery (MIT license)
- [@humanwhocodes/config-array](https://npmjs.org/@humanwhocodes/config-array): Glob-based configuration matching. (Apache-2.0 license)
- [config-chain](https://npmjs.org/config-chain): HANDLE CONFIGURATION ONCE AND FOR ALL
- [connect-busboy](https://npmjs.org/connect-busboy): Connect middleware for busboy
- [connect-static-file](https://npmjs.org/connect-static-file): connect and express middleware to serve a single static file (MIT license)
- [content-type](https://npmjs.org/content-type): Create and parse HTTP Content-Type header (MIT license)
- [continuable-cache](https://npmjs.org/continuable-cache): Cache a continuable
- [convert-source-map](https://npmjs.org/convert-source-map): Converts a source-map from/to  different formats and allows adding/changing properties. (MIT license)
- [cookie](https://npmjs.org/cookie): HTTP server cookie parsing and serialization (MIT license)
- [cors](https://npmjs.org/cors): Node.js CORS middleware (MIT license)
- [cosmiconfig](https://npmjs.org/cosmiconfig): Find and load configuration from a package.json property, rc file, TypeScript module, and more! (MIT license)
- [cross-spawn](https://npmjs.org/cross-spawn): Cross platform child_process#spawn and child_process#spawnSync (MIT license)
- [cssstyle](https://npmjs.org/cssstyle): CSSStyleDeclaration Object Model implementation (MIT license)
- [@esbuild/darwin-arm64](https://npmjs.org/@esbuild/darwin-arm64): The macOS ARM 64-bit binary for esbuild, a JavaScript bundler. (MIT license)
- [data-urls](https://npmjs.org/data-urls): Parses data: URLs (MIT license)
- [data-view-buffer](https://npmjs.org/data-view-buffer): Get the ArrayBuffer out of a DataView, robustly. (MIT license)
- [data-view-byte-length](https://npmjs.org/data-view-byte-length): Get the byteLength out of a DataView, robustly. (MIT license)
- [data-view-byte-offset](https://npmjs.org/data-view-byte-offset): Get the byteOffset out of a DataView, robustly. (MIT license)
- [debug](https://npmjs.org/debug): small debugging utility (MIT license)
- [decimal.js](https://npmjs.org/decimal.js): An arbitrary-precision Decimal type for JavaScript. (MIT license)
- [deep-equal](https://npmjs.org/deep-equal): node's assert.deepEqual algorithm (MIT license)
- [deep-is](https://npmjs.org/deep-is): node's assert.deepEqual algorithm except for NaN being equal to NaN (MIT license)
- [deepmerge](https://npmjs.org/deepmerge): A library for deep (recursive) merging of Javascript objects (MIT license)
- [default-browser](https://npmjs.org/default-browser): Get the default browser (MIT license)
- [default-browser-id](https://npmjs.org/default-browser-id): Get the bundle identifier of the default browser (macOS). Example: com.apple.Safari (MIT license)
- [defaults](https://npmjs.org/defaults): merge single level defaults over a config object (MIT license)
- [define-data-property](https://npmjs.org/define-data-property): Define a data property on an object. Will fall back to assignment in an engine without descriptors. (MIT license)
- [define-lazy-prop](https://npmjs.org/define-lazy-prop): Define a lazily evaluated property on an object (MIT license)
- [define-properties](https://npmjs.org/define-properties): Define multiple non-enumerable properties at once. Uses `Object.defineProperty` when available; falls back to standard assignment in older engines. (MIT license)
- [defined](https://npmjs.org/defined): return the first argument that is `!== undefined` (MIT license)
- [delayed-stream](https://npmjs.org/delayed-stream): Buffers events from a stream until you are ready to handle them. (MIT license)
- [depd](https://npmjs.org/depd): Deprecate all the things (MIT license)
- [destroy](https://npmjs.org/destroy): destroy a stream if possible (MIT license)
- [dettle](https://npmjs.org/dettle): A tiny fully-featured debounce and throttle implementation.
- [dir-glob](https://npmjs.org/dir-glob): Convert directories to glob compatible strings (MIT license)
- [doctrine](https://npmjs.org/doctrine): JSDoc parser (Apache-2.0 license)
- [dom-serializer](https://npmjs.org/dom-serializer): render domhandler DOM nodes to a string (MIT license)
- [domelementtype](https://npmjs.org/domelementtype): all the types of nodes in htmlparser2's dom (BSD-2-Clause license)
- [domhandler](https://npmjs.org/domhandler): Handler for htmlparser2 that turns pages into a dom (BSD-2-Clause license)
- [domutils](https://npmjs.org/domutils): Utilities for working with htmlparser2's dom (BSD-2-Clause license)
- [dotignore](https://npmjs.org/dotignore): ignorefile/includefile matching .gitignore spec (MIT license)
- [eastasianwidth](https://npmjs.org/eastasianwidth): Get East Asian Width from a character. (MIT license)
- [@noble/ed25519](https://npmjs.org/@noble/ed25519): Fastest JS implementation of ed25519. Independently audited, high-security, 0-dependency EDDSA, X25519 ECDH & ristretto255 (MIT license)
- [ed25519-keygen](https://npmjs.org/ed25519-keygen): Generate ed25519 keys deterministically for SSH, PGP (GPG) and TOR (MIT license)
- [editorconfig](https://npmjs.org/editorconfig): EditorConfig File Locator and Interpreter for Node.js (MIT license)
- [ee-first](https://npmjs.org/ee-first): return the first event in a set of ee/event pairs (MIT license)
- [emoji-regex](https://npmjs.org/emoji-regex): A regular expression to match all Emoji-only symbols as per the Unicode Standard. (MIT license)
- [encodeurl](https://npmjs.org/encodeurl): Encode a URL to a percent-encoded form, excluding already-encoded sequences (MIT license)
- [entities](https://npmjs.org/entities): Encode & decode XML and HTML entities with ease & speed (BSD-2-Clause license)
- [error](https://npmjs.org/error): Custom errors
- [error-ex](https://npmjs.org/error-ex): Easy error subclassing and stack customization (MIT license)
- [undefined](https://npmjs.org/undefined)
- [es-abstract](https://npmjs.org/es-abstract): ECMAScript spec abstract operations. (MIT license)
- [es-define-property](https://npmjs.org/es-define-property): `Object.defineProperty`, but not IE 8's broken one. (MIT license)
- [es-errors](https://npmjs.org/es-errors): A simple cache for a few of the JS Error constructors. (MIT license)
- [es-get-iterator](https://npmjs.org/es-get-iterator): Get an iterator for any JS language value. Works robustly across all environments, all versions. (MIT license)
- [es-object-atoms](https://npmjs.org/es-object-atoms): ES Object-related atoms: Object, ToObject, RequireObjectCoercible (MIT license)
- [es-set-tostringtag](https://npmjs.org/es-set-tostringtag): A helper to optimistically set Symbol.toStringTag, when possible. (MIT license)
- [es-to-primitive](https://npmjs.org/es-to-primitive): ECMAScript “ToPrimitive” algorithm. Provides ES5 and ES2015 versions. (MIT license)
- [esbuild](https://npmjs.org/esbuild): An extremely fast JavaScript and CSS bundler and minifier. (MIT license)
- [escalade](https://npmjs.org/escalade): A tiny (183B to 210B) and fast utility to ascend parent directories (MIT license)
- [escape-html](https://npmjs.org/escape-html): Escape string for use in HTML (MIT license)
- [escape-string-regexp](https://npmjs.org/escape-string-regexp): Escape RegExp special characters (MIT license)
- [eslint](https://npmjs.org/eslint): An AST-based pattern checker for JavaScript. (MIT license)
- [eslint-scope](https://npmjs.org/eslint-scope): ECMAScript scope analyzer for ESLint (BSD-2-Clause license)
- [@eslint-community/eslint-utils](https://npmjs.org/@eslint-community/eslint-utils): Utilities for ESLint plugins. (MIT license)
- [eslint-visitor-keys](https://npmjs.org/eslint-visitor-keys): Constants and utilities about visitor keys to traverse AST. (Apache-2.0 license)
- [@eslint/eslintrc](https://npmjs.org/@eslint/eslintrc): The legacy ESLintRC config file format for ESLint (MIT license)
- [undefined](https://npmjs.org/undefined)
- [@small-tech/esm-tape-runner](https://npmjs.org/@small-tech/esm-tape-runner): Basic test runner for tape that supports ECMAScript Modules (ESM; es6 modules). Runs your tests in isolation, one after the other. (ISC license)
- [espree](https://npmjs.org/espree): An Esprima-compatible JavaScript parser built on Acorn (BSD-2-Clause license)
- [esquery](https://npmjs.org/esquery): A query library for ECMAScript AST using a CSS selector like query language. (BSD-3-Clause license)
- [esrecurse](https://npmjs.org/esrecurse): ECMAScript AST recursive visitor (BSD-2-Clause license)
- [estraverse](https://npmjs.org/estraverse): ECMAScript JS AST traversal functions (BSD-2-Clause license)
- [esutils](https://npmjs.org/esutils): utility box for ECMAScript language tools (BSD-2-Clause license)
- [etag](https://npmjs.org/etag): Create simple HTTP ETags (MIT license)
- [express-busboy](https://npmjs.org/express-busboy): Busboy for express, mimics the old bodyParser (BSD-3-Clause license)
- [fast-deep-equal](https://npmjs.org/fast-deep-equal): Fast deep equal (MIT license)
- [fast-glob](https://npmjs.org/fast-glob): It's a very fast and efficient glob library for Node.js (MIT license)
- [fast-json-stable-stringify](https://npmjs.org/fast-json-stable-stringify): deterministic `JSON.stringify()` - a faster version of substack's json-stable-strigify without jsonify (MIT license)
- [fast-levenshtein](https://npmjs.org/fast-levenshtein): Efficient implementation of Levenshtein algorithm  with locale-specific collator support. (MIT license)
- [fastq](https://npmjs.org/fastq): Fast, in memory work queue (ISC license)
- [file-entry-cache](https://npmjs.org/file-entry-cache): Super simple cache for file metadata, useful for process that work o a given series of files and that only need to repeat the job on the changed ones since the previous run of the process (MIT license)
- [@kwsites/file-exists](https://npmjs.org/@kwsites/file-exists) (MIT license)
- [fill-range](https://npmjs.org/fill-range): Fill in a range of numbers or letters, optionally passing an increment or `step` to use, or create a regex-compatible range with `options.toRegex` (MIT license)
- [find-up](https://npmjs.org/find-up): Find a file or directory by walking up parent directories (MIT license)
- [node-forge-flash](https://npmjs.org/node-forge-flash): Flash build support for Forge. ((BSD-3-Clause OR GPL-2.0) license)
- [flat-cache](https://npmjs.org/flat-cache): A stupidly simple key/value storage using files to persist some data (MIT license)
- [flatted](https://npmjs.org/flatted): A super light and fast circular JSON parser. (ISC license)
- [flow-parser](https://npmjs.org/flow-parser): JavaScript parser written in OCaml. Produces ESTree AST (MIT license)
- [flow-remove-types](https://npmjs.org/flow-remove-types): Removes Flow type annotations from JavaScript files with speed and simplicity. (MIT license)
- [for-each](https://npmjs.org/for-each): A better forEach (MIT license)
- [foreground-child](https://npmjs.org/foreground-child): Run a child as if it's the foreground process. Give it stdio. Exit when it exits. (ISC license)
- [form-data](https://npmjs.org/form-data): A library to create readable "multipart/form-data" streams. Can be used to submit forms and file uploads to other web applications. (MIT license)
- [fresh](https://npmjs.org/fresh): HTTP response freshness testing (MIT license)
- [fs.realpath](https://npmjs.org/fs.realpath): Use node's fs.realpath, but fall back to the JS implementation if the native one fails (ISC license)
- [@nodelib/fs.scandir](https://npmjs.org/@nodelib/fs.scandir): List files and directories inside the specified directory (MIT license)
- [@nodelib/fs.stat](https://npmjs.org/@nodelib/fs.stat): Get the status of a file with some features (MIT license)
- [@nodelib/fs.walk](https://npmjs.org/@nodelib/fs.walk): A library for efficiently walking a directory recursively (MIT license)
- [fsevents](https://npmjs.org/fsevents): Native Access to MacOS FSEvents (MIT license)
- [function-bind](https://npmjs.org/function-bind): Implementation of Function.prototype.bind (MIT license)
- [function.prototype.name](https://npmjs.org/function.prototype.name): An ES2015 spec-compliant `Function.prototype.name` shim (MIT license)
- [functions-have-names](https://npmjs.org/functions-have-names): Does this JS environment support the `name` property on functions? (MIT license)
- [get-caller-file](https://npmjs.org/get-caller-file) (ISC license)
- [get-intrinsic](https://npmjs.org/get-intrinsic): Get and robustly cache all JS language-level intrinsics at first require time (MIT license)
- [get-package-type](https://npmjs.org/get-package-type): Determine the `package.json#type` which applies to a location (MIT license)
- [get-symbol-description](https://npmjs.org/get-symbol-description): Gets the description of a Symbol. Handles `Symbol()` vs `Symbol('')` properly when possible. (MIT license)
- [glob](https://npmjs.org/glob): a little globber (ISC license)
- [glob-parent](https://npmjs.org/glob-parent): Extract the non-magic parent path from a glob string. (ISC license)
- [globals](https://npmjs.org/globals): Global identifiers from different JavaScript environments (MIT license)
- [globalthis](https://npmjs.org/globalthis): ECMAScript spec-compliant polyfill/shim for `globalThis` (MIT license)
- [globby](https://npmjs.org/globby): User-friendly glob matching (MIT license)
- [gopd](https://npmjs.org/gopd): `Object.getOwnPropertyDescriptor`, but accounts for IE's broken implementation. (MIT license)
- [graphemer](https://npmjs.org/graphemer): A JavaScript library that breaks strings into their individual user-perceived characters (including emojis!) (MIT license)
- [has-bigints](https://npmjs.org/has-bigints): Determine if the JS environment has BigInt support. (MIT license)
- [has-dynamic-import](https://npmjs.org/has-dynamic-import): Does the current environment have `import()` support? (MIT license)
- [has-flag](https://npmjs.org/has-flag): Check if argv has a specific flag (MIT license)
- [has-property-descriptors](https://npmjs.org/has-property-descriptors): Does the environment have full property descriptor support? Handles IE 8's broken defineProperty/gOPD. (MIT license)
- [has-proto](https://npmjs.org/has-proto): Does this environment have the ability to get the [[Prototype]] of an object on creation with `__proto__`? (MIT license)
- [has-symbols](https://npmjs.org/has-symbols): Determine if the JS environment has Symbol support. Supports spec, or shams. (MIT license)
- [has-tostringtag](https://npmjs.org/has-tostringtag): Determine if the JS environment has `Symbol.toStringTag` support. Supports spec, or shams. (MIT license)
- [@noble/hashes](https://npmjs.org/@noble/hashes): Audited & minimal 0-dependency JS implementation of SHA2, SHA3, RIPEMD, BLAKE2/3, HMAC, HKDF, PBKDF2, Scrypt (MIT license)
- [hasown](https://npmjs.org/hasown): A robust, ES3 compatible, "has own property" predicate. (MIT license)
- [@babel/helper-validator-identifier](https://npmjs.org/@babel/helper-validator-identifier): Validate identifier/keywords name (MIT license)
- [undefined](https://npmjs.org/undefined)
- [@babel/highlight](https://npmjs.org/@babel/highlight): Syntax highlight JavaScript strings for output in terminals. (MIT license)
- [highlight.js](https://npmjs.org/highlight.js): Syntax highlighting with language autodetection. (BSD-3-Clause license)
- [hosted-git-info](https://npmjs.org/hosted-git-info): Provides metadata and conversions from repository urls for Github, Bitbucket and Gitlab (ISC license)
- [hsts](https://npmjs.org/hsts): HTTP Strict Transport Security middleware. (MIT license)
- [html-encoding-sniffer](https://npmjs.org/html-encoding-sniffer): Sniff the encoding from a HTML byte stream (MIT license)
- [html-escaper](https://npmjs.org/html-escaper): fast and safe way to escape and unescape &<>'" chars (MIT license)
- [html-validate](https://npmjs.org/html-validate): Offline html5 validator (MIT license)
- [htmlparser2](https://npmjs.org/htmlparser2): Fast & forgiving HTML/XML parser (MIT license)
- [http-errors](https://npmjs.org/http-errors): Create HTTP error objects (MIT license)
- [http-proxy-agent](https://npmjs.org/http-proxy-agent): An HTTP(s) proxy `http.Agent` implementation for HTTP (MIT license)
- [@small-tech/https](https://npmjs.org/@small-tech/https): A drop-in standard Node.js HTTPS module replacement with both automatic development-time (localhost) certificates via Auto Encrypt Localhost and automatic production certificates via Auto Encrypt. (AGPL-3.0 license)
- [https-proxy-agent](https://npmjs.org/https-proxy-agent): An HTTP(s) proxy `http.Agent` implementation for HTTPS (MIT license)
- [iconv-lite](https://npmjs.org/iconv-lite): Convert character encodings in pure javascript. (MIT license)
- [idiomorph](https://npmjs.org/idiomorph): an id-based DOM morphing library (BSD 2-Clause license)
- [ieee754](https://npmjs.org/ieee754): Read/write IEEE754 floating point numbers from/to a Buffer or array-like object (BSD-3-Clause license)
- [ignore](https://npmjs.org/ignore): Ignore is a manager and filter for .gitignore rules, the one used by eslint, gitbook and many others. (MIT license)
- [import-fresh](https://npmjs.org/import-fresh): Import a module while bypassing the cache (MIT license)
- [imurmurhash](https://npmjs.org/imurmurhash): An incremental implementation of MurmurHash3 (MIT license)
- [inflight](https://npmjs.org/inflight): Add callbacks to requests in flight to avoid async duplication (ISC license)
- [inherits](https://npmjs.org/inherits): Browser-friendly inheritance fully compatible with standard node.js inherits() (ISC license)
- [ini](https://npmjs.org/ini): An ini encoder/decoder for node (ISC license)
- [internal-slot](https://npmjs.org/internal-slot): ES spec-like internal slots (MIT license)
- [ip-regex](https://npmjs.org/ip-regex): Regular expression for matching IP addresses (IPv4 & IPv6) (MIT license)
- [is-arguments](https://npmjs.org/is-arguments): Is this an arguments object? It's a harder question than you think. (MIT license)
- [is-array-buffer](https://npmjs.org/is-array-buffer): Is this value a JS ArrayBuffer? (MIT license)
- [is-arrayish](https://npmjs.org/is-arrayish): Determines if an object can be used as an array (MIT license)
- [is-bigint](https://npmjs.org/is-bigint): Is this value an ES BigInt? (MIT license)
- [is-boolean-object](https://npmjs.org/is-boolean-object): Is this value a JS Boolean? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-callable](https://npmjs.org/is-callable): Is this JS value callable? Works with Functions and GeneratorFunctions, despite ES6 @@toStringTag. (MIT license)
- [is-core-module](https://npmjs.org/is-core-module): Is this specifier a node.js core module? (MIT license)
- [is-data-view](https://npmjs.org/is-data-view): Is this value a JS DataView? This module works cross-realm/iframe, does not depend on `instanceof` or mutable properties, and despite ES6 Symbol.toStringTag. (MIT license)
- [is-date-object](https://npmjs.org/is-date-object): Is this value a JS Date object? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-docker](https://npmjs.org/is-docker): Check if the process is running inside a Docker container (MIT license)
- [is-extglob](https://npmjs.org/is-extglob): Returns true if a string has an extglob. (MIT license)
- [is-fullwidth-code-point](https://npmjs.org/is-fullwidth-code-point): Check if the character represented by a given Unicode code point is fullwidth (MIT license)
- [is-glob](https://npmjs.org/is-glob): Returns `true` if the given string looks like a glob pattern or an extglob pattern. This makes it easy to create code that only uses external modules like node-glob when necessary, resulting in much faster code execution and initialization time, and a better user experience. (MIT license)
- [is-inside-container](https://npmjs.org/is-inside-container): Check if the process is running inside a container (Docker/Podman) (MIT license)
- [is-interactive](https://npmjs.org/is-interactive): Check if stdout or stderr is interactive (MIT license)
- [is-map](https://npmjs.org/is-map): Is this value a JS Map? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-negative-zero](https://npmjs.org/is-negative-zero): Is this value negative zero? === will lie to you (MIT license)
- [is-number](https://npmjs.org/is-number): Returns true if a number or string value is a finite number. Useful for regex matches, parsing, user input, etc. (MIT license)
- [is-number-object](https://npmjs.org/is-number-object): Is this value a JS Number object? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-path-inside](https://npmjs.org/is-path-inside): Check if a path is inside another path (MIT license)
- [is-plain-object](https://npmjs.org/is-plain-object): Returns true if an object was created by the `Object` constructor, or Object.create(null). (MIT license)
- [is-potential-custom-element-name](https://npmjs.org/is-potential-custom-element-name): Check whether a given string matches the `PotentialCustomElementName` production as defined in the HTML Standard. (MIT license)
- [is-regex](https://npmjs.org/is-regex): Is this value a JS regex? Works cross-realm/iframe, and despite ES6 @@toStringTag (MIT license)
- [is-set](https://npmjs.org/is-set): Is this value a JS Set? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-shared-array-buffer](https://npmjs.org/is-shared-array-buffer): Is this value a JS SharedArrayBuffer? (MIT license)
- [is-stream](https://npmjs.org/is-stream): Check if something is a Node.js stream (MIT license)
- [is-string](https://npmjs.org/is-string): Is this value a JS String object or primitive? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-symbol](https://npmjs.org/is-symbol): Determine if a value is an ES6 Symbol or not. (MIT license)
- [is-typed-array](https://npmjs.org/is-typed-array): Is this value a JS Typed Array? This module works cross-realm/iframe, does not depend on `instanceof` or mutable properties, and despite ES6 Symbol.toStringTag. (MIT license)
- [is-unicode-supported](https://npmjs.org/is-unicode-supported): Detect whether the terminal supports Unicode (MIT license)
- [is-url](https://npmjs.org/is-url): Check whether a string is a URL. (MIT license)
- [is-weakmap](https://npmjs.org/is-weakmap): Is this value a JS WeakMap? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-weakref](https://npmjs.org/is-weakref): Is this value a JS WeakRef? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-weakset](https://npmjs.org/is-weakset): Is this value a JS WeakSet? This module works cross-realm/iframe, and despite ES6 @@toStringTag. (MIT license)
- [is-wsl](https://npmjs.org/is-wsl): Check if the process is running inside Windows Subsystem for Linux (Bash on Windows) (MIT license)
- [is2](https://npmjs.org/is2): A type checking library where each exported function returns either true or false and does not throw. Also added tests. (MIT license)
- [isarray](https://npmjs.org/isarray): Array#isArray for older browsers (MIT license)
- [isexe](https://npmjs.org/isexe): Minimal module to check if a file is executable. (ISC license)
- [istanbul-lib-coverage](https://npmjs.org/istanbul-lib-coverage): Data library for istanbul coverage objects (BSD-3-Clause license)
- [istanbul-lib-report](https://npmjs.org/istanbul-lib-report): Base reporting library for istanbul (BSD-3-Clause license)
- [istanbul-reports](https://npmjs.org/istanbul-reports): istanbul reports (BSD-3-Clause license)
- [jackspeak](https://npmjs.org/jackspeak): A very strict and proper argument parser. (BlueOak-1.0.0 license)
- [jose](https://npmjs.org/jose): JSON Web Almost Everything - JWA, JWS, JWE, JWK, JWT, JWKS for Node.js with minimal dependencies (MIT license)
- [@eslint/js](https://npmjs.org/@eslint/js): ESLint JavaScript language implementation (MIT license)
- [js-beautify](https://npmjs.org/js-beautify): beautifier.io for node (MIT license)
- [js-cookie](https://npmjs.org/js-cookie): A simple, lightweight JavaScript API for handling cookies (MIT license)
- [js-tokens](https://npmjs.org/js-tokens): A regex that tokenizes JavaScript. (MIT license)
- [js-yaml](https://npmjs.org/js-yaml): YAML 1.2 parser and serializer (MIT license)
- [@small-tech/jsdb](https://npmjs.org/@small-tech/jsdb): A zero-dependency, transparent, in-memory, streaming write-on-update JavaScript database for Small Web applications that persists to a JavaScript transaction log. (AGPL-3.0-or-later license)
- [jsdom](https://npmjs.org/jsdom): A JavaScript implementation of many web standards (MIT license)
- [json-buffer](https://npmjs.org/json-buffer): JSON parse & stringify that supports binary via bops & base64 (MIT license)
- [json-parse-even-better-errors](https://npmjs.org/json-parse-even-better-errors): JSON.parse with context information on error (MIT license)
- [json-schema-traverse](https://npmjs.org/json-schema-traverse): Traverse JSON Schema passing each schema object to callback (MIT license)
- [json-stable-stringify-without-jsonify](https://npmjs.org/json-stable-stringify-without-jsonify): deterministic JSON.stringify() with custom sorting to get deterministic hashes from stringified results, with no public domain dependencies (MIT license)
- [json5](https://npmjs.org/json5): JSON for Humans (MIT license)
- [keyv](https://npmjs.org/keyv): Simple key-value storage with support for multiple backends (MIT license)
- [kleur](https://npmjs.org/kleur): The fastest Node.js library for formatting terminal text with ANSI colors~! (MIT license)
- [levn](https://npmjs.org/levn): Light ECMAScript (JavaScript) Value Notation - human written, concise, typed, flexible (MIT license)
- [@bcoe/v8-coverage](https://npmjs.org/@bcoe/v8-coverage): Helper functions for V8 coverage files. (MIT license)
- [lines-and-columns](https://npmjs.org/lines-and-columns): Maps lines and columns to character offsets and back. (MIT license)
- [linkify-it](https://npmjs.org/linkify-it): Links recognition library with FULL unicode support (MIT license)
- [locate-path](https://npmjs.org/locate-path): Get the first path that exists on disk of multiple paths (MIT license)
- [lodash](https://npmjs.org/lodash): Lodash modular utilities. (MIT license)
- [lodash.merge](https://npmjs.org/lodash.merge): The Lodash method `_.merge` exported as a module. (MIT license)
- [log-symbols](https://npmjs.org/log-symbols): Colored symbols for various log levels. Example: `✔︎ Success` (MIT license)
- [lru-cache](https://npmjs.org/lru-cache): A cache object that deletes the least-recently-used items. (ISC license)
- [make-dir](https://npmjs.org/make-dir): Make a directory and its parents if needed - Think `mkdir -p` (MIT license)
- [markdown-it](https://npmjs.org/markdown-it): Markdown-it - modern pluggable markdown parser. (MIT license)
- [markdown-it-anchor](https://npmjs.org/markdown-it-anchor): Header anchors for markdown-it. (Unlicense license)
- [markdown-it-footnote](https://npmjs.org/markdown-it-footnote): Footnotes for markdown-it markdown parser. (MIT license)
- [markdown-it-highlightjs](https://npmjs.org/markdown-it-highlightjs): Preset to use highlight.js with markdown-it. (Unlicense license)
- [markdown-it-image-figures](https://npmjs.org/markdown-it-image-figures): Render images occurring by itself in a paragraph as a figure with support for figcaptions. (MIT license)
- [markdown-it-ins](https://npmjs.org/markdown-it-ins): <ins> tag for markdown-it markdown parser. (MIT license)
- [markdown-it-mark](https://npmjs.org/markdown-it-mark): <mark> tag for markdown-it markdown parser. (MIT license)
- [markdown-it-sub](https://npmjs.org/markdown-it-sub): <sub> tag for markdown-it markdown parser. (MIT license)
- [markdown-it-sup](https://npmjs.org/markdown-it-sup): <sup> tag for markdown-it markdown parser. (MIT license)
- [markdown-it-task-checkbox](https://npmjs.org/markdown-it-task-checkbox): A markdown-it plugin to create GitHub-style task lists (ISC license)
- [markdown-it-toc-done-right](https://npmjs.org/markdown-it-toc-done-right): Table of contents (TOC) for markdown-it markdown parser with focus on semantic and security. (MIT license)
- [mdurl](https://npmjs.org/mdurl): URL utilities for markdown-it (MIT license)
- [media-typer](https://npmjs.org/media-typer): Simple RFC 6838 media type parser and formatter (MIT license)
- [merge2](https://npmjs.org/merge2): Merge multiple streams into one stream in sequence or parallel. (MIT license)
- [micro-aes-gcm](https://npmjs.org/micro-aes-gcm): Simple 0-dep wrapper over node.js/browser AES-GCM. (MIT license)
- [micro-packed](https://npmjs.org/micro-packed): Less painful binary encoding / decoding (MIT license)
- [micromatch](https://npmjs.org/micromatch): Glob matching for javascript/node.js. A replacement and faster alternative to minimatch and multimatch. (MIT license)
- [mime](https://npmjs.org/mime): A comprehensive library for mime-type mapping (MIT license)
- [mime-db](https://npmjs.org/mime-db): Media Type Database (MIT license)
- [mime-types](https://npmjs.org/mime-types): The ultimate javascript content-type utility. (MIT license)
- [mimic-fn](https://npmjs.org/mimic-fn): Make a function mimic another one (MIT license)
- [minimalistic-assert](https://npmjs.org/minimalistic-assert): minimalistic-assert === (ISC license)
- [minimatch](https://npmjs.org/minimatch): a glob matcher in javascript (ISC license)
- [minimist](https://npmjs.org/minimist): parse argument options (MIT license)
- [minipass](https://npmjs.org/minipass): minimal implementation of a PassThrough stream (ISC license)
- [undefined](https://npmjs.org/undefined)
- [mkdirp](https://npmjs.org/mkdirp): Recursively mkdir, like `mkdir -p` (MIT license)
- [mock-property](https://npmjs.org/mock-property): Given an object and a property, replaces a property descriptor (or deletes it), and returns a thunk to restore it. (MIT license)
- [@humanwhocodes/module-importer](https://npmjs.org/@humanwhocodes/module-importer): Universal module importer for Node.js (Apache-2.0 license)
- [moment](https://npmjs.org/moment): Parse, validate, manipulate, and display dates (MIT license)
- [mri](https://npmjs.org/mri): Quickly scan for CLI flags and arguments (MIT license)
- [ms](https://npmjs.org/ms): Tiny milisecond conversion utility (MIT license)
- [mz](https://npmjs.org/mz): modernize node.js to current ECMAScript standards (MIT license)
- [nanoid](https://npmjs.org/nanoid): A tiny (116 bytes), secure URL-friendly unique string ID generator (MIT license)
- [natural-compare](https://npmjs.org/natural-compare): Compare strings containing a mix of letters and numbers in the way a human being would in sort order. (MIT license)
- [negotiator](https://npmjs.org/negotiator): HTTP content negotiation (MIT license)
- [@types/node](https://npmjs.org/@types/node): TypeScript definitions for node (MIT license)
- [node-forge](https://npmjs.org/node-forge): JavaScript implementations of network transports, cryptography, ciphers, PKI, message digests, and various utilities. ((BSD-3-Clause OR GPL-2.0) license)
- [node-modules-regexp](https://npmjs.org/node-modules-regexp): A regular expression for file paths that contain a `node_modules` folder. (MIT license)
- [undefined](https://npmjs.org/undefined)
- [nopt](https://npmjs.org/nopt): Option parsing for Node, supporting types, shorthands, etc. Used by npm. (ISC license)
- [normalize-package-data](https://npmjs.org/normalize-package-data): Normalizes data that can be found in package.json files. (BSD-2-Clause license)
- [nwsapi](https://npmjs.org/nwsapi): Fast CSS Selectors API Engine (MIT license)
- [object-assign](https://npmjs.org/object-assign): ES2015 `Object.assign()` ponyfill (MIT license)
- [object-inspect](https://npmjs.org/object-inspect): string representations of objects in node and the browser (MIT license)
- [object-is](https://npmjs.org/object-is): ES2015-compliant shim for Object.is - differentiates between -0 and +0 (MIT license)
- [object-keys](https://npmjs.org/object-keys): An Object.keys replacement, in case Object.keys is not available. From https://github.com/es-shims/es5-shim (MIT license)
- [@humanwhocodes/object-schema](https://npmjs.org/@humanwhocodes/object-schema): An object schema merger/validator (BSD-3-Clause license)
- [object.assign](https://npmjs.org/object.assign): ES6 spec-compliant Object.assign shim. From https://github.com/es-shims/es6-shim (MIT license)
- [ocsp](https://npmjs.org/ocsp): OCSP Stapling implementation (MIT license)
- [on-finished](https://npmjs.org/on-finished): Execute a callback when a request closes, finishes, or errors (MIT license)
- [once](https://npmjs.org/once): Run a function exactly one time (ISC license)
- [onetime](https://npmjs.org/onetime): Ensure a function is only called once (MIT license)
- [open](https://npmjs.org/open): Open stuff like URLs, files, executables. Cross-platform. (MIT license)
- [optionator](https://npmjs.org/optionator): option parsing and help generation (MIT license)
- [ora](https://npmjs.org/ora): Elegant terminal spinner (MIT license)
- [p-limit](https://npmjs.org/p-limit): Run multiple promise-returning & async functions with limited concurrency (MIT license)
- [p-locate](https://npmjs.org/p-locate): Get the first fulfilled promise that satisfies the provided testing function (MIT license)
- [p-try](https://npmjs.org/p-try): `Start a promise chain (MIT license)
- [parent-module](https://npmjs.org/parent-module): Get the path of the parent module (MIT license)
- [parse-json](https://npmjs.org/parse-json): Parse JSON with more helpful errors (MIT license)
- [parse-srcset](https://npmjs.org/parse-srcset): A spec-conformant JavaScript parser for the HTML5 srcset attribute (MIT license)
- [parse5](https://npmjs.org/parse5): HTML parser and serializer. (MIT license)
- [parse5-htmlparser2-tree-adapter](https://npmjs.org/parse5-htmlparser2-tree-adapter): htmlparser2 tree adapter for parse5. (MIT license)
- [@pkgjs/parseargs](https://npmjs.org/@pkgjs/parseargs): Polyfill of future proposal for `util.parseArgs()` (MIT license)
- [@typescript-eslint/parser](https://npmjs.org/@typescript-eslint/parser): An ESLint custom parser which leverages TypeScript ESTree (BSD-2-Clause license)
- [path-complete-extname](https://npmjs.org/path-complete-extname): path.extname implementation adapted to also include multiple dots extensions.
- [path-exists](https://npmjs.org/path-exists): Check if a path exists (MIT license)
- [path-is-absolute](https://npmjs.org/path-is-absolute): Node.js 0.12 path.isAbsolute() ponyfill (MIT license)
- [path-key](https://npmjs.org/path-key): Get the PATH environment variable key cross-platform (MIT license)
- [path-parse](https://npmjs.org/path-parse): Node.js path.parse() ponyfill (MIT license)
- [path-scurry](https://npmjs.org/path-scurry): walk paths fast and efficiently (BlueOak-1.0.0 license)
- [path-type](https://npmjs.org/path-type): Check if a path is a file, directory, or symlink (MIT license)
- [picocolors](https://npmjs.org/picocolors): The tiniest and the fastest library for terminal output formatting with ANSI colors (ISC license)
- [picomatch](https://npmjs.org/picomatch): Blazing fast and accurate glob matcher written in JavaScript, with no dependencies and full support for standard and extended Bash glob features, including braces, extglobs, POSIX brackets, and regular expressions. (MIT license)
- [pirates](https://npmjs.org/pirates): Properly hijack require (MIT license)
- [playwright](https://npmjs.org/playwright): A high-level API to automate web browsers (Apache-2.0 license)
- [playwright-core](https://npmjs.org/playwright-core): A high-level API to automate web browsers (Apache-2.0 license)
- [polka](https://npmjs.org/polka): A micro web server so fast, it'll make you dance! :dancers: (MIT license)
- [possible-typed-array-names](https://npmjs.org/possible-typed-array-names): A simple list of possible Typed Array names. (MIT license)
- [postcss](https://npmjs.org/postcss): Tool for transforming styles with JS plugins (MIT license)
- [prelude-ls](https://npmjs.org/prelude-ls): prelude.ls is a functionally oriented utility library. It is powerful and flexible. Almost all of its functions are curried. It is written in, and is the recommended base library for, LiveScript. (MIT license)
- [prismjs](https://npmjs.org/prismjs): Lightweight, robust, elegant syntax highlighting. A spin-off project from Dabblet. (MIT license)
- [@kwsites/promise-deferred](https://npmjs.org/@kwsites/promise-deferred): Minimalist creation of promise wrappers, exposing the ability to resolve or reject the inner promise (MIT license)
- [promise-make-naked](https://npmjs.org/promise-make-naked): A simple function that makes a promise that can be resolved or rejected from the outside.
- [prompts](https://npmjs.org/prompts): Lightweight, beautiful and user-friendly prompts (MIT license)
- [proto-list](https://npmjs.org/proto-list): A utility for managing a prototype chain (ISC license)
- [punycode](https://npmjs.org/punycode): A robust Punycode converter that fully complies to RFC 3492 and RFC 5891, and works on nearly all JavaScript platforms. (MIT license)
- [qs](https://npmjs.org/qs): A querystring parser that supports nesting and arrays, with a depth limit (BSD-3-Clause license)
- [queue-microtask](https://npmjs.org/queue-microtask): fast, tiny `queueMicrotask` shim for modern engines (MIT license)
- [random-bytes](https://npmjs.org/random-bytes): URL and cookie safe UIDs (MIT license)
- [range-parser](https://npmjs.org/range-parser): Range header field string parser (MIT license)
- [raw-body](https://npmjs.org/raw-body): Get and validate the raw body of a readable stream. (MIT license)
- [re-emitter](https://npmjs.org/re-emitter): Re emit events from another emitter (MIT license)
- [@vue/reactivity](https://npmjs.org/@vue/reactivity): @vue/reactivity (MIT license)
- [read-pkg](https://npmjs.org/read-pkg): Read a package.json file (MIT license)
- [read-pkg-up](https://npmjs.org/read-pkg-up): Read the closest package.json file (MIT license)
- [readable-stream](https://npmjs.org/readable-stream): Streams3, a user-land copy of the stream library from Node.js (MIT license)
- [regexp.prototype.flags](https://npmjs.org/regexp.prototype.flags): ES6 spec-compliant RegExp.prototype.flags shim. (MIT license)
- [regexparam](https://npmjs.org/regexparam): A tiny (399B) utility that converts route patterns into RegExp. Limited alternative to `path-to-regexp` 🙇‍ (MIT license)
- [@eslint-community/regexpp](https://npmjs.org/@eslint-community/regexpp): Regular expression parser for ECMAScript. (MIT license)
- [require-directory](https://npmjs.org/require-directory): Recursively iterates over specified directory, require()'ing each file, and returning a nested hash structure containing those modules. (MIT license)
- [require-from-string](https://npmjs.org/require-from-string): Require module from string (MIT license)
- [resolve](https://npmjs.org/resolve): resolve like require.resolve() on behalf of files asynchronously and synchronously (MIT license)
- [resolve-from](https://npmjs.org/resolve-from): Resolve the path of a module like `require.resolve()` but from a given path (MIT license)
- [@jridgewell/resolve-uri](https://npmjs.org/@jridgewell/resolve-uri): Resolve a URI relative to an optional base URI (MIT license)
- [restore-cursor](https://npmjs.org/restore-cursor): Gracefully restore the CLI cursor on exit (MIT license)
- [@ljharb/resumer](https://npmjs.org/@ljharb/resumer): a through stream that starts paused and resumes on the next tick (MIT license)
- [reusify](https://npmjs.org/reusify): Reuse objects and functions with style (MIT license)
- [rimraf](https://npmjs.org/rimraf): A deep deletion module for node (like `rm -rf`) (ISC license)
- [rrweb-cssom](https://npmjs.org/rrweb-cssom): CSS Object Model implementation and CSS parser (MIT license)
- [run-applescript](https://npmjs.org/run-applescript): Run AppleScript and get the result (MIT license)
- [run-parallel](https://npmjs.org/run-parallel): Run an array of functions in parallel (MIT license)
- [sade](https://npmjs.org/sade): Smooth (CLI) operator 🎶 (MIT license)
- [safe-array-concat](https://npmjs.org/safe-array-concat): `Array.prototype.concat`, but made safe by ignoring Symbol.isConcatSpreadable (MIT license)
- [safe-buffer](https://npmjs.org/safe-buffer): Safer Node.js Buffer API (MIT license)
- [safe-json-parse](https://npmjs.org/safe-json-parse): Parse JSON safely without throwing
- [safe-regex-test](https://npmjs.org/safe-regex-test): Give a regex, get a robust predicate function that tests it against a string. (MIT license)
- [safer-buffer](https://npmjs.org/safer-buffer): Modern Buffer API polyfill without footguns (MIT license)
- [sanitize-html](https://npmjs.org/sanitize-html): Clean up user-submitted HTML, preserving allowlisted elements and allowlisted attributes on a per-element basis (MIT license)
- [saxes](https://npmjs.org/saxes): An evented streaming XML parser in JavaScript (ISC license)
- [@istanbuljs/schema](https://npmjs.org/@istanbuljs/schema): Schemas describing various structures used by nyc and istanbuljs (MIT license)
- [@typescript-eslint/scope-manager](https://npmjs.org/@typescript-eslint/scope-manager): TypeScript scope analyser for ESLint (MIT license)
- [semver](https://npmjs.org/semver): The semantic version parser used by npm. (ISC license)
- [@polka/send](https://npmjs.org/@polka/send): A response helper that detects & handles Content-Types (MIT license)
- [set-function-length](https://npmjs.org/set-function-length): Set a function's length property (MIT license)
- [set-function-name](https://npmjs.org/set-function-name): Set a function's name property (MIT license)
- [setprototypeof](https://npmjs.org/setprototypeof): A small polyfill for Object.setprototypeof (ISC license)
- [@vue/shared](https://npmjs.org/@vue/shared): internal utils shared across @vue packages (MIT license)
- [shebang-command](https://npmjs.org/shebang-command): Get the command from a shebang (MIT license)
- [shebang-regex](https://npmjs.org/shebang-regex): Regular expression for matching a shebang line (MIT license)
- [shell-quote](https://npmjs.org/shell-quote): quote and parse shell commands (MIT license)
- [side-channel](https://npmjs.org/side-channel): Store information about any JS value in a side channel. Uses WeakMap if available. (MIT license)
- [signal-exit](https://npmjs.org/signal-exit): when you want to fire an event no matter how a process exits. (ISC license)
- [simple-git](https://npmjs.org/simple-git): Simple GIT interface for node.js (MIT license)
- [simple-lru-cache](https://npmjs.org/simple-lru-cache)
- [simply-beautiful](https://npmjs.org/simply-beautiful): Beautify HTML, JS, CSS, and JSON in the browser or in Node.js! (MIT license)
- [sisteransi](https://npmjs.org/sisteransi): ANSI escape codes for some terminal swag (MIT license)
- [slash](https://npmjs.org/slash): Convert Windows backslash paths to slash paths (MIT license)
- [@sindresorhus/slugify](https://npmjs.org/@sindresorhus/slugify): Slugify a string (MIT license)
- [source-map](https://npmjs.org/source-map): Generates and consumes source maps (BSD-3-Clause license)
- [source-map-js](https://npmjs.org/source-map-js): Generates and consumes source maps (BSD-3-Clause license)
- [source-map-support](https://npmjs.org/source-map-support): Fixes stack traces for files with source maps (MIT license)
- [@jridgewell/sourcemap-codec](https://npmjs.org/@jridgewell/sourcemap-codec): Encode/decode sourcemap mappings (MIT license)
- [spdx-correct](https://npmjs.org/spdx-correct): correct invalid SPDX expressions (Apache-2.0 license)
- [spdx-exceptions](https://npmjs.org/spdx-exceptions): list of SPDX standard license exceptions (CC-BY-3.0 license)
- [spdx-expression-parse](https://npmjs.org/spdx-expression-parse): parse SPDX license expressions (MIT license)
- [spdx-license-ids](https://npmjs.org/spdx-license-ids): A list of SPDX license identifiers (CC0-1.0 license)
- [split](https://npmjs.org/split): split a Text Stream into a Line Stream (MIT license)
- [statuses](https://npmjs.org/statuses): HTTP status utility (MIT license)
- [stop-iteration-iterator](https://npmjs.org/stop-iteration-iterator): Firefox 17-26 iterators throw a StopIteration object to indicate "done". This normalizes it. (MIT license)
- [streamsearch](https://npmjs.org/streamsearch): Streaming Boyer-Moore-Horspool searching for node.js
- [string_decoder](https://npmjs.org/string_decoder): The string_decoder module from Node core (MIT license)
- [string-length](https://npmjs.org/string-length): Get the real length of a string - by correctly counting astral symbols and ignoring ansi escape codes (MIT license)
- [string-template](https://npmjs.org/string-template): A simple string template function based on named or indexed arguments
- [string-width](https://npmjs.org/string-width): Get the visual width of a string - the number of columns required to display it (MIT license)
- [string-width](https://npmjs.org/string-width): Get the visual width of a string - the number of columns required to display it (MIT license)
- [string.prototype.trim](https://npmjs.org/string.prototype.trim): ES5 spec-compliant shim for String.prototype.trim (MIT license)
- [string.prototype.trimend](https://npmjs.org/string.prototype.trimend): ES2019 spec-compliant String.prototype.trimEnd shim. (MIT license)
- [string.prototype.trimstart](https://npmjs.org/string.prototype.trimstart): ES2019 spec-compliant String.prototype.trimStart shim. (MIT license)
- [strip-ansi](https://npmjs.org/strip-ansi): Strip ANSI escape codes from a string (MIT license)
- [strip-ansi](https://npmjs.org/strip-ansi): Strip ANSI escape codes from a string (MIT license)
- [strip-json-comments](https://npmjs.org/strip-json-comments): Strip comments from JSON. Lets you use comments in your JSON files! (MIT license)
- [@ungap/structured-clone](https://npmjs.org/@ungap/structured-clone): A structuredClone polyfill (ISC license)
- [stubborn-fs](https://npmjs.org/stubborn-fs): Stubborn versions of Node's fs functions that try really hard to do their job.
- [@html-validate/stylish](https://npmjs.org/@html-validate/stylish): ESLint compatible stylish formatter (MIT license)
- [supports-color](https://npmjs.org/supports-color): Detect whether a terminal supports color (MIT license)
- [supports-preserve-symlinks-flag](https://npmjs.org/supports-preserve-symlinks-flag): Determine if the current node version supports the `--preserve-symlinks` flag. (MIT license)
- [symbol-tree](https://npmjs.org/symbol-tree): Turn any collection of objects into its own efficient tree or linked list using Symbol (MIT license)
- [systeminformation](https://npmjs.org/systeminformation): Advanced, lightweight system and OS information library (MIT license)
- [@small-tech/syswide-cas](https://npmjs.org/@small-tech/syswide-cas): Fork of syswide-cas by a now-defunct company called Capriza. Enable node to use system wide certificate authorities in conjunction with the bundled root CAs. (MIT license)
- [tail-file](https://npmjs.org/tail-file): Tail files fast, easy, persistent, fault tolerant and flexible (MIT license)
- [@small-tech/tap-monkey](https://npmjs.org/@small-tech/tap-monkey): A tap formatter that’s also a monkey. (ISC license)
- [@small-tech/tap-out](https://npmjs.org/@small-tech/tap-out): A different tap parser (MIT license)
- [tape](https://npmjs.org/tape): tap-producing test harness for node and browsers (MIT license)
- [tcp-port-used](https://npmjs.org/tcp-port-used): A simple Node.js module to check if a TCP port is already bound. (MIT license)
- [teen_process](https://npmjs.org/teen_process): A grown up version of Node's spawn/exec (Apache-2.0 license)
- [term-size](https://npmjs.org/term-size): Reliably get the terminal window size (columns & rows) (MIT license)
- [@playwright/test](https://npmjs.org/@playwright/test): A high-level API to automate web browsers (Apache-2.0 license)
- [test-exclude](https://npmjs.org/test-exclude): test for inclusion or exclusion of paths using globs (ISC license)
- [text-table](https://npmjs.org/text-table): borderless text tables with alignment (MIT license)
- [thenify](https://npmjs.org/thenify): Promisify a callback-based function (MIT license)
- [thenify-all](https://npmjs.org/thenify-all): Promisifies all the selected functions in an object (MIT license)
- [through](https://npmjs.org/through): simplified stream construction (MIT license)
- [tiny-readdir](https://npmjs.org/tiny-readdir): A simple promisified recursive readdir function.
- [tinyws](https://npmjs.org/tinyws): Tiny WebSocket middleware for Node.js based on ws. (MIT license)
- [tldts](https://npmjs.org/tldts): Library to work against complex domain names, subdomains and URIs. (MIT license)
- [tldts-core](https://npmjs.org/tldts-core): tldts core primitives (internal module) (MIT license)
- [to-regex-range](https://npmjs.org/to-regex-range): Pass two numbers, get a regex-compatible source string for matching ranges. Validated against more than 2.78 million test assertions. (MIT license)
- [toidentifier](https://npmjs.org/toidentifier): Convert a string of words to a JavaScript identifier (MIT license)
- [tough-cookie](https://npmjs.org/tough-cookie): RFC6265 Cookies and Cookie Jar for node.js (BSD-3-Clause license)
- [tr46](https://npmjs.org/tr46): An implementation of the Unicode UTS #46: Unicode IDNA Compatibility Processing (MIT license)
- [@jridgewell/trace-mapping](https://npmjs.org/@jridgewell/trace-mapping): Trace the original position through a source map (MIT license)
- [@sindresorhus/transliterate](https://npmjs.org/@sindresorhus/transliterate): Convert Unicode characters to Latin characters using transliteration (MIT license)
- [tree-kill](https://npmjs.org/tree-kill): kill trees of processes (MIT license)
- [tree-kill-promise](https://npmjs.org/tree-kill-promise): Simple wrapper around the tree-kill module that makes use of promises. (MIT license)
- [trouter](https://npmjs.org/trouter): 🐟 A fast, small-but-mighty, familiar ~fish~ router (MIT license)
- [ts-api-utils](https://npmjs.org/ts-api-utils): Utility functions for working with TypeScript's API. Successor to the wonderful tsutils. 🛠️️ (MIT license)
- [type-check](https://npmjs.org/type-check): type-check allows you to check the types of JavaScript values at runtime with a Haskell like type syntax. (MIT license)
- [type-fest](https://npmjs.org/type-fest): A collection of essential TypeScript types ((MIT OR CC0-1.0) license)
- [type-is](https://npmjs.org/type-is): Infer the content-type of a request. (MIT license)
- [typed-array-buffer](https://npmjs.org/typed-array-buffer): Get the ArrayBuffer out of a TypedArray, robustly. (MIT license)
- [typed-array-byte-length](https://npmjs.org/typed-array-byte-length): Robustly get the byte length of a Typed Array (MIT license)
- [typed-array-byte-offset](https://npmjs.org/typed-array-byte-offset): Robustly get the byte offset of a Typed Array (MIT license)
- [typed-array-length](https://npmjs.org/typed-array-length): Robustly get the length of a Typed Array (MIT license)
- [@typescript-eslint/types](https://npmjs.org/@typescript-eslint/types): Types for the TypeScript-ESTree AST spec (MIT license)
- [typescript](https://npmjs.org/typescript): TypeScript is a language for application scale JavaScript development (Apache-2.0 license)
- [@typescript-eslint/typescript-estree](https://npmjs.org/@typescript-eslint/typescript-estree): A parser that converts TypeScript source code into an ESTree compatible form (BSD-2-Clause license)
- [uc.micro](https://npmjs.org/uc.micro): Micro subset of unicode data files for markdown-it projects. (MIT license)
- [uid-safe](https://npmjs.org/uid-safe): URL and cookie safe UIDs (MIT license)
- [unbox-primitive](https://npmjs.org/unbox-primitive): Unbox a boxed JS primitive value. (MIT license)
- [undici-types](https://npmjs.org/undici-types): A stand-alone types package for Undici (MIT license)
- [unimported](https://npmjs.org/unimported): Scans your nodejs project folder and shows obsolete files and modules (MIT license)
- [unpipe](https://npmjs.org/unpipe): Unpipe a stream from all destinations (MIT license)
- [uri-js](https://npmjs.org/uri-js): An RFC 3986/3987 compliant, scheme extendable URI/IRI parsing/validating/resolving library for JavaScript. (BSD-2-Clause license)
- [@polka/url](https://npmjs.org/@polka/url): Super fast, memoized `req.url` parser (MIT license)
- [undefined](https://npmjs.org/undefined)
- [util-deprecate](https://npmjs.org/util-deprecate): The Node.js `util.deprecate()` function with browser support (MIT license)
- [uuid](https://npmjs.org/uuid): RFC4122 (v1, v4, and v5) UUIDs (MIT license)
- [@bcoe/v8-coverage](https://npmjs.org/@bcoe/v8-coverage): Helper functions for V8 coverage files. (MIT license)
- [v8-to-istanbul](https://npmjs.org/v8-to-istanbul): convert from v8 coverage format to istanbul's format (ISC license)
- [validate-npm-package-license](https://npmjs.org/validate-npm-package-license): Give me a string and I'll tell you if it's a valid npm package license string (Apache-2.0 license)
- [vary](https://npmjs.org/vary): Manipulate the HTTP Vary header (MIT license)
- [@typescript-eslint/visitor-keys](https://npmjs.org/@typescript-eslint/visitor-keys): Visitor keys used to help traverse the TypeScript-ESTree AST (MIT license)
- [vlq](https://npmjs.org/vlq): Generate, and decode, base64 VLQ mappings for source maps and other uses (MIT license)
- [w3c-xmlserializer](https://npmjs.org/w3c-xmlserializer): A per-spec XML serializer implementation (MIT license)
- [@one-ini/wasm](https://npmjs.org/@one-ini/wasm): Parse EditorConfig-INI file contents into AST (MIT license)
- [watcher](https://npmjs.org/watcher): The file system watcher that strives for perfection, with no native dependencies and optional rename detection support.
- [water.css](https://npmjs.org/water.css): A drop-in collection of CSS styles to make simple websites just a little nicer (MIT license)
- [wcwidth](https://npmjs.org/wcwidth): Port of C's wcwidth() and wcswidth() (MIT license)
- [webidl-conversions](https://npmjs.org/webidl-conversions): Implements the WebIDL algorithms for converting to and from JavaScript values (BSD-2-Clause license)
- [whatwg-encoding](https://npmjs.org/whatwg-encoding): Decode strings according to the WHATWG Encoding Standard (MIT license)
- [whatwg-mimetype](https://npmjs.org/whatwg-mimetype): Parses, serializes, and manipulates MIME types, according to the WHATWG MIME Sniffing Standard (MIT license)
- [whatwg-url](https://npmjs.org/whatwg-url): An implementation of the WHATWG URL Standard's URL API and parsing machinery (MIT license)
- [which](https://npmjs.org/which): Like which(1) unix command. Find the first instance of an executable in the PATH. (ISC license)
- [which-boxed-primitive](https://npmjs.org/which-boxed-primitive): Which kind of boxed JS primitive is this? (MIT license)
- [which-collection](https://npmjs.org/which-collection): Which kind of Collection (Map, Set, WeakMap, WeakSet) is this JavaScript value? Works cross-realm, without `instanceof`, and despite Symbol.toStringTag. (MIT license)
- [which-typed-array](https://npmjs.org/which-typed-array): Which kind of Typed Array is this JavaScript value? Works cross-realm, without `instanceof`, and despite Symbol.toStringTag. (MIT license)
- [word-wrap](https://npmjs.org/word-wrap): Wrap words to a specified length. (MIT license)
- [wrap-ansi](https://npmjs.org/wrap-ansi): Wordwrap a string with ANSI escape codes (MIT license)
- [wrap-ansi](https://npmjs.org/wrap-ansi): Wordwrap a string with ANSI escape codes (MIT license)
- [wrappy](https://npmjs.org/wrappy): Callback wrapping utility (ISC license)
- [ws](https://npmjs.org/ws): Simple to use, blazing fast and thoroughly tested websocket client and server for Node.js (MIT license)
- [xml-name-validator](https://npmjs.org/xml-name-validator): Validates whether a string matches the production for an XML name or qualified name (Apache-2.0 license)
- [xmlchars](https://npmjs.org/xmlchars): Utilities for determining if characters belong to character classes defined by the XML specs. (MIT license)
- [y18n](https://npmjs.org/y18n): the bare-bones internationalization library used by yargs (ISC license)
- [yaml](https://npmjs.org/yaml): JavaScript parser and stringifier for YAML (ISC license)
- [yargs](https://npmjs.org/yargs): yargs the modern, pirate-themed, successor to optimist. (MIT license)
- [yargs-parser](https://npmjs.org/yargs-parser): the mighty option parser used by yargs (ISC license)
- [yesno](https://npmjs.org/yesno): A simple library for asking boolean questions in cli programs (BSD license)
- [yocto-queue](https://npmjs.org/yocto-queue): Tiny queue data structure (MIT license)

### Licenses

- MIT (405 packages)
- ISC (40 packages)
- Apache-2.0 (15 packages)
- BSD-2-Clause (14 packages)
- BSD-3-Clause (12 packages)
- AGPL-3.0-or-later (3 packages)
- BlueOak-1.0.0 (2 packages)
- Unlicense (2 packages)
- BSD-3-Clause OR GPL-2.0 (2 packages)
- AGPL-3.0 (2 packages)
- Python-2.0 (1 package)
- Apache-2.0 AND MIT (1 package)
- BSD 2-Clause (1 package)
- CC0-1.0 (1 package)
- CC-BY-3.0 (1 package)
- MIT OR CC0-1.0 (1 package)
- BSD (1 package)
