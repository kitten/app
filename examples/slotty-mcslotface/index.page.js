import Slotty from './Slotty.component.js'
import Navigation from './Navigation.component.js'

export default () => kitten.html`
  <${Slotty} />
  <${Navigation} currentPage='Normal'/>
`
