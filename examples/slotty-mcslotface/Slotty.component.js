export default ({ SLOT }) => kitten.html`
  <if ${SLOT.head !== undefined}>
    <then>
      ${SLOT.head}
    <else>
      <img src='/images/head.png' alt='Head'>
  </if>

  <if ${SLOT.eyes !== undefined}>
    <then>
      ${SLOT.eyes}
    <else>
      <img src='/images/eyes.png' alt='Eyes'>
  </if>

  <if ${SLOT.mouth !== undefined}>
    <then>
      ${SLOT.mouth}
    <else>
      <img src='/images/mouth.png' alt='Mouth'>
  </if>

  <style>
    img { display: block; margin-left: auto; margin-right: auto;}
  </style>
`