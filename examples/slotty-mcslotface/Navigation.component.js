const pages = [
  {name: 'Normal', link: '/'},
  {name: 'Disguised', link: '/disguised'}
]

export default ({ currentPage }) => kitten.html`
  <nav class='Navigation'>
    ${pages.map(page => kitten.html`
      <if ${currentPage !== page.name}>
        <a href='${page.link}' class=''>${page.name}</a>
      <else>
        <span>${page.name}</span>
      </if>
    `)}
  </nav>

  <style>
    .Navigation {
      font-family: sans-serif;
      text-align: center;
      margin-top: 0.5em;
      font-size: 3em;
    }

    .Navigation a {
      color: deeppink;
    }

    .Navigation :first-child {
      margin-right: 1em;
    }

    .Navigation span {
      font-weight: bold;
    }

    .Navigation span::before {
      content: '▶'
    }
  </style>
`