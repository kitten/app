import Slotty from './Slotty.component.js'
import Navigation from './Navigation.component.js'

export default () => kitten.html`
  <${Slotty}>
    <content for='head'>
      <img src='/images/disguised/head.png' alt='Head with hat.'>
    </content>
    <content for='eyes'>
      <img src='/images/disguised/eyes.png' alt='Eyes with glasses.'>
    </content>
    <content for='mouth'>
      <img src='/images/disguised/mouth.png' alt='Mouth with moustache.'>
    </content>
  </>
  <${Navigation} currentPage='Disguised' />
`