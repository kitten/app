// An app about kittens is an edge case so import kitten global as _
// to avoid misunderstandings between when we’re using the library
// and when we’re talking about actual (well, virtual) kittens.

import _ from '@small-web/kitten'

// The great advantage of using database app modules is that your
// database can be strongly typed. View the type of the kitten variable
// in the html template, below, for example, to confirm it is a Kitten instance. 

/** @type {import('./app_modules/database/database.js').DatabaseSchema} */
const db = _.db

export default () => _.html`
  <h1>Kittens</h1>

  <ul>
    ${db.kittens.map(kitten => _.html`
      <li>${kitten}</li>
    `)}
  </ul>
`
