/**
  Database.

  Strongly-typed database module that wraps Kitten’s untyped global db instance.
  (See https://codeberg.org/kitten/app#using-javascript-database-jsdb-a-not-so-scary-database)

  The initialise() method is a hook that’s automatically called by Kitten when starting the server.
  It should not be called manually from within Domain. TODO: Document this in Kitten.
*/

import path from 'node:path'
import JSDB from '@small-tech/jsdb'

export class Kitten {
  constructor ({ name = '', age = 0 } = {}) {
    this.name = name
    this.age = age
  }

  toString () {
    return `${this.name} (${this.age} year${this.age === 1 ? '' : 's'} old)`
  }
}

/**
  @typedef {object} DatabaseSchema

  @property {Database} database
  @property {Array<Kitten>} kittens
*/

class Database {
  initialised = false

  constructor (parameters) {
    Object.assign(this, parameters)
  }
}


// When the database is being opened by the db commands, we don’t compact it.
const compactOnLoad = process.env.jsdbCompactOnLoad === 'false' ? false : true

export const db = /** @type {DatabaseSchema} */ (
  JSDB.open(
    path.join(globalThis.kitten.paths.APP_DATA_DIRECTORY, 'db'),
    {
      compactOnLoad, 
      classes: [
        Database,
        Kitten
      ]
    }
  )
)

export async function initialise () {
  if (!db.database) {
    db.database = new Database() 
  }

  if (!db.database.initialised) {
    db.kittens = [
      new Kitten({name: 'Fluffy', age: 1}),
      new Kitten({name: 'Ms. Meow', age: 3}),
      new Kitten({name: 'Whiskers', age: 7})
    ]
    db.database.initialised = true
    console.info(`\n  • Database initialised.`)
  }

  return db
}

export default db
