import kitten from '@small-web/kitten'

let count = 1

/**
  @param {{
    request: import('@small-web/kitten').KittenRequest,
    response: import('@small-web/kitten').KittenResponse
  }} parameters
*/
export default ({request, response}) => {
  console.info('Request:', request, 'Response:', response)

  return kitten.html`
    <h1>Kitten count</h1>
    <p>${'🐱️'.repeat(count++)}</p>
  `
}
