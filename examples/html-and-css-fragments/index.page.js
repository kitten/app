import Styles from './Styles.fragment.css'
import Markup from './Markup.fragment.html'

export default () => kitten.html`
  <page css>
  <h1>HTML and CSS fragments</h1>

  <${Markup}>
    <div>I’m content from index.page.js for the default slot.</div>
    <content for='other'>
      <div>I’m content from index.page.js for a named slot.</div>
    </content>
  </>

  <${Styles} />

  <style>
    /* Override styles from the imported style fragment. */
    :root {
      --accent-colour: lightblue;
      --border-width: 4px;
    }
  </style>
`
