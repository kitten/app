function ComponentA ({name, SLOT}) {
  return kitten.html`
    <div class='ComponentA'>
      <h2>Hello ${name}, this is Component A.</h2>
      <blockquote>
        <p>⬇⬇⬇ Slot in Component A starts here. ⬇⬇⬇</p>
        ${SLOT}
        <p>⬆⬆⬆ Slot in Component A ends here. ⬆⬆⬆</p>
      </blockquote>
    </div>
    <style>
      .ComponentA {
        padding: 1em;
        border: 1px solid cadetblue;
      }
    </style>
  `
}

function ComponentB ({name, SLOT}) {
  return kitten.html`
    <div class='ComponentB'>
      <ul>
        <li>This</li>
        <li>is…</li>
        ${SLOT}
        <li>…component</li>
        <li>B.</li>
      </ul>
    </div>
    <style>
      .ComponentB { border: 1px dashed green; }
      .ComponentB li { color: green; }
    </style>
  `
}

export default () => kitten.html`
  <h1>Page</h1>
  <p>This is the page.</p>

  <${ComponentA} name=Aral>
    <hr>
    <h3>This is slotted content defined on the page.</h3>

    <blockquote>
      <${ComponentB}>
        <li><strong>More slotted content</strong></li>
        <li><strong>from the page.</strong></li>
      </${ComponentB}>
    </blockquote>

    <p>This is still slotted content defined on the page.</p>
    <hr>
  </>

  <p>And we’re back in the page.</p>

  <p>Pretty cool no?</p>

  <p class='kittenmoji'>🐱 💕</p>

  <style>
    hr { border: 1px dotted green; }

    /* Example of how to override the style of slotted content by using the CSS cascade. */
    li strong { color: black; }

    .kittenmoji { font-size: 2em; }
  </style>
`
