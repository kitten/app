/**
  Updates displayed date when button is pressed.

  Showcases how events bubble up from pages to connected components.
*/

class CurrentDate extends kitten.Component {
  html () {
    return kitten.html`
      <h1 morph>${new Date()}</h1>
    `
  }

  onUpdateCurrentDate () {
    // Stream component to page.
    this.update()
  }
}

export default class MyPage extends kitten.Page {
  html () {
    return kitten.html`
      <page css>

      <${CurrentDate.connectedTo(this)} />

      <button
        name='updateCurrentDate'
        connect
      >Update date</button>
    `
  }
}
