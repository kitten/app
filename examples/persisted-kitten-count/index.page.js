if (!kitten.db.kittens) kitten.db.kittens = { count: 0 }

export default () => kitten.html`
  <h1>Kitten count</h1>
  <p>${'🐱️'.repeat(++kitten.db.kittens.count)}</p>
`
