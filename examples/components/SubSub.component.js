export default ({ parentName = 'unspecified' }) => (
  kitten.html`
    <div class='SubSub component'>
      <h3>I’m a sub-sub component.</h3>
      <p>I’m in the ${parentName} component and I’m orange.</p>
    </div>

    <style>
      .SubSub {
        padding: 0.25em;
        border: 3px solid orangered;
      }

      .SubSub p {
        color: orangered;
      }
    </style>
  `
)
