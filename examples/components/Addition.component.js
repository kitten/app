import SubSub from './SubSub.component.js'

export default ({firstOperand, secondOperand}) => (
  kitten.html`
    <div class='Addition component'>
      <h2>Addition</h2>
      <${SubSub} parentName=addition />
      <p>${firstOperand} + ${secondOperand} = ${Number(firstOperand) + Number(secondOperand)}</p>
    </div>

    <style>
      .Addition {
        padding: 0.25em;
        border: 3px solid green;
      }
      h2 {
        color: green;
      }
    </style>
  `
)
