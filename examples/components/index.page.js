import Addition from './Addition.component.js'
import Subtraction from './Subtraction.component.js'

export default () => {
  const firstOperand = Math.ceil(Math.random()*100)
  const secondOperand = Math.ceil(Math.random()*100)

  return kitten.html`
    <page css syntax-highlighter>

    <h1>Components, etc.</h1>

    <p>This example demonstrates use of components and other rendering.</p>
  
    <pre><code>
         ███                              ███      
       ███░                              ░░░███    
     ███░    ████████  ████████   ██████   ░░░███  
   ███░     ░░███░░███░░███░░███ ███░░███    ░░░███
  ░░░███     ░███ ░███ ░███ ░░░ ░███████      ███░ 
    ░░░███   ░███ ░███ ░███     ░███░░░     ███░   
      ░░░███ ░███████  █████    ░░██████  ███░     
        ░░░  ░███░░░  ░░░░░      ░░░░░░  ░░░       
             ░███                                  
             █████                                 
            ░░░░░         

            <strong>this
                is</strong>
                  preformatted!
    </code></pre>

    <p>A paragraph</p>

    <markdown>
      ## Markdown

      ### Table of contents

      [toc]

      You can also add markdown to your pages.

      ### Footnotes

      This makes it easier to author content.[^1]

      ### Lists

      - Such
      - as
      - lists

      ### Code

      \`\`\`js
      export default class PageRoute extends LazilyLoadedRoute {
        async initialise () {
          // One-time lazy initialisation. Imports the handler and,
          // if present, the page template.
          console.time(\`Initial render: \${this.filePath}\`)
          this._handler = (await import(this.filePath)).default
          this._template = this.defaultTemplate
          console.timeEnd(\`Initial render: \${this.filePath}\`)
        }
        //…
      }
      \`\`\`

      <${Addition} firstOperand=1 secondOperand=2 />

      ### Typography

      Subscript (H~2~0), Superscript (29^th^) and typographical niceties like "proper typographical 'quotes'" and symbols ((tm), (c), etc.), ==marked text==, ++inserted text++, ~~strike-through~~

      |        | Rock | Don’t Rock |
      | ------ | ---- | ---------- |
      | Tables |  ✅  |            |

      ### Make sure math doesn’t mess it up.

      I do believe 4 > 2 and is 2 < 4? And what about << and >> and <<< and >>>?
  
      ### Figures

      The following code:

      \`\`\`markdown
      ![Black and white close-up of Aral, a white-passing man with short hair and stubble, looking off camera with a blurred background](https://ar.al/images/aral-432.jpg "Hey, look, it’s a-me, Mario! Umm… Aral.")
      \`\`\`

      Creates the following figure with a figure caption:
  
      ![Black and white close-up of Aral, a white-passing man with short hair and stubble, looking off camera with a blurred background](https://ar.al/images/aral-432.jpg "Hey, look, it’s a-me, Mario! Umm… Aral.")
  
      [This is a link to my site](https://ar.al), as is this: https://ar.al.[^2]

      > And other stuff like blockquotes, etc.

      You know, the regular markdown things.

      [^1]: This is a footnote.
      [^2]: This is also a footnote.
    </markdown>

    <label for='textarea-test'>Text area test</label>
    <textarea id='textarea-test'>
      Does
        it
          preserve
            formatting?
    </textarea>

    <h2 id='components'>Components</h2>

    <ul>
      <li>Idiom: components have a root element with <code>class='ComponentName component'</code>.</li>
      <li>You scope styles using the above idiom and CSS class prefixes.</li>
      <li>You can pass values to components by defining properties (“props”).</li>
    </ul>

    <${Addition} firstOperand=1 secondOperand=2 />
    <${Addition} firstOperand=2 secondOperand=2 />

    <${Subtraction} firstOperand=${firstOperand} secondOperand=${secondOperand}/>

    <style>
      body {
        width: 80ch;
        font-family: sans-serif;
      }

      h1 {
        background-color: blueviolet;
        color: white;
        padding: 0.5em;
      }

      p {
        line-height: 1.5;
      }

      blockquote {
        margin-left: 0;
        padding-left: 1em;
        border-left: 5px solid green;
      }

      table { border: 1px solid black; width: 100%; border-spacing: 0; }
      td    { text-align: center; }
      th    { border-bottom: 1px solid black; }

      pre {
        width: 80ch;
        white-space: pre-wrap;
        border: 3px solid #ccc;
        font-size: 1.25em;
      }

      figure {
        border: 1px solid black;
        max-width: 25ch;
      }

      img {
        max-width: 100%;
      }

      label {
        display: block;
      }

      figcaption {
        text-align: center;
      }

      /* Based on our naming convention, this applies to all components. */
      .component {
        margin-bottom: 1em;
      }
    </style>
  `
}
