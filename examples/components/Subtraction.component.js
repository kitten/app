import SubSub from './SubSub.component.js'

export default ({firstOperand, secondOperand}) => (
  kitten.html`
    <div class='Subtraction component'>
      <h2>Subtraction</h2>
      <${SubSub} parentName=subtraction />
      <p>${firstOperand} - ${secondOperand} = ${firstOperand - secondOperand}</p>
    </div>

    <style>
      .Subtraction {
        padding: 0.25em;
        border: 3px solid blue;
      }
      .Subtraction h2 {
        color: blue;
      }
    </style> 
  `
)
