// Demonstration of the forgiving HTML parsing in the
// html template tag (xhtm; https://github.com/dy/xhtm).
// Note: comments are stripped out of resulting markup.
const strong = 'strong'

export default () => kitten.html`
  <h1>Forgive me, HTML…</h1>

  <!-- Self-closing tags -->
  <h2>Self-closing tags:</h2>
  <label for='short-and-sweet-textbox'>Short and sweet textbox:</label>
  <input id='short-and-sweet-textbox' type='text' value='I am a textbox, short and sweet.'>
  <hr>

  <!-- Optionally-closed tags -->
  <h2>Optionally-closed tags:</h2>
  <ul>
    <li>Hello
    <li>there!
  </ul>

  <!-- Interpolation in attributes -->
  <h2>Interpolation in attributes:</h2>
  <p class='red ${strong} emphasised'>This should be in bold red italics.</p>

  <style>
    .red { color: red; }
    .strong { font-weight: bold; }
    .emphasised { font-style: italic; }
    input[type=text] { width: 30ch; }
    p::before { content: "[BEFORE PARAGRAPH] "}
  </style>
`