/**
  A button component that demonstrates passing default HTML attributes to
  an element within your component (so people can listen for events, etc.,
  and use it with frameworks like Alpine.js).

  @param {{ label: string }} props
*/
export default function ({label = 'Missing label prop (label="…")', ...props}) {
  return kitten.html`
    <div class='MyButton'>
      <button class='MyButton' ...${props}>${label}</button>
      <style>
        .MyButton button {
          border: 2px solid darkviolet;
          color: darkviolet;
          border-radius: 0.25em;
          padding: 0.25em 0.5em;
          background: transparent;
          font-size: 2em;
        }
      </style>
    </div>
  `
}
