import MyButton from './MyButton.component.js'

export default () => kitten.html`
  <page alpinejs>

  <h1>My button</h1>

  <${MyButton}
    label='Press me!' 
    x-data
    @click='alert("Thank you for pressing me!")'
  />
`
