import Colours from './fragments/Colours.fragment.js'
import Characters from './fragments/Characters.fragment.js'
import { pascalCaseToKebapCase, sentenceCaseToKebapCase } from './utils.script.js' 

// @ts-ignore CSS
import Styles from './index.fragment.css'

const kitten = /** @type { import('./lib/index.js').Kitten } */ (globalThis.kitten)

const Toast = ({SLOT}) => kitten.html`
  <div id='toast' morph='settle:2s' aria-live='assertive'>
    ${SLOT}
  </div>
`

const SelectedMood = ({ mood }) => kitten.html`
  <span id='selected-mood' class='selectedMood' morph>: ${mood}</span>
`

const MoodInput = ({ selectedMood }) => kitten.html`
  <input
    id=mood
    name=mood
    type=range
    list=moods-list
    min=0
    max=6
    value=${Moods.list.indexOf(selectedMood)}
    aria-valuetext=${selectedMood}
    connect
    trigger='input, change'
    morph
  >
`

const Moods = ({ selectedMood }) => kitten.html`
  <label for=mood>Mood<${SelectedMood} mood=${selectedMood} /></label>
  <${MoodInput} selectedMood=${selectedMood} />
  <!--
    We can’t just style the datalist and have to repeat the labels
    because Safari will not visually display a datalist no matter what.

    Given the data list contains all the values that will be read out
    and this is just a visual enhancement, we mark it as presentational.
  -->
  <div id=mood-labels aria-hidden=true>
    ${Moods.list.map(moodId => kitten.html`
      <span>${Moods.label(moodId)}</span>
    `)}
  </div>
  <datalist id=moods-list>
    ${Moods.list.map((moodId, index) => kitten.html`
      <option value=${index} label=${Moods.label(moodId)}></option>
    `)}
  </datalist>
`
Moods.label = (/** @type {string} */ moodId) => moodId[0].toUpperCase() + moodId.slice(1)
Moods.list = [ 'sad', 'shocked', 'happy', 'blissful', 'lovestruck', 'excited', 'ko' ]
Moods.prettyString = () => Moods.list.reduce((prettyString, mood, index) => prettyString + (index === Moods.list.length - 1 ? `or, '${mood}'.` : `'${mood}', `),'One of ')


const theCode = ({ character, colour, mood }) => `import { ${Characters.map[character].class} } from 'kitten-kawaii'

export default () => kitten.html\`
  <\${${Characters.map[character].class}} mood='${mood}' colour='${Colours.map[colour].hexValue}' />
\``
theCode.escaped = ({ character, colour, mood }) => theCode({ character, mood, colour}).replace(/\$/g, '\\$').replace(/`/g, '\\`')
theCode.Component = ({ character, colour, mood }) => [kitten.md.render('```javascript\n' + theCode({ character, colour, mood }) + '\n```')]


const Details = {
  code: 'Code',
  ['run-locally']: 'Run it locally',
  api: 'API'
}

const shellCommands = () => `mkdir kawaii && cd kawaii
kitten-npm init -y
kitten-npm install kitten-kawaii
touch index.page.js`
shellCommands.markdown = () => kitten.md.render(`\`\`\`shell\n${shellCommands()}\n\`\`\``)

const DetailTabs = ({ character, colour, mood, detail }) => /** @type {string} */ (kitten.html`
  <div id='detail-tabs'>
    <ul role='tablist'>
      <li>
        <button
          id=code-tab
          name=detail
          connect
          data='{value: "code"}'
          role=tab
          aria-selected=${detail === 'code' ? 'true' : 'false'}
          aria-controls=code-tab-panel
        >${Details.code}</button>
      </li>
      <li>
        <button
          id=run-it-locally-tab
          name=detail
          connect
          data='{value: "run-locally"}'
          role=tab
          aria-selected=${detail === 'run-locally' ? 'true' : 'false'}
          aria-controls=run-it-locally-tab-panel
        >${Details['run-locally']}</button>
      </li>
    </ul>

    <div class=tabs>
      <div
        id=code-tab-panel
        class='detailTab ${detail === 'code' ? 'active' : ''}'
        aria-labelledby=code-tab
      >
        <div class=copyableCode aria-live=polite>
          <${theCode.Component} character=${character} mood=${mood} colour=${colour} />
          <button
            name=copy
            onclick="navigator.clipboard.writeText(\`theCode\`)"
            connect
          >Copy</button>
        </div>
        <markdown>
          | Prop   | Type                     | Default  |
          | ------ | ------------------------ | -------- |
          | size   | CSS size                 | 100%     |
          | colour | CSS colour               | #ffd882  |
          | mood   | String                   | blissful |
        </markdown>
      </div>

      <div
        id=run-it-locally-tab-panel
        class='detailTab ${detail === 'run-locally' ? 'active' : ''}'
        aria-labelledby=run-it-locally-tab
      >
        <markdown>
          1. <a href='https://kitten.small-web.org' target='_blank'>Install Kitten</a>

          2. In terminal:

              <div class='copyableCode'>
                ${[shellCommands.markdown()]}
                <button name='copy' onclick="navigator.clipboard.writeText(\`shellCommands\`)" connect>Copy</button>
              </div>

          3. <button name='copy' connect class='copyButton' onclick="navigator.clipboard.writeText(\`theCode\`)">Copy</button> <a name='detail' data='{value: "code"}' connect href='../code'>the character code</a> into _index.page.js_

          4. Run \`kitten --open\` and view the character in your browser.
        </markdown>
      </div>
    </div>

    <footer>
      <markdown>
        _[Kitten](https://kitten.small-web.org) port by [ar.al](https://ar.al) of [React Kawaii](https://react-kawaii.vercel.app) by [Miuki Miu](https://www.miukimiu.com) ⏵ Blog post ⏵ [View source](https://codeberg.org/aral/kitten-kawaii)_
      </markdown>
    </footer>
  </div>
`)
// Highlight colours in code using the colour itself as the background.
.replaceAll(/(#[a-f0-9]{6})/g, '<span style="background-color: $1; color: black; padding: 0.25em; border-radius: 0.5em;">$1</span>')
// Add the code without any markup to the client-side copy-to-clipboard function.
.replaceAll(/theCode/g, theCode.escaped({ character, mood, colour }))
.replace('shellCommands', shellCommands())

const DetailsDrawer = ({ character, colour, mood, show, detail }) => kitten.html`
  <div
    id=details-drawer
    style="grid-template-rows: ${show ? '1fr' : '0fr' };"
    aria-hidden=${show ? 'false' : 'true'}
    morph
  >
    <${Toast} />
    <${DetailTabs}
      character=${character}
      colour=${colour}
      mood=${mood}
      detail=${detail} />
  </div>
`


const TheCharacter = ({ character, colour, mood }) => kitten.html`
  <${Characters.get(character)}
    id='the-character'
    name='swipeCharacter'
    size='100%'
    mood=${mood}
    colour=${Colours.map[colour].hexValue}
    connect
    data='{theCharacter:"${character}"}'
    trigger='swiped-left, swiped-right'
    morph
  />
`

const URLState = ({ initial = false, character='cat', mood='blissful', colour='green', showDetails='false', detail='code' } = {}) => {
  const characterLink = `/character/${mood}/${sentenceCaseToKebapCase(colour)}/${pascalCaseToKebapCase(character)}/${showDetails ? `details/${detail}/` : ''}`
  return kitten.html`
    <script id='update-state'>
      if (${initial ? 'false' : 'true'}) {
        history.pushState({}, '', '${characterLink}')
      }
    </script>
  `
}


const Title = ({ character, mood, colour, showDetails, detail }) => kitten.html`
  <script id='title'>
    document.title = \`Kitten Kawaii character: ${Moods.label(mood)} ${colour.replace('-', ' ')} ${character.replace('-', ' ')}${showDetails ? ` (showing ${detail === 'code' ? 'code' : 'instructions on running it locally'})` : ''}\`
  </script>
`



const ToggleDetailsButton = ({ showDetails }) => kitten.html`
  <button
    id='toggle-details'
    name='toggleDetails'
    aria-expanded=${showDetails ? 'true' : 'false' }
    aria-controls=details-drawer
    connect
    morph
  >${showDetails ? 'Hide details' : 'Show details'}</button>
`

const DownloadLink = ({ page }) => {
  const downloadLink = `/download/${page.data.mood}/${sentenceCaseToKebapCase(page.data.colour)}/${pascalCaseToKebapCase(page.data.character)}/`

  return kitten.html`
    <a
      id='downloadLink'
      href=${downloadLink}
      download
      class=downloadButton
      title=Download
      name=download
      morph
    >
      <svg
        width="1em"
        height="1em"
        stroke-width="2"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        color="#000000"
      >
        <title>Download</title>
        <desc>Download your character as SVG.</desc>
        <path d="M6 20L18 20" stroke="var(--text-main)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
        <path d="M12 4V16M12 16L15.5 12.5M12 16L8.5 12.5" stroke="var(--text-main)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
      </svg>
      <style>
        .downloadButton {
          display: inline-block;
          /* Match look to regular button */
          outline: none;
          background-color: var(--button-base);
          border-radius: 6px;
          margin-bottom: 6px;
          margin-right: 6px;
          padding-top: 0.75em;
          padding-left: 0.75em;
          padding-right: 0.75em;
        }

        .downloadButton:hover {
          background-color: var(--button-hover);
        }

        .downloadButton:active {
          transform: translateY(2px);
        }

        .downloadButton:focus, .downloadButton:focus-visible {
          box-shadow: 0 0 0 2px var(--focus);
        }
      </style>
    </a>
  `
}

export default function () {
  // Validate state.
  if (
    this.request.params.name === undefined
    || this.request.params.mood === undefined
    || this.request.params.colour === undefined
    || Characters.map[this.request.params.name] === undefined
    || !Moods.list.includes(this.request.params.mood)
    || Colours.map[this.request.params.colour] === undefined
  ) {
    // Hmm, invalid state. Let’s show a shocked browser ;)
    // (It’s not a big deal so keep things flowing.)
    return this.response.redirect('/character/shocked/cotton-candy/browser')
  }

  // We want the configuration to be unique for each loaded page so
  // we store it in Kitten’s ephemeral page storage.
  const character = this.data.character = this.request.params.name
  const mood = this.data.mood =  this.request.params.mood
  const colour = this.data.colour = this.request.params.colour
  const detail = this.data.detail = (this.request.params.detail || 'code')
  const showDetails = this.data.showDetails = (this.request.params.details !== undefined)

  return kitten.html`
    <page css syntax-highlighter title='Kitten Kawaii'>
    <${Title} character=${character} colour=${colour} mood=${mood} showDetails=${showDetails} detail=${detail} />
    <div id='stage' aria-live='polite'>
      <${TheCharacter} character=${character} colour=${colour} mood=${mood}/>
      <div id='controls'>
        <div id='characters-list-and-toggle-details-button'>
          <${Characters} selectedCharacter=${character} />
          <${DownloadLink} page=${this} />
          <${ToggleDetailsButton} showDetails=${showDetails} />
        </div>
        <${Colours} selectedColour=${colour} />
        <${Moods} selectedMood=${mood} />
      </div>
      <${DetailsDrawer} character=${character} colour=${colour} mood=${mood} show=${showDetails} detail=${detail}/>
    </div>
    <${URLState} initial />
    <${Styles} />
    <!--
    Swiped Events library by John Doherty
    https://github.com/john-doherty/swiped-events
    -->
    <script src='/swiped-events.min.js'></script>
    <script>
      window.addEventListener('popstate', state => document.location.reload())
    </script>
  `
}

/**
  These are components sent in every handler to update the main interface state of the page.
*/
function updateMainState({ page, character, colour, mood, showDetails, detail }) {
  page.send(kitten.html`<${URLState} character=${character} colour=${colour} mood=${mood} showDetails=${showDetails} detail=${detail} />`)
  page.send(kitten.html`<${Title} character=${character} colour=${colour} mood=${mood} showDetails=${showDetails} detail=${detail} />`)
  page.send(kitten.html`<${DetailsDrawer} character=${character} colour=${colour} mood=${mood} show=${showDetails} detail=${detail} />`)
}


/**
  This function gets called when a page makes a socket connection.
*/
export function onCharacter (/** @type {{ character: string }} */ data) {
  const character = data.character
  this.data.character = character
  const colour = this.data.colour
  const mood = this.data.mood
  const showDetails = this.data.showDetails
  const detail = this.data.detail

  this.send(kitten.html`<${TheCharacter} character=${character} colour=${colour} mood=${mood} />`)
  this.send(kitten.html`<${DownloadLink} page=${this} />`)
  updateMainState({ page: this, character, colour, mood, showDetails, detail })
}

export function onSwipeCharacter (/** @type {{ HEADERS: Object<string, string>, theCharacter: string }} */ data) {
  const characters = Characters.keys
  const numCharacters = characters.length
  const indexOffset = data.HEADERS['HX-Trigger-Event'] === 'swiped-left' ? 1 : -1
  const currentIndex = characters.indexOf(data.theCharacter)

  let indexToShow = currentIndex + indexOffset

  // Wrap around if necessary.
  if (indexToShow === -1) indexToShow = numCharacters - 1
  if (indexToShow === numCharacters) indexToShow = 0

  const character = characters[indexToShow]
  this.data.character = character
  const colour = this.data.colour
  const mood = this.data.mood
  const showDetails = this.data.showDetails
  const detail = this.data.detail

  this.send(kitten.html`<${TheCharacter} character=${character} colour=${colour} mood=${mood} />`)
  this.send(kitten.html`<${Characters} selectedCharacter=${character} />`)
  this.send(kitten.html`<${DownloadLink} page=${this} />`)
  updateMainState({ page: this, character, colour, mood, showDetails, detail })
}

export function onMood (/** @type {{ mood: number }} */ data) {
  const mood = Moods.list[data.mood]
  this.data.mood = mood
  const character = this.data.character
  const colour = this.data.colour
  const showDetails = this.data.showDetails
  const detail = this.data.detail

  // We send the MoodInput back so the aria-valuetext can be updated
  // (this makes screen readers read the emotions instead of the
  // numeric values of the slider).
  this.send(kitten.html`<${MoodInput} selectedMood=${mood} />`)
  this.send(kitten.html`<${TheCharacter} character=${character} colour=${colour} mood=${mood}/>`)
  this.send(kitten.html`<${SelectedMood} mood=${mood} />`)
  this.send(kitten.html`<${DownloadLink} page=${this} />`)
  updateMainState({ page:this , character, colour, mood, showDetails, detail })
}

export function onColour (/** @type {{ colour: string }} */ data) {
  const colour = data.colour
  this.data.colour = colour
  const character = this.data.character
  const mood = this.data.mood
  const showDetails = this.data.showDetails
  const detail = this.data.detail

  this.send(kitten.html`<${TheCharacter} character=${character} colour=${colour} mood=${mood}/>`)
  this.send(kitten.html`<${DownloadLink} page=${this} />`)
  updateMainState({ page: this, character, colour, mood, showDetails, detail })
}

export function onToggleDetails () {
  this.data.showDetails = !this.data.showDetails
  const character = this.data.character
  const mood = this.data.mood
  const colour = this.data.colour
  const showDetails = this.data.showDetails
  const detail = this.data.detail

  this.send(kitten.html`<${ToggleDetailsButton} showDetails=${showDetails} />`)
  updateMainState({ page:this , character, colour, mood, showDetails, detail })
}

// Show toast when copy button is pressed.
export function onCopy () {
  this.send(kitten.html`<${Toast}>Copied!</>`)
}

// Handle detail tabs.
export function onDetail (/** @type {{ value: string }} */ data) {
  const detail = data.value
  this.data.detail = detail
  const character = this.data.character
  const mood = this.data.mood
  const colour = this.data.colour
  const showDetails = this.data.showDetails

  updateMainState({ page:this, character, colour, mood, showDetails, detail })
}
