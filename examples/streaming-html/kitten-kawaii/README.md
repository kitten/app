# Kitten Kawaii

This is a [Kitten](https://kitten.small-web.org) port of [Miuki Miu](https://www.miukimiu.com)’s [React Kawaii](https://react-kawaii.vercel.app) project.

View the web site at [kitten-kawaii.small-web.org](https://kitten-kawaii.small-web.org).

Any original code by Miuku Miu (Elizabet Oliveira) is copyright by her and released under the MIT license.

## Usage

  1. <a href='https://kitten.small-web.org' target='_blank'>Install Kitten</a>

  2. In terminal:
      ```shell
      mkdir kawaii && cd kawaii
      kitten-npm init -y
      kitten-npm install kitten-kawaii
      touch index.page.js
      ```

  3. Add the character components you want to _index.page.js_:
  
      ```html
      <${CharacterComponent} mood=[mood] colour=[CSS colour] size=[CSS size] />
      ```
    
      e.g.,

      ```html
        import { CatKigurumi } from 'kitten-kawaii'

        export default () => kitten.html`
          <${CatKigurumi} mood='blissful' colour='#a6e191' />
        `
      ```

  4. Run \`kitten --open\` and view the character in your browser.

### Character components (references):

- Astronaut
- Backpack
- Browser
- Cat
- Chocolate
- CreditCard
- Cyborg
- File
- Folder
- Ghost
- CatKigurumi
- DinosaurKigurumi
- IceCream
- Mug
- Planet
- SpeechBubble

### Moods (string):

- sad
- shocked
- happy
- blissful
- lovestruck
- excited
- ko 

### Colours:

Any valid CSS colour.

## License

Kitten Kawaii is released under [AGPL version 3.0](https://www.gnu.org/licenses/agpl-3.0.html).
