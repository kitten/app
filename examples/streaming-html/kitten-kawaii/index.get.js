// Redirect to default character.
export default ({ response }) => {
  response.redirect('/character/blissful/granny-smith-apple/cat')
}
