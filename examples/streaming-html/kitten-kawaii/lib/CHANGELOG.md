# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.1] - 2024-08-19

### Fixed

  - Regression in colours in character descriptions introduced in 2.0.0.

## [2.0.0] - 2024-08-12

### Changed

Breaking changes; major version bump.

  - Changed names of `HumanCat` and `HumanDinosaur` classes to `CatKigurumi` and `DinosaurKigurumi`.

## [1.0.2] - 2024-08-09

### Added

  - Required type information for Kitten global.

## [1.0.1] - 2024-08-05

### Added

  - Type definition file (index.d.ts).

## [1.0.0] - 2024-08-05

Initial release.

Ported from [React Kawaii](https://github.com/miukimiu/react-kawaii) by [Miuki Miu](https://www.miukimiu.com) (Elizabet Oliveira).
