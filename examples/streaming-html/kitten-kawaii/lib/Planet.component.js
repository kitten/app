import SvgCharacter from './SVGCharacter.component.js'
const kitten = /** @type { import('./index.js').Kitten} */ (globalThis.kitten)

export const Planet = ({
  mood = SvgCharacter.DEFAULT_MOOD,
  colour = SvgCharacter.DEFAULT_COLOUR,
  ...props
} = {}) => kitten.html`
  <${SvgCharacter}
    character=planet
    mood=${mood}
    colour=${colour}
    faceScale=66
    facePosition='87 110'
    ...props=${props}
  >
    <path
      fill=${colour}
      d="M120 187c37.003 0 67-29.997 67-67s-29.997-67-67-67-67 29.997-67 67 29.997 67 67 67Z"
    />
    <path
      fill="#000"
      d="M114.5 186.777c1.814.148 3.648.223 5.5.223 37.003 0 67-29.997 67-67s-29.997-67-67-67c-1.852 0-3.686.075-5.5.222C148.93 56.02 176 84.85 176 120c0 35.151-27.07 63.98-61.5 66.777Z"
      opacity=0.1
    />
  </>
`
