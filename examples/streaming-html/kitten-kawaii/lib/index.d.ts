// @ts-check
export * from './database.js'

// If we wanted full type safety for the global Kitten object,
// we’d add the @small-web/kitten module to our dependencies.
// As-is, we only use the `.html` tagged template (and, on the
// web site, the `md` function to render markdown dynamically)
// so that would be overkill.
//
// Learn more: https://kitten.small-web.org/tutorials/type-safety/
type taggedTemplate = (strings:TemplateStringsArray, ...properties:Any) => string|Array<string>
export type Kitten = { html: taggedTemplate, md: { render: (markdown:string) => string} }
