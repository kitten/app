# Kitten Kawaii

This is a [Kitten](https://kitten.small-web.org) port of [Miuki Miu](https://www.miukimiu.com)’s [React Kawaii](https://react-kawaii.vercel.app) library.

You can view the web site at [kitten-kawaii.small-web.org](https://kitten-kawaii.small-web.org).
Any original code by Miuku Miu (Elizabet Oliveira) is copyright by her and released under the MIT license.

Any modifications by Aral Balkan/the Kitten port and the Kitten Kawaii web site source code is released under [AGPL version 3.0](https://www.gnu.org/licenses/agpl-3.0.html).
