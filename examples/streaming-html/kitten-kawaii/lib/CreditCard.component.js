import SvgCharacter from './SVGCharacter.component.js'
const kitten = /** @type { import('./index.js').Kitten} */ (globalThis.kitten)

export const CreditCard = ({
  mood = SvgCharacter.DEFAULT_MOOD,
  colour = SvgCharacter.DEFAULT_COLOUR,
  ...props
} = {}) => kitten.html`
  <${SvgCharacter}
    character='credit card'
    mood=${mood}
    colour=${colour}
    faceScale=54.33
    facePosition='93.33 121.1'
    ...props=${props}
  >
    <path
      fill=${colour}
      d="M192.959 178.722H48.041c-4.994 0-9.041-4.036-9.041-9.017V70.017C39 65.037 43.047 61 48.04 61h144.92c4.994 0 9.041 4.036 9.041 9.017v99.688c0 4.981-4.047 9.017-9.041 9.017"
    />
    <path
      fill="#000"
      d="M183.904 178.722h9.055c4.994 0 9.041-4.036 9.041-9.017V70.017c0-4.98-4.047-9.017-9.041-9.017h-9.055c4.994 0 9.04 4.036 9.04 9.017v99.688c0 4.981-4.046 9.017-9.04 9.017Z"
      opacity=0.1
    />
    <path fill="#000" d="M39 74.995h163v22.227H39V74.995Z" />
  </>
`
