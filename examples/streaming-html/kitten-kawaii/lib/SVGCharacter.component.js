/**
  Base character traits and functionality
  inherited by all the characters.
*/
const kitten = /** @type { import('./index.js').Kitten} */ (globalThis.kitten)

import ntc from 'ntc-hi-js'
import Face from './Face.component.js'

/**
  Return ratio of component’s face width to that of Figma component SVG.
  The original face’s width in the moods Figma frame corresponds to 66px.

  @param {number} size
*/
const getFaceScale = (size) => {
  return size / 66;
}

const SvgCharacter = ({
  size = SvgCharacter.DEFAULT_SIZE,
  mood = SvgCharacter.DEFAULT_MOOD,
  colour = SvgCharacter.DEFAULT_COLOUR,
  faceScale = undefined,
  facePosition = undefined,
  character = undefined,
  costume = null,
  SLOT = undefined,
  ...props
} = {}) => {
  if (faceScale === undefined) throw new Error('Missing required faceScale property.')
  if (facePosition === undefined) throw new Error('Missing required facePosition property.')
  if (character === undefined) throw new Error('Missing required character property (accessibility).')
  if (SLOT === undefined) throw new Error('Missing character-specific SVG; slot content is undefined.')

  const hasCostume = !(costume === null)
  const colourName = ntc.name(colour).color.name.toLowerCase().replace(/ /g, '-')
  const title = `${character[0].toUpperCase()}${character.slice(1)} character`
  const description = `Cute minimalist vector illustration of ${hasCostume ? 'person wearing ' : ''}${colourName} coloured ${hasCostume ? costume : character}, looking ${mood}.`
  const ariaLabel = `${title}: ${description}`
  
  return kitten.html`
    <svg
      role=img
      aria-label=${ariaLabel}
      xmlns=http://www.w3.org/2000/svg
      xmlns:xlink=http://www.w3.org/1999/xlink
      width=${size}
      height=${size}
      viewBox='0 0 240 240'
      fill=none
      ...props=${props}
    >
      <title>${title}</title>
      <desc>${description}</description>
      ${SLOT}
      <${Face}
        mood=${mood}
        transform=${`translate(${facePosition}) scale(${getFaceScale(faceScale)})`}
      />
    </svg>
  `
}
SvgCharacter.DEFAULT_SIZE = '100%'
SvgCharacter.DEFAULT_MOOD = 'blissful'
SvgCharacter.DEFAULT_COLOUR = '#a6e191'

export default SvgCharacter
