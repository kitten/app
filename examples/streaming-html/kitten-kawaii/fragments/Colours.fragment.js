const kitten = /** @type { import('../lib/index.js').Kitten } */ (globalThis.kitten)

const Colours = ({ selectedColour }) => kitten.html`
  <fieldset id='colour'>
    <legend class='visuallyHidden'>Colour</legend>
    ${Object.entries(Colours.map).map(([id, colour]) => kitten.html`
      <div>
        <label
          for='${id}'
          class='visuallyHidden'
        >${colour.name}</label>
        <input
          id='${id}'
          type='radio'
          name='colour'
          value='${id}'
          connect
          ${id === selectedColour && 'checked'}
          title='${colour.name}'
        >
        <style>
          #${id} {
            background-color: ${colour.hexValue};
          }
          #${id}:checked {
            box-shadow: 0 0 0 0.25em hsl(from ${colour.hexValue} h s calc(l + var(--colour-highlight-offset)));
          }
          #${id}:focus {
            box-shadow: 0 0 0 0.15em hsl(from ${colour.hexValue} h s calc(l + var(--colour-highlight-offset)));
          }
        </style>
      </div>
    `)}
  </fieldset>
`
Colours.map = {
  'granny-smith-apple': { hexValue: '#a6e191', name: 'Granny Smith Apple' },
  'light-pink': { hexValue: '#ffb3ba', name: 'Light Pink' },
  'chardonnay': { hexValue: '#fccb7e', name: 'Chardonnay' },
  'pale-cornflower-blue': { hexValue: '#add2ff', name: 'Pale Cornflower Blue' },
  mauve: { hexValue: '#d7baff', name: 'Mauve' },
  'cotton-candy': { hexValue: '#ffbae1', name: 'Cotton Candy' }
}

export default Colours
