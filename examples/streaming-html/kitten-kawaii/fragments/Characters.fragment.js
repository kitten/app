import * as characters from 'kitten-kawaii'
import { pascalCaseToKebapCase, pascalCaseToSentenceCase } from '../utils.script.js' 
const kitten = /** @type { import('../lib/index.js').Kitten } */ (globalThis.kitten)

const Characters = ({ selectedCharacter }) => kitten.html`
  <div id='characters' morph>
    <label for='character' class='visuallyHidden'>Character</label>
    <select
      id='character'
      name='character'
      connect
    >
      ${Object.entries(Characters.map).map(([id, character]) => kitten.html`
        <option
          value='${id}'
          ${(id === selectedCharacter) && 'selected'}>${character.name}
        </option>
      `)}
    </select>
  </div>
`
Characters.map = Object.fromEntries(Object.entries(characters).map(([id, component ]) => [
  pascalCaseToKebapCase(id), { name: pascalCaseToSentenceCase(id), class: component.name, component }
]))
Characters.keys = Object.keys(characters).map(key => pascalCaseToKebapCase(key))
Characters.get = (/** @type {string} */ id) => Characters.map[id].component

export default Characters
