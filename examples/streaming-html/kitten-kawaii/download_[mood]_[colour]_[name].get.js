import * as characters from 'kitten-kawaii'
import Colours from './fragments/Colours.fragment.js'
import Characters from './fragments/Characters.fragment.js' 
const kitten = /** @type { import('./lib/index.js').Kitten } */ (globalThis.kitten)

export default ({ request, response }) => {
  const mood = request.params.mood
  const colour = request.params.colour
  const character = request.params.name

  if (mood === undefined || colour === undefined || character === undefined) {
    return response.notFound()
  }

  const svg = kitten.html`<${Characters.get(character)}
    mood=${mood}
    colour=${Colours.map[colour].hexValue}
  />`
  const fileName = `${mood}-${colour}-${character}.svg`
  response.file(svg, fileName, 'image/svg+xml')
}
