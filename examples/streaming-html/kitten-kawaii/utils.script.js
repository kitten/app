/**
  Utilities to help with case conversion.
*/

// The component names are in PascalCase. This converts
// them to sentence case for display in the drop-down list.
const capitalLettersExceptTheFirst = /(?!^)([A-Z])/g

export const pascalCaseToSentenceCase = (/** @type {string} */ pascalCase) => {
  // @ts-ignore ES library version mismatch.
  return pascalCase.replaceAll(
    capitalLettersExceptTheFirst,
    (/** @type {string} */ letter) => ' ' + letter.toLowerCase()
  )
}

export const pascalCaseToKebapCase = (/** @type {string} */ pascalCase) => {
  // @ts-ignore ES library version mismatch.
  return pascalCase.replaceAll(
    capitalLettersExceptTheFirst,
    (/** @type {string} */ letter) => '-' + letter
  ).toLowerCase()
}

export const sentenceCaseToKebapCase = (/** @type {string} */ sentenceCase) => {
  // @ts-ignore ES library version mismatch.
  return sentenceCase.replaceAll(/ /g, '-').toLowerCase()
}

export default {
  pascalCaseToSentenceCase,
  pascalCaseToKebapCase,
  sentenceCaseToKebapCase
}
