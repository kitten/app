export default function StatusIndicator () {
  return kitten.html`
    <p>Status: <span 
      id='status'
      x-data='${kitten.js`{ status: "Initialising…" }`}'
      @htmx:ws-connecting.window='${kitten.js`status = "Connecting…"`}'
      @htmx:ws-open.window='${kitten.js`status = "Online"`}'
      @htmx:ws-close.window='${kitten.js`status = "Offline"`}'
      x-text='status'
      :class='${kitten.js`
        status === "Online" ? "online" : status === "Offline" ? "offline" : ""
      `}'>
      Initialising…
    </span></p>

    <style>
      .online {color: green}
      .offline {color: red}
    </style>
  `
}
