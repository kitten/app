import Styles from './index.fragment.css'
import ClientSideJavaScript from './ClientSideJavaScript.fragment.js'
import Message from './Message.component.js'
import StatusIndicator from './StatusIndicator.component.js'

// Ensure the messages table exists in the database before using it.
if (kitten.db.messages === undefined) kitten.db.messages = []

export function onConnect ({ request, page }) {
  page.on('message', message => {
    // A new message has been received: broadcast it to all clients
    // in the same room after performing basic validation.
    if (!isValidMessage(message)) {
      console.warn(`Message is invalid; not broadcasting.`)
      return
    }

    // We don’t need to use the message HEADERS, delete them so
    // they don’t take unnecessary space when we persist the message.
    delete message.HEADERS

    // Persist the message in the messages table.
    kitten.db.messages.push(message)

    // Since we are not optimistically showing messages
    // as sent on the client, we send to `everyone` that’s connected
    // to this page, including the one that sent the message.
    // If we were optimistically updating the messages list,
    // we would call the `send()` method on the page’s `everyoneElse`
    // property instead.
    // 
    // Also, the outer <div /> (any outer element) is stripped by htmx (why?)
    const numberOfRecipients = page.everyone.send(
      kitten.html`
        <div swap-target="beforeend:#messages">
          <${Message} message='${message}' />
        </div>
      `
    )

    // Log the number of recipients message was sent to
    // and make sure we pluralise the log message properly.
    console.info(`🫧 Kitten ${request.originalUrl} message from ${message.nickname} broadcast to `
      + `${numberOfRecipients} recipient`
      + `${numberOfRecipients === 1 ? '' : 's'}.`)
  })
}

// Some basic validation.

// Is the passed object a valid string?
function isValidString(s) {
  return Boolean(s)                // Isn’t null, undefined, '', or 0
    && typeof s === 'string'       // and is the correct type
    && s.replace(/\s/g, '') !== '' // and is not just whitespace.
}

// Is the passed message object valid?
function isValidMessage(m) {
  return isValidString(m.nickname) && isValidString(m.text)
}

// Page.

export default () => kitten.html`
  <page alpinejs>

  <main>
    <h1>🐱 Persisted <a href='https://codeberg.org/kitten/app'>Kitten</a> Chat</h1>

    <${StatusIndicator} />

    <div 
      id='chat'
      x-data='{ showPlaceholder: true }'
    >
      <ul id='messages' @htmx:load='
        showPlaceholder = false
        $el.scrollTop = $el.scrollHeight
      '>
        <if ${kitten.db.messages.length === 0}>
          <then>
            <li id='placeholder' x-show='showPlaceholder'>
              No messages yet, why not send one?
            </li>
          <else>
            ${kitten.db.messages.map(message => kitten.html`<${Message} message=${message} />`)}
        </if>
      </ul>

      <form
        id='message-form'
        name='message'
        connect
      >
        <label for='nickname'>Nickname:</label>
        <input id='nickname' name='nickname' type='text' required />

        <label for='text'>Message:</label>
        <input id='text' name='text' type='text' required
          @htmx:ws-after-send.window='
            $el.value = ""

            // On mobile we don’t refocus the input box
            // in case there is a virtual keyboard so
            // the person can see the message they just sent.
            if (!isMobile()) { $el.focus() }
          '
        />
        <button id='sendButton' type='submit'>Send</button>
      </form>
    </div>
  </main>

  <!--
    Add our content after the Kitten libraries to ensure
    they have loaded before we access them. In this case,
    we need to make sure htmx is loaded.
  -->
  <content for='AFTER_LIBRARIES'>
    <${ClientSideJavaScript} />
  </content>

  <${Styles} />
`
