// Since messages are end-to-end encrypted, decryption occurs on
// the client (in the browser). So we wire up the Alpine.js x-init
// handler and use the magic $nextTick() call to ensure the decryption
// method is called once the DOM node has loaded.
export default function Message ({ message }) {
  return kitten.html`
    <li
      class='Message'
      x-data='{
        messageText: "",
        messageFrom: "",
        messageTo: ""
      }'
      x-init='$nextTick(() => {
        messageFrom = "${message.from}"
        messageTo = "${message.to}"
        decryptMessage("${message.cipherText}", "${message.from}", "${message.to}", $data)
      })'
    >
      <strong><span x-text='messageFrom'></span> → <span x-text='messageTo'></span></strong> <span class='text' x-text='messageText'>${message.cipherText}</span>
    </li>
    <style>
      .Message {
        line-height: 1.5;
        padding-top: 0.25em;
        padding-bottom: 0.25em;
      }
      .Message .text {
        font-family: monospace;
        font-size: 1.5em;
      }
      .Message:nth-of-type(2n) {
        background-color: #ddd;
      }
    </style>
  `
}
