export default function ({ request, response }) {
  const message = request.body
  console.info('📥 /inbox received:', message)
  kitten.events.emit('message', message)
  response.end('ok')
}
