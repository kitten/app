// Since messages are end-to-end encrypted, decryption occurs on
// the client (in the browser). So we wire up the Alpine.js x-init
// handler and use the magic $nextTick() call to ensure the decryption
// method is called once the DOM node has loaded.
export default function Message ({ message }) {
  return kitten.html`
    <li
      x-data='{
        messageText: "${message.cipherText}"
      }'
      x-init='$nextTick(() => messageText = decryptMessage("${message.cipherText}", "${message.from}", "${message.to}"))'
    >
      <strong>${message.from} → ${message.to}</strong> <span x-text='messageText'>${message.cipherText}</span>
    </li>
  `
}
