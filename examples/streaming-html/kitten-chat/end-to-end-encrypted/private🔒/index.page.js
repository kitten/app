import Styles from './index.fragment.css'
import Message from './Message.component.js'
import StatusIndicator from './StatusIndicator.component.js'

// Ensure the messages table exists in the database before using it.
if (kitten.db.messages === undefined) kitten.db.messages = []

export default () => kitten.html`
  <page alpinejs>

  <main>
    <h1>🐱 End-to-end encrypted <a href='https://codeberg.org/kitten/app'>Kitten</a> Chat</h1>

    <${StatusIndicator} />

    <div 
      id='chat'
      x-data='{ showPlaceholder: true }'
      @htmx:ws-config-send.prevent='encryptMessage'
    >
      <ul id='messages' @htmx:load='
        showPlaceholder = false
        $el.scrollTop = $el.scrollHeight
      '>
        <if ${kitten.db.messages.length === 0}>
          <then>
            <li id='placeholder' x-show='showPlaceholder'>
              No messages yet, why not send one?
            </li>
          <else>
            ${kitten.db.messages.map(message => kitten.html`<${Message} message=${message} />`)}
        </if>
      </ul>

      <form
        id='message-form'
        name='message'
        connect
      >
        <label for='domain'>To:</label>
        <input id='domain' name='domain' type='text' required>

        <label for='plainText'>Message:</label>
        <input id='plainText' name='plainText' type='text' required
          @htmx:ws-after-send.window='
            $el.value = ""

            // On mobile we don’t refocus the input box
            // in case there is a virtual keyboard so
            // the person can see the message they just sent.
            if (!isMobile()) { $el.focus() }
          '
        >
        <button id='sendButton' type='submit'>Send</button>
      </form>
      <script type='module' src='./index.js' />
    </div>
  </main>

  <${Styles} />
`

function messageHtml (message) {
  // Wrap the same Message component we use in the page
  // with a node instructing htmx to add this node to
  // the end of the messages list on the page.
  return kitten.html`
    <div hx-swap-oob="beforeend:#messages">
      <${Message} message=${message} />
    </div>
  `
}

// Some basic validation.

// Is the passed object a valid string?
function isValidString(s) {
  return Boolean(s)                // Isn’t null, undefined, '', or 0
    && typeof s === 'string'       // and is the correct type
    && s.replace(/\s/g, '') !== '' // and is not just whitespace.
}

// Is the passed message object valid?
// (We carry out only very basic validation in this example. In a
// real-world app you would do more like check for valid from/to values.)
function isValidMessage(message) {
  return isValidString(message.from) && isValidString(message.to) && isValidString(message.cipherText)
}

export function onConnect ({ request, page }) {
  const saveAndBroadcastMessage = message => {
    // Persist the message in the messages table.
    kitten.db.messages.push(message)

    // Since we are not optimistically showing messages
    // as sent on the client, we send to `everyone` that’s connected
    // to this page, including the one that sent the message.
    // If we were optimistically updating the messages list,
    // we would call the `send()` method on the page’s `everyoneElse`
    // property instead.
    const numberOfRecipients = page.everyone.send(messageHtml(message))

    // Log the number of recipients message was sent to
    // and make sure we pluralise the log message properly.
    console.info(`🫧 Kitten ${request.originalUrl} message from ${message.from} to ${message.to} broadcast to `
    + `${numberOfRecipients} recipient`
    + `${numberOfRecipients === 1 ? '' : 's'}.`)
  }

  //
  // Handle remote messages.
  //

  const remoteMessageHandler = message => {
    if (!isValidMessage(message)) {
      console.warn(`Message from remote place is invalid; not saving or broadcasting to local place.`)
      return
    }
    saveAndBroadcastMessage(message)
  }
  kitten.events.on('message', remoteMessageHandler)

  // Should we have a disconnect event on page for this?
  // i.e., page.on('disconnect', …)
  page.everyone.socket.addEventListener('close', () => {
    // Housekeeping: stop listening for remote message events
    // when the socket is closed so we don’t end up processing
    // them multiple times when the client disconnects/reconnects.
    kitten.events.off('message', remoteMessageHandler)
  })
 
  //
  // Handle local messages.
  //

  page.on('message', async message => {
    // A new message has been received from a client connected to
    // our own Small Web place: broadcast it to all clients
    // in the same room and deliver it to the remote Small Web place
    // it is being sent to after performing basic validation.
    if (!isValidMessage(message)) {
      console.warn(`Message from local place is invalid; not saving or delivering to remote place.`)
      return
    }
    
    // We don’t need to use the message HEADERS, delete them so
    // they don’t take unnecessary space when we persist the message.
    delete message.HEADERS

    saveAndBroadcastMessage(message)

    // Deliver message to remote place’s inbox.
    // Note: we are not doing any error handling here.
    // In a real-world application, we would be and we’d be
    // alerting the person if their message couldn’t be 
    // delivered, etc., and giving them the option to retry.

    const body = JSON.stringify(message)
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body 
    }

    const inbox = `https://${message.to}/inbox`
    try {
      await fetch(inbox, requestOptions)
    } catch (error) {
      console.error(`Could not deliver message to ${message.to}`, error)
    }
  })
}
