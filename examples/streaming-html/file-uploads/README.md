# Hybrid Streaming HTML + htmx POST (multipart/form-data) file upload example

[Kitten’s Streaming HTML workflow](https://ar.al/2024/03/08/streaming-html/) doesn’t handle file uploads natively.

Lower down the stack, this is because htmx’s WebSocket extension doesn’t handle them. See [htmx-extensions#17](https://github.com/bigskysoftware/htmx-extensions/issues/17).

This does actually make sense as any data sent to the server is automatically serialised and sent in JSON format.

While we could theoretically use a FileReader on the client and serialise uploaded files into a compatible format to include in the JSON (e.g., bson, etc.), this would be rather hacky and suboptimal for larger files.

It would also create two ways of working with uploaded files as Kitten already has excellent support for [file uploads using multipart forms](https://codeberg.org/kitten/app#multipart-forms-and-file-uploads).

So, instead of implementing a second, inferior, way of working with file uploads in a Streaming HTML workflow, the idiom in Kitten is to take a hybrid approach that makes use of Kitten’s built-in file upload handling for POST routes, along with having your page listen for `kitten.events` emitted by your POST route to keep your page’s status updated via Streaming HTML.

__The four key parts of this technique are:__

1. Connect the submit button on your form so your page gets informed when the upload starts:

    ```html
      <form
        …
      >
        <label for='image'>Image</label>
        <input type='file' id='image' name='image' accept='image/*'>
        …
        <button
          type='submit'
          name='uploadStarted'
          connect
        >Upload</button>
      </form>
    ```

2. Then, in the form itself, specify the POST to be handled via ajax by htmx, specify the `hx-post` attribute (we’re not providing a URL here as we want it to hit the index POST route). Also set the `hx-encoding` to `multipart/form-data` and, because we don’t care about the response, `hx-swap` to `none`. Finally, set the `hx-trigger` attribute so that the POST request gets fired simultaneously alongside the WebSocket call when the button is pressed.

    ```html
      <form
        hx-post
        hx-encoding='multipart/form-data'
        hx-swap='none'
        hx-trigger='click from:button[name="uploadStarted"]'
      >
        …
        <button
          type='submit'
          name='uploadStarted'
          connect
        >Upload</button>
      </form>
    ```

3. Next, create your POST route to handle the file upload and emit an event (in this case `upload\`):

    ```js
    if (!kitten.db.images) kitten.db.images = []

    export default function ({ request, response }) {
      request.uploads.forEach(upload => {
        kitten.db.images.push({
          path: upload.resourcePath,
          altText: request.body.altText ? request.body.altText : upload.fileName
        })
      })
      kitten.events.emit('uploadFinished')
    }
    ```

    Notice how you don’t return anything from the route as we ignore the return value anyway. All progress indication will be handled using the Streaming HTML workflow.

4. Finally, implement your `onConnect` handler, in which you react to `uploadStarted` events from the page to display a status message informing the person that the upload has started and listen for the `uploadFinished` event from the POST route to show a status message informing the person that the upload has finished and display the updated list of images.

    ```js
    export function onConnect ({ page }) {
      page.on('uploadStarted', () => {
        page.send(kitten.html`<${Status}>Uploading…</>`)
      })

      kitten.events.addListener('uploadFinished', () => {
        page.send(kitten.html`<${Status}>Upload complete.</>`)
        page.send(kitten.html`<${Images} />`)
      })
    }
    ```
