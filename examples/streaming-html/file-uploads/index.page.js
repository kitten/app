if (!kitten.db.images) kitten.db.images = []

/**
  Images component: display grid of uploaded images.
*/
function Images () {
  return kitten.html`
    <ul id='images' morph>
      ${kitten.db.images.map(image => kitten.html`
        <li><img src=${image.path} alt=${image.altText}></li>
      `)}
    </ul>
  `
}

// Note: Morphing a file input doesn’t appear to work.
// In this case, the control is not reset even if a
// fresh one is sent. 
const FileInput = () => kitten.html`
  <input type='file' id='image' name='image' accept='image/*'>
`

const AltTextInput = () => kitten.html`
  <input type='text' id='alt-text' name='altText' morph>
`

/**
  Status component.
*/
export function Status ({ SLOT }) {
  return kitten.html`
    <div id='status'>${SLOT}</div>
  `
}

export function onConnect ({ request, page }) {
  page.on('uploadStarted', () => {
    page.send(kitten.html`<${Status}>Uploading…</>`)
  })

  request.session.addListener('uploadFinished', () => {
    page.send(kitten.html`<${Status}>Upload complete.</>`)
    page.send(kitten.html`<${Images} />`)
    page.send(kitten.html`<${FileInput} />`)
    page.send(kitten.html`<${AltTextInput} />`)
  })
}

export default () => kitten.html`
  <page css htmx>
  <h1>Image gallery</h1>

  <if ${kitten.db.images.length === 0}>
    <markdown>
      No images yet. Why not [upload one](#upload)?
    </markdown>
  </if>

  <${Images} />

  <h2 id='upload'>Upload an image</h2>

  <form
    hx-post
    hx-encoding='multipart/form-data'
    hx-swap='none'
    hx-trigger='click from:button[name="uploadStarted"]'
  >
    <label for='image'>Image</label>
    <${FileInput} />
    <label for='alt-text'>Alt text</label>
    <${AltTextInput} />
    <button
      type='submit'
      name='uploadStarted'
      connect
    >Upload</button>
  </form>

  <${Status} />

  <style>
    body { max-width: 640px; margin: 0 auto; padding: 1em; font-family: sans-serif; }
    ul { padding: 0; display: grid; grid-template-columns: 1fr 1fr; }
    li { list-style-type: none; }
    img { max-height: 30vh; margin: 1em; }
    input { width: 100%; margin: 1em 0; }
    button { padding: 0.25em 1em; display: block; margin: 0 auto; }
  </style>
`
