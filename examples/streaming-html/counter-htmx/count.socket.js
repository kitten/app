import Count from './Count.fragment.js'

export default function socket ({ socket }) {
  socket.addEventListener('message', event => {
    const data = JSON.parse(event.data)

    if (data.HEADERS === undefined) {
      console.warn('No headers found in htmx WebSocket data, cannot route call.', event.data)
      return
    }

    const eventName = data.HEADERS['HX-Trigger-Name']
    
    switch (eventName) {
      case 'update':
        kitten.db.counter.count += data.value
        socket.send(kitten.html`<${Count} />`)
      break

      default:
        console.warn(`Unexpected event: ${eventName}`)
    }
  })
}
