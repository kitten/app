import Count from './Count.fragment.js'

export default function () {
  return kitten.html`
    <page htmx htmx-websocket htmx-idiomorph css>
    <main hx-ext='ws' ws-connect='wss://${kitten.domain}:${kitten.port}/count.socket'>
      <h1>Counter</h1>
      <${Count} />
      <button name='update' ws-send hx-vals='js:{value: -1}' aria-label='decrement'>-</button>
      <button name='update' ws-send hx-vals='js:{value: 1}' aria-label='increment'>+</button>
    </main>
  `
}
