export default () => kitten.html`
  <h1><b>h</b><b>e</b><b>l</b><b>l</b><b>o</b><b>,</b> <b>w</b><b>o</b><b>r</b><b>l</b><b>d</b><b>!</b></h1>
  <style>
    h1 {
      font-family: sans-serif;
      font-size: 30vh;
      color: black;
    }
  </style>
  <style id='stream'></style>
`

export function onConnect ({ page }) {
  const a = new Array(20).fill()

  setInterval(() => page.send(kitten.html`
    <style id='stream' morph>
      ${a.map((_, i) => `
        h1 b:nth-of-type(${i}) {
          color: lch(
            60%
            ${Math.floor(Math.random()*100)}
            ${Math.floor(Math.random()*100+200)}
          );
        }
      `)}
    </style>
  `), 300)
}
