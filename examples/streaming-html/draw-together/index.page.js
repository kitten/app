/**
  Draw Together

  See it live at https://draw-together.small-web.org

  @typedef {{
    rowIndex: number,
    columnIndex: number
  }} Data
*/

// Create 20×20 pixel grid.
const rows = new Array(20).fill().map(() => new Array(20).fill(0))

const colours = [
  'white',
  'black'
]

/** @param {Data} data */
const isValid = data => typeof data === 'object' && typeof data.rowIndex === 'number' && typeof data.columnIndex === 'number' && data.rowIndex >= 0 && data.rowIndex <= rows.length-1 && data.columnIndex >= 0 && data.columnIndex <= rows[0].length-1

/** @param {Data} data */
export function onPixel (data) {
  if (!isValid(data)) return
  rows[data.rowIndex][data.columnIndex]++
  if (rows[data.rowIndex][data.columnIndex] === colours.length) {
    rows[data.rowIndex][data.columnIndex] = 0
  }
  this.everyone.send(kitten.html`
    <${Pixel} rowIndex=${data.rowIndex} columnIndex=${data.columnIndex} pixelColourIndex=${rows[data.rowIndex][data.columnIndex]} />
  `)
}

const Pixel = ({ rowIndex, columnIndex, pixelColourIndex }) => kitten.html`
  <button
    id='pixel-${rowIndex}-${columnIndex}'
    aria-label='pixel-${rowIndex}-${columnIndex}'
    name='pixel'
    style='
      background-color: ${colours[pixelColourIndex]};
      top: calc(5% * ${rowIndex});
      left: calc(5% * ${columnIndex});
    '
    connect
    data='{rowIndex: ${rowIndex}, columnIndex: ${columnIndex}}'
    morph
  ></button>
`

const Canvas = () => kitten.html`${
  rows.map((row, rowIndex) => row.map((pixelColourIndex, columnIndex) => kitten.html`
    <${Pixel} rowIndex=${rowIndex} columnIndex=${columnIndex} pixelColourIndex=${pixelColourIndex} />
  `))
}`

const Styles = () => kitten.css`
  [name='pixel'] {
    position: fixed;
    width: 5%;
    height: 5%;
    border: 0;
  }
`

export default () => kitten.html`
  <page title='Draw Together'>
  <${Canvas} />
  <${Styles} />
`
