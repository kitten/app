import Count from './Count.fragment.js'

export default function socket ({ socket }) {
  socket.addEventListener('message', event => {
    const data = JSON.parse(event.data)

    if (data.event === undefined) {
      console.warn('No event found in message, cannot route call.', event.data)
      return
    }

    switch (data.event) {
      case 'update':
        kitten.db.counter.count += data.value
        socket.send(kitten.html`<${Count} />`)
      break

      default:
        console.warn(`Unexpected event: ${eventName}`)
    }
  })
}
