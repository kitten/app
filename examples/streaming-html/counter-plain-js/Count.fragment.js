if (kitten.db.counter === undefined) kitten.db.counter = { count: 0 }

export default function Count () {
  return kitten.html`
    <div
      id='counter'
      aria-live='assertive'
      hx-swap-oob='morph'
      style='font-size: 3em; margin: 0.25em 0;'
    >
      ${kitten.db.counter.count}
    </div>
  `
}
