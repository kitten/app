import Count from './Count.fragment.js'

export default function () {
  return kitten.html`
    <page css>
    <h1>Counter</h1>
    <${Count} />
    <button onclick='update(-1)' aria-label='decrement'>-</button>
    <button onclick='update(1)' aria-label='increment'>+</button>
    <script>
      ${[clientSideJS.render()]}
    </script>
  `
}

/**
  This is the client-side JavaScript we render into the page.
  It’s encapsulated in a function so we get syntax highlighting, etc. in our editors.
*/
function clientSideJS () {
  const socketUrl = `wss://${window.location.host}/count.socket`
  const ws = new WebSocket(socketUrl)
  ws.addEventListener('message', event => {
    const updatedElement = event.data

    // Get the ID of the new element.
    const template = document.createElement('template')
    template.innerHTML = updatedElement
    const idOfElementToUpdate = template.content.firstElementChild.id

    // Swap the element with the new version.
    const elementToUpdate = document.getElementById(idOfElementToUpdate)
    elementToUpdate.outerHTML = updatedElement
  })

  function update (value) {
    ws.send(JSON.stringify({event: 'update', value}))
  }
}
clientSideJS.render = () => clientSideJS.toString().split('\n').slice(1, -1).join('\n')
