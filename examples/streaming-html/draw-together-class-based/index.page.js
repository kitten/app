/**
  Draw Together

  See it live at https://draw-together.small-web.org

  @typedef {{
    rowIndex: number,
    columnIndex: number
  }} Data
*/

export default class DrawTogether extends kitten.Page {
  // 20×20 pixel grid.
  static rows = new Array(20).fill().map(() => new Array(20).fill(0))

  static colours = [
    'white',
    'black'
  ]

  html () {
    return kitten.html`
      <page title='Draw Together'>
      <${this.Canvas.bind(this)} />
      <${this.Styles} />
    `
  }

  /** @param {Data} data */
  isValid (data) {
    return typeof data === 'object'
      && typeof data.rowIndex === 'number'
      && typeof data.columnIndex === 'number'
      && data.rowIndex >= 0
      && data.rowIndex <= DrawTogether.rows.length-1
      && data.columnIndex >= 0
      && data.columnIndex <= DrawTogether.rows[0].length-1
  }
  
  /** @param {Data} data */
  onPixel (data) {
    if (!this.isValid(data)) return

    DrawTogether.rows[data.rowIndex][data.columnIndex]++
    if (DrawTogether.rows[data.rowIndex][data.columnIndex] === DrawTogether.colours.length) {
      DrawTogether.rows[data.rowIndex][data.columnIndex] = 0
    }

    this.everyone.send(kitten.html`
      <${this.Pixel} rowIndex=${data.rowIndex} columnIndex=${data.columnIndex} pixelColourIndex=${DrawTogether.rows[data.rowIndex][data.columnIndex]} />
    `)
  }

  Pixel ({ rowIndex, columnIndex, pixelColourIndex }) {
    return kitten.html`
      <button
        id='pixel-${rowIndex}-${columnIndex}'
        aria-label='pixel-${rowIndex}-${columnIndex}'
        name='pixel'
        style='
          background-color: ${DrawTogether.colours[pixelColourIndex]};
          top: calc(5% * ${rowIndex});
          left: calc(5% * ${columnIndex});
        '
        connect
        data='{rowIndex: ${rowIndex}, columnIndex: ${columnIndex}}'
        morph
      ></button>
    `
  }

  Canvas () {
    return kitten.html`${
      DrawTogether.rows.map((row, rowIndex) => row.map((pixelColourIndex, columnIndex) => kitten.html`
        <${this.Pixel} rowIndex=${rowIndex} columnIndex=${columnIndex} pixelColourIndex=${pixelColourIndex} />
      `))
    }`
  } 

  Styles () {
    return kitten.css`
      [name='pixel'] {
        position: fixed;
        width: 5%;
        height: 5%;
        border: 0;
      }
    `
  }
}
