# Draw Together (class-based version)

A very simple Kitten toy for drawing together on a 20×20 grid.

This is a class-based version using the new `kitten.Page` object.

(This isn’t the best example of using Kitten Pages and Kitten Components as the data is shared between pages and we don’t need a live DOM-like structure per-page to implement its functionality. This is why it doesn’t use `kitten.Component` instances for the `Canvas` and `Pixel` components, opting to make them helper methods instead. The full class-based Kitten Page/Kitten Component component workflow shines is when implementing page-specific component hierarchies but this is still a useful reference to showcase the differences between the function-based and class-based workflows even when not implementing a live/connected component hierarchy.)

## To play

Go to https://draw-together.small-web.org

## Like this? Fund us!

[We’re](https://small-tech.org) a tiny two-person not-for-profit working to build the [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/). If you want to see us continue to exist, please [fund us](https://small-tech.org/fund-us).

## The code

Here’s all the 🐱 [Kitten](https://codeberg.org/kitten/app) code you need:

```js
/**
  Draw Together

  See it live at https://draw-together.small-web.org

  @typedef {{
    rowIndex: number,
    columnIndex: number
  }} Data
*/

export default class DrawTogether extends kitten.Page {
  // 20×20 pixel grid.
  static rows = new Array(20).fill().map(() => new Array(20).fill(0))

  static colours = [
    'white',
    'black'
  ]

  html () {
    return kitten.html`
      <page title='Draw Together'>
      <${this.Canvas.bind(this)} />
      <${this.Styles} />
    `
  }

  /** @param {Data} data */
  isValid (data) {
    return typeof data === 'object'
      && typeof data.rowIndex === 'number'
      && typeof data.columnIndex === 'number'
      && data.rowIndex >= 0
      && data.rowIndex <= DrawTogether.rows.length-1
      && data.columnIndex >= 0
      && data.columnIndex <= DrawTogether.rows[0].length-1
  }
  
  /** @param {Data} data */
  onPixel (data) {
    if (!this.isValid(data)) return

    DrawTogether.rows[data.rowIndex][data.columnIndex]++
    if (DrawTogether.rows[data.rowIndex][data.columnIndex] === DrawTogether.colours.length) {
      DrawTogether.rows[data.rowIndex][data.columnIndex] = 0
    }

    this.everyone.send(kitten.html`
      <${this.Pixel} rowIndex=${data.rowIndex} columnIndex=${data.columnIndex} pixelColourIndex=${DrawTogether.rows[data.rowIndex][data.columnIndex]} />
    `)
  }

  Pixel ({ rowIndex, columnIndex, pixelColourIndex }) {
    return kitten.html`
      <button
        id='pixel-${rowIndex}-${columnIndex}'
        aria-label='pixel-${rowIndex}-${columnIndex}'
        name='pixel'
        style='
          background-color: ${DrawTogether.colours[pixelColourIndex]};
          top: calc(5% * ${rowIndex});
          left: calc(5% * ${columnIndex});
        '
        connect
        data='{rowIndex: ${rowIndex}, columnIndex: ${columnIndex}}'
        morph
      ></button>
    `
  }

  Canvas () {
    return kitten.html`${
      DrawTogether.rows.map((row, rowIndex) => row.map((pixelColourIndex, columnIndex) => kitten.html`
        <${this.Pixel} rowIndex=${rowIndex} columnIndex=${columnIndex} pixelColourIndex=${pixelColourIndex} />
      `))
    }`
  } 

  Styles () {
    return kitten.css`
      [name='pixel'] {
        position: fixed;
        width: 5%;
        height: 5%;
        border: 0;
      }
    `
  }
}
```

That’s it, that’s all the code you need in [Kitten](https://codeberg.org/kitten/app) to build this.

To run it yourself, locally:

1. Install [Kitten](https://codeberg.org/kitten/app)
2. Create a new folder (e.g., called `draw-together`) and a file inside it called `index.page.js` and add the code above into it.
3. Run `kitten` in that folder and hit `https://localhost`

## Learn more

So what dark art is this? Kitten combines a server with file system based routing with first-class support for [htmx](https://htmx.org), WebSockets, and some syntactic sugar and magic unique to Kitten to create the Streaming HTML workflow you see being used above.

To learn more about it, watch the video on the [Streaming HTML](https://ar.al/2024/03/08/streaming-html/) pots where you’ll see a more basic counter example being created from scratch and then read the article where the magic is peeled away one layer at a time.

## Scripts

As some of you quickly found out, yes, Draw Together is scriptable through the Web Developer console of your browsers.

If you are experimenting with scripting, please be considerate. Anyone can mess up a canvas folks are working on, there’s no skill in that. Instead, here are a couple of scripts you can experiment with without destroying what everyone else is working.

### Invert canvas

This is the easiest script, it merely inverts the colours of the pixels on the canvas:

```js
for (row = 0; row < 20; row++) { for (column = 0; column < 20; column++) {
  document.getElementById(`pixel-${row}-${column}`).click()
}}
```

### Save canvas

This snippet saves the current canvas in the format that you can use to recreate it in the next snippet.

```js
function save() {
  canvasCode = ''
  canvasCode += 'newCanvas = [\n'
  for (row = 0; row < 20; row++) {
    canvasCode += '  ['
    for (column = 0; column < 20; column++) {
      canvasCode += document.getElementById(`pixel-${row}-${column}`).style.backgroundColor === 'black' ? 1 : 0
      if (column !== 19) canvasCode += ', '
    }
    canvasCode += '],\n'
  }
  canvasCode += ']'
  return canvasCode
}
save()
```

This creates the `newCanvas` array that you can pop into the next snippet to recreate your design (or show it to folks for a couple of seconds). You can, for example, run Draw Together locally, create a design, save it, and then use the next snippet online to share your design.

### Briefly share a full design (with save and restore of what was already there)

I’ve seen a few folks playing with drawing the whole canvas from a pre-made design. If you’re going to do this, please be considerate and save the state of the canvas first and restore it afterwards.

Here’s a script that will let you do that:

```js
newCanvas = [
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
]


function save() {
  const oldCanvas = []
  for (row = 0; row < 20; row++) {
    oldCanvas[row] = []
    for (column = 0; column < 20; column++) {
      oldCanvas[row][column] = document.getElementById(`pixel-${row}-${column}`).style.backgroundColor === 'black' ? 1 : 0
    }
  }
  return oldCanvas
}

function draw(canvas) {
  for (row = 0; row < 20; row++) {
    for (column = 0; column < 20; column++) {
      const element = document.getElementById(`pixel-${row}-${column}`)
      if (canvas[row][column]) {
        if (element.style.backgroundColor === 'white') element.click()
      } else {
        if (element.style.backgroundColor === 'black') element.click()
      }
    }
  }
}

oldCanvas = save()
draw(newCanvas)
setTimeout(() => draw(oldCanvas), 2000)
```

To use it, create your design in the `newCanvas` array at the top of the script. You can use graphing paper to visualise it, if it’s easier. Zeros are white, ones are black.

By default, your design will replace what’s on the canvas and stay up for two seconds before restoring whatever was previously there.

Remember, please be considerate in your experiments and let everyone have a go.

🐱 💕
