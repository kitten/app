const Toast = ({ SLOT }) => kitten.html`
  <div id='toast' morph='settle:2s' aria-live='assertive'>${SLOT}</div>
`

const MessageInput = () => kitten.html`
  <input type='text' id='message' name='message' morph>
`

export default function () {
  return kitten.html`
    <page css>
    <h1>🍞 Mmm, Toast!</h1>
    <form name='toast' connect>
      <label for='message'>Message</label>
      <${MessageInput} />
      <button type='submit'>Toast</button>
    </form>
    <${Toast} />
    <style>
      #toast.settling { opacity: 1; top: 1em; }
      #toast {
        opacity: 0;
        top: -3em;
        position: fixed;
        transition: all 0.7s ease-out;
        left: 50%;
        transform: translate(-50%, 0); /* centre horizontally */
        border-radius: 1em;
        background-color: var(--background-alt);
        padding: 1em;
        font-weight: bold;
      }
      button, input { display: inline; }
      label { display: block; }
    </style>
  `
}

/** @param {{ message: string }} data */ 
export function onToast ( data ) {
  this.send(kitten.html`<${Toast}>${data.message}</>`)
  this.send(kitten.html`<${MessageInput} />`)
}
