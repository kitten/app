import Markdown from './supported.fragment.md'

export default () => kitten.html`
  <page css syntax-highlighter>
  <${Markdown} />
`
