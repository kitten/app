export default ({request}) => kitten.html`
  <h1>Bye, ${request.params.name}.</h1>
  <p>${request.params['some-last-words']}</p>
`
