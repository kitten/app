export default ({request}) => kitten.html`
  <h1>Hi${request.params.name ? `, ${request.params.name}` : ''}.</h1>
`
