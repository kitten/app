# Routing example

Try the following routes:

## Nested

```
hello/[name]/index.page.js
```

e.g., https://localhost/hello/oskar

## Flat

```
goodbye_[name].page.js
```

e.g., https://localhost/goodbye/oskar

## Multiple parameters

```
happy-birthday_[name]_[age].page.js
```

e.g., https://localhost/happy-birthday/oskar/11

## Optional

```
hi_[optional-name].page.js
```

e.g., https://localhost/hi

## Hyphenated (compound name)

```
bye_[name]_[some-last-words].page.js
```

e.g., [https://localhost/bye/oskar/Hope you have a lovely day.](https://localhost/bye/oskar/Hope%20you%20have%20a%20lovely%20day.)
