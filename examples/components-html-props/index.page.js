import MyButton from './MyButton.component.js'

export default () => kitten.html`
  <h1>My button</h1>

  <${MyButton} label='Press me!' onclick='alert("Thank you for pressing me!")' />
`
