export default () => kitten.html`
  <page htmx alpinejs>

  <content for='HEAD'>
    <title>Special page slots</title>
    <link rel='icon' href='/favicon.ico'>
  </content>

  <h1>Special page slots</h1>

  <h2>This is just regular content on the page.</h2>

  <content for='START_OF_BODY'>
    <p>This is the <strong>start of the body</strong>.</p>
  </content>

  <content for='BEFORE_LIBRARIES'>
    <p>This is right <strong>before Kitten libraries</strong> (i.e., htmx and Alpine.js) are declared.</p>
  </content>

  <content for='AFTER_LIBRARIES'>
    <script>
      console.info('This script is executing after the libraries have.')
    </script>
    <p>This is right <strong>after Kitten libraries</strong> are declared. It’s a good place to put scripts if you want to make sure the Kitten libraries are loaded and can be used.</p>
  </content>

  <content for='END_OF_BODY'>
    <p>This is the <strong>end of the body.</strong></p>
  </content>

  <style>
    body { font-family: system-ui, sans-serif; }
    p { padding: 1em; border: 0.25em solid deeppink; border-radius: 0.5em; }
  </style>
`
