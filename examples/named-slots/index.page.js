import Slotty from './Slotty.component.js'

export default () => kitten.html`
  <${Slotty}>
    <h2>I’m a regular heading from the page.</h2>

    <content for='header'>
      <div class='content'>
        <markdown>
          ### This is content for the header slot.
          ![Illustration of a sitting kitten.](/🐱/images/kitten-sitting.svg "Isn’t Kitten cute?")
        </markdown>
        <script>
          console.info('And it can contain preformatted content.')
          console.info('Like scripts, <pre> tags, etc.')
        </script>
      </div>
    </content>

    <p>I’m a regular paragraph from the page.</p>

    <content for='main'>
      <p class='content'>This is content for the main slot.</p>
    </content>

    <p>I’m another regular paragraph from the page.</p>

    <content for='footer'>
      <p class='content'>This is content for the footer slot.</p>
    </content>
  </>

  <style>
    .content {
      margin: 0.5em;
      padding: 0.5em;
      border: 2px dashed cadetblue;
    }
    img { width: 20ch; margin-left: auto; margin-right: auto; display: block; }
    figcaption { text-align: center; margin-top: 0.5em; font-weight: bold; }
  </style>
`
