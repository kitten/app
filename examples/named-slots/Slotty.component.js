export default ({ SLOT }) => kitten.html`
  <h1>Named slots</h1>

  <header class='slot'>
    <h2>Slot: header</h2>
    ${SLOT.header}
  </header>

  ${SLOT}

  <main class='slot'>
    <h2>Slot: main</h2>
    ${SLOT.main}
  </main>

  <footer class='slot'>
    <h2>Slot: footer</h2>
    ${SLOT.footer}
  </footer>

  <style>
    body { font-family: sans-serif; }
    header, main, footer {
      border: 2px solid deeppink;
    }
    .slot {
      margin-top: 1em;
    }
    .slot > h2:first-of-type {
      color: white;
      background-color: deeppink;
      display: block;
      margin: 0;
    }
  </style>
`
