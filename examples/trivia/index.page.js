import getRandomQuestion from './open-trivia-api.script.js'

export default async ({request}) => {
  // Initialise the game for this person.
  if (!request.session.game) {
    request.session.game = { score: 0 }
  }
  const game = request.session.game

  // Get any feedback that might exist for the person.
  if (!game.feedback) {
    game.feedback = 'Welcome! Let’s see how you do :)'
  } 

  // Get a new random question.
  const question = await getRandomQuestion(game)
  
  // Create and return the game interface.
  const page = kitten.html`
    <h1>Kitten Trivia</h1>

    <p class='state'>
      Score: ${game.score ? game.score : '0'} <span class='feedback'>(${game.feedback})</span>
    </p>

    <h2>Question</h2>
    <p class='question'>${question.value}</p>
    <p class='questionType'>(${question.category.name}, difficulty: ${question.difficulty})</p>

    <form method='POST' action='/actions'>
      <fieldset>
        <legend>Answer</legend>
        <ul>
          ${question.allAnswers.map((answer, index) => (
            kitten.html`
              <li>
                <input type='radio' id='answer-${index}' name='answer' value='${answer}'>
                <label for='answer-${index}'>${answer}</label>
              </li>
            `
          ))}
        </ul>
      </fieldset>
      <div id='actions'>
        <button name='action' value='restart-game' type='submit'>Restart game</button>
        <button name='action' value='check-answer' type='submit'>Check answer</button>
      </div>
    </form>
    <style>

      body { font-family: sans-serif; padding: 1em; }
      h1 { font-size: 4rem; margin: 0; }
      h2 { font-size: 2rem; margin: 0; background-color: black; color: white; }
      p, h2 { padding: 0.5rem; }
      li { list-style: none; font-size: 1.25em; }
      fieldset { border-radius: 0.5em; }
      legend { font-size: 1.5em; font-weight: bold; }
      button {
        color: white;
        border: 0;
        border-radius: 0.25em;
        background-color: black;
        padding: 0.25em;
        margin-top: 0.5em;
        margin-left: auto;
        margin-right: auto;
        font-size: 2em;
      }
      button:hover { background-color: gray; }
      button:active { background-color: white; color: black; border: 2px solid black; }
      button:nth-of-type(2) { margin-left: 0.5em; }
      input[type='radio'] {  
        appearance: none;
        position: relative;
        border: 2px solid #999;
        margin-right: 0.5em;
        border-radius: 50%;
        width: 1em;
        height: 1em;
        transition: 0.2s all linear;
      }
      input[type='radio']:checked {
        border: 4px solid black;
      }
      .state { font-size: 1.5em; margin-top: 0; }
      .feedback { font-weight: bold; }
      .question { font-size: 3em; margin: 0; }
      .questionType { font-style: italic; margin-top: -0.5em; }
      #actions { text-align: center; }
      /* Dark mode support. */
      /* See https://ar.al/2021/08/24/implementing-dark-mode-in-a-handful-of-lines-of-css-with-css-filters/ */
      @media (prefers-color-scheme: dark) {
        html { background-color: #111; }
        body { filter: invert(100%) hue-rotate(180deg); }
      }
    </style>
  `

  // Also, clear out the feedback, now that we’ve extracted it,
  // so that it doesn’t display again if the person reloads the page.
  game.feedback = 'Ah, refreshed the page to skip that question? That’s ok! ;)'

  return page
}
