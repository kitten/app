// Handle POST actions.
// (The game interface has two actions, restart game and check answer,
// corresponding to which button in the interface was pressed.)
export default async function ({request, response}) {
  const action = request.body.action
  
  switch (true) {
    case action === 'restart-game':
      request.session.game = { score: 0 }
      break
  
    case action === 'check-answer':
      const game = request.session.game
      const answer = request.body.answer

      if (answer === game.correctAnswer) {
        game.score += 1
        game.feedback = 'Correct!'
      } else {
        game.feedback = `Sorry, the correct answer was ${game.correctAnswer}.`
      }
      break
  
    default:
      // This should not happen if the interface is being used.
      // Deliver a forbidden response to alert the would-be
      // script kiddie that we are not amused.
      response.forbidden('Haha, good try! ;)')
      return
  }
  
  // We could just render the game interface and return it here but
  // that would lead to replay attacks (where the person could resubmit
  // the page over and over again). Instead we’ll use the
  // Post/Redirect/Get (PRG) pattern to redirect the person to the index page.
  // (See https://en.wikipedia.org/wiki/Post/Redirect/Get)
  //
  // Since we’re using Kitten’s built-in sessions support,
  // we won’t lose state when we do.
  response.get('/')
}
