/**
  Note that the Open Trivia DB module we’re using appears to
  have a bug that can cause a crash.

  See: https://github.com/Elitezen/open-trivia-db-wrapper/issues/11
*/
import { Session, getQuestions } from 'open-trivia-db'

export default async function getRandomQuestion (game) {
  // Check if an Open Trivia API session exists for this
  // person’s game session. If not, create and start one.
  // The Open Trivia API session is used to ensure that
  // the person doesn’t get any duplicate questions.
  if (!game.openTriviaSessionId) {
    const openTriviaSession = new Session()
    const openTriviaSessionId = await openTriviaSession.start()
    game.openTriviaSessionId = openTriviaSessionId
  }
  
  // Get a question from the Open Trivia API.
  try {
    const questions = await getQuestions({
      amount: 1,
      type: 'multiple',
      session: game.openTriviaSessionId
    })
    const question = questions[0]

    // Save the question in the database.
    game.correctAnswer = question.correctAnswer
  
    return question
  } catch (error) {
    // The Open Trivia API has started randomly returning
    // errors. Let’s catch them so we don’t crash.
    return {
      value: `The Open Trivia API returned an error. Please refresh.`,
      difficulty: 'n/a',
      category: {name: 'n/a'},
      allAnswers: [error]
    }
  }
}
