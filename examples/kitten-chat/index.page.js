import styles from './index.styles.js'
import StatusIndicator from './StatusIndicator.component.js'

// The page template is static so we render it outside
// the route handler for efficiency (so it only gets rendered
// once when this file first loads instead of on every request).
const page = kitten.html`
  <page htmx htmx-websocket alpinejs>

  <main>
    <h1>🐱 <a href='https://codeberg.org/kitten/app'>Kitten</a> Chat</h1>

    <${StatusIndicator} />

    <div 
      id='chat'
      hx-ext='ws'
      ws-connect='/chat.socket'
      x-data='${kitten.js`{ showPlaceholder: true }`}'
    >
      <ul id='messages' @htmx:load='${kitten.js`
        showPlaceholder = false
        $el.scrollTop = $el.scrollHeight
      `}'>
        <li id='placeholder' x-show='showPlaceholder'>
          No messages yet, why not send one?
        </li>
      </ul>

      <form id='message-form' ws-send>
        <label for='nickname'>Nickname:</label>
        <input id='nickname' name='nickname' type='text' required />

        <label for='text'>Message:</label>
        <input id='text' name='text' type='text' required
          @htmx:ws-after-send.window='${kitten.js`
            $el.value = ""

            // On mobile we don’t refocus the input box
            // in case there is a virtual keyboard so
            // the person can see the message they just sent.
            if (!isMobile()) { $el.focus() }
          `}'
        >
        <button id='sendButton' type='submit'>Send</button>
      </form>
    </div>
  </main>

  <!--
    Add our content after the Kitten libraries to ensure
    they have loaded before we access them. In this case,
    we need to make sure htmx is loaded.
  -->
  <content for='AFTER_LIBRARIES'>
    <script src='./index.js' />
  </content>

  <!--
    Since we trust the static CSS we just imported,
    add it bypassing string escaping.
  -->
  ${[styles]}
`

export default () => page
