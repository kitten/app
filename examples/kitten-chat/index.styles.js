export default kitten.css`
  * { box-sizing: border-box; }

  /* Make interface fill full browser canvas. */
  main {
    display: flex;
    font-family: sans-serif;
    height: calc(var(--vh, 1vh) * 100 - 1em);
    flex-direction: column;
    flex-wrap: nowrap;
    justify-content: flex-start;
    align-content: stretch;
    align-items: flex-start;
    padding: 1em;
  }

  h1 {
    margin-top: 0;
    margin-bottom: 0;
  }

  p {
    margin-top: 0;
    margin-bottom: 0;
  }

  a {
    color: #334b4c;
  }

  form {
    background: #eee;
    display: grid;
    grid-template-columns: [labels] auto [controls] 1fr;
    align-items: center;
    grid-row-gap: 0.5em;
    grid-column-gap: 0.5em;
    padding: 0.75em;
    width: 100%;
  }

  form > label { grid-column: labels; }

  form > input, form > button {
    grid-column: controls;
    min-width: 6em;
    max-width: 300px;
    padding: 0.5em;
    font-size: 1em;
  }

  button {
    text-align: center;
    cursor: pointer;
    font-size:16px;
    color: white;
    border-radius: 4px;
    background-color:#466B6A;
    border: none;
    padding: 0.75em;
    padding-top: 0.25em;
    padding-bottom: 0.25em;
    transition: color 0.5s;
    transition: background-color 0.5s;
  }

  button:hover {
    color: black;
    background-color: #92AAA4;
  }

  button:disabled {
    color: #999;
    background-color: #ccc;
  }

  /* The chat div should not affect the layout. This is actually a smell.
   It is purely being used to declaratively create a WebSocket connection.
   Need to think about this.
  */
  #chat {
    display: contents;
  }

  #messages {
    list-style: none;
    width: 100%;
    flex: 100 1 auto;
    align-self: stretch;
    overflow-y: scroll;
    background-color: #eee;
    padding: 0.75em;
  }
`
