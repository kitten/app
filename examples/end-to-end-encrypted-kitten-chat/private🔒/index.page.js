import styles from './index.styles.js'
import Message from './Message.component.js'
import StatusIndicator from './StatusIndicator.component.js'

// Ensure the messages table exists in the database before using it.
if (kitten.db.messages === undefined) kitten.db.messages = []

export default () => kitten.html`
  <page htmx htmx-websocket alpinejs>

  <main>
    <h1>🐱 End-to-end encrypted <a href='https://codeberg.org/kitten/app'>Kitten</a> Chat</h1>

    <${StatusIndicator} />

    <div 
      id='chat'
      hx-ext='ws'
      ws-connect='/private/chat.socket'
      x-data='${kitten.js`{ showPlaceholder: true }`}'
      @htmx:ws-config-send.prevent='encryptMessage'
    >
      <ul id='messages' @htmx:load='${kitten.js`
        showPlaceholder = false
        $el.scrollTop = $el.scrollHeight
      `}'>
        <if ${kitten.db.messages.length === 0}>
          <then>
            <li id='placeholder' x-show='showPlaceholder'>
              No messages yet, why not send one?
            </li>
          <else>
            ${kitten.db.messages.map(message => kitten.html`<${Message} message=${message} />`)}
        </if>
      </ul>

      <form id='message-form' ws-send>
        <label for='domain'>To:</label>
        <input id='domain' name='domain' type='text' required>

        <label for='plainText'>Message:</label>
        <input id='plainText' name='plainText' type='text' required
          @htmx:ws-after-send.window='${kitten.js`
            $el.value = ""

            // On mobile we don’t refocus the input box
            // in case there is a virtual keyboard so
            // the person can see the message they just sent.
            if (!isMobile()) { $el.focus() }
          `}'
        >

        <button
          id='sendButton'
          type='submit'
        >Send</button>
      </form>
      <script type='module' src='./index.js' />
    </div>
  </main>

  <!--
    Since we trust the static CSS we just imported,
    add it bypassing string escaping.
  -->
  ${[styles]}
`
