import Message from './Message.component.js'

if (kitten.db.messages === undefined) kitten.db.messages = [];

function messageHtml (message) {
  // Wrap the same Message component we use in the page
  // with a node instructing htmx to add this node to
  // the end of the messages list on the page.
  return kitten.html`
    <div hx-swap-oob="beforeend:#messages">
      <${Message} message=${message} />
    </div>
  `
}

// Kitten Chat example back-end.
export default function ({ request, socket }) {
  const saveAndBroadcastMessage = message => {
    // Persist the message in the messages table.
    kitten.db.messages.push(message)

    // Since we are not optimistically showing messages
    // as sent on the client, we send to all() local clients, including
    // the one that sent the message. If we were optimistically
    // updating the messages list, we would use the broadcast()
    // method on the socket instead.
    const numberOfRecipients = socket.all(messageHtml(message))

    // Log the number of recipients message was sent to
    // and make sure we pluralise the log message properly.
    console.info(`🫧 Kitten ${request.originalUrl} message from ${message.from} to ${message.to} broadcast to `
    + `${numberOfRecipients} recipient`
    + `${numberOfRecipients === 1 ? '' : 's'}.`)
  }

  // Handle remote messages.
  const remoteMessageHandler = message => {
    if (!isValidMessage(message)) {
      console.warn(`Message from remote place is invalid; not saving or broadcasting to local place.`)
      return
    }
    saveAndBroadcastMessage(message)
  }
  kitten.events.on('message', remoteMessageHandler)

  socket.addEventListener('close', () => {
    // Housekeeping: stop listening for remote message events
    // when the socket is closed so we don’t end up processing
    // them multiple times when the client disconnects/reconnects.
    kitten.events.off('message', remoteMessageHandler)
  })
  
  socket.addEventListener('message', async event => {
    // A new message has been received from a client connected to
    // our own Small Web place: broadcast it to all clients
    // in the same room and deliver it to the remote Small Web place
    // it is being sent to after performing basic validation.

    const message = JSON.parse(event.data)

    if (!isValidMessage(message)) {
      console.warn(`Message from local place is invalid; not saving or delivering to remote place.`)
      return
    }
    
    // We don’t need to use the message HEADERS, delete them so
    // they don’t take unnecessary space when we persist the message.
    delete message.HEADERS

    saveAndBroadcastMessage(message)

    // Deliver message to remote place’s inbox.
    // Note: we are not doing any error handling here.
    // In a real-world application, we would be and we’d be
    // alerting the person if their message couldn’t be 
    // delivered, etc., and giving them the option to retry.

    const body = JSON.stringify(message)
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body 
    }

    const inbox = `https://${message.to}/inbox`
    try {
      await fetch(inbox, requestOptions)
    } catch (error) {
      console.error(`Could not deliver message to ${message.to}`, error)
    }
  })
}

// Some basic validation.

// Is the passed object a valid string?
function isValidString(s) {
  return Boolean(s)                // Isn’t null, undefined, '', or 0
    && typeof s === 'string'       // and is the correct type
    && s.replace(/\s/g, '') !== '' // and is not just whitespace.
}

// Is the passed message object valid?
// (We carry out only very basic validation in this example. In a
// real-world app you would do more like check for valid from/to values.)
function isValidMessage(message) {
  return isValidString(message.from) && isValidString(message.to) && isValidString(message.cipherText)
}
