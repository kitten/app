// Create 20×20 pixel grid.
if (kitten.db.rows === undefined) {
  kitten.db.rows = new Array(20).fill().map(row => new Array(20).fill(0))
}

const colours = [
  'white',
  'black',
]

export function onConnect ({ page }) {
  page.on('pixel', data => {
    kitten.db.rows[data.rowIndex][data.columnIndex]++
    if (kitten.db.rows[data.rowIndex][data.columnIndex] === colours.length) {
      kitten.db.rows[data.rowIndex][data.columnIndex] = 0
    }
    page.everyone.send(kitten.html`
      <${Pixel} rowIndex=${data.rowIndex} columnIndex=${data.columnIndex} pixelColourIndex=${kitten.db.rows[data.rowIndex][data.columnIndex]} />
    `)
  })
}

const Pixel = ({ rowIndex, columnIndex, pixelColourIndex }) => kitten.html`
  <button
    id='pixel-${rowIndex}-${columnIndex}'
    class='pixel'
    name='pixel'
    style='
      background-color: ${colours[pixelColourIndex]};
      top: calc(5% * ${rowIndex});
      left: calc(5% * ${columnIndex});
    '
    connect
    data='{rowIndex: ${rowIndex}, columnIndex: ${columnIndex}}'
    morph
  ></button>
`

const Canvas = () => kitten.html`
  ${
    kitten.db.rows.map((row, rowIndex) => row.map((pixelColourIndex, columnIndex) => kitten.html`
      <${Pixel} rowIndex=${rowIndex} columnIndex=${columnIndex} pixelColourIndex=${pixelColourIndex} />
    `))
  }
  <style>
    .pixel {
      position: fixed;
      width: 5%;
      height: 5%;
      border: 0;
      border-radius: 0;
      padding:0;
      margin:0;
    }
  </style>
`

export default () => kitten.html`
  <page title='Draw together'>
  <${Canvas} />
`
