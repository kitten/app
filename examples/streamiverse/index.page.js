import Post from './Post.component.js'

export default async function route () {
  const response = await fetch('https://streamiverse.small-web.org/public/')
  // const response = await fetch('https://mastodon.ar.al/api/v1/timelines/public')
  const posts = await response.json()
  
  return kitten.html`
    <page htmx htmx-websocket>

    <h1>Aral’s Public Fediverse Timeline</h1>
    <ul id='posts' hx-ext='ws' ws-connect='/updates.socket'>
      ${posts.map(post => kitten.html`<${Post} post=${post} />`)}
    </ul>

    <style>
      body { font-family: sans-serif; font-size: 1.25em; padding-left: 1.5em; padding-right: 1.5em; }
      h1 { font-size: 2.5em; text-align: center; }
      ul { padding: 0; }
    </style>
  `
}
