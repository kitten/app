if (kitten.db.kittens === undefined) kitten.db.kittens = {count: 1}

export default () => {
  kitten.db.kittens.count++

  return kitten.html`
    <h1>Kitten count</h1>
    <p>${'🐱️'.repeat(kitten.db.kittens.count > 20 ? 20 : kitten.db.kittens.count)}</p>

    <if ${kitten.db.kittens.count > 20}>
      <then>
        <p>(and <strong>${kitten.db.kittens.count - 20}</strong> more.)</p>
      </then>
      <else>
        <small>Keep refreshing, we need more kittens!</small>
      </else>
    </if>
  `
}

// Instead of using Kitten’s conditional syntax, you can just use
// interpolation directly if you like but remember that interpolated
// strings are automatically escaped by Kitten (as Kitten has no
// way of knowing whether those strings were authored by you or by someone
// who used your app’s interface to enter a value that contains a script injection attempt.)
// So if you’re going to use that method, wrap your HTML in array:
//
// ${kitten.db.kittens.count > 20
//   && [kitten.html`<p>(and <strong>${kitten.db.kittens.count - 20}</strong> more.)</p>`]
//   || [kitten.html`<small>Keep refreshing, we need more kittens!</small>`]
// }
//
// Alternatively, you can also use an anonymous inline component if you don’t
// want to use array syntax:
//
// <${()=> 
//   kitten.db.kittens.count > 20
//     && kitten.html`<p>(and <strong>${kitten.db.kittens.count - 20}</strong> more.)</p>`
//     || kitten.html`<small>Keep refreshing, we need more kittens!</small>`
// }/>
//
// All that said, you should probably just use Kitten’s conditional syntax
// as it’s easier to author, read, and understand.
//
// All that said, remember that Kitten’s <if> tags do not short circuit. So
// if any values you references in any branch might be null or undefined,
// make sure you use optional chaining (e.g., valueThatMightBeNull?.doSomething()).
