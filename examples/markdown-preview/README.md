# Markdown Preview

This is an example of a local web application that is designed to only run on someone’s own machine, not on a server.

In this case, it shows a live preview of a Markdown file.

You can either enter the path to a Markdown file on your system in the _File:_ input box in the interface or you can specify the file to open from the command-line when invoking Kitten.

When doing the latter, you must pass the full path to the Markdown Preview application to Kitten as shown below (the actual location of the app will be different on your machine):

```shell
kitten ~/Projects/kitten/app/examples/markdown-preview/ README.md
```

That will make a live preview of the _README.md_ file in the directory you issued the command from available at https://localhost.

## Running as a command-line script

If you want a single, simple-to-use command, you can create a bash script (e.g., one called `md` in `~/.local/bin`) that runs the Kitten command and also launches your default browser. Here’s the one I’m using on my Linux machine:

```bash
#!/usr/bin/env bash

kitten ~/Projects/kitten/app/examples/markdown-preview/ $1 --port=0 --open
```
