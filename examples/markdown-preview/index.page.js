import fsPromises from 'node:fs/promises'
import fs, { watch } from 'node:fs'
import path from 'node:path'

// We use this private-use unicode character as a token
// to mark the location that we encounter the first change
// between versions of the document (so we can have the client
// scroll there while working).
const FIRST_CHANGE_TOKEN = '\ue100'

let watcher

// Initialise database if necessary.
if (kitten.db.document === undefined) {
  kitten.db.document = { path: '', content: '' }
}

// If a path to a file is passed as a command-line argument,
// use that. If this is how you want to use Markdown Preview,
// pass the absolute path to Markdown Preview to Kitten as
// the second argument. You canalso pass --port=0 to use a
// random port and --open to automatically launch it in your
// default web browser.
if (process.argv.length >= 5) {
  const workingDirectory = process.argv[2].replace('--working-directory=', '')
  const pathFromCLI = path.resolve(workingDirectory, process.argv[4])
  kitten.db.document.path = pathFromCLI
}

/** Preview fragment */
const Preview = () => kitten.html`
  <div id='preview' morph>
    ${[kitten.md.render(kitten.db.document.content).replace(FIRST_CHANGE_TOKEN, `<span id='first-change'></span>`)]}
    <script>
      // Note: we use a client-side script to scroll the first change
      // into view as htmx’s WebSocket extension makes use of out-of-band
      // swaps (hx-swap-oob) and they do not support the same properties
      // that hx-swap does yet.
      //
      // We’ll be able to do this much more elegantly once this issue
      // has been resolved:
      // https://github.com/bigskysoftware/htmx/issues/2308
      document.getElementById("first-change").scrollIntoView()
    </script>
  </div>
`

/** Error message fragment */
const ErrorMessage = ({ message = '' }) => kitten.html`
  <div id='error' morph>
    ${message}
    <style>
      #error {
        color:red; font-size: small; margin-top:-0.33em;
      }
    </style>
  </div>
`

/** Page route. */
export default async () => {
  if (kitten.db.document.path !== '') {
    // Load the latest version of the document each time the page renders.
    await updateContent()
  }

  return kitten.html`
    <page css>
    <h1 id='title'>📃 Markdown preview</h1>
    <form>
      <label for='path'>File:</label>
      <input id='path' name='path' value='${kitten.db.document.path}' connect>
    </form>
    <${ErrorMessage} />
    <${Preview} />
    <style>
      body { margin: 20px 10vw; max-width: inherit; padding: 0; }
      #preview {
        border: 3px solid var(--border); padding: 0.5em;
        position: absolute; overflow: scroll;
        top: 9em; left: 10vw; right: 10vw; bottom: 1em;
      }
      #preview > :first-child { margin-top: 0; } /* Adjust top of document. */
      #title { margin-left: -0.15em; } /* Optical adjustment; flush left. */ 
      form { display: flex; align-items: center; gap: 1em; margin: 0; padding: 0; }
      input { margin-right: 0; width: 100%; }
      label { font-weight: bold; }
    </style>
  `
}

/**
  onConnect handler

  Gets called when the page makes its automatic WebSocket connection.
*/
export function onConnect ({ page }) {
  // Create watcher when page first connects if we already have a path.
  if (kitten.db.document.path !== '') {
    updateWatcher(page)
  }

  page.on('path', async data => {
    // Ignore if its the same path as before.
    // Note: due to the null event being sent by htmx (see below),
    // this will never get called.
    if (data.path === kitten.db.document.path) {
      return
    }

    if (!data.path.endsWith('.md')) {
      page.send(kitten.html`<${ErrorMessage} message='Only markdown files are supported' />`)
      return
    }

    if (!fs.existsSync(data.path)) {
      page.send(kitten.html`<${ErrorMessage} message='File not found.' />`)
      return
    }

    // Path seems to be OK, update it.
    updatePath(data.path)
    await updateContent()
    updatePage(page)
    updateWatcher(page)
  })

  // Interestingly, if the input’s value has not changed, htmx
  // sends a null event. For now, I’m going to silently filter it while
  // I wait for a response to the issue I filed about this:
  // https://github.com/bigskysoftware/htmx/issues/2342
  page.on('null', () => {})
}

function updatePath (newPath) {
  kitten.db.document.path = newPath
}

/**
  Find the first character position where two
  strings differ.

  Returns -1 if strings are the same.

  Courtesy of https://stackoverflow.com/a/32859917 where it is
  listed as being a fast algorithm.
*/
function findFirstDiffPos(a, b) {
  let i = 0
  if (a === b) return -1
  while (a[i] === b[i]) i++
  return i
}

/**
  Loads in the latest version of the markdown document being edited
  and, if the document has changed, places a special token where
  the first change has occured so that we can scroll to that position
  in the client.

  @returns {Promise<boolean>} true if content has changed, false otherwise
*/
async function updateContent () {
  const newContent = await fsPromises.readFile(kitten.db.document.path, 'utf-8')

  if (kitten.db.document.content !== '') {
    const firstChange = findFirstDiffPos(newContent, kitten.db.document.content.replace(FIRST_CHANGE_TOKEN, ''))

    kitten.db.document.content = firstChange === -1 ? newContent : newContent.slice(0, firstChange) + FIRST_CHANGE_TOKEN + newContent.slice(firstChange, newContent.length)

    return firstChange === -1 ? false: true
  }

  kitten.db.document.content = newContent
  return true
}

/** Updates page with the latest preview of the markdown file. */
function updatePage (page) {
  page.send(Preview({content: kitten.db.document.content}))
}

/**
  Updates (creates and/or replaces) the file system watcher.
*/
let timeoutInterval

function updateWatcher (page) {
  // Close old watcher, if any.
  if (watcher) {
    watcher.close()
  }

  // Create new watcher.
  watcher = watch(kitten.db.document.path, async () => {
    // The watcher sends multiple events as a file is being written to
    // asynchronously. We have to debounce these events and reload
    // the file and look for the changed area only after the whole
    // file has been written.
    clearTimeout(timeoutInterval)
    timeoutInterval = setTimeout(async () => {
      const hasNewContent = await updateContent()
      if (hasNewContent) {
        console.info('Content has changed, updating page.')
        updatePage(page)
      } else {
        console.info('Content hasn’t changed, not updating.')
      }
    }, 10)
  })
}
