export default function ({request, socket}) {
  console.info('Echo socket: new client connection.', request.session)

  socket.addEventListener('message', event => {
    const message = JSON.parse(event.data).message

    socket.send(kitten.html`
      <ul id='echos' hx-swap-oob='beforeend'>
        <li><strong>“${message}”</strong> received on server.</li>
      </ul>
    `)
  })
}
