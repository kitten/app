export default () => kitten.html`
  <page htmx htmx-websocket alpinejs water>

  <main
    hx-ext='ws'
    ws-connect='/echo.socket'
  >
    <h1>WebSocket Echo</h1>
    <form id='message-form' ws-send>
      <label for='message'>Message</label>
      <input
        id='message' name='message' type='text' required
        x-data
        @htmx:ws-after-send.window='$el.value = ""'
      >
      <button type='submit'>Send</button>
    </form>

    <ul id='echos' hx-swap-oob='beforeend'></ul>
  </main>

  <!-- Log HTMX events to make debugging easier. -->
  <content for='AFTER_LIBRARIES'>
    <script>
      htmx.logger = function ( element, event, data ) {
        if (console) {
          console.info(event, element, data)
        }
      }
    </script>
  </content>
`
