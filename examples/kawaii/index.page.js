import { Cat, HumanDinosaur } from 'kitten-kawaii'

export default () => kitten.html`
  <page css>
  <h1>Hello, I’m pink cat…<br>…and this is my friend, blue dinosaur!</h1>
  <div style='position: relative'>
    <${Cat}
      colour='pink'
      size='50%'
    />
    <${HumanDinosaur}
      colour='lightblue'
      size='100%'
      style='position:absolute; bottom: -2em; right: 3em'
    />
  </div>
  <markdown>
  We are [Kitten Kawaii](https://kitten-kawaii.small-web.org) characters.

  Kitten Kawaii is a Kitten port of [React Kawaii](https://react-kawaii.vercel.app) by [Miuki Miu](https://www.miukimiu.com) (Elizabet Oliveira).

  [View source](https://codeberg.org/aral/kitten-kawaii/)
  </markdown>
`
