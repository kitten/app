if (!kitten.db.comments) kitten.db.comments = []

const Footer = () => kitten.markdown`
  <footer class='Footer'>

    ---
    Copyright (c) 2023-present, Aral Balkan.
    
    Released under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

  </footer>
  <style>
    .Footer { margin-top: 2em; margin-bottom: 3em; }
    .Footer p { text-align: center; font-size: small; }
  </style>
`

// This is a demonstration of using the markdown tagged template function.
// If your development environment doesn’t render it nicely, you can
// also use the html tagged template function and surround your Markdown
// section(s) with <markdown>…</markdown>. In fact, all the markdown tagged
// template function does is to do that for you behind the scenes :)
export default () => kitten.markdown`
  <page htmx syntax-highlighter>

  # My lovely blog

  ![Black and white photo (close-up) of a white passing man](https://ar.al/images/aral-432.jpg "It’s-a me… Mario! Umm… Aral.")

  Lots of interesting things, including code snippets:

  [code js]
  function say (message) {
    console.info(message)
  }
  [code]

  <h2>Comments</h2>

  <if ${kitten.db.comments.length === 0}>
    <p>No one’s commented yet… be the first?</p>
  <else>
    <ul>
      ${kitten.db.comments.map(comment => kitten.html`
        <li>
          <p>${comment.message}</p>
          <p class='nameAndDate'>${comment.name} (${new Date(comment.date).toLocaleString()})</p>
        </li>
      `)}
    </ul>
  </if>

  <h3>Leave a comment</h3>

  <form method='POST' action='/comment' hx-post='/comment' hx-target='body'>
    <label for='message'>Message</label>
    <textarea id='message' name='message' required></textarea>
    <label for='name'>Name</label>
    <input type='text' id='name' name='name' required>
    <button type='submit'>Comment</button>
  </form>

  <${Footer} />

  <style>
    body { font-family: sans-serif; margin-left: auto; margin-right: auto; max-width: 30em; }

    figure { margin-left: 0; width: 100%; }
    figcaption { text-align: center; font-weight: bold;}
    img { width: 100%; }

    form {
      display: flex;
      flex-flow: column;
    }

    textarea, input[type='text'] { font-family: sans-serif; }
    textarea { height: 4em; margin-bottom: 1em; }
    button { margin-top: 1em; font-size: 1em; }
    ul { list-style-type: none; padding: 0; }
    li { border-top: 1px dashed #999; }
    .nameAndDate { font-style: italic; font-size: small; text-align: right; }
  </style>
`
