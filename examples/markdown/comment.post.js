if (!kitten.db.comments) kitten.db.comments = []

export default ({request, response}) => {
  // Basic validation.
  if (!request.body || !request.body.message || !request.body.name) {
    response.forbidden()
    return
  }
  
  kitten.db.comments.push({
    message: request.body.message,
    name: request.body.name,
    date: Date.now()
  })
  
  response.get('/')
}
