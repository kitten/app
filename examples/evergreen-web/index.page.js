export default () => kitten.html`
  <page water>
  <markdown>
    # Evergreen Web (404 → 307)

    Send 404 (Not Found) requests to the previous version of your site so you don’t break URLs.

    ## Instructions

    1. Open [/🐱/settings/](/🐱/settings/)

    2. Set an Evergreen Web URL (e.g., ar.al)

    3. Hit the following links:

    ## A page that exists on the current site:

    - [/exists](/exists)

    (This will load normally.)

    ## Pages that exist on the previous version of this site:

    - [/2020/08/07/what-is-the-small-web/](/2020/08/07/what-is-the-small-web/)
    - [/2023/02/20/end-to-end-encrypted-kitten-chat/](/2023/02/20/end-to-end-encrypted-kitten-chat/)

    (These result in a 404 locally and are redirected to Aral’s blog.)

    ## Learn more

    For more information, see [4042307.org](https://4042307.org).
  </markdown>
`
