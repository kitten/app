// @ts-ignore CSS module.
import Styles from './index.fragment.css'

if (!kitten.db.images) kitten.db.images = []

export default () => kitten.html`
  <page css>
  <h1>Image gallery</h1>

  <if ${kitten.db.images.length === 0}>
    <markdown>
      No images yet. Why not [upload one](#upload)?
    </markdown>
  </if>

  <ul>
    ${kitten.db.images.map(image => kitten.html`
      <li><img src=${image.path} alt=${image.altText}></li>
    `)}
  </ul>

  <h2 id='upload'>Upload an image</h2>

  <form method='post' enctype='multipart/form-data'>
    <label for='image'>Image</label>
    <input type='file' id='image' name='image' accept='image/*'>
    <label for='alt-text'>Alt text</label>
    <input type='text' id='alt-text' name='altText'>
    <button type='submit'>Upload</button>
  </form>

  <footer>
    <a href='/🐱/settings'>🐱 Kitten Settings</a>
  </footer>

  <${Styles} />
`
