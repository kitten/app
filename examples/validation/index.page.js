const random = () =>
  // Take the sine of a random angle between 45° and 135° (-0.707 and 0.707)
  // (See it visually at https://www.mathsisfun.com/algebra/trig-interactive-unit-circle.html)
  Math.cos(Math.random() * 90 + 45) 
  // Add a larger random twitch every now and then (~0.05% of the time)
  // to keep things interesting.
  + (Math.random() > 0.95 ? Math.cos(Math.random() * 90 + 45) * 5 : 0)

const lettersToAnimate = [...'SCARY HTML VALIDATION ERRORS']

export default () => kitten.html`
  <main>
    <img src='/🐱/images/kitten-sitting.svg' width='200px'>
    <h2>Meow!</h2>
    <p>
      I’m just a sweet little kitten… surely I’m not hiding any ${
        lettersToAnimate.map((letter, index) => letter === ' ' ? '&nbsp;' : kitten.html`
          <span class='scary animation${index}'>${letter}</span>
        `)
      } (dare to look in your browser’s developer console?)
    </p>
  </main>

  <style>
    @font-face {
      /* 
        Subset of Creepser font by Sideshow (OFL license)

        The web font includes the letters we’re using in the scary message
        and so it’s only 8kb in size. 

        Font: https://www.1001fonts.com/creepster-font.html
        Online subsetting tool: https://everythingfonts.com/subsetter
        Online TTF to WOFF2 convertor: https://cloudconvert.com/ttf-to-woff2
      */
      font-family: 'scary';
      src: url('./scary.woff2') format('woff2');
      font-weight: normal;
      font-style: normal;
    }

    body {
      font-family: sans-serif;
      text-align: center;
      font-size: 1.33rem;
    }

    h2 { font-size: 5em; margin: 0; }

    p { max-width: 43ch; margin-left: auto; margin-right: auto; }

    img { width: 20em; }

    .scary { 
      font-family: 'scary';
      font-size: 1.5em;
      color: red;
      display: inline-block;
    }

    /* 
      Generate different animation classes for each letter and
      randomise the animation by setting a random place in
      the animation to start it.

      Note that we only make these classes available if the person
      has not expressed a preference for reduced motion to remain
      accessible to people with vestibular motion disorders
      (motion sickness).

      https://vestibular.org/article/coping-support/living-with-a-vestibular-disorder/motion-sickness/
      ).
    */
    @media screen and (prefers-reduced-motion: no-preference) {
      ${
        lettersToAnimate.map(
          (_, index) => `
            .animation${index} {
              animation: 0.66s jiggle ease -${Math.min(Math.random(), 0.66)}s infinite;
            }
          `
        )
      }
    }

    @keyframes jiggle
    {
      ${
        [...Array(11).keys()]
          .map(i => i*10)
          .map(percentage => kitten.html`
            ${percentage}%
            {
              transform: translate(${random()}px, ${random()}px) rotate(${random() * 5}deg);
            }
          `)
      }
    }
  </style>
`
