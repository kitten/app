export default function ({ request }) {
  if (!request.session.kittens) {
    request.session.kittens = { count: 1 }
  }

  return kitten.html`
    <h1>Kitten count</h1>
    <p>${'🐱️'.repeat(request.session.kittens.count++)}</p>
  `
}
