## And this is from a markdown file

As is this.

- Title: __${SLOT.title}__
- Date: __${SLOT.date}__

${SLOT}

And the content above was slotted into a markdown file.
