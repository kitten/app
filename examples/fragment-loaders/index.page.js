import Markup from './Markup.fragment.html'
import Styles from './Styles.fragment.css'
import Markdown from './Markdown.fragment.md'

export default () => kitten.html`
  <page css>
  <${Markup} />

  <${Markdown}>
    <content for='title'>This is the title</content>
    <content for='date'>${new Date()}</content>
    <button>I’ve been slotted in.</button>
  </>

  <${Styles} />
`
