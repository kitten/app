import { Images } from './index.page.js'

if (!kitten.db.images) kitten.db.images = []

export default function ({ request, response }) {
  request.uploads.forEach(upload => {
    kitten.db.images.push({
      path: upload.resourcePath,
      altText: request.body.altText ? request.body.altText : upload.fileName
    })
  })
  return kitten.html`
    <${Images} />
  `
}
