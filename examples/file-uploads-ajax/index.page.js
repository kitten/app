if (!kitten.db.images) kitten.db.images = []

/**
  Images component: display grid of uploaded images.
*/
export function Images () {
  return kitten.html`
    <div id='images'>
      <if ${kitten.db.images.length === 0}>
        <markdown>
          No images yet. Why not [upload one](#upload)?
        </markdown>
      </if>
      <ul>
        ${kitten.db.images.map(image => kitten.html`
          <li><img src=${image.path} alt=${image.altText}></li>
        `)}
      </ul>

      <h2 id='upload'>Upload an image</h2>

      <form
        hx-post
        hx-encoding='multipart/form-data'
        hx-target='#images'
      >
        <label for='image'>Image</label>
        <input type='file' id='image' name='image' accept='image/*'>
        <label for='alt-text'>Alt text</label>
        <input type='text' id='alt-text' name='altText'>
        <button type='submit'>Upload</button>
      </form>
    </div>
  `
}

export default () => kitten.html`
  <page htmx>
  <h1>Image gallery</h1>

  <${Images} />

  <style>
    body { max-width: 640px; margin: 0 auto; padding: 1em; font-family: sans-serif; }
    ul { padding: 0; display: grid; grid-template-columns: 1fr 1fr; }
    li { list-style-type: none; }
    img { max-height: 30vh; margin: 1em; }
    input { width: 100%; margin: 1em 0; }
    button { padding: 0.25em 1em; display: block; margin: 0 auto; }
  </style>
`
