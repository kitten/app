if (!kitten.db.entries) kitten.db.entries = []

export default ({request, response}) => {
  // Basic validation.
  if (!request.body || !request.body.message || !request.body.name) {
    return response.forbidden()
  }
  
  // Notice we use array.unshift() here to add the latest
  // entry to the top of the list of entries. (The
  // commonly used array.pop() method adds them to the end.)
  kitten.db.entries.unshift({
    message: request.body.message,
    name: request.body.name,
    date: Date.now()
  })
 
  response.get('/')
}
