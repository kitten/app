if (!kitten.db.entries) kitten.db.entries = []

export default () => kitten.html`
  <h1>Guestbook</h1>

  <h2>Sign</h2>

  <form method='POST' action='/sign'>
    <label for='message'>Message</label>
    <textarea id='message' name='message' required></textarea>
    <label for='name'>Name</label>
    <input type='text' id='name' name='name' required>
    <button type='submit'>Sign</button>
  </form>

  <h2>Entries</h2>

  <if ${kitten.db.entries.length === 0}>
    <p>No one’s signed yet… be the first?</p>
  </if>

  <ul>
    ${kitten.db.entries.map(entry => kitten.html`
      <li>
        <pre class='message'>${entry.message}</pre>
        <p class='nameAndDate'>${entry.name} (${new Date(entry.date).toLocaleString()})</p>
      </li>
    `)}
  </ul>

  <style>
    body { font-family: sans-serif; margin-left: auto; margin-right: auto; max-width: 20em; }
    label { display: block; }
    textarea, input[type='text'] { font-family: cursive; width: 100%; font-size: 1.5em; }
    textarea { height: 5em; }
    button { width: 100%; height: 2em; margin-top: 1em; font-size: 1em; }
    ul { list-style-type: none; padding: 0; }
    li { border-top: 1px dashed #999; }
    .message, .nameAndDate { font-family: cursive; }
    .message { font-size: 1.5em; }
    .nameAndDate { font-size: 1.25em; font-style: italic; text-align: right; }
  </style>
`
