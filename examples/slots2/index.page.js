const Box = ({lineStyle = 'solid', SLOT}) => kitten.html`
  <div class='Box'>
    ${SLOT}
  </div>

  <style>
    .Box {
      padding: 1em;
      border: 1px ${lineStyle} CornflowerBlue;
    }
    .Box em { color: DarkViolet; }
  </style>
`

export default () => kitten.html`
  <h1>Page</h1>
  <p>This is the page.</p>

  <${Box} lineStyle=dashed>
    <h2>Slotted content</h2>
    <p class='override'>This is <em>slotted content</em> from the page.</p>
  </>

  <p>This is the page again.</p>

  <style>
    body { font-family: sans-serif; }

    /* Example of using the cascade to override styles */
    /* in components for slotted content. */
    .override em { color: DeepPink; }
  </style>
`
