let count = 1

export default () => kitten.markdown`
  # Kitten count
  ${'🐱️'.repeat(count++)}
`
