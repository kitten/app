import Site from './Site.layout.js'
import { ABOUT } from './pages.script.js'

export default () => kitten.html`
  <page title='About me'>

  <${Site} pageId=${ABOUT}>
    <markdown>
      ## Hey, look, it’s me!
      Information about me.
    </markdown>

    <content for='footer'>
      <div class='funding'>
        <p>To fund my work, donate to <a href='https://small-tech.org/fund-us'>Small Technology Foundation</a></p>
      </div>
    </content>
  </>
  <style>
    .funding {
      padding: 1em;
      margin-bottom: 1em;
      background-color: aquamarine;
      border-radius: 1em;
      font-size: 1rem;
    }
    .funding a {
      color: inherit;
    }
  </style>
`
