import Header from './Header.component.js'
import Navigation from './Navigation.component.js'
import Footer from './Footer.component.js'

export default ({ pageId, SLOT }) => kitten.html`
  <${Header} pageId=${pageId} class='header'>
    <content for='aboveHeading'>
      <${Navigation} pageId=${pageId} class='navigation'/>
    </content>

    ${SLOT.header}
  </>

  <main>
    ${SLOT}
  </main>

  <${Footer} class='footer'>
    ${SLOT.footer}
  </>

  <style>
    body {
      font-family: system-ui, sans-serif;
      padding: 1em;
    }
    /*
      This is how you style components. Any styles
      declared here will override the ones defined in
      the header, navigation and footer components.
      If you have styles in your component that you
      do not want overriden (careful with that, it’s
      best to be flexible with such things),
      use !important.
    */
    .header {
      font-variant: small-caps;
    }
    .navigation {
      font-variant: none;
      color: white;
      padding: 1em;
      background-color: cadetblue;
    }
    .footer {
      margin-top: 2em;
    }
  </style>
`
