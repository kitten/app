export default ({ SLOT, CLASS }) => kitten.html`
  <footer class='Footer ${CLASS}'>
    ${SLOT}
    <markdown>
      Copyright (c) 2023-present, Me.

      The content on this site is released under [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

      The source code of this site is released under [GNU AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

      Powered with love by Kitten. 🐱 💕
    </markdown>
  </footer>

  <style>
    .Footer {
      border-top: 1px solid gray;
      padding-top: 1em;
      text-align: center;
      font-size: small;
    }
    .Footer p { margin: 0.25em; }
  </style>
`
