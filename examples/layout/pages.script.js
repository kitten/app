export const [ HOME, ABOUT, CONTACT ] = ['home', 'about', 'contact-me']
export const pages = {
  [HOME]: { title: 'Home', link: '/' },
  [ABOUT]: { title: 'About', link: '/about' },
  [CONTACT]: { title: 'Contact', link: '/contact' }
}
