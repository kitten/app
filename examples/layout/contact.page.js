import Site from './Site.layout.js'
import { CONTACT } from './pages.script.js'

export default () => kitten.html`
  <${Site} pageId=${CONTACT}>
    <markdown>
      ## Get in touch!
      Normally, there’d be a form here. Look in the guestbook and markdown examples if you want to see examples of such forms.
    </markdown>
    <content for='header'>
      <p class='awayMessage'>I’m away on holiday at the moment</p>
    </content>
  </>
  <style>
    .awayMessage {
      text-align: center;
      font-weight: bold;
      font-size: 1.5em;
      background-color: yellow;
    }
  </style>
`
