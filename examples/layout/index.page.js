import Site from './Site.layout.js'
import { HOME } from './pages.script.js'

export default () => kitten.html`
  <${Site} pageId=${HOME}>
    <markdown>
      ## Welcome to my home page!
      There are many home pages but this one is mine.
      I hope you enjoy it.
    </markdown>
  </>
`