import { pages } from './pages.script.js'

export default ({ pageId, SLOT, CLASS }) => kitten.html`
  <header class='Header ${CLASS}'>
    ${SLOT.aboveHeading}
    <h1>${pages[pageId].title}</h1>
    ${SLOT}
  </header>

  <style>
    .Header {
      border-bottom: 1px solid gray;
    }
  </style>
`
