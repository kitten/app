import { pages } from './pages.script.js'

export default ({ pageId, CLASS }) => kitten.html`
  <nav class='Navigation ${CLASS}'>
    <ul>
      ${Object.entries(pages).map(([__pageId, page]) => kitten.html`
        <li>
          <if ${ __pageId === pageId }>
            <span class='currentPage'>${page.title}</a>
          <else>
            <a href='${page.link}'>${page.title}</a>
          </if>
        </li>
      `)}
    </ul>
  </nav>

  <style>
    .Navigation { background-color: red; }
    .Navigation ul { list-style-type: none; display: flex; padding: 0; }
    .Navigation li:not(:first-of-type) { margin-left: 1em; }
    .Navigation a { color: white; }
    .Navigation .currentPage { font-weight: bold; }
  </style>
`
