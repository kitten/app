export default () => kitten.html`
  <page
    water
    syntax-highlighter-theme-light='/color-brewer.css'
    syntax-highlighter-theme-dark='/nord.css'
    title='Water.css example'
  >

  <markdown>
    # Water

    ![Water CSS library logo: the letters CSS with waves showing through them.](https://github.com/kognise/water.css/raw/master/assets/logo.svg)

    ## What is it?

    [Water](https://watercss.kognise.dev/)[^1] is a minimalist CSS stylesheet for semantic HTML that is responsive and has light and dark mode support. It’s convenient for quick experiments, teaching, and demos and could also be used as a good base stylesheet for your Small Web sites and apps.

    ## How do you use it in Kitten?

    Just add \`water\` to your \`page\`:

    [code js]
    export default () => html\`
      <page water>
      <h1>Hello, world!</h1>
    \`
    [code]

    ## View source

    [View the source code for this example.](https://codeberg.org/kitten/app/src/branch/main/examples/trivia/index.page.js)

    [^1]: Source: https://github.com/kognise/water.css
  </markdown>

  <style>
    pre { font-size: 1.25em; }
  </style>
`
