export default function Message ({ message }) {
  return kitten.html`
    <li>
      <strong>${message.nickname}</strong> ${message.text}
    </li>
  `
}
