let count = 1

export default () => kitten.html`
  <h1>Kitten count</h1>
  <p>${'🐱️'.repeat(count++)}</p>
`
