# 🐱️ Kitten

A 💕 [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) development kit.

- Build using HTML, CSS, and JavaScript.
- Progressively enhance with Streaming HTML and htmx.
- Go beyond traditional web apps to create peer-to-peer 💕 [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) apps.

> ### 🍼 __Warning:__ Kitten is still a baby.
> 
> __Expect breaking changes until API version 1.__

## Get started

To get started, head over to the [Kitten web site](https://kitten.small-web.org).

The Kitten web site includes [installation instructions](https://kitten.small-web.org), [tutorials](https://kitten.small-web.org/tutorials), [FAQ](https://kitten.small-web.org/faq), and a [reference guide](https://kitten.small-web.org/reference).

## Like this? Fund us!

[Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.

## Questions?

Contact <a href="https://mastodon.ar.al/@aral" rel="me">Aral</a> on the fediverse.
