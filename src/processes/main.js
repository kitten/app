/**
  Kitten main process; command-line interface (CLI)

  Copyright ⓒ 2021-present, Aral Balkan
  Small Technology Foundation
  License: AGPL version 3.0.
*/

import fs from 'node:fs'
import paths from '../lib/paths.js'
import CLI from '../cli/index.js'
import Version from '../lib/Version.js'
import GlobalInternalDatabase from '../GlobalInternalDatabase.js'

// Ensure global kitten namespace always exists. 
globalThis.kitten = {
  version: Version.getInstance()
}

// Monkeypatch console’s debug method so it only logs if the
// environment variable VERBOSE is set.
const originalDebug = console.debug
console.debug = process.env.VERBOSE ? function () { originalDebug(...arguments) } : () => {}

// Ensure we run migrations if necessary.
if (!fs.existsSync(paths.KITTEN_DATA_DIRECTORY)) {
  // Run data encapsulation migration (where we went from having three separate folders
  // for databases, uploads, and repl data to one app-specific folder per app with _db,
  // db, uploads, and repl data under it in a general data folder.
  const migration = (await import('../migrations/data-encapsulation.js')).default
  migration()
}

// Monkeypatch bind() so that the bound function has a boundObject property
// that points to the object it is bound to. This is used in the Kitten
// component model when rendering to automatically inject the automatically
// generated universally unique ID (UUID) as the ID of the the top-level DOM
// element of a component if one was not manually set. This, in turn,
// simplifies authoring by not requiring a manually-entered unique ID for
// components before they are streamed back (morphed, etc.)
//
// Idea/implementation courtesy of Nathan Wall (https://stackoverflow.com/a/14309359)

var _bind = Function.prototype.apply.bind(Function.prototype.bind);
Object.defineProperty(Function.prototype, 'bind', {
  value: /** @param {any} obj */ function (obj) {
    var boundFunction = _bind(this, arguments)
    boundFunction.boundObject = obj
    return boundFunction
  }
})

// Initialise the global internal database.
await GlobalInternalDatabase.initialise()

const cli = new CLI()
await cli.parse()
