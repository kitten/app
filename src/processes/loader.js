/**
  🐱️ Kitten loader.

  Associates Kitten’s <ext>.js file extensions with JavaScript modules so
  we don’t have to use the ridiculous .mjs extension or require people to
  add a package.json file with "type": "module" into their projects just to
  make the Node runtime happy.

  Copyright © 2022-present Aral Balkan, Small Technology Foundation.
  Licensed under GNU AGPL version 3.0.

  https://codeberg.org/kitten/app
*/

// @ts-ignore mixin
console.profileTime = process.env.PROFILE ? function () { console.time(...arguments) } : () => {}
// @ts-ignore mixin
console.profileTimeEnd = process.env.PROFILE ? function () { console.timeEnd(...arguments) } : () => {}

import path from 'node:path'
import fsPromises from 'node:fs/promises'
import { pathToFileURL, fileURLToPath } from 'node:url'
import pathCompleteExtname from 'path-complete-extname'
import { parse } from 'yaml'
import markdownIt from '../lib/markdown.js'

const __dirname = fileURLToPath(new URL('.', import.meta.url))
const layoutComponentClassNameRegExp = /([^\/]+?)\.layout\.js$/

const createComponentsRegExp = componentNames => new RegExp(
  `<\\$\\{(${componentNames.join('|')})\\}.*?\\/>`,
  'g'
)

// Token to represent component in markdown page.
const COMPONENT_IN_MARKDOWN_PAGE = '\ue050'

/**
  Return a resolved object from a path.

  @param {string} importPath
*/
function resolvedFromPath(importPath) {
  return {
    url: pathToFileURL(importPath).toString(),
    format: 'module',
    shortCircuit: true
  }
}

// TODO: We’re no longer testing using the context object.
// If we don’t introduce any new tests that need this
// functionality, we should remove it from here.
export const context = {
  fsPromises,
  __dirname
}

const kittenExtensions = [
  'page', 'post', 'get', 'put', 'head', 'patch', 'options', 'connect', 'delete', 'trace',
  'socket', 'component', 'layout', 'fragment', 'script', 'styles'
].map(extension => `.${extension}.js`)

// Markdown files are pages.
kittenExtensions.push('.page.md')

/**
  @typedef {{
      format?: string?,
      importAssertons?: Object,
      shortCircuit?: boolean?
  }} ResolveReturnType
*/

export const resolve = (
  /**
    @param {string} specifier
    @param {{
      conditions: string[],
      importAssertions: Object,
      parentURL?: string
    }} context
    @param {Function} nextResolve

    @returns {Promise<{
      format?: string?,
      importAssertons?: Object,
      shortCircuit?: boolean?
    }>}
  */
  async function (specifier, context, nextResolve) {
  const specifierExtension = pathCompleteExtname(specifier)

  let parentUrl = null
  let parentFilePath = null
  let parentDirectoryPath = null
  let specifierAbsolutePath = null

  if (context.parentURL) {
    parentUrl = new URL(context.parentURL)
    parentFilePath = parentUrl.pathname
    parentDirectoryPath = path.dirname(parentFilePath)
    
    // Note: running the absolute path through fileURLToPath is
    // necessary or else path.resolve URL escapes any emojis in the path.
    specifierAbsolutePath = fileURLToPath('file://' + path.resolve(parentDirectoryPath, specifier))
  }

  const isKittenJavaScriptModule = kittenExtensions.includes(specifierExtension)
  let resolved = null

  switch (true) {
    case isKittenJavaScriptModule:
      if (specifierAbsolutePath === null) {
        console.error('[LOADER] ERROR: Could not resolve JavaScript module', specifier, context)
        console.trace()
        process.exit(1)
      }
      resolved = resolvedFromPath(specifierAbsolutePath)
      break

    default:
      // For anything else, let Node do its own magic.
      try {
        resolved = nextResolve(specifier)
      } catch (error) {
        // The default resolver has failed. We cannot recover from this. Panic!
        console.error('[LOADER] ERROR: Could not resolve', specifier, context)
        console.trace()
        process.exit(1)
      }
  }

  return resolved
}).bind(context)

/**
  Wrap supported non-JavaScript files (CSS and HTML) into JavaScript routes.
*/
export const load = (async function(urlString, context, nextLoad) {
  const url = new URL(urlString)
  const isFile = url.protocol === 'file:'

  // fileURLToPath is necessary for correct decoding of special characters (like emoji).
  const filePath = fileURLToPath('file://' + path.resolve(url.pathname))

  let loadedObject

  switch (true) {
    case isFile && filePath.endsWith('.fragment.css'):
      loadedObject = htmlFragmentModule(`
        <style>
          ${await fsPromises.readFile(filePath, 'utf-8')}
        </style>
      `)
    break

    case isFile && (filePath.endsWith('.fragment.md')): {
      const markdown = await fsPromises.readFile(filePath, 'utf-8')
      const escapedMarkdown = markdown.replace(/`/g, '\\`')
      loadedObject = htmlFragmentModule(`<div>\${[kitten.md.render(\`${escapedMarkdown}\`)]}</div>`)
    }
    break

    case isFile && (filePath.endsWith('.page.md')): {
      let markdown = await fsPromises.readFile(filePath, 'utf-8')
      let frontMatter
      let props
      let layoutTemplatePath
      let layoutTemplateClass
      let imports
      let componentNames
      let componentMatches

      // Extract front matter. (Must start with --- on the first line and end
      // with --- on its own line.
      const lines = markdown.split('\n')
      if (lines[0] === '---') {
        const frontMatterEndIndex = lines.findIndex((line, index) => index > 0 && line === '---')
        if (frontMatterEndIndex !== -1) {
          frontMatter = parse(lines.splice(1, frontMatterEndIndex-1).join('\n'))
          lines.shift()
          lines.shift()
          markdown = lines.join('\n')

          // Handle layout template (if specified).
          if (frontMatter.layout) {
            layoutTemplatePath = frontMatter.layout
            const match = layoutComponentClassNameRegExp.exec(layoutTemplatePath)
            if (match !== null) {
              layoutTemplateClass = match[1]
              // Delete the import property now that we’ve processed it.
              // Anything else in the layout object will be treated as a
              // prop to pass to the layout component.
              delete frontMatter.layout
              props = frontMatter
            } else {
              console.warn(`⚠️ Layout import has incorrect name (should be <ComponentName>.layout.js). Not using layout template.`, filePath)
            }
          }

          // Handle imports (if any).
          if (frontMatter.imports) {
            imports = /** @type {string[]} */ (frontMatter.imports)

            // Get the class names from the imports so we can extract the components
            // from the source and replace them with placeholders before carrying out
            // the Markdown rendering and then insert them back in for kitten.html
            // to handle.
            componentNames = imports.map(currentImport => {
              const match = /import (.+?) from/.exec(currentImport)
              if (match === null) {
                console.error(`❌ Imports must be in the form: import Component from '../some/Component.component.js`, currentImport)
                return null
              } else {
                return match[1]
              }
            })
            if (componentNames.includes(null)) {
              console.warn(`⚠️ Syntax error in imports. Imports will be ignored.`)
              imports = undefined
            }
            const componentsRegExp = createComponentsRegExp(componentNames)
            componentMatches = markdown.match(componentsRegExp)

            if (componentMatches === null) {
              console.warn('⚠️ All imported components/fragments unused in', filePath)
              componentMatches = undefined
            } else {
              // Replace all Kitten components with a token that will survive
              // Markdown rendering. After we render the Markdown, we will put the
              // components back and then let Kitten render them.
              componentMatches.forEach(component => markdown = markdown.replace(component, COMPONENT_IN_MARKDOWN_PAGE))
            }
            
            delete frontMatter.imports
          }
        } else {
          console.warn(`⚠️ Front matter missing ending delimeter (---), ignoring front matter.`, filePath)
        }
      }
      
      const escapeMarkdown = m => typeof m === 'string'
        ? m
            .replace(/`/g, '\\`')
            .replace(/\\n/g, '\\\\n')
            .replace(/\$\{/g, '\\${')
        : m
      let renderedMarkdown = escapeMarkdown(markdownIt.render(markdown))

      // If any components were wrapped in paragraph tags by the Markdown
      // renderer, strip them.
      renderedMarkdown = renderedMarkdown.replaceAll(`<p>${COMPONENT_IN_MARKDOWN_PAGE}</p>`, COMPONENT_IN_MARKDOWN_PAGE)

      // Add back Kitten components/fragments to the Markdown if necessary.
      if (componentMatches !== undefined) {
        let index = 0
        renderedMarkdown = renderedMarkdown.replace(/\ue050/g, () => {
          return componentMatches[index++]
        })
      }
      
      const source = `
        ${
          // Import layout template if one is specified in the front matter.
          (layoutTemplateClass !== undefined)
          ? `import ${layoutTemplateClass} from '${layoutTemplatePath}'`
          : ''
        }
        ${
          // Import custom imports (e.g., components) if any are specified in the front matter.
          (imports !== undefined)
          ? imports.join('\n')
          : ''
        }
        export default ({ SLOT = '', page, request, response }) => kitten.html\`
          ${
            // Include layout template if one is specified in the
            // front matter and pass it any props that might also
            // have been specified.
            //
            // Also pass references to the page (if any), request,
            // and respones objects to the layout template as there
            // is no other way the author can do so themselves in
            // Markdown (while it is trivial to do so in JavaScript pages).
            (layoutTemplateClass !== undefined)
            ?
              `<\${${layoutTemplateClass}}`
              + Object.entries(props).map(entry =>
                // Flatten arrays and also escape commas so a split on comma can reform the array.
                ` ${entry[0]}="${Array.isArray(entry[1]) ? entry[1].map(e => escapeMarkdown(e).replace(/,/g, '&comma;')).join(',') : escapeMarkdown(entry[1])}"`
              ).join('')
              + ' page=${page} request=${request} response=${response}'
              + '>'
            : ''
          }
          ${renderedMarkdown}
          ${
            // Close the layout template tag if one is specified.
            (layoutTemplateClass !== undefined) ? '</>' : ''
          }
        \`
      `

      loadedObject = {
        format: 'module',
        shortCircuit: true,
        source 
      }
    }
    break

    case isFile && filePath.endsWith('.fragment.html'):
      loadedObject = htmlFragmentModule(await fsPromises.readFile(filePath, 'utf-8'))
    break

    default:
      loadedObject = await nextLoad(urlString, context, nextLoad)
  }

  return loadedObject
})

/**
  Transform passed source string into a Kitten HTML fragment.
  @param {string} source
*/
function htmlFragmentModule (source) {
  return {
    format: 'module',
    shortCircuit: true,
    source: `
      export default ({ SLOT = '' }) => kitten.html\`
        ${source}
      \`
    `
  }
}
