/**
  Kitten Server.

  Copyright © 2021-present, Aral Balkan
  Small Technology Foundation
  License: AGPL version 3.0.
*/

import fs from 'node:fs'
import path from 'node:path'
import process from 'node:process'
import { randomInt } from 'node:crypto'

import chalk from 'chalk'
import link from './lib/terminal-link.js'
import open from 'open'
import polka from 'polka'

import { random32ByteTokenInHex } from './lib/crypto.js'
import startKittenRepl from './REPL.js'

import { WebSocketServer } from 'ws'

import addAllMiddlewareToApp from './middleware/index.js' 
import expressBusBoy from 'express-busboy'
import serverErrorHandler from './ServerError.js'

import { downloadHandler } from './Uploads.js' 
import AutomaticUpdates from './AutomaticUpdates.js'
import AppRepository from './AppRepository.js'
import InternalDatabase from './InternalDatabase.js'
import AppDatabase from './AppDatabase.js'

// FIXME: Add type definitions to this module.
import https from '@small-tech/https'
import http from 'node:http'

import {
  exitWithError,
  getDomainsAndPort
} from './Utils.js'
import paths from './lib/paths.js'
import Globals from './lib/globals.js'
import { runNpmCiOnModulePath } from './Utils.js'

import Routes from './Routes.js'
import KittenPackage from './lib/KittenPackage.js'
import systemExitCodes from './lib/system-exit-codes.js'
import GlobalInternalDatabase from './GlobalInternalDatabase.js'

/**
  @typedef {{
    domain?: string,
    port?: number,
    aliases?:string,
    open?:boolean,
    'working-directory'?: string,
    'domain-token'?: string,
    'small-web-host-domain'?: string
  }} ServerOptions
*/

export default class Server {
  /** @type {Server} Singleton instance. */
  static instance
  static isBeingConstructedViaFactoryMethod = false

  options
  port

  /**
    Singleton factory method.

    Await this instead of calling `new Server(…)` and 
    `await server.initialise()`` manually.

    Returns a promise for a reference to the server singleton,
    that resolves immediately if it exists, and otherwise
    resolves once it is created and initialised.
  */
  static async getInstanceAsync (options) {
    if (this.instance === undefined) {
      this.isBeingConstructedViaFactoryMethod = true
      this.instance = new this(options)
      await this.instance.initialise()
    }
    return this.instance
  }

  /**
    Create server instance using passed options.

    @param {ServerOptions} options
  */
  constructor (options) {
    // Ensure singleton access at development time.
    if (!Server.isBeingConstructedViaFactoryMethod) {
      throw new Error('Server is a singleton, please await the getInstanceAsync static factory method to get a reference to it.')
    }
    // Do NOT reset the flag as it is also used by the async initialise() method.
    
    // Do not allow this process to crash. A faulty app should
    // not be able to crash the server.
    process.on('uncaughtException', (error, origin) => {
      console.error('🙀 Uncaught exception', error, origin)
      process.exit(systemExitCodes.restartRequest)
    })

    // The port can come in as boolean (true) if --port is passed.
    // If so, we treat it as if it was --port=0 and pick a random one.
    // @ts-ignore
    if (options.port === true) options.port = 0

    const { domains, port } = getDomainsAndPort(options)
    const domain = domains[0]

    this.domain = domain
    this.port = port

    this.open = options.open 

    // Also save references to the domain and port in the
    // global kitten namespace so places can make use of them.
    globalThis.kitten.domain = domain
    globalThis.kitten.port = port

    this.options = {
      domains,
      settingsPath: paths.KITTEN_TLS_DIRECTORY,
      domainToken: options['domain-token'],
      smallWebHostDomain: options['small-web-host-domain']
    }

    // Create the app.
    this.app = polka({ onError: serverErrorHandler })

    // Add all middleware.
    addAllMiddlewareToApp(this.app)

    // Initialise globals. These will be available for us from all routes.
    //
    // Note that if a random port is being requested, we pick a random fake port number
    // between 100,000 and 200,000 that’s outside of the actual port number space (0-65,535)
    // so the data for this run will have its own location. Remember that we won’t know the
    // kernel-assigned random/free port until after we’ve started the server so we can’t
    // use that.
    //
    // Important: If a server is being run on a random port, you should not have any expectation
    // that its data will be available in the next run (as it will get a different port
    // and ports are part of what constitute an app’s identity in Kitten and, thereby, where
    // its data is stored).
    Globals.initialise(domain, this.port === 0 ? randomInt(100_000, 200_000) : this.port)

    // Add body and multi-part form parsing middleware (Express Busboy)
    this.projectUploadsPath = globalThis.kitten.paths.APP_UPLOADS_DIRECTORY

    // Make sure uploads directory exists.
    fs.mkdirSync(this.projectUploadsPath, { recursive: true })

    // @ts-ignore calling method that expects Express application instance with Polka application instance.
    expressBusBoy.extend(this.app, {
      upload: true,
      path: this.projectUploadsPath
    })
  }

  async initialise () {
    // Ensure singleton access at development time.
    // (Instance is configured via a combination of the synchronous constructor
    // and this asynchronous method.)
    if (!Server.isBeingConstructedViaFactoryMethod) {
      throw new Error('Server is a singleton, please await the getInstanceAsync static factory method to get a reference to it.')
    }
    // Reset the flag as this is the second and last step in instance creation/configuration.]
    Server.isBeingConstructedViaFactoryMethod = false

    // Initialise the internal database. We need to do this before anything else
    // so that Kitten has its own database available (it’s necessary before
    // we install any npm modules as we keep package-lock.json hashes in the
    // database, for example.)
    const internalDatabase = new InternalDatabase()
    try {
      await internalDatabase.initialise()
    } catch (error) {
      console.error(error)
      console.error(chalk.red.bold('\n❌ Cannot continue. Exiting.'))
      process.exit(1)
    }

    // Initialise the app repository if we’re in production so we can handle
    // versioning, updates, etc. If we can’t do it for some reason, keep
    // going but add a warning to the log.
    if (process.env.PRODUCTION) {
      this.appRepository = await AppRepository.getInstance()
      globalThis.kitten.appRepository = this.appRepository

      try {
        this.automaticUpdates = AutomaticUpdates.getInstance()
        await this.automaticUpdates.start()
        globalThis.kitten.automaticUpdates = this.automaticUpdates
      } catch (error) {
        console.warn('\n⚠️ Could not set up automatic updates. kittens.small-web.org might be down or there might be an issue with the app’s source code repository.')
      }
    }

    // // TODO: Is this used anywhere? Perhaps in Domain. Find out and either remove or document.
    // const __kitten__ = globalThis.__kitten__
    // __kitten__.database = database

    const _db = globalThis.kitten._db

    const domainTokenOption = this.options.domainToken
    const smallWebHostDomainOption = this.options.smallWebHostDomain

    const smallWebHostDomainSpecified = smallWebHostDomainOption !== undefined

    // Every Small Web place has an identity (ID). This is a cryptographic
    // identity (ed25519 public key). The corresponding secret is generated
    // the first time a specific route /hello/[domainToken] is hit.
    const idExists = _db.settings.id !== undefined
    if (!idExists) {
      // If this Kitten instance has been created by a Domain host¹, it will
      // have a secret token that is used to communicate with the host and also
      // used in the path to the initial secret generation page (to circumvent a
      // possible attack where a malicious party listens for subdomain creations
      // on a Small Web domain and hits a well-known route to invalidate the secret.
      // (Since the secret generation is a one-time thing, this will be immediately
      // detectable but could constitute a denial of service attack of sorts and
      // become rather annoying for Small Web hosts otherwise.)
      //
      // If the token doesn’t exist, it means the place was created on a
      // development machine and so we will generate one ourselves so don’t
      // create different flow during development.
      const domainTokenExists = _db.settings.domainToken !== undefined
      if (!domainTokenExists) {
        if (domainTokenOption != undefined) {
          // Use domain token and small web host domain passed by the Domain host.
          _db.settings.domainToken = domainTokenOption
        } else {
          // Generate a domain token.
          _db.settings.domainToken = random32ByteTokenInHex()
        }
      }
      const smallWebHostExists = _db.settings.smallWebHostDomain !== undefined
      if (!smallWebHostExists) {
        if (smallWebHostDomainSpecified) {
          _db.settings.smallWebHostDomain = smallWebHostDomainOption
        }
      }
    }
    const domainToken = _db.settings.domainToken

    // Initialise the Kitten package object so we can provide a manual
    // Kitten update mechanism through the Settings web interface.
    globalThis.kitten.package = KittenPackage.getInstance()

    /**
      We automatically install node modules (using npm ci) for:
 
      - The project being served.
      - Any app modules within the project being served.

      In order to keep server (re)starts as quick as possible, we keep
      a hash of every package-lock.json file that we’re aware of and
      only carry out the `npm ci` call for a location when that hash changes.

      Note that if a package-lock.json file doesn’t exist for a given
      project, we use `npm install` instead.

      @param {string} projectPath
    */
    const installNodeModulesIfNecessary = (projectPath) => {
      console.debug('📦 Install node modules if necessary for', projectPath)

      const modulesPaths = [projectPath]

      // See if app modules exist. If they do, then we add
      // them to the list of projects to handle node modules installation/update for.
      const projectAppModulesPath = path.join(projectPath, 'app_modules')

      installAppModules: if (fs.existsSync(projectAppModulesPath)) {
        // One or more app modules exist, we need to run our checks
        // on all of them.
        const appModules = fs
          .readdirSync(projectAppModulesPath, {withFileTypes: true})
          .filter(directoryEntry => directoryEntry.isDirectory())

          if (appModules.length === 0) break installAppModules

          console.debug('✨ App modules exist:', appModules.map(appModule => appModule.name))
        // @ts-ignore path was added in Node v20.1.0
        modulesPaths.unshift(...(appModules.map(directoryEntry => path.join(directoryEntry.path, directoryEntry.name))))
      }

      modulesPaths.forEach(modulePath => runNpmCiOnModulePath(modulePath))
    }

    installNodeModulesIfNecessary(/** @type string */ (process.env.basePath))
    
    const appDatabase = new AppDatabase()
    try {
      await appDatabase.initialise()
    } catch (error) {
      // TODO: Should the app database not being accessible be a exit-level
      // issue or should be it recoverable (so that perhaps an app can
      // recover from a database corruption, for exampe?)
      console.error(error)
      console.error(chalk.red.bold('\n❌ Cannot continue. Exiting.'))
      process.exit(1)
    }
    
    // If there is a main.script.js in the project, import it. Since it could
    // be asynchronous, we await it.
    const mainScriptPath = path.join(/** @type string */ (process.env.basePath), 'main.script.js')
    if (fs.existsSync(mainScriptPath)) {
      let mainScript = null
      try {
        mainScript = await import(mainScriptPath)
        await mainScript.default({router: this.app})
      } catch (error) {
        throw new Error(`Cannot run main.script.js ${typeof mainScript.default !== 'function' ? '\n\nYou must export a default function from main.script.js:\n\nexport default function ({ app }) {\n  // Initialise global stuff here.\n}\n\n' : ''}${error}`)
      }
    }

    //
    // Add routes
    // 

    // Serve uploaded files when requested by id.
    this.app.get('/uploads/:id/:download?', downloadHandler())

    // Get routes from project being served.
    this.routes = await Routes.getRoutes()
    
    // Get Kitten’s own Settings application routes.
    this.kittenSettingsApplicationRoutes = await Routes.getRoutes(paths.KITTEN_WEB_APP_DIRECTORY_PATH)

    // Add routes from Kitten’s Settings app to server.
    for (const routesOfType of Object.values(this.kittenSettingsApplicationRoutes)) {
      for (const [pattern, route] of Object.entries(routesOfType)) {
        console.debug('Adding Kitten Settings App route', route.method, pattern, route.handler)
        // Save the route’s filePath on the handler itself so we
        // can do a lookup to see if authentication is required for it.
        const handler = route.handler
        handler.filePath = route.filePath
        this.app[route.method](pattern, handler)
      }
    }

    // Add routes from project being served to server.
    for (const routesOfType of Object.values(this.routes)) {
      for (const [pattern, route] of Object.entries(routesOfType)) {
        console.debug('Adding route', route.method, pattern, route.handler)
        // Save the route’s filePath on the handler itself so we
        // can do a lookup to see if authentication is required for it.
        const handler = route.handler
        handler.filePath = route.filePath
        this.app[route.method](pattern, handler)
      }
    }

    // If there are still domains left, these are aliases.
    const aliases = this.options.domains.slice(1)
    let aliasesIfAny = ''
    if (aliases.length > 0) {
      // Show a list of the aliases (just the subdomains).
      const numAliases = aliases.length
      const prettyAliases = aliases.map(alias => alias.replace(`.${this.domain}`, '')).join(', ')
      aliasesIfAny = `(and subdomain${numAliases > 1 ? 's' : ''} ${prettyAliases})`
    }

    // Get the handler from the Polka instance and create a secure site using it.
    // (Certificates are automatically managed by @small-tech/https).
    const { handler } = this.app

    this.options.minVersion = 'TLSv1.3'

    // @ts-ignore FIXME: Add type annotations to @small-tech/https to fix this issue.
    this.server = https.createServer(this.options, handler)

    // Listen for WebSocket upgrade events and handle them.
    this.wss = new WebSocketServer({ noServer: true })
    this.server.on('upgrade', this.upgradeHandler.bind(this))

    this.server.on('error', error => {
      /**
        This is a SystemError but Node.js does not expose it so we
        have to introspect the actual object to handle it specifically.
        See https://codeberg.org/aral/gists/src/branch/main/SystemError.md

        @typedef {{ code?: string }} OptionalCodeProperty
        @typedef { Error & OptionalCodeProperty } ErrorWithOptionalCodeProperty
      */
      const errorWithOptionalCodeProperty = /** @type ErrorWithOptionalCodeProperty */ (error)
      if (errorWithOptionalCodeProperty.code && errorWithOptionalCodeProperty.code === 'EADDRINUSE') {
        exitWithError(`${chalk.bold(`${this.domain}:${this.port}`)} address is already in use.`)
      }
    })

    this.server.listen(this.port, async () => {
      const actualPort = /** @type {import('node:net').AddressInfo} */(this.server?.address()).port 
      globalThis.kitten.port = actualPort
      const domainWithPort = `https://${this.domain}${actualPort === 443 ? '' : `:${actualPort}`}`

      globalThis.kitten.app.router = this.app
      globalThis.kitten.app.server = this.server

      console.info(
        '\n' +
        `🌍 ${chalk.bold('Domain ')} ${link(`${domainWithPort} ${aliasesIfAny}\n`, domainWithPort)}` +
        `📂 ${chalk.bold('Source ')} ${link(/** @type {string} */ (process.env.basePath).replace(/** @type {string} */ (process.env.HOME), '🏠'), `file://${process.env.basePath}`)}\n` +
        `💾 ${chalk.bold('Data   ')} ${link('Open app data folder', globalThis.kitten.paths.APP_DATA_DIRECTORY)}\n`
      )

      if (!idExists) {
        // An ID for the person who owns the Small Web place does not exist yet, display the link
        // for the page where it will be generated.
        console.info('╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌')
        console.info()
        console.info(`🔒 ${chalk.bold('Don’t forget to create your identity')}`)
        console.info()
        console.info(`   ${link('One-time use identity creation link', `${domainWithPort}/💕/hello/${domainToken}/`)}`)
        console.info()
        console.info('╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌\n')
      }

      if (process.stdout.isTTY) {
        // Show interactive Kitten shell instructions (REPL).
        console.info(`${chalk.blue.italic(`🐢 To launch interactive shell, press the [${chalk.bold('s')}] key.`)}\n`)
      }

      // Show server status.
      console.info(`   ${chalk.green.bold(`Server is running ${process.env.PRODUCTION ? chalk.green.bold.underline('in production ') : ''}and listening for connections…`)}\n`)

      // If app repository exists (if we’re running in production), display the app version.
      this.appRepository?.displayAppVersion()

      if (this.open) {
        // If we’ve been asked to open a browser, do so.
        open(domainWithPort)
      }
      
      // If we’re being deployed by a Small Web Host (Domain instance), notify it
      // that we have successfully launched (so it can inform the person who own the
      // new Small Web place that it is ready).
      const __db = await GlobalInternalDatabase.initialise()

      if (__db.state.isBeingDeployedByDomain) {
        console.info('🚚 Server is being deployed by Domain – informing it of successful start.')
        const url = `https://${_db.settings.smallWebHostDomain}/domain/${domainTokenOption}/status/server-ready`
        try {
          const response = await fetch(url)
        } catch (error) {
          console.error('Error while attempting to notify the Small Web Domain Host that server is ready.', error)
        }
      }
      // Ensure being deployed by domain flag is unset in global internal database.
      if (__db.state.isBeingDeployedByDomain) __db.state.isBeingDeployedByDomain = false

      // Set up the Kitten REPl.
      startKittenRepl(domainWithPort)
    })
  }

  /**
    Node http(s) upgrade event handler
    (https://nodejs.org/api/http.html#event-upgrade)

    Implementation based on express-websocket and tinyws, to keep
    some of the modern interface of tinyws (async/await syntax)
    while avoiding the client connection timeout issue in tinyws.
    (See https://codeberg.org/kitten/app/issues/113).

    @param {http.IncomingMessage} request
    @param {import('node:net').Socket} socket
    @param {Buffer} head
  */
  upgradeHandler (request, socket, head) {
    var response = new http.ServerResponse(request)
    response.assignSocket(socket)

    // Avoid hanging onto upgradeHead as this will keep the entire
    // slab buffer used by node alive.
    var copyOfHead = Buffer.alloc(head.length)
    head.copy(copyOfHead)

    response.on('finish', function () {
      if (response.socket !== null) {
        response.socket.destroy()
      }
    });

    /**
      Mix in WebSocket to request object.
    
      @typedef {{ ws: function }} WebSocketMixin
      @typedef { http.IncomingMessage & WebSocketMixin } IncomingMessageWithWebSocket
    */
    /** @type IncomingMessageWithWebSocket */ (request).ws = () =>
      new Promise((resolve) => {
        this.wss?.handleUpgrade(request, request.socket, copyOfHead, ws => {
          this.wss?.emit('connection', ws, request)
          resolve(ws)
        })
      })

    // @ts-ignore We don‘t have a next route to pass.
    return this.app.handler(request, response)
  }
}
