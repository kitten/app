/**
  Authentication middleware.

  Any route can require authentication if it contains the lock emoji in its name.

  @typedef {{ filePath: string }} FilePathMixin 
  @typedef {import('polka').Middleware<any> & FilePathMixin} HandlerWithFilePathMixin
*/

import { encodeFilePath, decodeFilePath } from '../Utils.js'
const authenticationRequiredIndicator = '🔒'

/**
  @param {import('polka').Polka} app
*/
export default function (app) {
  /** @type {import('polka').Middleware<any>} */
  return async (request, response, next) => {
    if (request.session && request.session.authenticated) {
      next()
      return
    }

    // Does this route require authentication?
    let authenticationRequired = false
    const route = app.find(request.method, request.url)

    route.handlers.forEach(handler => {
      const handlerWithFilePathMixin = /** @type HandlerWithFilePathMixin */ (handler)
      if (handlerWithFilePathMixin.filePath !== undefined) {
        if (handlerWithFilePathMixin.filePath.match(authenticationRequiredIndicator) !== null) {
          authenticationRequired = true
        }
      }
    })

    if (authenticationRequired) {
      // Check if this route has created a session.
      if (request.session === undefined) {
        // No session, return 403
        // @ts-ignore mixin
        response.forbidden()
        return
      }
      
      // Person needs to authenticate to access this route.
      request.session.redirectToAfterSignIn = decodeFilePath(request.path)
      response.writeHead(303, { Location: encodeFilePath('/💕/sign-in/') })
      response.end()
    } else {
      // Person is authenticated.
      next()
    }
  }
}
