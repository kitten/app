/**
  Add trailing slash to all URL paths that need one.
  So, /hello will become /hello/, for example. This avoids
  issues with relative URLs breaking when the path is missing a trailing
  slash (e.g., <script src='./index.js'></script>).

  See: https://stackoverflow.com/questions/31111257/relative-links-on-pages-without-trailing-slash
*/

const pathNeedsTrailingSlashRegExp = /\/(?:.*\/)?[^\/.]+$/

export default function () {
  /** @type {import('polka').Middleware<any>} */
  return (request, response, next) => {
      if (pathNeedsTrailingSlashRegExp.exec(request.path) !== null) {
        // A 308 is a permanent redirect that also tells the browser to not
        // change the method of the request. So if it’s a POST to /hello, it’ll 
        // be a POST to /hello/ as well.
        response.statusCode = 308
        response.setHeader('Location', request.path + '/' + request.search)
        response.end()
      } else {
        next()
      }
    }
}
