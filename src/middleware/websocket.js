/**
  WebSocket middleware.

  Handles WebSocket connections using the Kitten `ws` mixin.

  @returns {import('polka').Middleware<any>}
*/

export default function () {
  return async (request, _response, next) => {
    if (request.ws) {
      await request.ws()
    } else {
      next()
    }
  }
}
