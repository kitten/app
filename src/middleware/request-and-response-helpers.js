import typeIs from 'type-is'

/**
  Adds helpers to request and response objects.
*/
export default function () {
  /** @type {import('polka').Middleware<any>} */
  return (request, response, next) => {
    /**
      Add request.is for Express Busboy compatibility.

      From https://github.com/expressjs/express/blob/master/lib/request.js#LL252C1-L291C1
      See https://github.com/lukeed/polka/issues/193

      @param {Array | string} types
      @returns {string | false | null}
    */
    request.is = (function is (types) {
      var arr = types

      // Support flattened arguments.
      if (!Array.isArray(types)) {
        arr = new Array(arguments.length)
        for (var i = 0; i < arr.length; i++) {
          arr[i] = arguments[i]
        }
      }

      return typeIs(this, /** @type string[] */ (arr))
    }).bind(request)

    /**
      A response closure that returns an inline
      JSON response with the proper headers.

      @param {Object} data
    */
    response['json'] = data => {
      let json
      try {
        json = JSON.stringify(data)
      } catch (error) {
        response.statusCode = 500
        response.end(`Error: cannot stringify JSON object.`)
        return
      }
      response.statusCode = 200
      response.setHeader('Content-Type', 'application/json')
      response.end(json)
    }

    /**
      Response closure that returns an attachment
      JSON response with the proper headers and the
      requested file name (or data.json as a fallback).

      @param {Object} data
      @param {string} fileName?
    */
    response['jsonFile'] = (data, fileName = 'data.json') => {
      let json
      try {
        json = JSON.stringify(data)
      } catch (error) {
        response.statusCode = 500
        response.end(`Error: cannot stringify JSON object.`)
        return
      }
      response.statusCode = 200
      response.setHeader('Content-Type', 'application/json')
      response.setHeader('Content-Disposition', `attachment; filename="${fileName}"`)
      response.end(json)
    }

    /**
      Response closure that returns a general file.

      @typedef {Int8Array|Uint8Array|Uint8ClampedArray|Int16Array|Uint16Array|Int32Array|Uint32Array|Float32Array|Float64Array|BigInt64Array|BigUint64Array} TypedArray
  
      @param {string|Buffer|TypedArray|DataView} data
      @param {string} fileName?
      @param {string} mimeType?
    */
    response['file'] = (data, fileName = 'download', mimeType='application/octet-stream') => {
      response.statusCode = 200
      response.setHeader('Content-Type', mimeType)
      response.setHeader('Content-Disposition', `attachment; filename="${fileName}"`)
      response.end(data)
    }

    /**
      Creates and returns a redirection closure that redirects requests
      with the given status code.

      @param {number} statusCode
      @returns {(location:string) => void} redirectionClosure
    */
    function redirectionClosureWithStatusCode (statusCode) {
      /** @param {string} location */
      return location => {
        response.statusCode = statusCode
        response.setHeader('Location', encodeURI(location))
        response.end()
      }
    }

    /**
      Creates and returns a response closure with the given status code
      that accepts an optional body.

      @param {number} statusCode
      @returns {(body:string) => void} responseClosure
    */
    function responseClosureWithStatusCode (statusCode) {
      return (body = '') => {
        const stats = globalThis.kitten._db.stats
        if (statusCode === 404) {
          if (stats.missing[request.url] === undefined) {
            stats.missing[request.url] = 1
          } else {
            stats.missing[request.url]++
          }
        }
        if (statusCode === 500) {
          if (stats.serverErrors[request.url] === undefined) {
            stats.serverErrors[request.url] = 1
          } else {
            stats.serverErrors[request.url]++
          }
        }
        response.statusCode = statusCode
        response.end(body)
      }
    }

    /**
      Successful responses (200 OK, 201 Created).
      https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/200
      https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/201
    */
    response['ok'] = responseClosureWithStatusCode(200)
    response['created'] = responseClosureWithStatusCode(201)

    /**
      Redirect response via a GET request (303 See Other) to the given location.
      Alias: seeOther.
      @param {string} location
    */
    response['get']
     = response['seeOther']
     = redirectionClosureWithStatusCode(303)

    /**
      Redirect (temporary) to a new location without changing the request method.
    */
    response['redirect']
      = response['temporaryRedirect']
      = redirectionClosureWithStatusCode(307)

    /**
      Redirect (permanent) to a new location without changing the request method.
    */
    response['permanentRedirect'] = redirectionClosureWithStatusCode(308)

    /**
      400 Bad Request

      [I]ndicates that the server cannot or will not process the request due to something
      that is perceived to be a client error (e.g., malformed request syntax, invalid request
      message framing, or deceptive request routing).
  
      https://httpwg.org/specs/rfc9110.html#rfc.section.15.5.1
    */
    response['badRequest'] = responseClosureWithStatusCode(400)

    /**
      “401 Unauthorized” (actually, unauthenticated) response.
      Aliases: unauthorised, unauthorized.
    */
    response['unauthenticated']
      = response['unauthorised']
      = response['unauthorized']
      = responseClosureWithStatusCode(401)

    /**
      403 Forbidden response. This should be returned
      if the request is authenticated but lacks the authorisation
      (i.e., sufficient rights) to access the resource.

      If the request requires authentication but has not been
      authenticated, you should return a “401 Unauthorized”
      (actually: unauthenticated) response.
    */
    response['forbidden'] = responseClosureWithStatusCode(403)

    /**
      404 Not Found response.
    */
    response['notFound']
      = responseClosureWithStatusCode(404)

    /**
      General shorthand helper for setting the status code and
      ending the response with an optional body

      @param {number} statusCode
      @param {string} body
    */
    response['withCode'] = (statusCode, body = '') => {
      response.statusCode = statusCode
      response.end(body)
    }

    /**
      500 Internal Server Error response.
      Alias: internalServerError.
      @param {string | null} body
    */
    response['error']
      = response['internalServerError']
      = responseClosureWithStatusCode(500)

    next()
  }
}
