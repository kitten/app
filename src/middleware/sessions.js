/**
  Mixes in session property to requests.
*/
import Sessions from '../Sessions.js'

export default function () {
  /** @type {import('polka').Middleware<any>} */
  return async (request, response, next) => {
    // Internal web app URL to exclude from session creation.
    // /💕/id/ can be called often (although implementations would ideally cache it)
    // during end-to-end encrypted communication.
    // 
    // See: https://codeberg.org/kitten/app/issues/223
    // 
    // TODO: Should this be generalised into a per-route setting of some sort?
    if (request.url === '/%F0%9F%92%95/id/') {
      next()
      return
    }
    request.session = await Sessions.getInstance().session(request, response)
    next()
  }
}
