/**
  Stats middleware.

  Logs referrers and hits.

  
*/

export default function (ownDomainWithHttps) {
  /** @type {import('polka').Middleware<any>} */
  return (request, response, next) => {
    const stats = globalThis.kitten._db.stats

    // Log referrers
    // (It’s good to know where people are linking to your site from.)
    if (request.headers && request.headers.referer != undefined && !request.headers.referer.includes(ownDomainWithHttps)) {
      if (stats.referrers[request.headers.referer] === undefined) {
        stats.referrers[request.headers.referer] = 1
      } else {
        stats.referrers[request.headers.referer]++
      }
    }

    // Log hits
    // (This might be overkill. Will remove if so.)
    if (request.url.includes('/default.socket?id=')) {
      // Don’t log default socket connections as they contain
      // unique IDs so they’ll appear as different URLs. Also,
      // hits on default sockets are not really important. There will
      // be an equal number to the hits on the pages that they’re linked to.
      next()
      return
    }
    if (stats.hits[request.url] === undefined) {
      stats.hits[request.url] = 1
    } else {
      stats.hits[request.url]++
    }
    next()
  }
}
