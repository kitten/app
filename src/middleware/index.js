import process from 'node:process'

import { encodeFilePath } from '../Utils.js'

// Third-party middleware.
import cors from 'cors'
import hsts from 'hsts'

// Custom Kitten middleware.
import requestLog from './request-log.js'
import requestAndResponseHelpersMiddleware from './request-and-response-helpers.js'
import statsMiddleware from './stats.js'
import trailingSlashesMiddleware from './trailing-slashes.js'
import sessionsMiddleware from './sessions.js'
import authenticationMiddleware from './authentication.js'
import websocketMiddleware from './websocket.js'

/**
  Adds all Kitten middleware to the Kitten Server’s Polka app.

  @param {import('polka').Polka} app
*/
export default function addAllMiddlewareToApp(app) {
  // Log incoming requests (useful for debugging).
  app.use(requestLog())

  // Allow all CORS requests.
  // TODO: Once peer-to-peer endpoints like /inbox
  // and /follow are implemented, only allow
  // CORS requests for those endpoints.
  // (See https://www.npmjs.com/package/cors#enable-cors-for-a-single-route)
  app.use(cors())

  // In production, specify HTTP Strict Transport Security (HSTS) header.
  if (process.env.PRODUCTION) {
    app.use(hsts())
  }

  // Referrer and hit counter.
  const ownDomainWithHttps = `https://${globalThis.kitten.domain}`
  app.use(statsMiddleware(ownDomainWithHttps))

  // Request and response helpers.
  app.use(requestAndResponseHelpersMiddleware())

  // Add trailing salshes to URLs.
  app.use(trailingSlashesMiddleware())

  // Add session mixin to requests.
  app.use(sessionsMiddleware())

  // Authentication middleware.
  app.use(authenticationMiddleware(app))

  // If we’re in development mode, create the development WebSocket route that’s
  // used to detect server restarts and refresh pages.
  if (!process.env.PRODUCTION) {
    const socketPath = encodeFilePath('/🐱/development-socket/')
    app.use(socketPath, websocketMiddleware())
  }
}
