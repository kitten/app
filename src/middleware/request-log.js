/**
  Keeps a truncated log of the last 25 request URLs for debugging purposes.

  This could be merged wih the stats plugin or expanded to log more than just URLs
  or even removed altogether in the future. Just keeping it in here for the moment
  to see if it’s useful for debugging certain types of issues.
*/

export default function () {
  const MAX_REQUESTS_LOG_SIZE = 25
  globalThis.kitten.requests = []

  /** @type {import('polka').Middleware<any>} */
  return (request, response, next) => {
    globalThis.kitten.requests.unshift(request.url)
    if (globalThis.kitten.requests.length > MAX_REQUESTS_LOG_SIZE) {
      globalThis.kitten.requests.length = MAX_REQUESTS_LOG_SIZE
    }
    next()
  }
}
