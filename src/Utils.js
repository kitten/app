/**
  General utility functions.
*/
import fs from 'fs'
import path from 'path'
import process from 'process'
import childProcess from 'node:child_process'

import chalk from 'chalk'
import pathCompleteExtname from 'path-complete-extname'
import paths from './lib/paths.js'
import { sha256 } from '@noble/hashes/sha256'
import { bytesToHex as toHex } from '@noble/hashes/utils'

const kittenNpm = path.join(paths.KITTEN_RUNTIME_DIRECTORY, 'bin', 'npm')

/**
  Display error in console and exit process.

  @param {string} message
*/
export function exitWithError (message) {
  console.error(`\n❌ ${chalk.redBright('Error:')} ${message}\n`)
  process.exit(1)
}

/**
  Determine which places to listen at.

  @param {import('./Server.js').ServerOptions} options

  @returns {{
    domains: string[],
    port: number
  }}
*/
export function getDomainsAndPort (options) {
    // Determine which places to listen at.
    let domains = /** @type string[] */ ([])
    
    // If a set of domains to listen on is provided use that (this means
    // we’re serving in production). Otherwise, default to listening on
    // local interfaces.
    const domain = options.domain || 'localhost'
    domains.push(domain)

    const localhostAliases = ['place1.localhost', 'place2.localhost', 'place3.localhost', 'place4.localhost']
    const isLocal = domain === 'localhost' || localhostAliases.includes(domain)
    if (!isLocal && options.aliases) {
      // We’re running in production and a set of aliases has been passed,
      // also include those in the set of domains to listen for.
      domains = domains.concat(options.aliases.split(','))
    }

    // You can pass a shorthand alias, www, to stand for www.<domain>.
    // Rewrite that as the full domain URL if we encounter it.
    domains = domains.map(value => value === 'www' ? `www.${domain}` : value)

    // Set the port or use the default SSL port.
    const port = options.port === undefined ? 443 : options.port

    return { domains, port }
}

/**
  Calculate the base path used by the Kitten server to find files in the served app.

  @param {string} workingDirectory
  @param {string} pathToServe
*/
export function setBasePath (workingDirectory, pathToServe) {
  // Resolve the path to serve so that it works both when run as
  // kitten <path to serve> from anywhere and, from the source folder, as
  // bin/kitten <path to serve>.
  let basePath = path.resolve(workingDirectory, pathToServe)

  if (!fs.existsSync(basePath)) {
    throw new Error(`${basePath} – path does not exist.`)
  }

  // You can place your source files either in the project
  // folder directly or in a subfolder called src. Use the 
  // latter if it exists.
  const srcFolder = path.join(basePath, 'src')
  basePath = fs.existsSync(srcFolder) ? srcFolder : basePath

  // Set the basePath as an environment variable so the ESM Module Loader
  // can access it also.
  process.env.basePath = basePath

  // Change the current working directory to that basepath.
  process.chdir(basePath)

  return basePath
}

/**
  Returns unique project identifier based on the path of the project
  that can be used as the name of a directory.

  @param {string} domain
  @param {number} port

  @returns {string} 
*/
export function getProjectIdentifierForDomain(domain, port) {
  const projectIdentifier = /** @type string */ (process.env.basePath).replace(/\//g, '.').slice(1) + `.${domain}.${port}`

  return projectIdentifier
}

export const HTTP_METHODS = ['get.js', 'head.js', 'patch.js', 'options.js', 'connect.js', 'delete.js', 'trace.js', 'post.js', 'put.js']
export const BACKEND_EXTENSIONS = HTTP_METHODS.concat(['socket.js'])
export const FRONTEND_EXTENSIONS = ['page.js', 'page.md']
export const DEPENDENCY_EXTENSIONS = ['component.js', 'fragment.js', 'fragment.html', 'fragment.css', 'fragment.md', 'layout.js', 'script.js', 'styles.js']
export const DYNAMIC_ROUTE_EXTENSIONS = BACKEND_EXTENSIONS.concat(FRONTEND_EXTENSIONS)
export const STATIC_ROUTE_EXTENSIONS = ['html', 'htm']

// TODO: This is getting messy. The whole route categorisation system needs
// a rethink following the change to implement seamless handling of static routes.
export const ALL_ROUTE_EXTENSIONS = DYNAMIC_ROUTE_EXTENSIONS

export const extensionCategories = {
  backendRoutes: BACKEND_EXTENSIONS,
  frontendRoutes: FRONTEND_EXTENSIONS,
  dependencies: DEPENDENCY_EXTENSIONS,
  dynamicRoutes: DYNAMIC_ROUTE_EXTENSIONS,
  staticRoutes: STATIC_ROUTE_EXTENSIONS,
  allRoutes: ALL_ROUTE_EXTENSIONS
}

export const supportedExtensionsRegExp = `\.(${DYNAMIC_ROUTE_EXTENSIONS.concat(STATIC_ROUTE_EXTENSIONS).join('|')})$`

const indexWithExtensionRegExp = new RegExp(`index${supportedExtensionsRegExp}`)
const indexWithPropertiesRegExp = /\/index_(?!\/)/
const extensionRegExp = new RegExp(supportedExtensionsRegExp)

/**
  Since Polka does not handle unicode in paths correctly
  (see https://github.com/lukeed/polka/issues/187), we have to
  split the file path and URI encode each component ourselves.

  @param {string} filePath
  @returns {string}
*/
export const encodeFilePath = filePath => filePath
  .split(path.sep)
  .map(value => encodeURIComponent(value))
  .join(path.sep)

/**
  Inverse of encodeFilePath. Used when we want to avoid
  double escaping of file paths.

  @param {string} filePath
  @returns {string}
*/
export const decodeFilePath = filePath => filePath
  .split(path.sep)
  .map(value => decodeURIComponent(value))
  .join(path.sep)

const firstDot = /^\./

/**
  Returns the complete extension (e.g., page.js not just .js)
  of the passed file path. Works with any number of extensions,
  returning the last two without a dot at the start.

  @param {string} filePath
*/
export function extensionOfFilePath (filePath) {
  return pathCompleteExtname(filePath).split('.').splice(-2).join('.').replace(firstDot, '')
}

/**
  Given a file path, derives the unique class name for its route.

  @param {string} filePath
  @param {string} basePath

  @returns {string}
*/
export function classNameFromFilePath (filePath, basePath) {
  return classNameFromRoutePattern(routePatternFromFilePath(filePath, basePath))
}

/**
  Derives the route pattern for the passed file path based on the base path.

  @param {string} filePath
  @param {string} basePath

  @returns {string}
*/
export function routePatternFromFilePath (filePath, basePath) {
  let routePattern = encodeFilePath(filePath)
    .replace(encodeFilePath(basePath), '')    // Remove base path.
    .replace('%F0%9F%94%92', '')              // Remove authentication required indicator suffix (🔒).
    .replace(indexWithPropertiesRegExp, '/')  // Handle index files that contain properties.
    .replace(/_%5B/g, '/%5B')                 // Replace underscores that start parameter names with slashes.
    .replace(/%5D_/g, '/%5D')                 // Replace underscores that end parameter names with slashes.
    .replace(/\(/g, '%28')                    // Encode parentheses (encodeUriComponent doesn’t).
    .replace(/\)/g, '%29')
    .replace(/%5Bany%5D/g, '*')               // Replace wildcard properties. i.e., [any] becomes *.
    .replace(/%5Boptional-(.*?)%5D/g, ':$1?') // Replace optional properties. e.g., [optional-prop] becomes :prop?.
    .replace(/%5B(.*?)%5D/g, ':$1')           // Replace properties. e.g., [prop] becomes :prop.
    .replace(indexWithExtensionRegExp, '')    // Remove index path fragments (and their extensions).
    .replace(extensionRegExp, '')             // Remove extension.

  // Exception: WebSocket routes end with .socket.
  // Reason: We 308 forward URIs without a file extension and no trailing slash to the version
  // of the URI with a trailing slash to have canonical URIs. However, this does not work
  // with WebSocket connections as clients are not required to follow redirects during the
  // handshake/protocol update (because goodness knows why). So by creating the convention that
  // WebSocket routes end in .socket we avoid this situation and the possible confusion arising
  // from it. It’s also has greater semantics.
  if (filePath.endsWith('.socket.js')) {
    routePattern += '.socket'
  } else {
    // Make sure route pattern ends in a forward slash.
    if (routePattern[routePattern.length - 1] !== '/') {
      routePattern += '/'
    }
  }

  return routePattern
}

/**
  Converts a route in the form of, e.g.,
  '/some_thing/with/underscores-and-hyphens' to
  SomeThingWithUnderscoresAndHyphensPage.

  @param {string} pattern
  @returns {string}
*/
export function classNameFromRoutePattern (pattern) {
  const className = pattern
    .split('/').join('*')
    .split('-').join('*')
    .split('_').join('*')
    .split(':').join('*')
    .split('*')
    .map(fragment => fragment.charAt(0).toUpperCase() + fragment.slice(1))
    .join('')
    + 'Page'

  return className === 'Page' ? 'IndexPage' : className
}

// Format the Kitten app path in a way that works regardless of 
// whether Kitten was invoked using the global command
// from the bundled distribution or via bin/kitten from source.
export const kittenAppPath = process.argv[1]
  .replace('kitten-bundle.js', '')          // When running in production.
  .replace('kitten-process-manager.js', '') // When running in development.
  .replace('src/processes/main.js', '')     // When running from source.
  .replace(/tests\/.*?$/, '')               // Ditto (when running from tests).

/**
  Run passed command (either 'ci' or 'install') on the given module path.

  @param {'ci'|'install'} command
  @param {string} modulePath
*/
export function npm (command, modulePath) {
  const env = Object.assign(process.env, {NODE_OPTIONS: ''})
  env.PATH = `${paths.KITTEN_RUNTIME_BIN_DIRECTORY}:${env.PATH}`
  try {
    childProcess.execSync(`${kittenNpm} ${command}`, { 
      encoding: 'utf-8',
      env,
      cwd: modulePath
    })
    return true
  } catch (error) {
    console.error(`npm ${command} failed on path ${modulePath}`, error)
    return false
  }
}

/**
  Runs npm ci on the passed module path.

  @param {string} modulePath
*/
export function runNpmCiOnModulePath (modulePath) {
  const modulePackageFilePath = path.join(modulePath, 'package.json')
  let prettyModulePath = modulePath.replace(/** @type {string} */ (process.env.basePath), '')
  if (prettyModulePath.includes('small-tech.org/kitten/app/web')) prettyModulePath = 'Kitten’s internal web app'
  if (prettyModulePath === '') prettyModulePath = 'root module (place being served)'

  // First, let’s check if a package.json file even exists for the
  // location (as we don’t have to go further if it doesn’t).
  if (fs.existsSync(modulePackageFilePath)) {
    //
    // package.json file exists.
    //
    console.debug('Project package file path exists', modulePackageFilePath)

    const modulePackageLockFilePath = path.join(modulePath, 'package-lock.json')

    const nodeModulesFolderPath = path.join(modulePath, 'node_modules')
    const nodeModulesFolderExists = fs.existsSync(nodeModulesFolderPath)
    
    // The npm ci command can only be used when a package-lock.json file
    // (or npm-shrinkwrap.json) file exists with lockfileVersion >= 1 or else
    // we’ll get an EUSAGE error from npm. So use install if the package-lock.json
    // file hasn’t been generated yet. 
    const packageLockFileExists = fs.existsSync(modulePackageLockFilePath)
    const command = packageLockFileExists ? 'ci' : 'install'

    let modulePackage = null

    const npmCiOrInstallIfNecessary = () => {
      if (modulePackage === null) {
        modulePackage = JSON.parse(fs.readFileSync(modulePackageFilePath, 'utf-8'))
      }
      if (modulePackage.dependencies != undefined) {
        //
        // Module has dependencies; ci or install them based on whether a package-lock.json file exists or not.
        //
        console.info(`✨ Installing module dependencies (npm ${command}) for ${prettyModulePath}\n`)
        npm(command, modulePath)
      } else {
        //
        // Module does NOT have dependencies. We’re done here.
        //
        console.debug(`✨ Module ${prettyModulePath} has no dependencies. Nothing to install.\n`)
      }
    }
    
    if (!nodeModulesFolderExists) {
      console.debug('Module node modules folder path does not exist', nodeModulesFolderPath)
      npmCiOrInstallIfNecessary()
      return
    }

    //
    // node_modules folder exists. Check if dependencies need updating.
    //

    // The slice is there to remove the leading underscore as these
    // are absolute paths and JSDB, as of version 5.1.3, does not
    // persist keys that begin with an underscore.
    // (See https://codeberg.org/kitten/app/issues/200)
    // @ts-ignore Valid for our runtime.
    const pathId = modulePath.replaceAll(path.sep, '_').slice(1)

    if (fs.existsSync(modulePackageLockFilePath)) {
      //
      // package-lock.json file exists in the directory.
      //
      // Read it so we can compare its hash with the one we have already
      // (if we have one) and decide whether to npm ci or not based on that.
      //
      console.debug('Module package lock file path exists.')
      const kitten = globalThis.kitten
      const existingPackageLockFileHash = kitten._db.packageLockFileHashes[pathId]

      const packageLockFile = fs.readFileSync(modulePackageLockFilePath, 'utf-8')
      const currentPackageLockFileHash = toHex(sha256(packageLockFile))

      console.debug('existing = current hash?', existingPackageLockFileHash, currentPackageLockFileHash)

      if (existingPackageLockFileHash !== currentPackageLockFileHash) {
        //
        // Packages have changed. Run npm ci.
        //
        console.info(`✨ Installing module dependencies (npm ci) for ${prettyModulePath}\n`)
        npm('ci', modulePath)
        console.debug('Ran npm ci; saving package-lock file hash.')
        kitten._db.packageLockFileHashes[pathId] = currentPackageLockFileHash
        console.debug('Done.')
      }
      else {
        //
        // Packages have not changed. We’re done here.
        //
        console.info(`✨ Module dependencies have not changed for ${prettyModulePath}, not updating.`)
      }
    } else {
      //
      // package-lock.json does NOT exist. (At this point, this can only happen
      // if the package-lock.json file was manually removed (since the node_modules
      // folder exists so npm install must have been run on the directory). While
      // this is an edge case, we still attempt to handle it gracefully.
      //
      console.debug('Module package lock file does not exist (even though node_modules folder does) – edge case; attempting to handle gracefully.', modulePackageFilePath)
      npmCiOrInstallIfNecessary()
    }
  } else {
    //
    // package.json file does NOT exist.
    //
    console.debug(`✨ Module ${prettyModulePath} does not have a package.json file. Nothing to install.\n`)
  }
}
