// Handles Polka server errors.
// TODO: Eventually make this part of the Kitten’s
// own Kitten app (how meta), served alongside the
// person’s own project (and including things like
// Kitten’s administration web interface).

import fsAsync from 'node:fs/promises'

import Prism from 'prismjs'
import chalk from 'chalk'

import jsBeautify from 'js-beautify'

import { developmentTimeHotReloadScript } from './lib/fragments.js'

const errorStyles = `
  <style>
    :root {
      --light-violet: rgb(253, 240, 253);
    }
    body {
      display: grid;
      grid-template-areas:
        "kitten            errorMessage"
        "relevantCodeTitle relevantCodeTitle"
        "relevantCode      relevantCode"
        "stackTraceTitle   stackTraceTitle"
        "stackTrace        stackTrace";
      grid-template-rows: auto auto auto auto 1fr;
      /*
        The minmax() is there to stop the pre blocks from blowing
        out of the grid in Firefox (not an issue without on WebKit/Chromium).
      */
      grid-template-columns: minmax(0, auto) minmax(0, 1fr);
      max-width: 920px;
      margin-left: auto;
      margin-right: auto;
      padding: 2em;
      font-family: sans-serif;
      font-size: 1.5em;
      padding: 1em;
    }
    h1 {
      color: deeppink;
      font-size: 1.5em;
      word-break:break-all;
    }
    h2 {
      color: darkviolet;
      margin-top: 0;
      font-size: 1em;
      word-break:break-all;
    }
    h2 small {
      font-size: clamp(0.75rem, 0.12rem + 2.4vw, 1.5rem);
    }
    h2 small a {
      color: deeppink;
    }
    #stack-trace-title {
      grid-area: stackTraceTitle;
    }
    #relevant-code-title {
      grid-area: relevantCodeTitle;
    }
    h3 {
      background-color: darkviolet;
      color: white;
      margin: 0;
      border-radius: 0.66em 0.66em 0 0;
      padding: 0.75em 0.5em 0.5em 0.5em;
    }
    h4 {
      font-size: clamp(0.75rem, 0.12rem + 2.4vw, 1.5rem);
      background-color: deeppink;
      color: white;
      margin: -1.5rem -1.5rem 1.5rem -1.5rem;
      padding: 0.25em 0.66em;
    }
    .solitary-stack-frame-separator h4 {
      margin-top: -1em !important;
    }
    #kitten-stack-frames h4 {
      margin-top: 1.5em;
    }
    a {
      text-decoration: none;
      color: black;
      border-bottom: 2px solid deepskyblue;
    }
    img {
      grid-area: kitten;
      height: clamp(7.5rem, -0.9rem + 32vw, 17.5rem);
    }
    ul {
      list-style-type: none;
      padding-left: 1em;
    }
    li {
      font-size: clamp(1rem, 0.895rem + 0.4vw, 1.125rem);
      margin-bottom: 1em;
    }
    li:not(:last-of-type) {
      padding-bottom: 1em;
    }
    pre[class*="language-"] {
      margin: 2em 0;
    }
    pre[class*="language-"].line-numbers > code {
      overflow-x: scroll;
    }
    #relevant-code {
      grid-area: relevantCode;
      word-break: break-word;
      margin-bottom: 1em;
    }
    #relevant-code pre {
      margin-top: 0;
      margin-bottom: 0;
    }
    #relevant-code code {
      font-size: 0.75em;
      padding-left: inherit;
    }
    code {
      font-family: monospace;
      font-size: 1em;
      font-weight: 600;
    }
    #error-message {
      grid-area: errorMessage;
      margin-left: 1em;
    }
    #stack-trace {
      grid-area: stackTrace;
    }
    .mainSection {
      border: 2px solid darkviolet;
      border-radius: 0 0 1em 1em;
      margin: 0;
      padding: 1em;
    }
    .stack-location {
      display: block;
      font-size: 0.9em;
    }
    .error-line {
      font-weight: 900;
    }
    span.error-column::before {
      color: deeppink;
      content: '^';
      position: relative;
      margin-right: -1ch;
      top: 1rlh;
    }
    code[class*="language-"] {
      overflow: visible;
    }

    @media screen and (max-width: 500px) {
      body {
        /* Use single-column layout on narrower viewports. */
        grid-template-areas:
          "kitten"
          "errorMessage"
          "relevantCodeTitle"
          "relevantCode"
          "stackTraceTitle"
          "stackTrace";
        grid-template-rows: auto auto auto auto auto 1fr;
        padding-top: 0;
      }
      h1 {
        margin-top: 0;
      }
      #error-message {
        margin-left: 0;
      }
      #kitten-with-yarn {
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 0.5em;
      }
    }
  </style>
`

const generalErrorTemplate = `
  <html lang="en">
  <head>
    <meta charset='utf-8' />
    <meta name='viewport' content='width=device-width' />
    <title>Error {CODE}</title>
    <link rel='stylesheet' type='text/css' href='/🐱/library/water-2.css' />
    <link rel='stylesheet' type='text/css' href='/🐱/library/prism-1.css' />
    <link rel='stylesheet' href='/🐱/library/highlight.js/styles/measured-light.css'>
    <link
      rel='stylesheet'
      href='/🐱/library/highlight.js/styles/measured-dark.css'
      media='(prefers-color-scheme: dark)'
    >
    ${errorStyles}
  </head>
  <body>
    <img
      id='kitten-with-yarn'
      alt='Cute illustration of a grey kitten playing with a purple ball of yarn.'
      src='/🐱/images/kitten-playing.svg'
    />
    <div id='error-message'>
      <h1>{CODE}</h1>
      <h2>{ERROR}</h2>
    </div>
    <!-- Development only from here -->
    {RELEVANT_CODE}
    <h3 id='stack-trace-title'>Stack trace</h3>
    <ul id='stack-trace' class='mainSection'>
      {STACK}
    </ul>
    ${developmentTimeHotReloadScript}
    <!-- Development only to here -->
    </body>
  </html>
`

const pageNotFoundErrorPageStyles = `
  <style>
    body {
      font-family: sans-serif;
      font-size: 1.5em;
      padding: 1em;
      text-align: center;
      height: fit-content;
    }
    h1 {
      color: deeppink;
      font-size: clamp(3rem, 0.48rem + 8vw, 10rem);
      margin: 0;
      margin-top: 1rem;
    }
    h2 {
      color: darkviolet;
      margin-top: -0.5rem;
      font-size: clamp(1rem, 0.16rem + 3.2vw, 2rem);
    }
    a {
      text-decoration: none;
      color: black;
      border-bottom: 2px solid deepskyblue;
    }
    img {
      max-height: 60%;
      max-width: 100%;
    }
  </style>
`

const pageNotFoundErrorPage = `
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width" />
    <title>Error 404</title>
    ${pageNotFoundErrorPageStyles}
  </head>
  <body>
    <h1>404</h1>
    <h2>Page not found.</h2>
    <img alt='Cute little kitten looking at you innocently.' src='/🐱/images/kitten-sitting.svg' />
    ${developmentTimeHotReloadScript}
    </body>
  </html>
`

/**
  Formats and displays server errors.

  @param {any} error
  @param {any} request
  @param {any} response
  @param {any} _next
*/
export default async function (error, request, response, _next) {
  const stats = globalThis.kitten._db.stats
  if (error.status === 404) {
    // Handle 404 → 307 (Evergreen Web) if configured.
    const previousVersionOfSite = globalThis.kitten._db.settings.evergreenWebUrl
    if (previousVersionOfSite !== undefined && previousVersionOfSite !== '') {
      // We only support https urls.
      const httpsUrl = `https://${previousVersionOfSite.replace('http://', '').replace('https://', '')}`
      let newUrl
      try {
        newUrl = new URL(request.url, httpsUrl)
        console.info(`🌲 Evergreen Web (404 → 307) redirect to ${newUrl.href}`)
        return response.redirect(newUrl.href)
      } catch (error) {
        console.error('Evergreen Web URL is invalid, not redirecting.', error)
      }
    }
    response.statusCode = 404
    response.end(pageNotFoundErrorPage)
    // Log the missing page.
    if (stats.missing[request.url] === undefined) {
      stats.missing[request.url] = 1
    } else {
      stats.missing[request.url]++
    }
    return
  }
  // TODO: Only show detailed error information (stack trace, etc.)
  // in development mode.

  // If we’re here, the error occured while page was initially loading
  // so display the basic error template.
  const errorCode = error.code || error.status || 500
  const errorNumericCode = parseInt(errorCode)
  response.statusCode = Number.isNaN(errorNumericCode) ? 500 : errorNumericCode

  // Add the error (with URL) to the stats.
  if (response.statusCode === 500) {
    if (stats.serverErrors[request.url] === undefined) {
      stats.serverErrors[request.url] = 1
    } else {
      stats.serverErrors[request.url]++
    }
  }

  // Let’s make the stack trace nicer.
  let stackFramesSeparated = false
  let lastIndexOfAppStackFrames = 0

  // Used to detect kitten stack frames in the input.
  const kittenStackFrameRegExp1 = /.*small-tech\.org\/kitten\/app/

  // Used to replace kitten stack frames in the output.
  const kittenStackFrameRegExp2 = /\(.*small-tech\.org\/kitten\/app/

  const nodeModuleRegExp = /\/node_modules\/(.*?)\//

  /**
    Removes HTML from error message for use before displaying in a console.

    @param {string} message
  */
  const removeHtml = message => message
    .replace(/<.*?>|  +?/g, '')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
    .replace(/&apos;/g, "'")
    .replace(/&quot/g, '"')

  if (error.code >= 500) {
    // Also log server errors to the console to aid in debugging.
    console.error(chalk.bold.red(`🞫 Error ${errorCode}: ${removeHtml(error.message)}\n\n`), error)
  }

  // Only add stack trace if we’re not running in production.
  let errorPage = generalErrorTemplate

  if (process.env.PRODUCTION) {
    // Don’t show stack trace or source code in production.
    errorPage = errorPage.replace(/<!-- Development only from here -->.+?<!-- Development only to here -->/s, '')
  } else {
    // Replace the error message in the stack trace with an easy to look up
    // private-use Unicode character (token) to make it easier for our
    // algorithm later on.
    const errorMessage = '\ue042'

    // Handle error where HTML might have been included directly (as opposed)
    // to wrapped within a kitten.html`` tagged template (can happen
    // by mistake when copy/pasting).
    if (error.message === `Unexpected token '<'`) {
      error.message = `Unexpected token '<' <p style='font-size: 1.25rem; font-weight: normal; word-break: normal;'>(Did you put raw HTML inside a function by mistake instead of wrapping it in a <code>kitten.html\`\`</code> tagged template?)</p>`
    }
    
    const errorStack = error.stack.replace(error.message, '\ue042')

    const stackLines = errorStack.split("\n")

    // At the top of the stack trace there _might_ be the actual
    // line of code that caused the error and a line with a caret
    // symbol pointing to it. This will then be followed by the error message.
    //
    // Note: the “might” part might be a bug in EcmaScript Module Loaders
    // in Node.js. TODO: Reproduce with a minimal example and file a bug.
    let stackHeaderIndex = 0
    while (!stackLines[stackHeaderIndex].includes(errorMessage)) {
      stackHeaderIndex++
    }

    const stackHeaderLines = stackLines.splice(0, stackHeaderIndex+1)

    for (let lineIndex = 0; lineIndex < stackLines.length; lineIndex++) {
      const stackLine = stackLines[lineIndex]
      const isKittenStackFrame = stackLine.match(kittenStackFrameRegExp1) !== null
      const isNodeModule = stackLine.match(nodeModuleRegExp) !== null

      // Display source for Kitten stack frames that aren’t related
      // to Node modules (we simply link those to npm, to help in case
      // the person wants to quickly find more information about them).
      let snippetOfConcern = null
      let snippetStartLine = 0
      let snippetEndLine = 0
      if (!isKittenStackFrame && !isNodeModule) {
        const sourceDetails = stackLine.match(/file:\/\/(.*?):(\d*?):(\d*?)\)/)
        if (sourceDetails !== null) {
          // We run decodeURI() as the source file path might contain
          // URI-encoded emoji (e.g., 🔒 for private routes).
          const sourceFilePath = decodeURI(sourceDetails[1])
          const errorLine = parseInt(sourceDetails[2])
          const errorColumn = parseInt(sourceDetails[3])
          const source = await fsAsync.readFile(sourceFilePath, {encoding: 'utf-8'})
          const sourceLines = source.split("\n")
      
          const snippetNumberOfLookaroundLines = 3
          snippetStartLine = errorLine - snippetNumberOfLookaroundLines - 1
          snippetEndLine = errorLine + snippetNumberOfLookaroundLines

          // Make sure we don’t overflow the bounds of the source file.
          const firstLineIndexOfSourceFile = 0
          const lastLineIndexOfSourceFile = sourceLines.length - 1

          snippetStartLine = snippetStartLine < firstLineIndexOfSourceFile ? 0 : snippetStartLine
          snippetEndLine = snippetEndLine > lastLineIndexOfSourceFile ? lastLineIndexOfSourceFile : snippetEndLine

          const lineIndexToHighlight = errorLine - snippetStartLine - 1
          const snippetOfConcernLines = sourceLines.slice(snippetStartLine, snippetEndLine)

          // Mark up the code prior to syntax highlighting it to demarcate the error line
          // and error column. Do this using invisible Unicode space characters so as not
          // to affect the code.
          const ERROR_LINE_START_TOKEN = '\ue043'
          const ERROR_LINE_END_TOKEN = '\ue044'
          const ERROR_COLUMN_START_TOKEN = '\ue045'
          const ERROR_COLUMN_END_TOKEN = '\ue046'

          let lineToHighlight = snippetOfConcernLines[lineIndexToHighlight]
          // Mark up the column location.
          const lineToHighlightLetters = lineToHighlight.split('')
          lineToHighlightLetters[errorColumn-1] = `${ERROR_COLUMN_START_TOKEN}${lineToHighlightLetters[errorColumn-1]}${ERROR_COLUMN_END_TOKEN}`
          // The ERROR_COLUMN_END marker is at the end of the line so as not to
          // mess up the code and thus the tokeniser/highlighting. It doesn’t matter where it
          // goes as long as it goes after the ERROR_COLUMN_START marker as the start position
          // is the only one we’re actually interested in.
          lineToHighlight = `${ERROR_LINE_START_TOKEN}${lineToHighlightLetters.join('')}${ERROR_LINE_END_TOKEN}`
          snippetOfConcernLines[lineIndexToHighlight] = lineToHighlight

          snippetOfConcern = snippetOfConcernLines.join("\n")
      
          // We can’t use the Prism line numbers plugin in Node directly as it uses DOM and is meant for the browser.
          // Instead, here’s a workaround based on https://stackoverflow.com/a/59577306
      
          // https://github.com/PrismJS/prism/blob/master/plugins/line-numbers/prism-line-numbers.js#L109
          let NEW_LINE_EXP = /\n(?!$)/g
          let lineNumbersWrapper

          Prism.hooks.add('after-tokenize', function (env) {
            var match = env.code.match(NEW_LINE_EXP)
            var linesNum = match ? match.length + 1 : 1
            var lines = new Array(linesNum + 1).join('<span></span>')

            lineNumbersWrapper = `<span aria-hidden="true" class="line-numbers-rows">${lines}</span>`
          })

          snippetOfConcern = Prism.highlight(snippetOfConcern, Prism.languages.javascript, 'javascript')
      
          snippetOfConcern = snippetOfConcern.replace(ERROR_LINE_START_TOKEN, "<span class='error-line'>")
          snippetOfConcern = snippetOfConcern.replace(ERROR_LINE_END_TOKEN, "</span>")
          snippetOfConcern = snippetOfConcern.replace(ERROR_COLUMN_START_TOKEN, "<span class='error-column'>")
          snippetOfConcern = snippetOfConcern.replace(ERROR_COLUMN_END_TOKEN, "</span>")

          snippetOfConcern = snippetOfConcern + lineNumbersWrapper
        }
      }

      if (!stackFramesSeparated && isKittenStackFrame) {
        lastIndexOfAppStackFrames = lineIndex
        stackFramesSeparated = true
      }

      stackLines[lineIndex] = stackLine
        .replace(/</g, '&lt;') // Encode any angular brackets
        .replace(/>/g, '&gt;') // there may be prior to marking it up.
        .replace(/at (file:\/\/.*?$)/, 'at &lt;anonymous&gt; ($1)') // In case error location is anonymous function.
        .replace(/^\s*at\s*/, '<li>While running ')
        .replace(/$/, '</li>')
        .replace(`file://${process.env.basePath}`, '')
        .replace(/\<li\>(.*?)\(/, '<li><code>$1</code>(')
        .replace(/<\/code>(.*?)<\/li>/, '</code><span class=\'stack-location\'>$1</span></li>')
        .replace(kittenStackFrameRegExp2, 'in ')
        .replace(/\/node_modules\/(.*?)\//, ' module <a target=\'_blank\' href=\'https://www.npmjs.com/package/$1\'>$1</a> /')
        .replace(/\(/, 'in ')
        .replace(/:(\d*?):(\d*?)\)<\/span><\/li>$/, ' <em>(line $1, column $2)</em>')
        .replace(/&gt;\)/, '&gt;') // in case line numbers didn’t exist (e.g., <anonymous>))
        .replace('%F0%9F%94%92', '🔒')

      if (snippetOfConcern) {
        stackLines[lineIndex] = stackLines[lineIndex].replace('<li>', `<li><pre style='--start-value: ${snippetStartLine}' class='language-javascript line-numbers'><code class='language-javascript'>${snippetOfConcern}</code></pre>`)
      }
    }

    // Add the exact code location if we have it to the top stack frame.
    if (stackHeaderLines.length > 1) {
      // OK, we have a location; let’s format it.
      let codeWithError = stackHeaderLines[1]
      let errorColumnLocation = stackHeaderLines[2]
      const padding = codeWithError.match(/\s+/)[0]
      codeWithError = codeWithError.replace(padding, '')
      errorColumnLocation = errorColumnLocation.replace(padding, '')
      const lineWithReintroducedAt = stackLines[0].replace(/<li><code>/, '<li><code>at ')
      stackLines[0] = ` <pre><code>${codeWithError}</code></pre><pre><code>${errorColumnLocation}</code></pre>${lineWithReintroducedAt}`
    }
  
    // If we have stack frames both from the app/site being served and from
    // Kitten itself, we separate them visually in the output to make it
    // easier for people to debug things and focus on the important bits.
    if (!stackFramesSeparated || (stackFramesSeparated && lastIndexOfAppStackFrames > 0)) {
      const solitarySeparatorMark = stackFramesSeparated ? `class='solitary-stack-frame-separator'` : ''
      stackLines.unshift(`<section ${solitarySeparatorMark} id='app-stack-frames'><h4>From your app</h4>`)
    }
    if (stackFramesSeparated) {
      const solitarySeparatorMark = lastIndexOfAppStackFrames === 0 ? `class='solitary-stack-frame-separator'` : ''
      const insertionPoint = lastIndexOfAppStackFrames === 0 ? 0 : lastIndexOfAppStackFrames + 1
      stackLines.splice(insertionPoint, 0, `</section><section ${solitarySeparatorMark} id='kitten-stack-frames'><h4>From Kitten</h4>`)
      stackLines.push('</section>')
    }

    const prettyStackTraceHtml = stackLines.join("\n")
    errorPage = errorPage
      .replace('{STACK}', prettyStackTraceHtml)

    let relevantCodeSection = ''
    let relevantCode = error.relevantCode
    if (relevantCode != undefined) {
      // If we have multiple lines, join them into a string.
      if (Array.isArray(relevantCode)) relevantCode = relevantCode.join('\n')

      // Break up adjacent tabs for better formatting.
      relevantCode = relevantCode.replace(/></g, '>\n<')

      // Fix the indentation.
      relevantCode = jsBeautify.html(relevantCode, {
        indent_size: 2
      })

      // Add syntax highlighting.
      relevantCode = Prism.highlight(relevantCode, Prism.languages.html, 'html')

      relevantCodeSection = ` 
      <h3 id='relevant-code-title'>Relevant code</h3>
      <div id='relevant-code' class='mainSection'>
        <pre class='language-html line-numbers'><code class='language-html'>${relevantCode}</code></pre>
      </div>
      `
    }
    errorPage = errorPage.replace('{RELEVANT_CODE}', relevantCodeSection)
  }

  errorPage = errorPage
    .replace(/\{CODE\}/g, errorCode)
    .replace('{ERROR}', error.toString())

  response.end(errorPage)
}
