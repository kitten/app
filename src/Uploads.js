import fs from 'node:fs/promises'
import { createReadStream } from 'node:fs' 

import send from '@polka/send'

import { Upload } from './routes/PostRoute.js'

/**
  Uploads collection class.

  Persisted in the internal database at `_db.uploads`.
*/
export default class Uploads {
  constructor (parameters = {}) {
    // Since we have no required parameters (we’re essentially a glorified
    // object with helper methods), we simply assign any passed
    // properties to ourself.
    Object.assign(this, parameters)
  }

  /** @param {string} id */
  get (id) {
    return /** @type {Upload} */ (this[id])
  }

  length () {
    return this.allIds().length
  }

  /** @returns {Upload[]} */
  all () {
    return Object.values(this)
  }

  allIds () {
    return Object.keys(this)
  }

  /** @param {string} id */
  async delete (id) {
    const upload = /** @type {Upload} */ (this[id])
    if (upload !== undefined) {
      try {
        await fs.unlink(upload.filePath)
        delete this[id]
      } catch (error) {
        // Log the error but keep server running.
        // There’s really nothing we can do to handle this more gracefully or recover from it.
        console.error('Could not delete upload.', error)
      }
    }
  }
}


/**
  Download handler (GET route) for uploaded files.
*/
export function downloadHandler () {
  const PARTIAL_CONTENT = 206

  return (request, response) => {
    const id = request.params.id
    const download = request.params.download
    const upload = globalThis.kitten._db.uploads.get(id)
    if (upload === undefined) {
      return response.notFound('Upload not found')
    }
    const stream = createReadStream(upload.filePath)
    const headers = {
      'Content-Type': upload.mimetype,
      'Content-Disposition': `${download === 'download' ? 'attachment' : 'inline'}; filename="${upload.fileName}"; filename*=utf-8''${encodeURIComponent(upload.fileName)}`
    }
    send(response, PARTIAL_CONTENT, stream, headers)
  }
}
