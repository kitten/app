/**
  Initialise Kitten’s own internal database (kitten._db).
*/

import path from 'node:path'
import process from 'node:process'
import { random32ByteTokenInHex } from './lib/crypto.js'

import JSDB from '@small-tech/jsdb'

import { Session } from './Sessions.js'
import Uploads from './Uploads.js'
import { Upload } from './routes/PostRoute.js'

export default class InternalDatabase {
  /** @type string? */ path = null

  async initialise () {
    if (process.env.basePath === undefined) throw new Error('Kitten base path is undefined.')

    const appDataDirectory = globalThis.kitten.paths.APP_DATA_DIRECTORY
    const _dbDirectory = path.join(appDataDirectory, '_db')

    // Open Kitten’s own internal database for this place.
    const _db = JSDB.open(_dbDirectory, {
      classes: [
        Session,
        Uploads,
        Upload
      ],
      compactOnLoad: process.env.jsdbCompactOnLoad === 'false' ? false : true
    })
    globalThis.kitten._db = _db

    const kitten = globalThis.kitten

    // Create settings table, if necessary.
    if (kitten._db.settings === undefined) kitten._db.settings = {}

    // Create a webhook token, if necessary. This is used, optionaly, to trigger
    // app updates from a git repository for deployed apps.
    if (kitten._db.settings.webhookSecret === undefined) kitten._db.settings.webhookSecret = random32ByteTokenInHex()
    
    // Create the uploaded files metadata table, if necessary.
    if (kitten._db.uploads === undefined) kitten._db.uploads = new Uploads()

    // Create the package lock file hashes table, if necessary.
    if (kitten._db.packageLockFileHashes === undefined) kitten._db.packageLockFileHashes = {}

    // Create the statistics table, if necessary.
    if (kitten._db.stats === undefined) kitten._db.stats = {}
    if (kitten._db.stats.hits === undefined) kitten._db.stats.hits = {}
    if (kitten._db.stats.pages === undefined) kitten._db.stats.pages = {}
    if (kitten._db.stats.missing === undefined) kitten._db.stats.missing = {}
    if (kitten._db.stats.referrers === undefined) kitten._db.stats.referrers = {}
    if (kitten._db.stats.serverErrors === undefined) kitten._db.stats.serverErrors = {}
    
    // Save the database path.
    // FIXME: This is already in a global (kitten.paths.APP_DATA_DIRECTORY) so refactor to use that in the db commands.
    this.path = appDataDirectory

    // Expose uploads object publicly.
    globalThis.kitten.uploads = globalThis.kitten._db.uploads
  }
}
