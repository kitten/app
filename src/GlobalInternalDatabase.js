/**
  Initialise Kitten’s global internal database (kitten.__db).

  (That’s two underscores to – uhum – underscore the fact that this
  is really, really internal so don’t go messing in here. *makes scary noises*
  Here, have a ghost emoji: 👻)

  Unlike Kitten‘s internal and app databases, this database is
  scoped to the Kitten instance, *not* to the app that Kitten is serving.
*/

import process from 'node:process'
import paths from './lib/paths.js'
import JSDB from '@small-tech/jsdb'

class State {
  isBeingDeployedByDomain = false

  constructor (properties = {}) {
    Object.assign(this, properties)
  }
}

/**
  @typedef {{
    state: State
  }} GlobalInternalDatabaseType
*/

export default class GlobalInternalDatabase {
  /** @type GlobalInternalDatabaseType? */ static __db = null

  static async initialise () {
    if (this.__db === null) {
      // Open Kitten’s global internal database.
      this.__db = /** @type GlobalInternalDatabaseType */ (JSDB.open(paths.KITTEN_GLOBAL_INTERNAL_DATABASE_DIRECTORY, {
        classes: [
          State
        ],
        compactOnLoad: false
      }))

      if (this.__db.state === undefined) this.__db.state = new State()

      // Expose the database on the global kitten object.
      // (So it can be easily interacted with from the interactive shell, etc.)
      globalThis.kitten.__db = this.__db
    }

    return this.__db
  }
}
