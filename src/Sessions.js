import EventEmitter from 'node:events'
import cookie from 'cookie'
import uid from 'uid-safe'

// 1 hour = 60 mins × 60 secs × 1000 milliseconds
const hoursToMilliseconds = /** @param {number} hours */ hours => hours * 3600000 
const millisecondsToHours = /** @param {number} ms */ ms => ms / 3600000

const MAX_SESSION_LENGTH_IN_HOURS = 8
const MAX_SESSION_LENGTH_IN_MILLISECONDS = hoursToMilliseconds(MAX_SESSION_LENGTH_IN_HOURS)
const GARBAGE_COLLECTION_INTERVAL_IN_HOURS = 1
const GARBAGE_COLLECTION_INTERVAL_MILLISECONDS = hoursToMilliseconds(GARBAGE_COLLECTION_INTERVAL_IN_HOURS )


/**
  Represents an active session.

  Important: since sessions can be deleted during the lifecycle of a server and since
  you can listen for events on sessions, these need to be cleaned up before a session
  object is destroyed. As we don’t have destructors in JavaScript, you must call the
  cleanUp() method manually deleting a session.
*/
export class Session extends EventEmitter {
  /** @type {string|undefined} */
  redirectToAfterSignIn

  /**
    @param {{
      id: string,
      createdAt?: number,
      authenticated?: boolean
    }} parameterObject
  */
  constructor ({
    id,
    createdAt = Date.now(),
    authenticated = false,
    ...customData
  }) {
    super()
    if (id === undefined) throw new Error('Cannot create Session without id')
    this.id = id
    this.createdAt = createdAt
    this.authenticated = authenticated

    // Also save any custom data that might have been saved on the session.
    Object.assign(this, customData)
  }

  hasExpired () {
    const sessionLengthInHours = millisecondsToHours(Date.now() - this.createdAt)
    const expired = sessionLengthInHours >= MAX_SESSION_LENGTH_IN_HOURS
    return expired
  }
}

export default class Sessions {
  /** @type {Sessions|null} */
  static instance = null

  /**
    Singleton accessor.
    @returns {Sessions}
  */
  static getInstance() {
    if (this.instance === null) {
      this.instance = new Sessions()
    }
    return this.instance
  }

  constructor () {
    const kitten = globalThis.kitten

    if (!kitten._db.sessions) kitten._db.sessions = {}

    // Run session garbage collection every hour to expire
    // sessions older than 8 hours from the database.
    //
    // Note that because JSDB is an append-only log, the effect
    // of deleting sessions will actually make the size of the
    // database on disk grow larger until the server restarts
    // and it can be compacted. However, it will immediately
    // reduce memory usage. Neither of these should be terribly
    // critical under normal usage.
    const garbageCollectSessions = () => {
      const allSessionIds = Object.keys(kitten._db.sessions)
      allSessionIds.forEach(sessionId => {
        if (/** @type Session */ (kitten._db.sessions[sessionId]).hasExpired()) {
          // Ensure any listeners on the session are removed before deleting it.
          /** @type Session */ (kitten._db.sessions[sessionId]).removeAllListeners()
          delete kitten._db.sessions[sessionId]
        }
      })
    }
    setInterval(garbageCollectSessions, GARBAGE_COLLECTION_INTERVAL_MILLISECONDS)

    // And garbage collect sessions now.
    garbageCollectSessions()
  }

  async session (request, response) {
    const kitten = globalThis.kitten

    // See if person has active session and use it if they do.
    const cookies = cookie.parse(request.headers.cookie || '')
    let sessionId = cookies.sessionId

    if (!sessionId || !kitten._db.sessions[sessionId]) {
      // Person doesn’t have a session yet. Create a new one.
      sessionId = await uid(18)

      // Save the session ID in a cookie.
      response.setHeader('Set-Cookie', cookie.serialize('sessionId', sessionId, {
        path: '/',
        httpOnly: true,
        sameSite: 'strict',
        secure: true,
        maxAge: MAX_SESSION_LENGTH_IN_MILLISECONDS
      }))

      // Initialise a persisted data object to hold this person’s session.
      kitten._db.sessions[sessionId] = new Session({ id: sessionId })
    }

    return /** @type Session */ (kitten._db.sessions[sessionId])
  }
}
