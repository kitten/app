import LazilyLoadedRoute from './LazilyLoadedRoute.js'
import KittenPage from '../lib/KittenPage.js'

/**
  @typedef {import('ws').WebSocket & {isAlive: boolean}} WebSocketWithIsAlive
  @typedef {import('polka').Request & {ws: function}} RequestWithWebSocket
*/

const isValidUUID = /^kid-[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/
const isEventHandler = /^on[A-Z]/

export default class PageSocketRoute extends LazilyLoadedRoute {
  /** @type {[WebSocketWithIsAlive]} */ 
  sockets
  eventHandlers

  /**
    Create a lazily-loaded PageSocket route instance with the provided file path.

   (A PageSocket route is a specialised WebSocket route that simplifies streaming
   updates ot the page it is connected to.)
   
   @param {string} filePath
   @param {string} basePath
  */
  constructor (filePath, basePath) {
    super(filePath, basePath)
    this.initialised = false
    this.eventHandlers = {}
    this.sockets = /** @type {[WebSocketWithIsAlive]} */ (/** @type {unknown} */ ([]))

    // While this route is kept in the same file as the Page route, it is a socket
    // route and thus we must use the 'use' method to add it.
    this.method = 'use'
    this.extension = 'socket.js'

    // Update the pattern also so it is correct.
    this.pattern = this.pattern.replace(/\?/g, '-optional')
    this.pattern = `${this.pattern}${this.pattern.endsWith('/') ? '' : '-'}default.socket`

    // FIXME: Leaving this here for now as it showcases an issue where additional
    // routes are being created that look like a mish-mash of the ones from the main
    // app and the Kitten web app.
    console.debug('constructed', this)
  }

  /**
    Lazily load the handler.

    Unlike in other route types, we’re not loading in the default
    export here (our _handler() function is already defined, below) but
    the exported `onConnect()` function from a page route.
  
    @param {import('polka').Request} request
    @param {import('polka').Response} response
    @param {import('polka').NextHandler} next
  */
  async lazilyLoadedHandler (request, response, next) {
    // @ts-ignore mixin
    const page = /** @type {KittenPage} */ (globalThis.kitten.pages[request.query.id])

    // When a person leaves a live/connected page (a page connected
    // to its default web socket), we clean up and remove that live page
    // from memory. However, browsers being what they are, cache the page
    // in their own memory. If a person uses the back/forward buttons to
    // return to the page, the browser will serve the cached source from
    // memory, which has the old page ID, for the page that no longer exists
    // in Kitten’s memory. So now we have a problem.
    //
    // The only way to recover from this is to tell the page to reload
    // itself. So we accept the WebSocket connection, send that command
    // to the page, and then close it. That should make the page self heal.
    if (page === undefined) {
      // @ts-ignore mixin
      const ws = await request.ws()
      ws.send(globalThis.kitten.html`<script id='kitten-internal-commands'>window.location.reload(true)</script>`)
      ws.close()
      return
    }

    if (page.isAuthoredPage) {
      // The page was exported as a Page instance created by the
      // author of the Kitten app/site we’re running. So it already
      // contains its own event handlers and we don’t have to import
      // the source again. We already have everything we need.
      console.debug('[PageSocketRoute] Page is class-based, event handlers already loaded.')
      this.initialised = true
    } else if (!this.initialised) {
      // Page was exported as a simple function.
      // 
      // We expect it to export event handlers as `onEventName` (e.g., `onSomething`).
      // We make a list of any such handlers we find so that the Page instance can mix
      // them into itself when the socket connects.
      // 
      // Note: The `onConnect()` and `onDisconnect()` handlers are specific to the socket/page lifecycle
      // and reserved for use by Kitten (no not name your client-side events `connect` or `disconnect`.)
      console.debug('[PageSocketRoute] Page is function-based, loading event handlers…')
      const imports = await import(this.filePath)
      for (let exportName in imports) {
        if (exportName.match(isEventHandler) !== null) {
          // We save unbound event handlers so that we can bind them in the
          // handler to the correct page. If we did it here they’d forever
          // remain bound to the first page that resulted in the route
          // getting initialised and stop working on page reload.
          this.eventHandlers[exportName] = imports[exportName]
        }
      }
      console.debug('event handlers:', this.eventHandlers)
      this.initialised = true
    }
    this._handler(/** @type Object */ ({ request, response, next }))
  }

  /**
    This is a specialised WebSocket route handler specifically for
    routing to an `onConnect()` handler exported from a page.

    TODO: Remove redundancy with routes/WebSocketRoute.js
    
    @param {{
      request: RequestWithWebSocket
      next: import('polka').NextHandler 
    }} parameterObject
  */
  async _handler ({ request, next }) {
    if (request.method === 'GET' && request.ws) {
      console.debug('[PageSocketRoute Handler]', request.method, request.originalUrl)
      const ws = /** @type {WebSocketWithIsAlive} */ (await request.ws())

      ws.isAlive = true
      ws.on('pong', function heartbeat() {
        /** @type WebSocketWithIsAlive */ (this).isAlive = true
      })

      // Doing this here isn’t going to scale very well.
      // Might be best to inline tinyws and do it there at the
      // WebSocket server level and iterate through all
      // clients in a loop as demonstrated at:
      // https://github.com/websockets/ws/tree/master#how-to-detect-and-close-broken-connections
      const pingInterval = setInterval(function ping() {
        if (ws.isAlive === false) {
          console.debug('PageSocket Ping: web socket not alive, terminating connection.')
          return ws.terminate()
        }
        ws.isAlive = false
        ws.ping()
      }, 30000)

      ws.on('error', console.error)

      // When closed, remove from the list of sockets.
      ws.on('close', event => {
        console.debug('Closing…', event)
        clearInterval(pingInterval)
        this.sockets.some((socket, index) => {
          if (socket === ws) {
            console.debug('Removing socket from connections list.')
            // It’s ok that we splice the array in place because
            // we’re about to short circuit the loop by returning true from some().
            this.sockets.splice(index, 1)

            // @ts-ignore mixin
            const page = /** @type {KittenPage} */ (globalThis.kitten.pages[ws.id])

            // Tell the page it is disconnecting.
            page.bubbleEvent('_onDisconnect')

            // Remove page storage for this page if there is any.
            // @ts-ignore ws mixin property id
            if (ws.id && globalThis.kitten.pages[ws.id] !== undefined) {
            // @ts-ignore ws mixin property id
              console.debug('Removing page from memory', ws.id)
            // @ts-ignore ws mixin property id
              delete globalThis.kitten.pages[ws.id]

            // Broadcast page deletion events (generic and specific).
            const kittenPageDelete = {
            // @ts-ignore ws mixin property id
              id: ws.id
            }
            kitten.events.emit('kittenPageDelete', kittenPageDelete)
            // @ts-ignore ws mixin property id
            kitten.events.emit(`kittenPageDelete-${ws.id}`)
          }
            return true
          } else {
            return false
          }
        })
      })

      this.sockets.push(ws)

      const pageId = /** @type {string} */ (request.query.id)

      let pageNeedsReload = false
      if (pageId === undefined) {
        console.error('Unexpected error: PageSocketRoute error: No page ID for page', request.url)
        pageNeedsReload = true
      } else if (!isValidUUID.test(pageId)) {
        // This should never happen but better to be paranoid then sorry since we
        // receive this from the client, where it could be altered. If the page ID
        // is not a valid UUID, we ask for a page reload.
        console.error('Unexpected error: Page ID is not valid UUID', pageId, request.url)
        pageNeedsReload = true
      } else if (globalThis.kitten.pages[pageId] === undefined) {
        // This will happen when the Kitten server restarts and there
        // are pages open in browsers that try to reconnect. Since page
        // storage is ephemeral, the data for the page will have been wiped out
        // so we ask the page to reload itself.
        pageNeedsReload = true
      } else {
        // All is OK.
        // FIXME: this.id is not pageId. This is the page socket route, not the Page instance.
        // this.id = pageId
        // @ts-ignore Custom property we’re mixing in to WebSocket instances
        // so we can uniquely identify them (makes connected hierarchies
        // of kitten.Component instances and page-level storage possible).
        //
        // This page ID is generated via JavaScript on the client side.
        ws.id = pageId
      }

      // Sometimes we cannot recover and need the page reload itself.
      if (pageNeedsReload) {
        ws.send(globalThis.kitten.html`<script id='kitten-internal-commands'>window.location.reload(true)</script>`)
        return
      }

      const page = /** @type {KittenPage} */ (globalThis.kitten.pages[pageId])

      // Bind the event handlers to the page so “this” in an event handler
      // will refer to the page.
      const boundEventHandlers = {...this.eventHandlers}
      Object.keys(boundEventHandlers).forEach(key => boundEventHandlers[key] = boundEventHandlers[key].bind(page))

      page.connect(ws, this.sockets, boundEventHandlers)
    } else {
      // This is not a socket route, call next in case there is an HTTP route
      // at the same path that wants to handle the request.
      next()
    }
  }
}
