/**
  HTML Validator.
*/
import { HtmlValidate } from 'html-validate'

// FIXME: Add type definitions for this module.
import beautify from 'simply-beautiful'

import chalk from 'chalk'

/**
  Use either passed singular or plural form based on value of amount.

  @param {number} amount
  @param {string} singular
  @param {string} plural
*/
const pluralise = (amount, singular, plural) => amount === 1 ? singular : plural

const validator = new HtmlValidate({
  extends: [
    // https://html-validate.org/rules/presets.html
    'html-validate:recommended',
    'html-validate:document'
  ],
  elements: [
    'html5',
    {
      // Button type is not required in the spec and defaults to type='submit'. Reflect that.
      button: {
        attributes: {
          type: {
            required: false
          }
        }
      }
    }
  ], 
  rules: {
    // Errors.

    // https://html-validate.org/rules/no-unknown-elements.html
    'no-unknown-elements': 'error',

    // https://html-validate.org/rules/no-raw-characters.html
    'no-raw-characters': ['error', { relaxed: true }],

    // Warnings.

    // https://html-validate.org/rules/void-style.html
    'void-style': 'warn',

    // https://html-validate.org/rules/heading-level.html
    // Note: regarding relaxing the multiple H1s rule, see this
    // comment by a person who uses a screenreader:
    // https://stackoverflow.com/questions/35314891/do-multiple-h1s-cause-screen-readers-problems#comment102637865_58107524
    // (Also: this could be the case if more than one <h1> tag
    // is on the site because sections are conditionally activated via client-side JavaScript.)
    'heading-level': ['warn', { allowMultipleH1: true }],

    // Disabled rules.

    // The deprecated elements rule triggers for name attribute
    // being deprecated on <a> tags but that’s actually a valid use
    // case for Streaming HTML, so we have to disable this.
    'no-deprecated-attr': 'off',

    // Disabling this rule although it is a useful one due to false positives
    // until the default either works correctly or overriding it does.
    // See https://gitlab.com/html-validate/html-validate/-/issues/212
    'form-dup-name': 'off', // ['error', { shared: ['radio', 'submit', 'button', 'reset'] }],

    // Folks should be able to create quick experiments and play with
    // Kitten so inline styles are just a warning, not an error.
    'no-inline-style': 'off',

    // https://html-validate.org/rules/require-sri.html
    // (This kicks in for all URLs, not just remote ones and
    // requiring SRI for local/inline content is not a security
    // feature since if the server is compromised to the point
    // where arbitrary code is running on it, the security model
    // is already broken).
    'require-sri': 'off',

    // WCAG 2.1 H63: Using the scope attribute to associate header
    // cells and data cells does not apply to simple tables and
    // the Markdown renderer does not generate <th> elements with
    // the scope attribute so we’re disabling this.
    // See https://html-validate.org/rules/wcag/h63.html#markdown
    'wcag/h63': 'off',

    // WCAG H32: Providing submit buttons
    // Is a technique (informative not normative) that is one way
    // of achieving guideline 3.2.2 On Input
    // (https://www.w3.org/TR/2008/REC-WCAG20-20081211/#consistent-behavior-unpredictable-change)
    //
    // More details: https://stackoverflow.com/a/21938082
    //
    // Given it is common in Kitten to carry out Streaming HTML
    // changes, this rule is disabled. Before a Streaming HTML
    // update causes a context change, the person should be informed
    // (e.g., with text in the form, an informative label) to expect
    // that change.
    // 
    // See: https://www.w3.org/TR/2013/NOTE-WCAG20-TECHS-20130905/G13
    'wcag/h32': 'off',
    
    // https://html-validate.org/rules/doctype-style.html
    'doctype-style': 'off',

    // https://html-validate.org/rules/attr-quotes.html
    'attr-quotes': 'off',

    // https://html-validate.org/rules/no-trailing-whitespace.html
    'no-trailing-whitespace': 'off',

    // https://html-validate.org/rules/attr-case.html
    'attr-case': 'off',

    // We don’t want any SEO rules. You should optimise for people
    // not for search engines.
    'long-title': 'off'
  }
})

/**

Validates for HTML and accessibility correctness.

@param {string} source - HTML source to validate
@param {string} filePath - path of current file

**/
function validate (source, filePath) {
  // Beautify the source so we get more meaningful source snippets in case
  // the source is badly formatted (e.g., a minified HTML snippet from a
  // Mastodon API response that has no spaces/newlines between tags.)
  let beautifiedSource
  try {
    beautifiedSource = beautify.html(source)
  } catch (error) {
    console.warn('⚠️ Could not beautify validation error source.')
    beautifiedSource = source
  }

  const validationResult = validator.validateString(beautifiedSource)
  const markupLines = beautifiedSource.split('\n')

  // Based on https://gitlab.com/html-validate/html-validate/-/blob/master/src/config/severity.ts#L4
  const severityLabels = [
    'Disabled',
    'Warning',
    'Error'
  ]

  const validationIssues = []
  if (!validationResult.valid || validationResult.warningCount > 0) {
    // Page issues header (console).
    console.info('─'.repeat(60))
    console.info(
      (validationResult.errorCount > 0 ? '🔴 ' : '🟡 ')
      + chalk.bold('HTML validation failed (')
      + (validationResult.errorCount   > 0 ? `${validationResult.errorCount} ${pluralise(validationResult.errorCount, 'error', 'errors')}` : '')
      + (validationResult.errorCount > 0 && validationResult.warningCount > 0 ? ', ' : '')
      + (validationResult.warningCount > 0 ? `${validationResult.warningCount} ${pluralise(validationResult.warningCount, 'warning', 'warnings')}` : '')
      + ')'
      + '\n  '
      + ((validationResult.errorCount > 0 ? chalk.redBright : chalk.yellow)(filePath.replace(/** @type string */ (process.env.basePath), '')))
      + '\n  '
      + chalk.italic('(See your browser’s developer console for full details.)')
    )
    console.info('─'.repeat(60))

    // Create a text-only representation
    // (for the browser’s console.)
    validationResult.results.forEach(result => {
      result.messages.forEach((message, _index) => {
        let preformattedIssueHighlight = ''
          + markupLines[message.line-1]
          + '\n'
          + ' '.repeat(message.column-1)
          + ((message.size === 1) ? '▲' : '')
          + ((message.size > 1) ? '━'.repeat(message.size) : '')

        const initialIndent = preformattedIssueHighlight.match(/^( *?)[^ ]/)

        if (initialIndent !== null) {
          preformattedIssueHighlight = preformattedIssueHighlight
            .replace(new RegExp(initialIndent[1], 'g'), '')
          const lines = preformattedIssueHighlight.split('\n')
          // Truncate at 60 characters.
          // (Try and display on one line so error location highlight doesn’t fail.)
          if (lines[0].length > 60) {
            lines[0] = lines[0].slice(0,59) + '…'
          }
          preformattedIssueHighlight = lines.join('\n')
        }

        const prettySeverityString = severityLabels[message.severity]
        
        const output = 'HTML Validation '
          + prettySeverityString
          + `: ${message.ruleId}`
          + '\n'
          + `(${message.ruleUrl})`
          + '\n\n'
          + ((message.selector === null) ? '' : `Element: ${message.selector} `)
          + '\n'
          + `Issue  : ${message.message}`
          + '\n\n'
          + preformattedIssueHighlight

        const severity = message.severity === 2 ? 'error': 'warn'

        // Return object with all the data (including a few
        // calculations we don’t want to repeat) so they can
        // be used when displaying the validation errors
        // in the browser itself.
        validationIssues.push({
          severity,
          selector: message.selector,
          ruleId: message.ruleId,
          ruleUrl: message.ruleUrl,
          issue: message.message,
          prettySeverityString,
          preformattedIssueHighlight,
          details: output
        })
      })
    })
  }

  return validationIssues
}

/**
  Adds validation support to the passed HTML document.

  @param {string} htmlDocument
  @param {string} filePath
  @param {string} url
*/
export default function addValidationSupportInDevelopment (htmlDocument, filePath, url) {
  // Carry out validation (HTML, accessibility, etc.) but only during development.
  const validationIssues = validate(htmlDocument, filePath)

  // Page has HTML validation issues. Write out the code to log them out
  // into the browser’s console and a means for showing the validation
  // errors in-place on the page should the person want to do so.
  if (validationIssues.length > 0) {
    const html = globalThis.kitten.html
    const domain = globalThis.kitten.domain

    htmlDocument = htmlDocument.replace('</body>', html`
      <script>
        const validationIssues = {};

        ${validationIssues.map(validationIssue => {
          // If the URL itself has a trailing question mark (or marks),
          // this will corrupt the resulting validation URL so we remove them.
          const urlWithoutTrailingQuestionMark = url.replace(/\?+$/, '')

          let validationCode = `
            console.${validationIssue.severity}(\`${validationIssue.details}\`);
            console.info("🔍 Show validation issues on page: https://${domain}${urlWithoutTrailingQuestionMark}?showValidationIssues");
          `

          // If the selector contains double quotes, it will result in a syntax
          // error in the validation code so replace any found with single quotes.
          let safeSelector = 'body'
          if (validationIssue.selector !== null) {
            safeSelector = validationIssue.selector.replace('"', "'")
          }
        
          // More than one validation issue can target a single element so we group
          // validation issues by selector so we can display them all correctly.
          // if (validationIssue.severity) {
            validationCode += `
              if (!validationIssues["${safeSelector}"]) {
                validationIssues["${safeSelector}"] = []
              };
              validationIssues["${safeSelector}"].push({
                severity: "${validationIssue.severity}",
                ruleId: "${validationIssue.ruleId}",
                ruleUrl: "${validationIssue.ruleUrl}",
                issue: \`${validationIssue.issue.replace(/</g, '&lt;').replace(/>/g, '&gt;')}\`,
                prettySeverityString: "${validationIssue.prettySeverityString}",
                preformattedIssueHighlight: \`${validationIssue.preformattedIssueHighlight.replace(/</g, '&lt;').replace(/>/g, '&gt;')}\`,
              });
            `
          // }
          return validationCode
        })}

        if ((new URLSearchParams(window.location.search)).has('showValidationIssues')) {
          Object.entries(validationIssues).forEach(([selector, issues]) => {
            const element = document.querySelector(selector);
            element.outerHTML = \`
              <div class="validationIssue \${issues[0].severity}">
                <div class='exactLocation'>\${element.outerHTML}</div>
                \${issues.length > 1 ? '<ol>' : ''}
                \${issues.map((issue, index) => {
                  return \`\${issues.length > 1 ? '<li>': ''}
                    <strong>HTML Validation \${issue.prettySeverityString}: <a href='\${issue.ruleUrl}'>\${issue.ruleId}</a></strong>
                    <p>\${issue.issue}</p>
                    <pre><code>\${issue.preformattedIssueHighlight}</code></pre>
                  \${issues.length > 1 ? '</li>': ''}\`
                }).join('\\n')}
                \${issues.length > 1 ? '</ol>' : ''}
              </div>
            \`
          });
        };
      </script>

      <style>
        .validationIssue {
          background-color: white;
          box-shadow: rgba(0,0,0,0.75) 0.5rem 0.5rem 1rem;
          color: black;
          padding: 1rem;
          margin: 1rem;
          font-family: sans-serif;
          font-size: 1rem;
          text-align: left;
        }
        .validationIssue p::before, validationIssue p::after {
          content: ''
        }
        .validationIssue p {
          margin: 1em 0 1em 0;
          max-width: 80ch;
        }
        .validationIssue strong {
          font-weight: bold;
        }
        .validationIssue pre {
          background-color: black;
          color: white;
          padding: 1rem;
        }
        .validationIssue.error {
          border: 0.5rem solid red;
        }
        .validationIssue.warn {
          border: 0.5rem solid orange;
        }
        .exactLocation {
          padding: 0.125rem;
          margin-bottom: 1rem;
        }
        .validationIssue.error .exactLocation {
          border: 0.125rem dashed red;
        }
        .validationIssue.warn .exactLocation {
          border: 0.125rem dashed orange;
        }
      </style>
    `.join('\n') + '</body>')
  }
  return htmlDocument
}
