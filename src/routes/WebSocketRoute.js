import LazilyLoadedRoute from './LazilyLoadedRoute.js'

// Represents one WebSocket route.
// The handler wraps the actual handler to decorate it with a
// broadcast method.
export default class WebSocketRoute extends LazilyLoadedRoute {
  connections

  /**
   * Create a lazily-loaded WebSocket route instance with the provided file path.
   * 
   * @param {function|string} filePath 
   */
  constructor (filePath, basePath) {
    super(filePath, basePath)
    this.connections = []
  }
  
  async lazilyLoadedHandler (request, response, next) {
    if (!this.__handler) {
      this.__handler = (await import(this.filePath)).default
    }
    this._handler({request, response, next})
  }

  async _handler ({request, next}) {
    if (request.method === 'GET' && request.ws) {
      console.debug('[WebSocketRoute Handler]', request.method, request.originalUrl)
      const ws = await request.ws()

      ws.isAlive = true
      ws.on('pong', function heartbeat() {
        this.isAlive = true
      })

      // Doing this here isn’t going to scale very well.
      // Might be best to inline tinyws and do it there at the
      // WebSocket server level and iterate through all
      // clients in a loop as demonstrated at:
      // https://github.com/websockets/ws/tree/master#how-to-detect-and-close-broken-connections
      const pingInterval = setInterval(function ping() {
        if (ws.isAlive === false) {
          console.debug('WebSocket Ping: web socket not alive, terminating connection.')
          return ws.terminate()
        }
        ws.isAlive = false
        ws.ping()
      }, 30000)

      ws.on('error', console.error)

      // When closed, remove from the list of connections.
      ws.on('close', event => {
        console.debug('Closing…', event)
        clearInterval(pingInterval)
        this.connections.some((connection, index) => {
          if (connection === ws) {
            console.debug('Removing socket from connections list.')
            // It’s ok that we splice the array in place because
            // we’re about to short circuit the loop by returning true from some().
            this.connections.splice(index, 1)
            return true
          } else {
            return false
          }
        })
      })

      this.connections.push(ws)

      const _connections = this.connections

      const socket = new Proxy({}, {
        get: function (_obj, prop) {
          // Add helper methods to ws.
          switch (prop) {
            case 'broadcast':
              // Send to everyone but the socket that is broadcasting.
              return (message => {
                _connections.forEach(socket => {
                  if (socket !== ws) {
                    socket.send(message)
                  }
                })
                // Return the number of connections sent to.
                return _connections.length - 1
              })
            // break // unreachable

            case 'all':
              // Send to everyone (including the current client)
              return (message => {
                _connections.forEach(socket => {
                  socket.send(message)
                })
                // Return the number of connections sent to.
                return _connections.length
              })
            // break // unreachable

            default:
              // For everything else, proxy to the WebSocket object.
              return ws[prop]
          }
        }
      })

      // Call the actual handler.
      this.__handler({ request, socket })
    } else {
      // This is not a socket route, call next in case there is an HTTP route
      // at the same path that wants to handle the request.
      next()
    }
  }
}
