import path from 'path'
import util from 'node:util'

// FIXME: Add type information to this module.
import parseAttributes from '@small-tech/attribute-parser'

import LazilyLoadedRoute from './LazilyLoadedRoute.js'
import { developmentTimeHotReloadScript } from '../lib/fragments.js'
import addValidationInterface from './lib/validator.js'
import chalk from 'chalk'
import KittenPage from '../lib/KittenPage.js'

/**
  @typedef {import('polka').Request & {
    ws: function,
    session: import('../Sessions.js').Session,
  }} KittenRequest

  @typedef {import('polka').Response & {
    get (location:string):void
    redirect (location:string):void
    permanentRedirect (location:string):void
    badRequest (body?:string):void
    unauthenticated (body?:string):void
    forbidden (body?:string):void
    notFound (body?:string):void
    error (body?:string):void
  }} KittenResponse
*/

const KITTEN_PAGE_STYLES_PLACEHOLDER = '<!-- Kitten page styles -->'

const isEventHandler = /^on[A-Z]/

console.time = process.env.PROFILE ? console.time : () => {}
console.timeEnd = process.env.PROFILE ? console.timeEnd : () => {}

const baseLayoutContentRegExp = /<content.*?for=['"]?(HTML|HEAD|START_OF_BODY|BEFORE_LIBRARIES|AFTER_LIBRARIES|END_OF_BODY)['"]?>(.*?)<\/content>/gs

// This is the outermost shell, the base layout component. It’s very basic and everything is
// overridable either via returning page properties or by targeting the HEAD slot.
// Note: we place libraries at the end so that any modules loaded by the page
// execute first (so any exported values are made available to Alpine.js, htmx, etc.).

const hxBoostRegExp = /hx-boost=['"]?true['"]?/g
const pageTagRegExp = /<page(.*?)><\/page>/g
const formTagRegExp = /<form(.*?)>/g
const formTagHasHxExtWs = /hx-ext=['"]?ws['"]?/

// Code blocks that are scrollable must have tabindex='0' set to be accessible.
// (ensure elements that have scrollable content are accessible by keyboard)
const codeBlocksWithoutTabIndex = /<code(?![^>]*?tabindex[^>]*?)(.*?)>/g

/**
  Checks if a thing is a native class.
  (For differentiating between regular functions and classes.)

  @param {any} thing
*/
function isNativeClass (thing) {
  return util.inspect(thing).startsWith('[class')
}

export default class PageRoute extends LazilyLoadedRoute {
  async initialise () {
    // One-time lazy initialisation. Imports the handler and,
    // if present, the page template.
    // 
    // Supports two use cases:
    //
    // 1. Simple (page function)
    //
    //    Export your page function as the default export. e.g.,
    //    export default () => kitten.html`Hello`.
    //
    // 2. kitten.Component (object)
    //
    //    Export an instance of kitten.Component. e.g.,
    //    const Index = new kitten.Component()
    //    Index.page = () => kitten.html`<${AComponent.connectedTo(Index)} />`. 
    //    export default Index
    //
    const timeImport = `⌛ ${chalk.bold('Import:')} ${this.filePath.replace(this.basePath, '')}`
    console.time(timeImport)
    const imports = await import(this.filePath)

    this.isClassBasedRoute = false


    if (imports.default !== undefined) {
      if (isNativeClass(imports.default)) {
        // TODO: We should probably also confirm that the class and prototype are exactly
        // ===== what we expect (a kitten.Page instance that extends kitten.Component)
        //       as anything else will likely not behave as we need it to.
        // 
        // The page is a Page instance.
        this.isClassBasedRoute = true
        this.source = imports.default
        this.isLive = true
        console.debug('page is class based', this.source)
      } else if (typeof imports.default === 'function') {
        this.source = imports.default

        // If the page has exported event handler, we will wire it up to communicate
        // with its automatically generated default socket route by injecting the
        // necessary htmx attributes onto the page. This slight bit of magic does
        // expect the page to adhere to certain conventions:
        //
        // 1. It expects a body tag to be present so it can add the hx-ext='ws'
        //    and ws-connect directives.
        //
        // 2. We do not automatically add ws-send to forms to enable the Streaming HTML
        //    workflow to be used on the same page as regular hx routes (e.g., a POST route
        //    to handle file uploads. Add the connect (alias for ws-send) attribute to your
        //    forms manually.
        //
        // 3. It expects the author to add a name attribute to any element that
        //    triggers a send event.
        this.isLive = Object.keys(imports).find(importName => importName.match(isEventHandler) !== null) !== undefined
      }
    }

    if (this.source === undefined) {
      throw new Error(`Page route at <em>${this.filePath.replace(/** @type {string} */ (process.env.basePath), '')}</em> missing default handler export. <p style='font-size: 1.25rem; font-weight: normal; word-break: normal;'>Please ensure you’re exporting a default function (or <code>kitten.Page</code> subclass) from your page. e.g.</p>
        ${[kitten.md.render(`
\`\`\`js
export default function () {
  return kitten.html\`<h1>Hello, world!</h1>\`
}
\`\`\`
        `)]}
      <style>
        h2 { word-break: normal; }
        h2 pre { font-size: 1rem; padding: 0;}
        h2 code.hljs { padding: 0; margin-bottom: 2em; }
      </style>
      `)
    }

    console.debug(`${this.filePath.replace(this.basePath, '')} is class-based? ${this.isClassBasedRoute} is live? ${this.isLive}`)
    
    console.timeEnd(timeImport)
  }

  /**
    Concrete overload that lazily loads the page route handler
    as necessary and handles requests.
  
    @param {KittenRequest} request
    @param {KittenResponse} response
  */
  async lazilyLoadedHandler (request, response) {
    const timeRender = `⌛ ${chalk.bold('Render:')} ${this.filePath.replace(this.basePath, '')}`
    console.time(timeRender)
    console.debug(`${this.method} About to show page at file path ${this.filePath}`)
    console.debug(`Base path: ${process.env.basePath}`)

    if (!this.source) {
      await this.initialise()
    }

    this.url = request.url 

    if (!request.url.endsWith('/%F0%9F%92%95/sign-in/')) {
      // Set the current page as the one to redirect to after sign in.
      // (Unless this is the Kitten sign-in page itself.)
      request.session.redirectToAfterSignIn = kitten.utils.decodeFilePath(request.url)
    }

    let page = null
    let result = null
    if (this.isLive) {
      if (this.isClassBasedRoute) {
        // This is a class-based route (the author exported a specialised
        // Page instance). All we need to do is instantiate it.
        page = new this.source(request, response, /* isAuthoredPage = */ true)
      } else {
        // This is a function-based route.
        //
        // We create a new generic Page instance and bind the handler function
        // the author wrote and exported to it.
        page = new KittenPage(request, response)
        page['html'] = this.source.bind(page)
      }
      result = await page['html']({ page, request, response })
    } else {
      // This is not a live page. It does not require the creation of a
      // Page instance and so we also don’t need to bind the exported
      // function to anything. (So, unless you’re doing Streaming HTML
      // this is the way to create lightweight dynamic routes.)
      result = await this.source({ page, request, response })
    }

    // Ensure page has not ended the response itself.
    // (As it might do if it wants to return a different status code,
    // for example, like a 404 not found.)
    if (response.writableEnded) {
      console.timeEnd(timeRender)
      return
    }

    if (result === undefined) {
      // The author likely forgot to return the kitten.html from
      // the page render function.
      throw new Error(`Page did not return <code>kitten.html\`\`</code> <p style='font-size: 1.25rem; font-weight: normal;'>at ${this.filePath.replace(this.basePath, '')}</p>`)
    }
    
    const finalHtml = this.renderHtml(result, page, request)
    response.end(finalHtml)

    // Update page stats.
    const stats = globalThis.kitten._db.stats
    if (stats.pages[this.url] === undefined) {
      stats.pages[this.url] = 1
    } else {
      stats.pages[this.url]++
    }

    console.timeEnd(timeRender)
  }

  /**
    @param {{
      headSlotOverrides: Array<string>,
      page: KittenPage,
      request: import('polka').Request,
      SLOT: any
    }} parameterObject
  */
  renderBaseLayoutComponent ({
    headSlotOverrides,
    page,
    request,
    SLOT
  }) {
    // Default page settings.
    this.lang = 'en'
    let charset = 'UTF-8'
    let viewport = 'width=device-width, initial-scale=1.0'
    let icon = 'data:,'
    let title = undefined
    let syntaxHighlighterThemeLight = undefined
    let syntaxHighlighterThemeDark = undefined
    let bodyClass = ''

    const kitten = globalThis.kitten
    const HTMX = kitten.libraries.htmx
    const HTMX_IDIOMORPH = kitten.libraries.htmxIdiomorph
    const HTMX_WEBSOCKET = kitten.libraries.htmxWebSocket
    const ALPINEJS = kitten.libraries.alpineJs
    const WATER = kitten.libraries.water

    const libraries = []

    let content = SLOT.default.flat('infinity').join('\n')

    // Remove <page…></page> tag(s) and update the libraries list, etc.
    // based on the attributes therein.
    const pageTags = [...content.matchAll(pageTagRegExp)]

    // Collate all attributes specified in all page tags.
    let attributes = {}
    pageTags.forEach(pageTag => {
      Object.assign(attributes, parseAttributes(pageTag[0]))
    })

    // Head values
    this.lang = attributes.lang !== undefined ? attributes.lang : this.lang
    charset = attributes.charset !== undefined ? attributes.charset : charset
    viewport = attributes.viewport !== undefined ? attributes.viewport : viewport
    icon = attributes.icon !== undefined ? attributes.icon : icon
    bodyClass = attributes.bodyClass !== undefined ? attributes.bodyClass : bodyClass
    title = attributes.title

    if (attributes.syntaxHighlighter !== undefined) {
      syntaxHighlighterThemeLight = '/🐱/library/highlight.js/styles/measured-light.css'
      syntaxHighlighterThemeDark = '/🐱/library/highlight.js/styles/measured-dark.css'
    } else {
      syntaxHighlighterThemeLight = attributes.syntaxHighlighterThemeLight
      syntaxHighlighterThemeDark = attributes.syntaxHighlighterThemeDark
    }

    // Libraries.
    // (Include some shorthand and alternate-capitalisation aliases to make authoring forgiving.)
    if (attributes.htmx && !libraries.includes(HTMX)) libraries.push(HTMX)
    if (
      (attributes.htmxIdiomorph || /* alias */ attributes.htmxidiomorph || /* alias */ attributes.idiomorph || /* alias */ attributes.morph)
      && !libraries.includes(HTMX_IDIOMORPH)
    ) libraries.push(HTMX_IDIOMORPH)
    if (
      (attributes.htmxWebsocket || /* alias */ attributes.htmxwebsocket || /* alias */ attributes.htmxws || /* alias */ attributes.websocket || /* alias */ attributes.ws )
      && !libraries.includes(HTMX_WEBSOCKET)
    ) libraries.push(HTMX_WEBSOCKET)
    if ((attributes.alpineJs || /* alias */ attributes.alpineJS || attributes.alpinejs /* alias */ || attributes.alpine) && !libraries.includes(ALPINEJS)) libraries.push(ALPINEJS)
    if ((attributes.water || attributes.css) && !libraries.includes(WATER)) libraries.push(WATER)

    // If the page has exposed an onConnect handler, then we ensure that the HTMX library
    // as well as the HTMX idiomorph and HTMX WebSocket extensions are included on the page.
    let pageSocketConnectionCode = ''
    if (this.isLive) {
      console.debug(`Page at ${this.filePath} has onConnect handler; automatically adding HTMX and HTMX WebSocket extension.`)
      if (!libraries.includes(HTMX)) libraries.push(HTMX)
      if (!libraries.includes(HTMX_IDIOMORPH)) libraries.push(HTMX_IDIOMORPH)
      if (!libraries.includes(HTMX_WEBSOCKET)) libraries.push(HTMX_WEBSOCKET)

      let safePattern = this.pattern.replace(/\?/g, '-optional')
      // If there are request parameters, we substitute them when creating the connection code so the
      // default socket has access to all of the same parameters.
      const parameters = Object.entries(request.params)
      if (parameters.length > 0) {
        safePattern = parameters.reduce((safePattern, parameter) => safePattern.replace(`:${parameter[0]}`, parameter[1]), safePattern)
      }
      const pageSocketRoute = `${safePattern}${safePattern.endsWith('/') ? '' : '-'}default.socket`
      pageSocketConnectionCode = `hx-ext=ws ws-connect=wss://${kitten.domain}:${kitten.port}${pageSocketRoute}?id=${page.id}`
    }

    // Automatically include HTMX if hx-boost is present to make authoring easier.
    // TODO: Should we include all hx-prefixed elements?
    // TODO: Should we remove this now that we have the <page> tag?
    // It’s also causing issues.
    const hasHxBoost = hxBoostRegExp.exec(content) !== null
    if (hasHxBoost && !libraries.includes(HTMX)) {
      libraries.push(HTMX)
    }

    // Water is a CSS library so we treat it separately and remove it from
    // the list so it doesn’t get added as a JavaScript library.
    let addWater = false
    if (libraries.includes(WATER)) {
      libraries.splice(libraries.indexOf(WATER), 1)
      addWater = true
    }

    return kitten.html`
      <head>
        <if ${!headSlotOverrides.includes('charset')}>
          <meta charset='${charset}'>
        </if>
        <if ${!headSlotOverrides.includes('viewport')}>
          <meta name='viewport' content='${viewport}'>
        </if>
        <if ${!headSlotOverrides.includes('icon')}>
          <link rel='icon' href='${icon}'>
        </if>
        <if ${!headSlotOverrides.includes('title')}>
          <if ${title === undefined}>
            <title>[FALLBACK_TITLE]</title>
          <else>
            <title>${title}</title>
          </if>
        </if>
        <if ${addWater}>
          <link rel='stylesheet' href='/🐱/library/${WATER}'>
        </if>
        <if ${syntaxHighlighterThemeLight !== undefined}>
          <link rel='stylesheet' href='${syntaxHighlighterThemeLight}'>
        </if>
        <if ${syntaxHighlighterThemeDark !== undefined}>
          <link
            rel='stylesheet'
            href='${syntaxHighlighterThemeDark}'
            media='(prefers-color-scheme: dark)'
          >
        </if>
        ${[KITTEN_PAGE_STYLES_PLACEHOLDER]}
        ${SLOT.HEAD}
      </head>
      <body ${pageSocketConnectionCode} class="${bodyClass}">
        ${SLOT.START_OF_BODY}
        ${SLOT.default}
        ${SLOT.BEFORE_LIBRARIES}
        ${libraries.map(library => kitten.html`
          <script ${library === ALPINEJS ? 'defer': ''} src='/🐱/library/${library}'></script>
        `)}
        <if ${libraries.includes(HTMX)}>
          <script>
            htmx.config.useTemplateFragments = true;

            // Use kitten- prefix for htmx classes.
            htmx.config.indicatorClass = 'showDuringRequest'
            htmx.config.requestClass = 'requestInProgress'
            htmx.config.addedClass = 'added'
            htmx.config.settlingClass = 'settling'
            htmx.config.swappingClass = 'swapping'

            htmx.config.disableSelector = '[kitten-disable], [hx-disable], [data-hx-disable]'

            document.body.addEventListener('htmx:afterRequest', event => {
              console.log('htmx:afterRequest', event)
            })
        </script>
        </if>
        <script id='kitten-internal-commands'></script>
        ${SLOT.AFTER_LIBRARIES}
        ${SLOT.END_OF_BODY}
      </body>
    `
  }

  /**
    Renders the final HTML given the results of the
    html tagged template function.

    @param {string | string[]} contents
    @param {KittenPage} page
    @param {import('polka').Request} request
  */
  renderHtml (contents, page, request) {
    const allStyles = new Set()
    let html = null

    // If the contents are undefined (e.g., if the route itself
    // returned an empty string or null, etc.), let’s at least render
    // and empty string so that the development-time WebSocket, head
    // material (like <page css>), etc., have a chance to render.
    // See https://codeberg.org/kitten/app/issues/164
    if (contents == undefined) {
      contents = ''
    }

    if (typeof contents === 'string') {
      // Single HTML fragment.
      html = contents
    } else if (contents instanceof Array) {
      // Multiple HTML fragments.
      html = contents.flat(Infinity).join('\n')
    } else {
      throw new Error(`Unexpected markup encountered in page (returning objects values from pages is no longer supported. This message will also be removed in a future update.): ${contents}`)
    }
    
    // If we are in development mode, add support for hot reloads by adding a simple
    // script that connects to the development web socket, detects disconnects,
    // and refreshes the page on reconnect.
    if (!process.env.PRODUCTION) {
      html += developmentTimeHotReloadScript
    }

    // Render the page inside of the HTML template.

    // There are three special base layout component slots: HEAD, START_OF_BODY,
    // and END_OF_BODY. At this point, the only content elements that are left in
    // page can be those. Here we strip them and pass them to the base layout component
    // while rendering the page in its outermost shell.
    const SLOT = {}

    /** @type {Array<string>} */
    const headSlotOverrides = []
    html = html.replace(baseLayoutContentRegExp, (_match, slotName, slotContents) => {
      if (slotName === 'HEAD') {
        // If this is the HEAD slot, let’s take note of what’s included so that
        // if we have defaults for any tags, we don’t write them out twice.
        if (slotContents.includes('<title>')) headSlotOverrides.push('title')
        if (slotContents.includes('viewport')) headSlotOverrides.push('viewport')
        if (slotContents.includes('icon')) headSlotOverrides.push('icon')
        if (slotContents.includes('charset')) headSlotOverrides.push('charset')
      }
      if (SLOT[slotName] === undefined) {
        SLOT[slotName] = []
      }
      SLOT[slotName].push([slotContents])
      return ''
    })
    SLOT.default = [html]
    
    const renderedHtml = this.renderBaseLayoutComponent({
      headSlotOverrides,
      page,
      request,
      SLOT
    })

    // Handle wrapping the page with the doctype and html tags separately as the former
    // causes issues with the render and the latter needs to support slots but slots
    // require valid (closed) HTML snippets. We couldn’t use a conditional in the
    // template do this as we’re only conditionally writing the opening HTML tag.
    let htmlWithDoctype = "<!doctype html>\n"
      + (SLOT.HTML ? SLOT.HTML : `<html lang="${this.lang}">\n`)
      + (typeof renderedHtml.join === 'function' ? renderedHtml.flat(Infinity).join('\n') : renderedHtml)
      + '\n</html>'

    // If a custom title is not provided, try to set the title
    // from the h1 tag contents (sans any HTML tags), if it exists. 
    // If not, set it to the path from the project folder.
    // (We try to have beautiful defaults whenever possible.)
    let fallbackTitle = ''
    if (!headSlotOverrides.includes('title')) {
      const h1Text = htmlWithDoctype.match(/\<h1.*?\>(.+?)\<\/h1\>/)
      
      if (h1Text !== null) {
        fallbackTitle = h1Text[1].replace(/\<.*?\>/g, '')
      } else {
        /**
          Attempt to create a passable title by combining the same of the project folder
          with the path of the page itself.
        */
        const replaceDashesAndUnderscroresWithSpaces = /** @param {string} string */ string =>
          string.replace(/-/g, ' ').replace(/_/g, ' ')
        
        const titleCase = /** @param {string} string */ string =>
          string.replace(/\b[a-z]/g, c => c.toUpperCase())

        const removeIndexAndExtension = /** @param {string} string */ string =>
          string.replace('index.page.js', '').replace('.page.js', '')

        const trimSlashesAndReplaceRemainingOnesWithColons = /** @param {string} string */ string =>
          string.replace(/^\//, '').replace(/\/\s*$/, '').replace(/\//g, ': ')

        const prettify = /** @param {string} string */ string =>
          titleCase(replaceDashesAndUnderscroresWithSpaces(removeIndexAndExtension(string)))

        const basePath = /** @type {string} */ (process.env.basePath)
        const projectTitle = prettify(basePath.slice(basePath.lastIndexOf(path.sep) + 1))
        const pageTitle = trimSlashesAndReplaceRemainingOnesWithColons(prettify(this.filePath.replace(basePath, '')))

        // Only display project name – page title separator if 
        // page title exists (e.g., if it’s index.page, then the title will be empty.)
        const separator = pageTitle.length > 0 ? ' – ' : ' '

        fallbackTitle = `${projectTitle}${separator}${pageTitle}`
      }
      // Replace the placeholder.
      htmlWithDoctype = htmlWithDoctype.replace('[FALLBACK_TITLE]', fallbackTitle)
    }

    // If the page has event handlers (uses Streaming HTML), inject HTMX WebSocket
    // ws-send directives into any forms we find to automatically wire them
    // up to sent when a triggering event (e.g., submit button press) occurs. 
    // Note: we also add hx-ext='ws' to every form element so that button names
    // are correctly included in the HTMX headers. We might be able to remove this
    // once this issue has been dealth with in HTMX.
    // (See https://github.com/bigskysoftware/htmx/discussions/970
    //      https://github.com/bigskysoftware/htmx/discussions/1337
    //      https://github.com/bigskysoftware/htmx/issues/2245)
    if (this.isLive) {
      [...htmlWithDoctype.matchAll(formTagRegExp)].forEach(form => {
        const formTag = form[0]
        // Only add the attribute if it doesn’t already have it.
        // (e.g., the connect attribute automatically sets it and we 
        // don’t want to result in an HTML validation error by
        // including it more than once.)
        if (!formTagHasHxExtWs.test(formTag)) {
          htmlWithDoctype = htmlWithDoctype.replace(form[0], `<form${form[1]} hx-ext='ws'>`)
        }
      })
    }

    // Add inline styles without redundancy.
    const inlineStyles = htmlWithDoctype.match(/<style>.*?<\/style>/gs) || []
    inlineStyles.forEach(inlineStyle => {
      allStyles.add(inlineStyle.replace('<style>', '').replace('</style>', ''))
      htmlWithDoctype = htmlWithDoctype.replace(inlineStyle, '')
    })

    let styleTag = ''
    if (allStyles.size > 0) {
      styleTag = '<style>\n' + Array.from(allStyles).join('\n') + '\n</style>'
    }

    let htmlDocument = htmlWithDoctype
      .replace(KITTEN_PAGE_STYLES_PLACEHOLDER, styleTag)
      .replace(pageTagRegExp, '')

    if (this.url === undefined) {
      throw new Error('URL is undefined in Page Route, cannot continue.')
    }

    htmlDocument = htmlDocument.replaceAll(codeBlocksWithoutTabIndex, (_match, attributes) => `<code${attributes} tabindex='0'>`)

    if (!process.env.PRODUCTION) {
      htmlDocument = addValidationInterface(htmlDocument, this.filePath, this.url)
    }

    return htmlDocument
  }
}
