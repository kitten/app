import pathCompleteExtName from 'path-complete-extname'

import { routePatternFromFilePath, HTTP_METHODS } from '../Utils.js'

export default class LazilyLoadedRoute {
  /**
   Abstract base class for lazily-loaded routes.

   @param {string} filePath
   @param {string} basePath
  */
  constructor (filePath, basePath) {
    this.filePath = filePath
    this.basePath = basePath
    this.extension = pathCompleteExtName(this.filePath).replace('.', '')
    this.pattern = routePatternFromFilePath(this.filePath, this.basePath)
    this.method = HTTP_METHODS.includes(this.extension) ? this.extension.replace('.js', '') : this.extension === 'socket.js' ? 'use' : 'get' 
  }

  // Get a bound reference to the handler.
  get handler () {
    return this.lazilyLoadedHandler.bind(this)
  }
  
  // Override this method in subclasses to customise lazy loading and rendering logic.
  async lazilyLoadedHandler (request, response, next) {
    throw new Error(`LazilyLoadedRoute abstract base class handler called. You must override the lazilyLoadedHandler() method in your concrete subclass.`, request, response, next)
  }
}

