import LazilyLoadedRoute from './LazilyLoadedRoute.js'

export default class HttpRoute extends LazilyLoadedRoute {
  async initialise () {
    // One-time lazy initialisation. Imports the handler and,
    // if present, the page template.
    this._handler = (await import(this.filePath)).default
  }

  /**
    Concrete overload that lazily loads the route handler if
    necessary and handles requests.
  
    @param {import('polka').Request} request
    @param {import('polka').Response} response
  */
  async lazilyLoadedHandler (request, response) {
    if (!this._handler) {
      await this.initialise()
    }
    let result = await this._handler({ request, response })
    response.end(result)
  }
}
