import LazilyLoadedRoute from './LazilyLoadedRoute.js'

/**
  We convert the file upload metadata returned by Busboy
  to a leaner format that’s more ergonomic when authoring so
  we do a little bit of gymlastics with types so our intent
  is clear and the type checker is happy.

  @typedef {{
    uuid: string,
    filename: string,
    file: string,
    mimetype: string,
    field: string,
    encoding: string,
    truncated: boolean,
    done: boolean
  }} BusboyFileUploadMetadata

  @typedef {import('polka').Request & {
    uploads: Upload[],
    files: BusboyFileUploadMetadata[]
  }} KittenRequest

  @typedef {import('polka').Request & {
    files: BusboyFileUploadMetadata[]
  }} OriginalRequest
*/

export class Upload {
  /** @type {string} */ id
  /** @type {string} */ filePath
  /** @type {string} */ mimetype
  /** @type {string} */ field
  /** @type {string} */ encoding
  /** @type {boolean} */ truncated
  /** @type {boolean} */ done
  
  /**
    @param {{
      id: string,
      fileName: string,
      filePath: string,
      mimetype: string,
      field: string,
      encoding: string,
      truncated: boolean,
      done: boolean
    }} parameters
  */
  constructor (parameters) {
    Object.assign(this, parameters)
  }

  /**
    Returns absolute resource path for accessing this upload via a GET request
    (content-disposition: inline).
  */
  get resourcePath () {
    return `/uploads/${this.id}/`
  }

  /**
    Returns the absolute download path (content-disposition: attachment).
  */
  get downloadPath () {
    return `${this.resourcePath}download`
  }

  /**
    Deletes this uploaded file from the file system and also from the internal database. 
  */
  async delete () {
    // Carry out the delete at the collection level. Ideally, we
    // should’t know anything about the collection level but I
    // think we can get away with this little violation of encapsulation
    // as the alterntive would require unnecessary overhead to either
    // inject the collection reference to every upload or implement some
    // sort of event system, etc. The important thing is that we are not
    // redundantly implementing the delete functionality at both the collection
    // and the individual item level.
    globalThis.kitten._db.uploads.delete(this.id)
  }
}

/**
  Persists upload metadata.

  (Recursive; handles arrays/batches of metadata.)

  @param {BusboyFileUploadMetadata | BusboyFileUploadMetadata[]} metadata

  @returns {Upload | Upload[]}
*/
function persistMetadata (metadata) {
  if (Array.isArray(metadata)) {
    // Recurse on batches.
    const files = /** @type Upload[] */ ([])
    metadata.forEach(file => {
      files.push(/** @type Upload */ (persistMetadata(file)))
    })
    return files
  } else {
    // Persist and return metadata.

    // Convert the Busboy field into the ones that the Upload class expects
    // (these are easier to remember and use and are more consistent – e.g., with
    // capitalisation, etc.).
    const parameters = {
      // Fields with altered names.
      id: metadata.uuid,
      fileName: metadata.filename,
      filePath: metadata.file,

      // Fields with same names as Busboy metadata.
      mimetype: metadata.mimetype,
      field: metadata.field,
      encoding: metadata.encoding,
      truncated: metadata.truncated,
      done: metadata.done
    }

    const upload = new Upload(parameters)
    globalThis.kitten._db.uploads[upload.id] = upload
    return upload
  }
}

export default class PostRoute extends LazilyLoadedRoute {
  async initialise () {
    // One-time lazy initialisation. Imports the handler and,
    // if present, the page template.
    this._handler = (await import(this.filePath)).default
  }

  /**
    Concrete overload that lazily loads the POST route handler if
    necessary and handles requests.
  
    @param {OriginalRequest} request
    @param {import('polka').Response} response
  */
  async lazilyLoadedHandler (request, response) {
    if (!this._handler) {
      await this.initialise()
    }

    // Handle file uploads if there are any.
    if (request.files) {
      const files = []
      
      const batches = Object.values(request.files)
      batches.forEach(batch => {
        files.push(persistMetadata(batch))
      })

      // Terminate previous statement to remove ambiguity
      // about whether this is a function call or not as next
      // statement is a cast that starts with an opening parenthesis.
      ;

      // Expose the uploads on the request object.
      /** @type {KittenRequest} */ (request).uploads = files.flat(Infinity)
    }

    /** Since we’ve changed the metadata structure, this is a new data type; make the conversion explicit. */
    /** @type {any} */ let _request = request
    let kittenRequest = /** @type {KittenRequest} */ (_request)

    let result = await this._handler({ request: kittenRequest, response })
    response.end(result)
  }
}
