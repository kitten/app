// FIXME: See if there’s a type definition for this module. If not, provide one.
import staticFile from 'connect-static-file'

import { routePatternFromFilePath } from '../Utils.js'

export default class StaticRoute {
  /**
    Represents static routes.

    @param {string} filePath
    @param {string} basePath
  */
  constructor (filePath, basePath) {
    this.filePath = filePath
    this.basePath = basePath
    this.pattern = routePatternFromFilePath(this.filePath, this.basePath)
    this.method = 'get' 
  }

  // This is a simple static file; serve it as such.
  get handler () {
    const options = this.filePath.endsWith('.gz') ? {encoded: 'gzip'} : {}
    return staticFile(this.filePath, options)
  }
}
