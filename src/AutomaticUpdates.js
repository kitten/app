import AppRepository from './AppRepository.js'
import KittenPackage from './lib/KittenPackage.js'
import systemExitCodes from './lib/system-exit-codes.js'

/**
  Schedules and runs automatic updates of both Kitten itself and the
  app that Kitten is running. Relevant in production mode only.

  Singleton.
*/

const hoursInMilliseconds = hours => hours * 60 /* minutes */ * 60 /* seconds */ * 1000 /* milliseconds */

export default class AutomaticUpdates {
  /** @type {AutomaticUpdates} */ static instance
  running = false

  /** @type {Date} */ lastCheck

  get interval () {
    return globalThis.kitten._db.settings.autoUpdate.interval
  }

  set interval(intervalInMiliseconds) {
    this.running = intervalInMiliseconds !== -1
    globalThis.kitten._db.settings.autoUpdate.interval = intervalInMiliseconds
  }
  
  static getInstance () {
    if (this.instance === undefined) {
      this.instance = new this()
    }
    return this.instance
  }

  async start () {
    this.kittenPackage = KittenPackage.getInstance()
    this.appRepository = await AppRepository.getInstance()

    if (globalThis.kitten._db.settings.autoUpdate === undefined) {
      globalThis.kitten._db.settings.autoUpdate = {
        interval: hoursInMilliseconds(6) // Default: every six hours; four times a day.
     }
    }

    // Test
    // this.interval = 5000 // every five seconds.

    if (this.interval === -1) {
      // Automatic updates are turned off.
      console.info('Automatic updates are turned off.')
      return
    }

    await this.checkForUpdates()

    this.intervalId = setInterval(this.checkForUpdates.bind(this), this.interval)
    this.running = true
  }

  requestRestart () {
    console.info('🪄 About to restart the server to run latest Kitten and app versions.')
    process.exit(systemExitCodes.restartRequest)
  }

  updateLastCheck () {
    this.lastCheck = new Date()
  }

  async checkForUpdates () {
    console.info('🪄 Checking for updates…')
    this.updateLastCheck()

    if (this.kittenPackage === undefined || this.appRepository === undefined) {
      console.error('Cannot check for updates. Either the kitten package or app repository are undefined.')
      return
    }

    await this.kittenPackage.update()
    await this.appRepository.update()

    console.info(`🪄 Current app version: ${this.appRepository.currentVersion}`)
    console.info(`🪄 Current Kitten version: ${this.kittenPackage.currentVersion.exactVersion}`)

    if (this.appRepository.hasNewerApiVersion) {
      //
      // App has newer version available that requires
      // newer version of Kitten to be installed.
      //
      console.info(`🪄 The latest version of the installed app (${this.appRepository.latestVersion}) requires a Kitten distribution with an API version that is newer than the one that’s currently installed (current: ${this.kittenPackage.apiVersion}, required: ${this.appRepository.latestApiVersion}).`)

      if (this.kittenPackage.latestVersion === undefined || this.appRepository.latestApiVersion === undefined) {
        console.error('Either kitten package or app repository does not have api version defined for latest available version.')
        return
      }

      // Check if there is a compatible Kitten version available.
      if (this.kittenPackage.latestVersion.apiVersion >= this.appRepository.latestApiVersion) {
        // OK, a Kitten distribution with this API version exists. Let’s install that.
        console.info(`🪄 About to upgrade Kitten to API version ${this.appRepository.latestApiVersion}`)
        // TODO: Shouldn’t this be async?
        this.kittenPackage.upgrade(this.appRepository.latestApiVersion)
        console.info(`🪄 About to upgrade app to latest version (${this.appRepository.latestVersion})`)
        await this.appRepository.updateAppToVersion(this.appRepository.latestVersion)
        this.requestRestart()
      } else {
        // A Kitten distribution with required API version doesn’t exist (yet?)
        // so we cannot update app yet either.
        console.warn(`🪄 A Kitten distribution with the required API version (${this.appRepository.latestApiVersion}) doesn’t exist yet (the latest available Kitten version, ${this.kittenPackage.latestVersion?.exactVersion}, only support API version ${this.kittenPackage.latestVersion?.apiVersion}) so we cannot update the app to its latest version yet.`)
      }
      // Note: we fall through here so that even if we cannot upgrade to the latest available
      // app version, we still have a chance to upgrade to the latest compatible app version,
      // if one exists.
    }

    //
    // Handle case where a compatible upgrade is available.
    //
    if (this.appRepository.canBeUpgraded) {
      console.info('🪄 Newer compatible version of app is available, about to upgrade.')
      await this.appRepository.upgradeAppToLatestCompatibleVersion()
      this.requestRestart()
    }

    //
    // At this point, we know that there isn’t an app update available but
    // there might still be a compatible Kitten upgrade available. If there
    // is, let’s just upgrade Kitten itself.
    //
    if (this.kittenPackage.canBeUpgraded) {
      // TODO: Again, this should be async.
      this.kittenPackage.upgrade()
      this.requestRestart()
    }

    console.info('🪄 No compatible app or Kitten updates currently available.')
  }

  get intervalInHours () {
    if (!this.running) return this.interval
    return this.interval / 60 / 60 / 1000
  }

  timeToNextCheck () {
    return this.interval - (new Date().getTime() - this.lastCheck.getTime()) /* milliseconds */
  }

  timeToNextCheckPretty () {
    if (!this.running) return this.interval
    const t = this.timeToNextCheck()
    let prettyTime
    let unit = 'hour'
    prettyTime = Math.round(t/60/60/1000) // Time in hours.
    if (prettyTime < 1) {
      prettyTime = Math.round(t/60/1000) // Time in minutes.
      unit = 'minute'
    }
    if (prettyTime !== 1) {
      unit += 's'
    }
    return prettyTime === 0 ? 'imminent' : `in roughly ${prettyTime} ${unit}`
  }

  stop () {
    clearInterval(this.intervalId)
    this.running = false
  }
}
