/**
  Files: handles the files in the project.

  Copyright ⓒ 2021-present, Aral Balkan (mail@ar.al)
  Small Technology Foundation (https://small-tech.org)
  License: AGPL version 3.0.
*/

import fs from 'fs'

import readdir from 'tiny-readdir'
import Watcher from 'watcher'

import {
  DYNAMIC_ROUTE_EXTENSIONS,
  DEPENDENCY_EXTENSIONS,
  extensionCategories,
  extensionOfFilePath
} from './Utils.js'

const extensionCategoryNames = Object.keys(extensionCategories)

const ignoreForRoutingRegExp = /(^|[\/\\])\.(?!local\/share\/|well-known)+|node_modules|app_modules/

// Note: we can ignore node_modules as you should really not be fiddling with its contents directly
// even when developing. Any changes via package.json will get picked up.
const ignoreForWatchingRegExp = /(^|[\/\\])\.(?!local\/share\/|well-known)+|node_modules/

export class FileEventDetails {
  constructor (itemType = '', eventType = '', itemPath = '') {
    this.itemType = itemType
    this.eventType = eventType
    this.itemPath = itemPath
  }
}

export class FilesByExtensionCategoryType {
  constructor () {
    this.backendRoutes = {}
    this.frontendRoutes = {}
    this.dependencies = {}
    this.dynamicRoutes = {}
    this.staticRoutes = {}
    this.allRoutes = {}
  }
}

export default class Files extends EventTarget {
  static isBeingInstantiatedByFactoryMethod = false

  /**
    Static factory method: constructs a Files instance, asynchronously
    initialises it, and returns the list of files found organised by
    their extension category type.
  */
  static async new (basePath = /** @type string */ (process.env.basePath)) {
    if (basePath === '/') throw new Error('Cannot serve root')

    this.isBeingInstantiatedByFactoryMethod = true
    const instance = new this(basePath)
    await instance.initialise()
    this.isBeingInstantiatedByFactoryMethod = false
    return instance
  }

  /** @type Watcher */ watcher

  /**
    Find files at given base path and start watching for changes. 
  
    @param {string} basePath
  */
  constructor (basePath) {
    // Control access.
    if (!Files.isBeingInstantiatedByFactoryMethod) {
      throw new Error('Error: constructor called directly. Please use `await Files.new() to construct a Files instance.`')
    }
    super()
    this.pathToWatch = basePath
    this.byExtensionCategoryType = new FilesByExtensionCategoryType()
  }

  async initialise () {
    // Control access.
    if (!Files.isBeingInstantiatedByFactoryMethod) {
      throw new Error('Error: initialise() called directly. Please use `await Files.new() to construct a Files instance.`')
    }

    // Sanity check: ensure the path to watch exists.
    if (!fs.existsSync(this.pathToWatch)) {
      throw new Error(`File watcher: ${this.pathToWatch} does not exist. Panic!`)
    }

    // First, we use tiny-readdir (which is also used by Watcher), to create our
    // initial list of files. Then we instantiate Watcher.
    // Note: we should be able to pass the returned readdirMap to Watcher
    // so it doesn’t have to replicate the directory traversal but there is a bug
    // currently preventing this.
    //
    // Keep an eye on the following two issues:
    // - https://github.com/fabiospampinato/watcher/issues/18
    // - https://github.com/fabiospampinato/watcher/issues/10
    const readdirMap = await readdir (this.pathToWatch, {
      followSymlinks: false,
      ignore: targetPath => ignoreForRoutingRegExp.test(targetPath.replace(this.pathToWatch, ''))
    })

    readdirMap.files.forEach(filePath => this.addFile(filePath))

    this.watcher = new Watcher(this.pathToWatch, {
      recursive: true,
      ignoreInitial: true,
      ignore: targetPath => ignoreForWatchingRegExp.test(targetPath.replace(this.pathToWatch, ''))
    })
    
    this.watcher.on('add', filePath => {
      console.debug('Watcher add:', filePath)

      // A file added event fires regardless of whether this file is a
      // supported route extension (or even a dependency) – for example,
      // it could just be a static file. This is so that, in production mode,
      // the server can restart conservatively whenever _any_ file changes so
      // we know that we are always running the latest deployed version of a site/app.
      // TODO: if “change” is the level of granularity we end of up needing for this
      // functionality, it might make sense to only have a single “change” event for all
      // changes fire. No need to make a decision on this now; let’s evaluate through
      // usage as Kitten matures.
      this.notifyListeners(new FileEventDetails('file', 'added', filePath))

      // Filter out directories.
      if (extensionOfFilePath(filePath) != '') {
        this.addFile(filePath)
      } 
    })

    this.watcher.on('change', filePath => {
      console.debug('Watcher changed:', filePath)
      this.notifyListeners(new FileEventDetails('file', 'changed', filePath)) 
    })

    this.watcher.on('unlink', filePath => { 
      console.debug('Watcher unlink:', filePath)
      this.notifyListeners(new FileEventDetails('file', 'unlinked', filePath))
      // TODO
      // this.removeFile(filePath)
    })

    this.watcher.on('addDir', filePath => {
      console.debug('Watcher addDir:', filePath)
      this.notifyListeners(new FileEventDetails('directory', 'added', filePath))
    })

    // When a directory is unlinked, we should sync the state of the files in that directory.
    // (Does an unlink directory result in specific unlink events for all files in the directory?) TODO: Test.
    this.watcher.on('unlinkDir', filePath => {
      console.debug('Watcher unlinkDir:', filePath)
      this.notifyListeners(new FileEventDetails('directory', 'unlinked', filePath))
    })

    this.watcher.on('error', async error => {
      console.error('Watcher error', error)
      this.watcher.close()
    })
  }

  /**
    Adds file to the list of files.

    @param {string} filePath
  */
  addFile(filePath) {
    const extension = extensionOfFilePath(filePath)

    for (const extensionCategoryName of extensionCategoryNames) {
      const extensionCategory = extensionCategories[extensionCategoryName]
      
      const isStaticRoute = (extensionCategoryName === 'staticRoutes' && !DYNAMIC_ROUTE_EXTENSIONS.includes(extension) && !DEPENDENCY_EXTENSIONS.includes(extension))
      if (extensionCategory.includes(extension) || isStaticRoute) {
        // This might be the first time a file of this extension is encountered.
        // If so, create the category before attempting to add to it.
        const filesByExtension = this.byExtensionCategoryType[extensionCategoryName]
        if (!filesByExtension[extension]) {
          filesByExtension[extension] = []
        }

        filesByExtension[extension].push(filePath)

        this.notifyListeners(new FileEventDetails('route', 'added', filePath))
      }  
    }
  }

  /**
    Notifies file listeners that an action has happened on a file.

    @param {FileEventDetails} fileEventDetails
  */
  async notifyListeners(fileEventDetails) {
    this.dispatchEvent(new CustomEvent('file', {
      detail: fileEventDetails
    }))
  }

  /**
    Closes the watcher.
  */
  close() {
    this.watcher.close()
  }
}
