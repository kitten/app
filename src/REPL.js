import fs from 'node:fs'
import path from 'node:path'
import util from 'node:util'
import readline from 'node:readline'
import net from 'node:net'
import { Transform } from 'node:stream'
import repl from 'node:repl'
import chalk from 'chalk'
import { createKeys } from './lib/crypto.js'
import systemInformation from 'systeminformation'

/**
  Displays the global kitten object keys, and, if this is an initial
  launch-time tutorial, also shows the command you need to use.
*/
function showKittenKeys ({isTutorial = false, print = console.info } = {}) {
  print()
  print(chalk.green(`   » 💡${chalk.bold('.ls')} is an alias for ${chalk.bold('Object.keys(kitten)')}`))
  if (!isTutorial) {
    print()
    print(chalk.blue.italic(`   These are the keys of the global ${chalk.bold('kitten')} object:`))
  }
  print()
  print(util.inspect(Object.keys(globalThis.kitten), { colors: true }).split('\n').map(line=>`   ${line}`).join('\n'))
  print()
}

/**
  Sets up Kitten REPL (either an interactive one if running locally or on a socket if running as a daemon).

  @param {string} domainWithPort
*/
export default function (domainWithPort) {
  // Listen for commands on running process.
  if (process.stdout.isTTY) {
    let rl
    rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })

    if (process.stdin.setRawMode != null) {
      process.stdin.setRawMode(true)
    }

    /**
      Key listener.

      @param {string} _data
      @param {{ ctrl:boolean, meta:boolean, shift:boolean, name:string }} key
    */
    const keyListener = async (_data, key) => {
      // Make it easy to exit with one Ctrl-c.
      if (key.name === 'c' && key.ctrl === true) {
        console.info(`\n👋 ${chalk.bold('Exit requested (ctrl-c)')} Goodbye!`)
        console.info()
        console.info('🐱 Thank you for playing with Kitten!')
        console.info()
        console.info(' ╭───────────────────────────────────────────╮')
        console.info(` │ ${chalk.bold('Like this? Fund us!')}                       │`)
        console.info(' │                                           │')
        console.info(' │ We’re a tiny, independent not-for-profit. │')
        console.info(' │ https://small-tech.org/fund-us            │')
        console.info(' ╰───────────────────────────────────────────╯')
        process.exit(0)
      }

      // Launch interactive shell (REPL).
      // When REPL is running, do not listen for any other keys.
      if (key.name === 's') {
        console.info(`\n\n🐢 ${chalk.bold('Launching interactive shell')} ctrl-d to exit; .help for assistance`)
        console.info()
        console.info(chalk.blue.italic(`   A good place to start is to run the ${chalk.bold('.ls')} command to see the keys of the global ${chalk.bold('kitten')} object:`))

        showKittenKeys({isTutorial: true})

        // Destroy our readline interface before launching the REPL.
        rl.close()

        // Stop listening for keypresses (we don’t want to interfere
        // with the Node REPL’s standard behaviour).
        process.stdin.off('keypress', keyListener)

        const kittenRepl = repl.start({
          prompt: '🐱 💬 ',
          useGlobal: true,
          writer: function (output) {
            return `\n${util.inspect(output, {
              colors: true,
              showProxy: true
            }).split('\n').map(line=>`  ${line}`).join('\n')}\n`
          }
        })
        setupRepl(kittenRepl, console.info, domainWithPort)

        kittenRepl.on('exit', () => {
          console.info(`\n🐢 ${chalk.bold('Exited interactive shell.')} ${chalk.blue.italic(`Press [${chalk.bold('s')}] to relaunch.`)}`)
        })

        kittenRepl.on('close', function () {
          // Recreate out own readline interface.
          rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
          })
          // Reattach our regular key listener.
          process.stdin.on('keypress', keyListener)
        })
      }
    }

    // Listen for command keys.
    process.stdin.on('keypress', keyListener)
  } else {
    console.info('🐢 Interactive shell is available on 127.0.0.1, port 1337.')
    console.info('🐢 To access it, run kitten shell.')

    const server = net.createServer(socket => {
      const controlChannel = new Transform({
        transform(chunk, _encoding, callback) {
          if (chunk.at(0) === 22) {
            // SYN (0x16; dec 22) signals a synchronisation message
            // following a resize of the remote terminal client.
            // The next two bytes are the columns and rows values, respectively.
            // @ts-ignore (columns property does exist on socket)
            socket.columns = chunk.at(1)
            // @ts-ignore (rows property does exist on socket)
            socket.rows = chunk.at(2)
            callback()
          } else {
            // Passthrough.
            callback(null, chunk)
          }
        }
      })

      // Give the control channel a chance to handle control messages.
      socket.pipe(controlChannel)
      
      const kittenRepl = repl.start({
        prompt: `🐱 💬 (${socket.remoteAddress}:${socket.remotePort}) `,
        input: controlChannel,
        output: socket,
        terminal: true,
        useGlobal: false,
        writer: function (output) {
          return `\n${util.inspect(output, {
            colors: true,
            showProxy: true
          }).split('\n').map(line=>`  ${line}`).join('\n')}\n`
        }
      })

      setupRepl(kittenRepl, (/** @type {string} */ output) => socket.write(output === undefined ? '\n' : output + '\n'), domainWithPort)
      kittenRepl.on('exit', () => socket.end())
      kittenRepl.context.socket = socket
    }).listen({
      host: '127.0.0.1',
      port: 1337
    })

    server.on('error', error => {
      console.warn('🙀 Could not initialise interactive shell, port 1337 may already be in use. Ignoring…')
    })
  }
}

/**
  Sets up the Kitten REPL

  @param {repl.REPLServer} kittenRepl
  @param {any} print
  @param {string} domainWithPort
*/
async function setupRepl (kittenRepl, print, domainWithPort) {
  // Set up the history file (this is per-app).
  const replHistoryFolderPath = globalThis.kitten.paths.APP_REPL_DIRECTORY
  const replHistoryFilePath = path.join(replHistoryFolderPath, 'history.txt')
  fs.mkdirSync(replHistoryFolderPath, { recursive: true })

  try {
    await new Promise((resolve, reject) => {
      kittenRepl.setupHistory(replHistoryFilePath, error => {
        if (error !== null) { reject(error) }
        resolve(true)
      })
    })
  } catch (error) {
    console.warn(`Warning: could not set up history for interactive shell (REPL).`, error)
  }

  /**
    Display settings URL.
  */
  kittenRepl.defineCommand('stats', {
    help: 'Show current statistics on Kitten server',
    async action () {
      const heading = chalk.bold.magentaBright
      const heading2 = (/** @type {string} */ str) => chalk.bold.blue(`  ${str}`)
      const p = (/** @type {string} */ str) => `  ${str}`
      const stat = chalk.green
      const errorMessage = chalk.bold.red
      this.clearBufferedCommand()
      print()
      let processes
      try {
        processes = await systemInformation.processes()
        // Kitten processes
        const kittenProcessManagerProcesses = processes.list.filter(process => process.params.includes('kitten-process-manager.js'))
        const kittenProcesses = kittenProcessManagerProcesses.filter(
          process => kittenProcessManagerProcesses.find(otherProcess => otherProcess.pid === process.parentPid)?.name === 'node'
        )
        const kittenDaemonProcesses = processes.list.filter(process => process.params.includes('kitten-bundle.js'))

        if (kittenDaemonProcesses.length > 1) {
          // There should only be one Kitten daemon process running on any given
          // machine so something has clearly gone wrong here.
          print(errorMessage(`There should only be one Kitten daemon process per server but found ${kittenDaemonProcesses.length} processes:`))
          for (const kittenDaemonProcess of kittenDaemonProcesses) {
            print(` • [${kittenDaemonProcess.pid}] ${kittenDaemonProcess.command} ${kittenDaemonProcess.params}`)
          }
          print()
        }

        const showProcessState = (process, custom = {}) => {
          const processPath = custom.processPath ? custom.processPath : process.params.split('--working-directory=')[1]
          print(`  ${chalk.bold.blue('Serving   ')} ${chalk.blue(processPath)}`)
          print()
          if (custom.mode) {
            print(`  ${chalk.bold('Mode      ')} ${custom.mode}`)
          }
          print(`  ${chalk.bold('State     ')} ${process.state}`)
          print(`  ${chalk.bold('CPU       ')} ${stat(process.cpu.toFixed(2))}% (user: ${stat(process.cpuu.toFixed(2))}%, system: ${stat(process.cpus.toFixed(2))}%)`)
          print(`  ${chalk.bold('Memory    ')} ${stat(process.mem)}% `)
        }
        const showProcessDetails = process => {
          const dateTime = process.started.split(' ')
          print(`  ${chalk.bold('Owner     ')} ${process.user}`)
          print(`  ${chalk.bold('Started   ')} ${dateTime[0]} at ${dateTime[1]}`) 
          print(`  ${chalk.bold('ID        ')} ${process.pid}`)
          print(`  ${chalk.bold('Parent ID ')} ${process.parentPid}`)
        }

        // What sort of process are we? (Interactive or deamon?)
        let currentProcess
        let currentProcessIsInteractive = false
        currentProcess = kittenProcessManagerProcesses.find(process => process.pid === globalThis.process.pid)
        if (currentProcess !== undefined) {
          currentProcessIsInteractive = true
        } else {
          currentProcess = kittenDaemonProcesses.find(process => process.pid === globalThis.process.pid)
          if (currentProcess === undefined) {
            print(p(errorMessage('Cannot find current process.')))
            return
          }
        }

        // Current server statistics.
        print(heading(`Kitten`))
        print()
        const custom = {
          mode: (currentProcessIsInteractive ? 'interactive (development)' : 'daemon (run via systemd)')
        }
        if (!currentProcessIsInteractive) {
          let processPath = currentProcess.params.split('--working-directory=')[1]
          processPath = processPath.slice(0, processPath.indexOf(' '))
          processPath = '…' + processPath.slice(processPath.indexOf('/deployments'))
          custom.processPath = processPath
        }
        showProcessState(currentProcess, custom)
        print()
        print(heading2(`Events and listeners`))
        print()
        const events = Object.entries(globalThis.kitten.events._events)
        if (events.length === 0) {
          print(p(chalk.italic('There are currently no Kitten events being listened for.')))
        } else {
          const totalEvents = events.length
          const totalListeners = events.reduce((previous, current) => previous += current[1].length, 0)
          print(p(`${stat(totalEvents)} events and ${stat(totalListeners)} listeners.`))
          print()
          for (const [eventName, eventListeners] of events) {
            print(`• ${eventName}: ${stat(eventListeners.length)}`)
          }
        }
        print()
        print(heading(`Access statistics`))
        print()

        const activePages = Object.keys(globalThis.kitten.pages).length
        // const activeSessions = Object.keys(globalThis.kitten._db.sessions).length
        print(`  There are currently ${stat(activePages)} pages being viewed.`) //  and ${activeSessions} sessions
        print()

        const stats = globalThis.kitten._db.stats

        print(heading2('Top 10 viewed pages'))
        const sortedPages = Object.entries(stats.pages)
          .sort(([,a],[,b]) => b - a)
          .splice(0, 10)
          .reduce((output, page) => `${output}\n${chalk.blue(page[1])} ${page[0].replace('%F0%9F%92%95', '💕')}`, '')
        print(sortedPages)
        print()

        print(chalk.yellow.bold('  Missing pages (404)'))
        const missing = Object.entries(stats.missing)
        if (missing.length > 0) {
          const sortedMissing = missing
            .sort(([,a],[,b]) => b - a)
            .splice(0, 10)
            .reduce((output, page) => `${output}\n${chalk.yellow(page[1])} ${page[0].replace('%F0%9F%92%95', '💕')}`, '')
            print(sortedMissing)
            print()
        } else {
          print()
          print(chalk.italic('  Yay, there haven’t been any 404 errors yet!'))
          print()
        }

        print(chalk.yellow.red.bold('  Server errors (500)'))
        const serverErrors = Object.entries(stats.serverErrors)
        if (serverErrors.length > 0) {
          const sortedServerErrors = serverErrors
            .sort(([,a],[,b]) => b - a)
            .splice(0, 10)
            .reduce((output, page) => `${output}\n${chalk.red(page[1])} ${page[0].replace('%F0%9F%92%95', '💕')}`, '')
            print(sortedServerErrors)
            print()
        } else {
          print()
          print(chalk.italic('  Yay, there haven’t been any 500 errors yet!'))
          print()
        }

        print(heading2('Top 10 referrers'))
        const referrers = Object.entries(stats.referrers)
        if (referrers.length > 0) {
          const sortedReferrers = referrers
            .sort(([,a],[,b]) => b - a)
            .splice(0, 10)
            .reduce((output, page) => `${output}\n${chalk.blue(page[1])} ${page[0]}`, '')
            print(sortedReferrers)
            print()
        } else {
          print()
          print(chalk.italic('  There aren’t any referrers yet.'))
          print()
        }

        print(heading('Process details'))
        print()
        print(heading2('Current Kitten process'))
        print()
        showProcessDetails(currentProcess)
        print()

        // If there are other Kitten processes, also display information about them.

        const otherKittenProcesses = kittenProcesses.length > 1 || (currentProcessIsInteractive && kittenDaemonProcesses.length === 1)
        if (otherKittenProcesses) {
          print(heading('Other Kitten processes'))
          print()
          if (kittenProcesses.length > 0) {
            for (const [index, process] of kittenProcesses.entries()) {
              if (process.pid !== globalThis.process.pid) {
                showProcessState(process, {mode: 'interactive (development)'})
                showProcessDetails(process)
                print()
              }
            }
          }

          if (currentProcessIsInteractive && kittenDaemonProcesses.length === 1) {
            for (const [index, process] of kittenDaemonProcesses.entries()) {
              let processPath = process.params.split('--working-directory=')[1]
              processPath = processPath.slice(0, processPath.indexOf(' '))
              processPath = '…' + processPath.slice(processPath.indexOf('/deployments'))

              showProcessState(process, { processPath, mode: 'daemon (managed by systemd)' })
              showProcessDetails(process)
            }
            print()
          }
        }
      } catch (error) {
        print()
        print(p(errorMessage(`Could not get process data: ${error.message}`)))
        print()
      }

      try {
        const cpu = await systemInformation.cpu()
        const processorLabel = cpu.processors > 1 ? 'processors' : 'processor'
        print(heading(`Machine`))
        print()
        print(heading2(`CPU`))
        print()
        print(`• ${cpu.manufacturer} ${cpu.brand} ${stat(cpu.speed)}GHz (min ${stat(cpu.speedMin)}GHz, max ${stat(cpu.speedMax)}GHz)`)
        print(`• ${stat(cpu.processors)} ${processorLabel} (${stat(cpu.cores)} cores: ${stat(cpu.physicalCores)} physical, ${cpu.performanceCores} performance, ${stat(cpu.efficiencyCores)} efficiency)`)
        print(`• Virtualised? ${cpu.virtualization ? 'Yes' : 'No'}`)
      } catch (error) {
        print(p(errorMessage(`Could not get CPU information: ${error.message}`)))
      }

      print()

      try {
        const load = await systemInformation.currentLoad()
        print(heading2(`Load`))
        print()
        print(p(`${stat(load.currentLoad.toFixed(2))}% (current), ${stat(load.avgLoad.toFixed(2))}% (average)`))
      } catch (error) {
        print(p(errorMessage(`Could not get current load data: ${error.message}`)))
      }
      print()
      if (processes !== undefined) {
        print(heading2(`All Processes`))
        print()
        print(p(`${stat(processes.all)} (total), ${stat(processes.running)} (running), ${stat(processes.blocked)} (blocked), ${stat(processes.sleeping)} (sleeping)`))
        print()
      }
      this.displayPrompt()
    }
  })
  
  kittenRepl.defineCommand('settings', {
    help: 'Display the Kitten Settings page link',
    action () {
      this.clearBufferedCommand()
      print()
      print(`   ${domainWithPort}/🐱/settings/`)
      print()
      this.displayPrompt()
    }
  })

  /**
    Display sign-out link.
  */
  kittenRepl.defineCommand('sign-out', {
    help: 'Display the Kitten sign out page link',
    action () {
      this.clearBufferedCommand()
      print()
      print(`   ${domainWithPort}/💕/sign-out/`)
      print()
      this.displayPrompt()
    }
  })

  /**
    Display sign-in link.
  */
  kittenRepl.defineCommand('sign-in', {
    help: 'Display the Kitten sign-in page link',
    action () {
      this.clearBufferedCommand()
      print()
      print(`   ${domainWithPort}/💕/sign-in/`)
      print()
      this.displayPrompt()
    }
  })

  /**
    Display public ID information.
  */
  function showId() {
    print()
    print(chalk.magentaBright.bold('   Public keys:'))
    print()
    print(`   ${chalk.magentaBright.bold('ed25519:')}`)
    print(`   ${globalThis.kitten._db.settings.id.ed25519.asString}`)
    print()
    print(`   ${chalk.magentaBright.bold('ssh:')}`)
    print(`   ${globalThis.kitten._db.settings.id.ssh.asString}`)
    print()
  }

  kittenRepl.defineCommand('id', {
    help: 'Show your Small Web place ID (and create it and your secret if necessary).',
    async action(subcommand) {
      const idExists = globalThis.kitten._db.settings.id !== undefined

      // Display common header for all commands.
      this.clearBufferedCommand()
      print()
      print(chalk.bold('   🆔 Identity'))

      switch (subcommand) {
        case 'create':
          if (idExists) {
            console.warn('\n   💡 Your ID already exists. Not replacing.')
            showId()
          } else {
            // ID and corresponding secret do not exist. Create them.
            const keys = await createKeys(globalThis.kitten.domain)
            // TODO: There is redundancy between this and hello/[token]/index.page.js, refactor.
            /**
              @typedef {{
                private?: Object,
                public: {
                  ed25519: { asBytes?: Uint8Array },
                  ssh: { asBytes?: Uint8Array }
                }
              }} DeletableKeys 
            */
            const secret = keys.private.ed25519.asString

            // Persist the string versions of the ed25519
            // public key and other keys derived from it.
            delete(/** @type DeletableKeys */ (keys).private)
            delete(/** @type DeletableKeys */ (keys).public.ed25519.asBytes)
            delete(/** @type DeletableKeys */ (keys).public.ssh.asBytes)
            globalThis.kitten._db.settings.id = keys.public

            print()
            print(chalk.green(' » New ID created.'))
            print()
            print(chalk.red.bold('🔑 This is your secret:'))
            print(`   ╭${'─'.repeat(secret.length)}╮`)
            print(`   │${secret}│`)
            print(`   ╰${'─'.repeat(secret.length)}╯`)
            print(chalk.bold.italic.blue(`   Important: Please save your secret in your password manager.`))
            print()
            print(chalk.italic.blue(`   You need your secret to sign into your Small Web place and to communicate with your domain host.`))
            print(chalk.italic.blue(`   It’s the most important part of your Small Web identity.`))
            showId()
          }
        break

        default:
          // Show ID information if it exists.
          if (idExists) {
            showId()
          } else {
            print()
            print('   💡 Your ID doesn’t exist yet.')
            print(`      Use the ${chalk.bold('.id create')} command to create it.`)
            print()
          }
        break
      }
      this.displayPrompt()
    }
  })

  kittenRepl.defineCommand('ls', {
    help: 'List keys of global kitten object.',
    action() {
      this.clearBufferedCommand()
      showKittenKeys({ print })
      this.displayPrompt()
    }
  })
}
