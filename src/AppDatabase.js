/**
  Initialise the default database of the app we’re serving.
  This can either be via the database app module defined in
  the app, or, if one does not exist, the default untyped
  Kitten database.
*/

import fs from 'node:fs'
import path from 'node:path'
import process from 'node:process'
import JSDB from '@small-tech/jsdb'

import { Upload } from './routes/PostRoute.js'

export default class Database {
  /** @type string? */ path = null

  async initialise () {
    if (process.env.basePath === undefined) throw new Error('Kitten base path is undefined.')

    const appDataDirectory = globalThis.kitten.paths.APP_DATA_DIRECTORY
    const dbDirectory = path.join(appDataDirectory, 'db')

    const kitten = globalThis.kitten

    // Save the database path.
    // FIXME: This is already in a global (kitten.paths.APP_DATA_DIRECTORY) so refactor to use that in the db commands.
    this.path = appDataDirectory

    // See if the app has a database module, and import that first if it does…
    const databaseAppModulePath = path.join(process.env.basePath, 'app_modules', 'database')
    const databaseAppModuleFilePath = path.join(databaseAppModulePath, 'database.js')
    if (fs.existsSync(databaseAppModuleFilePath)) {
      let databaseAppModule
      try {
        // Load and initialise database app module.
        // The method gets passed an object with internal classes
        // that the app might want to specify when opening the database
        // (e.g., if it persists Upload instances in its own app database.)
        databaseAppModule = await import(databaseAppModuleFilePath)
        globalThis.kitten.db = await databaseAppModule.initialise({
          internalClasses: {
            Upload
          }
        })
      } catch (error) {
        // @ts-ignore throw with cause.
        throw new Error('😿 Found database module but could not import and initialise it.\n', {cause: error })
      }
    }

    // …otherwise (or if that module didn’t open the database), open the database.
    //
    // We provide the Upload class so Kitten apps can save copies of uploads in their
    // own app databases, instead of having to access the internal database. (This makes
    // for a natural way of working in routes by simply persisting the `request.uploads`
    // array in the app’s own database.) 
    //
    // Note: The reason we’re looking for an environment variable (jsdbCompactOnLoad)
    // instead of receiving that value as an argument is to give people the freedom
    // of initialising their database on module load in their database App Modules
    // (see above). We cannot be certain that Kitten Database App Module will initialise
    // the database in the initialise() method. It might do so when the module is loaded.
    //
    // e.g., See Domain:
    // https://codeberg.org/domain/app/src/branch/main/app_modules/database/database.js#L103
    //
    // So, instead of having two ways of doing it, we use the environment variable also.
    if (globalThis.kitten.db === undefined) {
      globalThis.kitten.db = JSDB.open(dbDirectory, {
        compactOnLoad: process.env.jsdbCompactOnLoad === 'false' ? false : true,
        classes: [
          Upload
        ]
      })
    }
  }
}
