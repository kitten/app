/**
  Kitten Command Line Interface (CLI) header.

  Copyright © 2022-present Aral Balkan, Small Technology Foundation.
  Licensed under GNU AGPL version 3.0.
*/

import fs from 'node:fs'
import path from 'node:path'

import link from '../../lib/terminal-link.js'
import chalk from 'chalk'

import paths from '../../lib/paths.js'
import Version from '../../lib/Version.js'

//////////////////////////////////////////////////////////////////////////////////////////
//
// These are the supports-sixel¹ and replied² modules Richie Bendall. The version here
// fixes a bug with the release version on npm where stdin is closed by replied, which
// causes any subsequent access attempts (e.g., via readline) to crash silently.
//
// See: https://github.com/Richienb/replied/issues/1
//
// ¹ https://github.com/Richienb/supports-sixel
// ² https://github.com/Richienb/replied
//
//////////////////////////////////////////////////////////////////////////////////////////

function _replied (message, { stdout, stdin }) {
  return new Promise(resolve => {
  	stdin.setEncoding("utf8")
  	stdin.setRawMode(true)

  	stdin.once("data", data => {
  		resolve(data)
      stdin.setEncoding(null)
  		stdin.setRawMode(false)
  	})

  	stdout.write(message)
  })
}

async function replied (message, { stdout, stdin } = process) {
	if (typeof message !== "string") {
		throw new TypeError(`Expected a string, got ${typeof message}`)
	}

	return _replied(message, { stdout, stdin })
}

async function supportsSixel () {
  // Only valid if this is a TTY.
  // See https://stackoverflow.com/a/53050098
  // And https://codeberg.org/kitten/app/issues/132#issuecomment-1103986
  if (!process.stdin.isTTY) {
    return false
  }
  const ESC = "\u001B["
	try {
		const attributes = await replied(ESC + "0c")
		return attributes.includes(";4")
	} catch (error) {
    console.warn('Got error while checking for sixel support:', error)
		return false
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

// Only show Kitten sixel in development mode and only if the terminal in use supports it.
// (The environment variable is set by the Kitten Process Manager used in development mode.)
const showKittenSixel = process.env.PRODUCTION ? false : await supportsSixel()
let kittenSixel
if (showKittenSixel) {
  kittenSixel = fs.readFileSync(path.join(paths.KITTEN_APP_DIRECTORY_PATH, 'kitten-day.sixel'), 'utf-8')
}

/**
  Display command-line interface header.
*/
export default function header () {
  if (showKittenSixel) {
    console.info(kittenSixel)
  }
  console.info(chalk.redBright.bold(`${showKittenSixel ? '  ' : '🐱'} Kitten`))
  console.info(`   by ${chalk.blue.italic(link('Aral Balkan', 'https://ar.al'))}, ${chalk.blue.italic(link('Small Technology Foundation', 'https://small-tech.org'))}`)

  Version.getInstance().printToConsole()  

  console.info(' ╭───────────────────────────────────────────╮')
  console.info(` │ ${chalk.bold('Like this? Fund us!')}                       │`)
  console.info(' │                                           │')
  console.info(' │ We’re a tiny, independent not-for-profit. │')
  console.info(` │ ${link('https://small-tech.org/fund-us', 'https://small-tech.org/fund-us')}            │`)
  console.info(' ╰───────────────────────────────────────────╯')
  console.info(
    chalk.bold.blue('   Need help?\n'),
    chalk.blue(`  ${link('Tutorials', 'https://kitten.small-web.org/tutorials/')}`),
    chalk.blue(`• ${link('Reference', 'https://kitten.small-web.org/reference/')}`),
    chalk.blue(`• ${link('Issues', 'https://codeberg.org/kitten/app/issues')}`)
  )
}
