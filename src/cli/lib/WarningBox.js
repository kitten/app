import actualStringLength from 'string-length'

export default class WarningBox {
  constructor () {
    this.lines = []
  }

  /** @param {string} line */ 
  line (line) {
    this.lines.push(line)
  }

  emptyLine() {
    this.lines.push('')
  }

  render() {
    // Create the box based on the length of the longest line.
    // With 1 space padding on each side of a passed line.
    const boxWidth = this.lines.reduce((longestLineLengthSoFar, currentLine) => Math.max(longestLineLengthSoFar, actualStringLength(currentLine)), /* initial longestLineLengthSoFar value is */ 0) + 2

    /**
      Create string with passed character repeated this many times.

      @param {number} thisMany; @param {string} character
    */
    const repeat = (thisMany, character) => Array(thisMany).fill(character).join('')

    /**
      Render a line within vertical box lines, adjusting spacing accordingly.

      @param {string} line
    */
    const renderLine = (line) => ` ║ ${line}${repeat(boxWidth - actualStringLength(line) - 1, ' ')}║\n`

    const horizontalLine = repeat(boxWidth, '═')
    const top = ` ╔${horizontalLine}╗\n`
    const body = this.lines.reduce((body, currentLine) => `${body}${renderLine(currentLine)}`, /* initial body is */ '')
    const bottom = ` ╚${horizontalLine}╝\n`

    return top + renderLine('') + body + renderLine('') + bottom
  }

  print() {
    const box = this.render()
    console.info(box)
  }
}
