/**
  Kitten command-line interface (CLI): start command.

  Copyright ⓒ 2022-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import childProcess from 'node:child_process'
import serviceStatus from '../../lib/service-status.js'
import { exitWithError } from '../../Utils.js'

export default function stop (_options) {
  const { isActive } = serviceStatus()
  
  if (isActive) {
    exitWithError('Kitten service is already active. Nothing to start.')
  }

  console.info('  • Starting Kitten service.')

  const options = {env: process.env, stdio: 'pipe'}
  try {
    childProcess.execSync('systemctl --user start kitten', options)
  } catch (error) {
    exitWithError(`Could not start Kitten service: ${error}.`)
  }
  
  console.info("\nDone.")
  process.exit()
}
