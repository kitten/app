import chalk from 'chalk'
import yesNo from 'yesno'
import KittenPackage from '../../lib/KittenPackage.js'

/**
  @typedef {{
    versionStamp: number,
    gitHash: string,
    apiVersion: number,
    nodeVersion: string,
    exactVersion: string,
    _buildDate:string,
    _releaseDate:string
  }} VersionInfo
*/

/**
  Prompt for confirmation.

  @param {string} prompt
*/
async function confirm (prompt) {
  console.warn('📦', chalk.bgRedBright.white.bold(prompt))
  console.warn()
  const confirmation = await yesNo({
    question: chalk.redBright(chalk.bold('   Are you sure?') + ' Please type “yes” (without quotes) to confirm.')
  })
  return confirmation
}

/**
  Update the Kitten package.

  @param {import('../index.js').ServerOptions} options
*/
export default async function (options) {
  /**
    @type {{ date: Date } & VersionInfo}
  */
  const apiVersion = options['api-version']
  const limitByApiVersion = apiVersion !== undefined

  const indent = '   '
  const errorMessagePrefix = chalk.red.bold(`\n${indent}Error:`)
  const consoleInfoIndented = (/** @type string */ message) => console.info(`${indent}${message || ''}`)

  /** @param {boolean} uppercaseL */
  const latestAvailablePackageText = (uppercaseL = false) => `${uppercaseL ? 'L' : 'l'}atest available package${limitByApiVersion ? ` for API version ${apiVersion}` : ''}`

  console.info(`📦 ${chalk.magenta.bold(`Updating Kitten to ${latestAvailablePackageText()}…`)}`)

  const kittenPackage = KittenPackage.getInstance()
  await kittenPackage.update()
  let latestRecommendedVersion = kittenPackage.latestRecommendedVersion

  if (latestRecommendedVersion === undefined) {
    console.error(`${errorMessagePrefix} Sorry, no packages are available that support API version ${apiVersion}.`)
    process.exit(1)
  }

  // Display version details for latest available package.
 
  console.info()
  consoleInfoIndented(`${latestAvailablePackageText(true)}:`)
  console.info()
  consoleInfoIndented(`${chalk.blue('Version   :')} ${latestRecommendedVersion.exactVersion}`)
  consoleInfoIndented(`${chalk.blue('Created on:')} ${latestRecommendedVersion.buildDate}`)

  if (kittenPackage.isLatestReleaseVersion) {
    console.info(chalk.green.bold(`\n${indent}You are already running the ${latestAvailablePackageText()}.`))
    process.exit()
  }

  let typeOfInstall = 'upgrade'
  if (kittenPackage.isMoreRecentThanReleaseVersion) {
    console.info(chalk.green.bold(`\n${indent}You are running a development version that’s\n${indent}more recent than the ${latestAvailablePackageText()}.\n`))
    const confirmDowngrade = await confirm('Are you sure you want to downgrade your Kitten version?')
    if (!confirmDowngrade) process.exit()
    typeOfInstall = 'downgrade'
  }

  // OK, update using the regular installer script.
  try {
    kittenPackage.upgrade()
  } catch (error) {
    console.error(`${errorMessagePrefix} ${typeOfInstall} failed. Details:`)
    console.error(error)
    process.exit(1)
  }

  console.info()
  consoleInfoIndented(chalk.green('Done.'))
  
  process.exit()
}
