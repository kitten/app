/**
  Commands for viewing, tailing, and deleting database and table information.

  Copyright © 2023-present Aral Balkan, Small Technology Foundation.
  Licensed under GNU AGPL version 3.0.
*/

import vm from 'node:vm'
import process from 'node:process'

import yesNo from 'yesno'
import chalk from 'chalk'

// FIXME: Add type definitions for this module.
import Tail from 'tail-file'

import { highlight } from 'cli-highlight'
import fs from 'node:fs'

import { getDomainsAndPort, setBasePath } from '../../Utils.js'
import Globals from '../../lib/globals.js'
import InternalDatabase from '../../InternalDatabase.js'
import AppDatabase from '../../AppDatabase.js'
import path from 'node:path'

/**
  @typedef {{
    internalDatabase: InternalDatabase,
    appDatabase: AppDatabase,
    domain: string,
    port: number
  }} Databases
*/

/**
  Initialise databases (internal and app).

  @param {import('../index.js').ServerOptions} options
  @returns {Promise<Databases>}
*/
export async function initialiseDatabases (options) {
// Signal to JSDB that it should not compact databases on load.
  process.env.jsdbCompactOnLoad = 'false'

  const pathToServe = '.'
  const workingDirectory = options['working-directory']

  try {
    setBasePath(workingDirectory, pathToServe)
  } catch (error) {
    console.error(`❌ Error: cannot get database information ${error.message}`)
    process.exit(1)
  }

  const {domains, port} = getDomainsAndPort(options)
  const domain = domains[0]
  Globals.initialise(domain, port)

  const internalDatabase = new InternalDatabase()
  await internalDatabase.initialise()

  const appDatabase = new AppDatabase()
  await appDatabase.initialise()

  return { internalDatabase, appDatabase, domain, port }
}

/**
  Display database information including folder and list of tables.

  @param {Databases} databases
  @param {boolean} internal
*/
function showDatabaseInformation (databases, internal) {
  const db = internal ? globalThis.kitten._db : globalThis.kitten.db
  const databasePath = internal ? databases.internalDatabase.path : databases.appDatabase.path

  console.info(chalk.magentaBright.bold(`💾 ${internal ? 'Internal' : 'App'} database details`))
  console.info()
  console.info(chalk.blue.bold('   App    :'), chalk.italic(_appFolder()))
  console.info(chalk.blue.bold('   Runs on:'), chalk.italic(`${databases.domain}:${databases.port}`))
  console.info()
  console.info(chalk.blue.bold('   Database folder:'))
  console.info()
  console.info('   ' + chalk.italic(databasePath + (internal ? '/_db' : '/db')))
  console.info()
  console.info(chalk.blue.bold('   Tables:'))
  console.info()
  Object.keys(db).forEach(tableName => console.info(`    • ${tableName}`))
  console.info()
  console.info(chalk.green('For more information on JSDB, please see https://codeberg.org/small-tech/jsdb'))

  process.exit()
}

/**
  Return reference to table with passed name or exit with error
  if the table does not exist.

  @param {string} tableName
  @param {boolean} internal
  @returns {object|array} table
*/
function tableWithName(tableName, internal) {
  const db = internal ? globalThis.kitten._db : globalThis.kitten.db
  const table = db[tableName]

  if (table === undefined) {
    console.error(chalk.redBright.bold(`💾 Table “${tableName}” does not exist.`))
    process.exit()
  } else {
    return table
  }
}

/**
  Display table information including name, type, and path on disk.

  @param {Databases} databases
  @param {string} tableName
  @param {boolean} internal
*/
function showTableInformation (databases, tableName, internal) {
  const table = tableWithName(tableName, internal)

  console.info(chalk.magentaBright.bold(`💾 Table details for ${tableName}`))
  console.info()
  console.info(chalk.blue.bold('   Name  :'), tableName)
  console.info(chalk.blue.bold('   Type  :'), typeof table)
  console.info()
  console.info(chalk.blue.bold('   File:'))
  console.info()
  console.info('  ', chalk.italic(table.__table__.tablePath))
  console.info()
  console.info(chalk.blue.bold('   Database :'), chalk.italic(internal ? 'Internal (_db)' : 'App (db)'))
  console.info(chalk.blue.bold('   App      :'), chalk.italic(_appFolder()))
  console.info(chalk.blue.bold('   Runs on  :'), chalk.italic(`${databases.domain}:${databases.port}`))
  console.info()

  process.exit()
}

/**
  Prompt for confirmation before deleting something.

  @param {string} prompt
*/
async function confirmDelete (prompt) {
  console.warn('💾', chalk.bgRedBright.white.bold(prompt))
  console.warn()
  const confirmation = await yesNo({
    question: chalk.redBright(chalk.bold('   Are you sure?') + ' Please type “yes” (without quotes) to confirm.')
  })
  return confirmation
}


function displayActionTaken (/** @type {string} */ actionTaken) {
  console.info()
  console.info(indented(actionTaken))
}

function displayCancellationNotice () {
  displayActionTaken('Cancelled.')
}

function indented (/** @type {string} */ string) { return `   ${string}` }

/**
  Delete table with the passed name after requesting confirmation.

  @param {string} tableName
  @param {boolean} internal
  @param {string} databaseAndAppIdentifier
*/
async function deleteTable(tableName, internal, databaseAndAppIdentifier) {
  // Confirm table delete.
  const table = tableWithName(tableName, internal)
  const confirmation = await confirmDelete(`About to delete table ${tableName} from ${databaseAndAppIdentifier}…`)

  if (confirmation === true) {
    // Delete table.
    await table.__table__.delete()
    displayActionTaken(`Table ${tableName} deleted.`)
  } else {
    // Cancel.
    displayCancellationNotice()
  }
  
  process.exit()
}

function _appFolder () {
  return process.cwd().split(path.sep).pop()
}

function _databaseAndAppIdentifier (internal, domain, port) {
  return `${internal ? 'internal database (_db)' : 'app database (db)'} for ${_appFolder()} app on ${domain}:${port}…`
}

/**
  Delete entire database for current Kitten place after requesting confirmation.

  @param {boolean} internal
  @param {string} databaseAndAppIdentifier
*/
async function deleteDatabase(internal, databaseAndAppIdentifier) {
  // Confirm database delete.
  const confirmation = await confirmDelete(`About to delete whole ${databaseAndAppIdentifier}…`)

  if (confirmation === true) {
    // Delete database.
    console.info(chalk.red.bold('   Deleting tables:'))
    console.info()
    const db = internal ? globalThis.kitten._db : globalThis.kitten.db
    for (const [tableName, table] of Object.entries(db)) {
      console.info(indented(` • ${tableName}`))
      await table.__table__.delete()
    }
    displayActionTaken(`Database deleted.`)
  } else {
    // Cancel.
    displayCancellationNotice()
  }

  process.exit()
}

/**
  Display file location as well as head and tail of requested
  database table and follow the tail.

  @param {string} tableName
  @param {import('../index.js').AllOption} options
  @param {boolean} internal
  @param {string} databaseAndAppIdentifier
*/
function tailTable(tableName, options, internal, databaseAndAppIdentifier) {
  const table = tableWithName(tableName, internal)
  const tablePath = table.__table__.tablePath

  console.info(chalk.magentaBright.bold(`💾 Listing ${chalk.underline(tableName)} table head with live tail\n   from ${databaseAndAppIdentifier}\n\n   (Press Ctrl+c to break.)`))
  console.info()
  console.info(chalk.blue.bold('   File:'))
  console.info()
  console.info('   ' + chalk.italic(tablePath))
  console.info()
  console.info(chalk.blue.bold('   Head and live tail:'))
  console.info()

  const tableLines = fs.readFileSync(tablePath, 'utf-8').split('\n')

  // Show a snippet of the head to create context
  // (given the initialisation statement is the first line).
  const numInitialLines = options.all ? tableLines.length : Math.min(tableLines.length, 3)
  for (let i = 0; i < numInitialLines; i++) {
    if (i === 0) {
      // The first line is special in that it creates the root object of the
      // JSDB table. This can get rather long and, when presented without indentation,
      // can be rather difficult to read. So here, we execute it in a virtual machine
      // and run the JSON stringifier on the resulting data structure to indent it.
      // (There are modules that carry out “beautification” but they’re usually
      // large/cumbersome/imprecise.)
      let vmScript
      try {
        vmScript = new vm.Script(tableLines[0].replace('export const ', ''))
        const vmContext = vm.createContext({})
        vmScript.runInContext(vmContext)
        const rootObject = vmContext._
        const indentedRepresentation = JSON.stringify(rootObject, null, 2)
        tableLines[0] = `export const _ = ${indentedRepresentation}`
      } catch (error) {
        console.debug('Could not format first line; likely because it has newlines in it.', error)
      }
    }
    console.info(highlight('   ' + tableLines[i], { language: 'js' }))
  }

  // Also display a preview of the tail if there are enough lines.
  if (!options.all && tableLines.length > 3) {
    console.info(chalk.green('   …'))
    let tailPreviewStartIndex = tableLines.length - 3
    if (tailPreviewStartIndex < 4) { tailPreviewStartIndex += 3 } 
    for (let i = tailPreviewStartIndex; i < tableLines.length; i++) {
      process.stdout.write(highlight('   ' + tableLines[i], { language: 'js' }))
      // Don’t write a newline after the last line of the tail preview
      // so there isn’t an empty line between tail preview and live tail output.
      if (i+1 !== tableLines.length) {
        process.stdout.write('\n')
      }
    }
  }

  // Start the tail (follow).
  const tail = new Tail(tablePath, {
    startPos: 'end'
  }, (/** @type {string} */ line) => {
     console.info(highlight('   ' + line, { language: 'js' }))
  })
  tail // TODO: Should we listen for error events, etc., to handle them gracefully?
}

//
// Commands.
//

/**
  Show database information for default database.

  @param {string?} tableName
  @param {import('../index.js').ServerOptions} options
*/
export async function showDefaultDatabaseInformationCommand (tableName, options) {
  await showDatabaseInformationCommand(tableName, options, false)
}

/**
  Show database information for internal database.

  @param {string?} tableName
  @param {import('../index.js').ServerOptions} options
*/
export async function showInternalDatabaseInformationCommand (tableName, options) {
  await showDatabaseInformationCommand(tableName, options, true)
}

/**
  Show information about whole database or specified table.

  @param {string?} tableName
  @param {import('../index.js').ServerOptions} options
  @param {boolean} internal
*/
async function showDatabaseInformationCommand (tableName, options, internal) {
  const databases = await initialiseDatabases(options)
  if (databases.internalDatabase.path === null || databases.appDatabase.path === null)
    throw new Error('Unexpected error: Database path is null.')
  const databaseAndAppIdentifier = _databaseAndAppIdentifier(internal, databases.domain, databases.port)

  if (tableName == undefined) {
    // Database command.
    showDatabaseInformation(databases, internal)
  } else {
    // Table command.
    showTableInformation(databases, tableName, internal)
  } 
}

/**
  Delete whole database or specified table in default database.

  @param {string?} tableName
  @param {import('../index.js').ServerOptions} options
*/
export async function deleteDefaultDatabaseCommand (tableName, options) {
  await deleteDatabaseCommand (tableName, options, false)
}

/**
  Delete whole database or specified table in internal database.

  @param {string?} tableName
  @param {import('../index.js').ServerOptions} options
*/
export async function deleteInternalDatabaseCommand (tableName, options) {
  await deleteDatabaseCommand (tableName, options, true)
}

/**
  Delete whole database or specified table.

  @param {string?} tableName
  @param {import('../index.js').ServerOptions} options
  @param {boolean} internal
*/
async function deleteDatabaseCommand (tableName, options, internal) {
  const { domain, port } = await initialiseDatabases(options)

  const databaseAndAppIdentifier = _databaseAndAppIdentifier(internal, domain, port)

  if (tableName == undefined) {
    // Database command.
    await deleteDatabase(internal, databaseAndAppIdentifier)
  } else {
    // Table command.
    deleteTable(tableName, internal, databaseAndAppIdentifier)
  } 
}

/**
  Show database information for default database.

  @param {string} tableName
  @param {import('../index.js').TailOptions} options
*/
export async function tailDefaultDatabaseTableCommand (tableName, options) {
  await tailDatabaseTableCommand (tableName, options, false)
}

/**
  Show database information for internal database.

  @param {string} tableName
  @param {import('../index.js').TailOptions} options
*/
export async function tailInternalDatabaseTableCommand(tableName, options) {
  await tailDatabaseTableCommand (tableName, options, true)
}

/**
  Show head and tail summary of specified table and follow additions to it.

  @param {string} tableName
  @param {import('../index.js').TailOptions} options
  @param {boolean} internal
*/
export async function tailDatabaseTableCommand (tableName, options, internal) {
  const { domain, port } = await initialiseDatabases(options)
  const databaseAndAppIdentifier = _databaseAndAppIdentifier(internal, domain, port)
  tailTable(tableName, options, internal, databaseAndAppIdentifier)
}
