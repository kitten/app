/**
  Kitten command-line interface (CLI): serve command.

  Copyright ⓒ 2021-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import { setBasePath } from '../../Utils.js'
import Server from '../../Server.js'

/**
  Start Kitten server instance.

  @param {import('../index.js').ServerOptions} options
*/
export default async function serve (pathToServe = '.', options) {
  const workingDirectory = options['working-directory']

  try {
    setBasePath(workingDirectory, pathToServe)
  } catch (error) {
    console.error(`❌ Error: cannot serve ${error.message}`)
    process.exit(1)
  }
  
  const server = await Server.getInstanceAsync(options)
}
