/**
  Kitten command-line interface (CLI): stop command.

  Copyright ⓒ 2022-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import childProcess from 'node:child_process'

import { exitWithError } from '../../Utils.js'
import serviceStatus from '../../lib/service-status.js'

export default function stop (/** @type object */ _options) {
  const { isActive } = serviceStatus()
  
  if (!isActive) {
    exitWithError('Kitten service is not active. Nothing to stop.')
  }

  console.info('  • Stopping Kitten service.')

  const options = /** @type import('node:child_process').ExecSyncOptionsWithStringEncoding */
    ({env: process.env, stdio: 'pipe'})
  try {
    childProcess.execSync('systemctl --user stop kitten', options)
  } catch (error) {
    exitWithError(`Could not stop the Kitten service: ${error}.`)
  }
  
  console.info("\nDone.")
  process.exit()
}
