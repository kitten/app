/**
  Kitten command-line interface (CLI): status command.

  Display Kitten systemd service status.

  Copyright ⓒ 2022-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import childProcess from 'node:child_process'
import chalk from 'chalk'

/**
  Display error in console and reject promise.

  @param {string} message
  @param {function} reject
*/
export function rejectWithError (message, reject) {
  console.error(`\n❌ ${chalk.redBright('Error:')} ${message}\n`)
  reject(message)
}

export default async function status (/** @type object */ _options) {
  return new Promise((resolve, reject) => {
    console.info(chalk.bold(' 🚦Status:'))
    console.info(chalk.italic.blue('  (systemctl --user --no-pager --full --lines=0 status kitten)\n'))

    const statusProcess = childProcess.spawn(
      'systemctl', 
      ['--user', '--no-pager', '--full', '--lines=0', 'status', 'kitten'],
      {env: process.env, stdio: 'inherit'}
    )
    statusProcess.on('error', error => {
      rejectWithError(`Could not get Kitten service status (systemctl): ${error}.`, reject)
    })
    statusProcess.on('exit', () => {
      console.info(chalk.bold('\n📜 Logs:'))
      console.info(chalk.italic.blue('  (journalctl --user --follow --unit=kitten; tailing – press Ctrl+c to exit.)\n'))
      const logProcess = childProcess.spawn(
        'journalctl', 
        ['--user', '--follow', '--unit=kitten'],
        {env: process.env, stdio: 'inherit'}
      )
      logProcess.on('error', error => {
        rejectWithError(`Could not get Kitten logs (journalctl) ${error}.`, reject)
      })
      logProcess.on('exit', () => resolve(true))
    })
  })
}
