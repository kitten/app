/**
  Kitten command-line interface (CLI): deploy command.

  Deploys an app from a Git repository as a
  systemd --user service.

  Copyright ⓒ 2021-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import _deploy from '../../lib/deploy.js'

/**
  @param {string} gitHttpsCloneUrl - git clone url (HTTPS)
  @param {import('../index.js').DeploymentOptions} options
*/
export default async function deploy (gitHttpsCloneUrl, options) {
  let exitCode = 0
  try {
    await _deploy(gitHttpsCloneUrl, options)
  } catch (error) {
    console.error(error.message)
    exitCode = 1
  }
  process.exit(exitCode)
}
