/**
  Kitten command-line interface (CLI): logs command.

  Follow Kitten systemd service logs.

  Copyright ⓒ 2022-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import childProcess from 'node:child_process'
import { exitWithError } from '../../Utils.js'

/**
  Show systemd server logs for Kitten process.

  @param {import('../index.js').SinceOption} options
*/
export default function logs (options) {
  console.info(`📜 Following logs (press Ctrl+C to exit).\n`)
  try {
    childProcess.spawn(
      'journalctl', 
      ['--since', options['since'], '--no-pager', '--follow', '--user', '--unit', 'kitten'],
      {env: process.env, stdio: 'inherit'}
    )
  } catch (error) {
    exitWithError(`Could not show Kitten service logs: ${error}.`)
  }
}
