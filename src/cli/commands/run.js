/**
  Kitten command-line interface (CLI): run command.

  Clones an app from a Git repository and runs it locally.

  Copyright ⓒ 2024-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import fs from 'node:fs'
import path from 'node:path'

import chalk from 'chalk'
import simpleGit from 'simple-git'

import paths from '../../lib/paths.js'
import serve from './serve.js'

const git = simpleGit()

/**
  @param {string} gitHttpsCloneUrl - git clone url (HTTPS)
  @param {import('../index.js').ServerOptions} options
*/
export default async function run (gitHttpsCloneUrl, options) {
  console.info(chalk.bold(` 🏃‍♀️Running ${gitHttpsCloneUrl}`))

  // TODO: Remove redundancy, code is based on the deploy command.
  if (!gitHttpsCloneUrl.startsWith('https://') || !gitHttpsCloneUrl.endsWith('.git')) {
    if (gitHttpsCloneUrl.startsWith('git@')) {
      const message = `Please provide an HTTPS protocol git clone URL, not SSH.`
      console.error(`${chalk.bold.red('Error:')} ${message}`)
      console.error(chalk.blue('(It should start with https://, not git@)'))
    } else {
      const message = `Invalid git clone URL ${chalk.blue('(must begin with https:// and end with .git).')}`
      console.error(`${chalk.bold.red('Error:')} ${message}`)
    }
    console.error(chalk.italic('\nTo avoid errors, copy the HTTPS URL from your repository host’s web interface to ensure it’s correct.'))
    process.exit(1)
  }

  // Generate working copy path based on clone URL.
  const cloneDirectoryName = gitHttpsCloneUrl.replace('https://', '').replace('.git', '').replace(/\//g, '.')
  const clonePath = path.join(paths.KITTEN_CLONES_DIRECTORY, cloneDirectoryName)
  
  // Ensure main clones directory exists.
  fs.mkdirSync(paths.KITTEN_CLONES_DIRECTORY, { recursive: true })
  
  // Make sure we start with a fresh directory for the clone in case this app was cloned previously.
  fs.rmSync(clonePath, {recursive: true, force: true})

  await git.clone(gitHttpsCloneUrl, clonePath)
  await serve(clonePath, options)
}
