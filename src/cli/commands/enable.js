/**
  Kitten command-line interface (CLI): enable command.

  Copyright ⓒ 2022-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import childProcess from 'node:child_process'
import serviceStatus from '../../lib/service-status.js'
import { exitWithError } from '../../Utils.js'

export default function enable (_options) {
  const { isEnabled } = serviceStatus()
  
  if (isEnabled) {
    exitWithError('Kitten service is already enabled. Nothing to enable.')
  }

  console.info('  • Enabling Kitten service.')

  const options = /** @type import('node:child_process').ExecSyncOptionsWithStringEncoding */ ({env: process.env, stdio: 'pipe'})
  try {
    childProcess.execSync('systemctl --user enable kitten', options)
  } catch (error) {
    exitWithError(`Could not enable Kitten service: ${error}.`)
  }
  
  console.info("\nDone.")
  process.exit()
}
