import net from 'node:net'
import { Transform } from 'node:stream'

export default function shell () {
  const SPACE = 32 // decimal

  const EMPTY_BUFFER = Buffer.alloc(0)
  const PREVIEW_COMPLETION_PREFIX = Buffer.from([0x2f, 0x2f, 0x20])

  // \x1b[38;5;104m - a dim greyish-blue
  // See https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797#256-colors
  const ANSI_SET_DIM_BLUE_TEXT = Buffer.from([0x1b, 0x5b, 0x33, 0x38, 0x3b, 0x35, 0x3b, 0x36, 0x30, 0x6d])
  const ANSI_CLEAR_FORMATTING = Buffer.from([0x1b, 0x5b, 0x30, 0x6d])

  const renderPreviewCompletion = new Transform({
    transform (chunk, encoding, callback) {
      // Node’s REPL sends two types of completion preview chunks, starting with the following prefixes:
      // 0x20 0x2f 0x2f 0x20: completion for token currently being typed
      // 0x0a 0x2f 0x2f 0x20: dump of the object being completed to
      const chunkPrefix = chunk.subarray(1,4)
      if (chunkPrefix.equals(PREVIEW_COMPLETION_PREFIX)) {
        // Remove the slashes from the preview and dim it for display.
        // Also, if this is the token completion (starts with a space), do not
        // add the initial space.
        const spacing = chunk.at(0) === SPACE ? EMPTY_BUFFER : chunk.subarray(0,1)
        const preview = Buffer.concat([ANSI_SET_DIM_BLUE_TEXT, spacing, chunk.subarray(4), ANSI_CLEAR_FORMATTING])
        callback(null, preview)
      } else {
        // Passthrough.
        callback(null, chunk)
      }
    }
  })

  // Create socket connection.
  const socket = net.connect(1337)

  /**
    Lets remote Node REPL know how wide and tall our screen
    is so that it can format the preview completions properly.
    (The former is the one that’s actually useful but the latter
    can’t hurt.)
  */
  // @ts-ignore process.stdout.columns does exist.
  const sendScreenSize = () => {
    // In our basic, currently single-command protocol,
    // SYN (0x16; decimal 22) signals a synchronisation message
    // sent to communicate the size of the remote terminal client (us).
    // The next two bytes are the columns and rows values, respectively.
    const SYN = 0x16
    const resizeCommand = Buffer.from(new Uint8Array([SYN, process.stdout.columns, process.stdout.rows]))
    socket.write(resizeCommand)
  }

  // Socket event handlers.
  
  socket.on('error', error => {
    // @ts-ignore code property does exist on error object.
    if (error.code === 'ECONNREFUSED') {
      console.error('❌ Shell not running on port 1337. Are you sure Kitten daemon is active?')
    } else {
      console.error('❌', error)
    }
    process.exit(1)
  })

  socket.on('connect', () => {
    if (process.stdin.setRawMode) {
      process.stdin.setRawMode(true)
    }
    sendScreenSize()
    process.stdout.on('resize', sendScreenSize)
  })

  socket.on('close', () => {
    if (process.stdin.setRawMode)
      process.stdin.setRawMode(false)
    process.stdin.emit('end')
    console.info('')
    process.exit()
  })

  // Plumbing :)
  
  process.stdin
    .pipe(socket)
    .pipe(renderPreviewCompletion)
    .pipe(process.stdout)
}
