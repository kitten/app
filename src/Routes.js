/**
  Kitten Routes

  Copyright ⓒ 2021-present, Aral Balkan
  Small Technology Foundation
  License: AGPL version 3.0.
*/

import pathCompleteExtname from 'path-complete-extname'

import Files, { FileEventDetails } from './Files.js'
import { routePatternFromFilePath } from './Utils.js'
import chalk from 'chalk'

import systemExitCodes from './lib/system-exit-codes.js'

import HttpRoute from './routes/HttpRoute.js'
import PostRoute from './routes/PostRoute.js'
import PageRoute from './routes/PageRoute.js'
import SocketRoute from './routes/WebSocketRoute.js'
import PageSocketRoute from './routes/PageSocketRoute.js'
import StaticRoute from './routes/StaticRoute.js'

export default class Routes {
  routes
  static isBeingInstantiatedByFactoryMethod = false

  /**
    Returns the routes for the passed base path.

    (The default base path is the primary place being served, as kept
    in the `basePath` environment variable.)
  */
  static async getRoutes (basePath = /** @type string */ (process.env.basePath)) {
    this.isBeingInstantiatedByFactoryMethod = true
    const instance = new this(basePath)
    await instance.initialise()
    this.isBeingInstantiatedByFactoryMethod = false
    return instance.routes
  }

  /**
    Private constructor.

    @param {string} basePath
  */
  constructor (basePath) {
    if (!Routes.isBeingInstantiatedByFactoryMethod) {
      throw new Error ('Error: constructor called directly. Please use static async getRoutes() method to get the routes at a given file path.')
    }
    this.basePath = basePath
    this.routes = {}
  }

  /**
    Creates and returns the routes at the base path.
  */
  async initialise () {
    if (!Routes.isBeingInstantiatedByFactoryMethod) {
      throw new Error ('Error: async getRoutes() method called directly. Please use static async construct() method to to get the routes at a given file path.')
    }
    const routeCreationTime = `⌛ ${chalk.bold(`Routes (${this.basePath === process.env.basePath ? 'your app’s' : 'Kitten’s internal web app’s'})`)}`
    console.time(routeCreationTime)
    // Get the files in the site being served.
    this.files = await Files.new(this.basePath)

    // Start listening for file events after initialisation for
    // performance reasons (since we ignore events prior to initialisation anyway).
    // (Only in development mode. In production, the server will only exit if
    // there is an unhandled exception or if we specifically ask it to after
    // upgrading Kitten, the deployed app, or, in cases of major version changes, both.
    if (!process.env.PRODUCTION) {
      this.files.addEventListener('file', /** @type EventListener */ (this.handleFileChange))
    }

    // Create the routes from the files structure.
    this.createRoutes()
    console.timeEnd(routeCreationTime)

    return this.routes
  }

  /**
    Handle file change events.
  
    @param {CustomEvent<FileEventDetails>} event
  */
  handleFileChange(event) {
    if (process.env.basePath == undefined) {
      throw new Error(`Basepath is undefined during event ${event}`)
    }
    const { itemType, eventType, itemPath } = event.detail
    // When anything changes in the project, we simply exit. Restarting
    // the Kitten service is handled by the Kitten Process Manager during
    // development and by systemd in production.
    console.info(`🔔 ${itemType.charAt(0).toUpperCase()+itemType.slice(1)} ${eventType} (${itemPath.replace(process.env.basePath, '')}), exiting to trigger reload.`)
    process.exit(systemExitCodes.restartRequest)
  }

   /**
    Creates routes of a certain category type.

    @param {string} categoryType
   */ 
  createRoutesOfCategoryType (categoryType) {
    const category = this.files.byExtensionCategoryType[`${categoryType}Routes`]
    const extensions = Object.keys(category)

    for (const extension of extensions) {
      const filePaths = category[extension]
      for (const filePath of filePaths) {
        const extension = pathCompleteExtname(filePath).replace('.', '')

        const RouteType = categoryType === 'static'
          ? StaticRoute
          : {
              'page.js': PageRoute,
              'page.md': PageRoute,
              'post.js': PostRoute,
              'socket.js': SocketRoute
            }[extension] || HttpRoute

        if (this.routes[extension] === undefined) {
          this.routes[extension] = []
        }
        const pattern = routePatternFromFilePath(filePath, this.basePath)
        this.routes[extension][pattern] = new RouteType(filePath, this.basePath)

        // Handle PageSocketRoutes, which are an exception to the one route per file
        // expectation. These routes are kept in the same file as page routes and
        // exported via a named onConnect function. Page routes do not need a corresponding
        // PageSocketRoute but we create one for each page anyway given they are lazily
        // evaluated. Otherwise, we would have to read in every page route and that would
        // undo any initial startup performance gains we get from lazily loading routes.
        if (extension === 'page.js') {
          const pageSocketRoutePattern = `${pattern}${pattern.endsWith('/') ? '' : '-'}default.socket`
          if (this.routes['socket.js'] === undefined) {
            this.routes['socket.js'] = []
          }
          this.routes['socket.js'][pageSocketRoutePattern] = new PageSocketRoute(filePath, this.basePath)
        }
      }
    }
  }
  
  // Create the routes and add them to the server.
  // The ESM Loaders will automatically handle any processing that needs to
  // happen during the import process.
  createRoutes () {
    // Add static routes.
    this.createRoutesOfCategoryType('static')
    
    // Add dynamic routes.
    this.createRoutesOfCategoryType('dynamic')

    // Log the routes if in verbose mode.
    console.debug('Routes', this.routes, '\n')
  }
}
