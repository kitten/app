/**
  App class

  Represents the git repository of the app that Kitten is currently serving.

  (Only relevant in production.)
*/

import { simpleGit } from 'simple-git'
import SvgSpinner from './components/SvgSpinner.component.js'
import Version from './lib/Version.js'

/**
  @typedef {import('simple-git').SimpleGit} SimpleGit
  @typedef {import('simple-git').SimpleGitOptions} SimpleGitOptions
*/

export default class AppRepository {
  /** @type {string} */ path
  /** @type {SimpleGit} */ git

  /** @type {Array<string> | undefined} */ remoteTags
  /** @type {Date | undefined} */ lastRemoteTagUpdate

  /** @type {AppRepository | undefined } */ static instance
  static isBeingInitialisedAsSingleton = false


  /** Matches Kitten version tag strings in the form '<number>.<number>' */
  static  kittenVersionRegExp = /^\d+?\.\d+?$/

  /**
    Singleton accessor.

    Use this method to get an initialised/updated instance.

    You can always call the update() method again later to get the latest data
    from the remote repository. 
  */
  static async getInstance () {
    if (this.instance === undefined) {
      this.isBeingInitialisedAsSingleton = true
      this.instance = new AppRepository() 
      this.isBeingInitialisedAsSingleton = false

      await this.instance.update()
    }
    return this.instance
  }

  /**
    Create an instance (should only be called in production).
  */
  constructor () {
    // Guard: do not allow multiple instances.
    if (!AppRepository.isBeingInitialisedAsSingleton) {
      throw new Error('AppRepository is a singleton. Please use the static async getInstance() method to get yourself a copy.')
    }
    
    if (process.env.basePath === undefined) {
      throw new Error('Cannot initialise: basePath is undefined')
    }

    this.path = process.env.basePath
    this.git = simpleGit(this.path)
    this.kittenVersion = Version.getInstance()
  }

  /**
    Updates the app to the latest compatible version.
  */
  async upgradeAppToLatestCompatibleVersion () {
    if (this.latestCompatibleVersion !== undefined) {
      await this.git.checkout(this.latestCompatibleVersion)
    }
  }

  /**
    Updates the app to the latest available commit.
  */
  async upgradeAppToLatestAvailableCommit () {
    if (this.latestAvailableCommit !== undefined) {
      await this.git.checkout(this.latestAvailableCommit)
    }
  }

  /**
    Updates the app to the specified version tag.

    @param {string} versionTag
  */
  async updateAppToVersion (versionTag) {
    await this.git.checkout(versionTag)
  }
  
  /**
    Updates the app repository with the latest information and content.

    After this method is run, you should have all tags and their content available locally so
    you can simply use the checkout() method to update the application to the required version.
  */
  async update () {
    const commonErrorSuffix = 'App version information and updates will not be available.'
    const logError = message => console.error(`${message} ${commonErrorSuffix}`)

    try {
      this.originRemoteUrl = String(await this.git.remote(['get-url', 'origin'])).replace('\n', '')
      this.hasOrigin = true
    } catch (error) {
      this.hasOrigin = false
      logError('The deployed app does not have an origin git remote.')
      return
    }

    try {
      this.currentVersion = String(await this.git.raw(['describe', '--always'])).replace('\n', '')
    } catch (error) {
      logError('Git describe failed: this should never happen since we’re using the --always option.')
      return
    }

    // Run git fetch so we get the latest information from the remote.
    // We do this regardless of whether Kitten versioning is used as we
    // also support manual updates to the latest git commit (without,
    // of course, any guarantees of Kitten API version compatibility
    // in that case).
    try {
      await this.git.fetch(['--tags'])
    } catch (error) {
      // Could not carry out a remote fetch. Fail.
      logError(`Could not fetch from deployed app’s git remote. ${error}`)
      return
    }

    try {
      // Note that we only support “main” as the, well, main branch.
      this.latestAvailableCommit = String(await this.git.show(['origin/main', '--format=%h', '--no-patch'])).replace('\n', '')
    } catch (error) {
      // Could not carry out git log. Fail.
      logError(`Could not run git show. ${error}`)
      return
    }

    // At this point, current version is either a Kitten tag in the form
    // \d\.\d, or some other tag, or the commit hash of the latest commit.
    // Unless it’s the first one, we will be handling neither versioning
    // during updates nor automatic updates – both of which rely on the
    // usage of Kitten version tags.
    this.usesKittenVersioning = AppRepository.kittenVersionRegExp.exec(this.currentVersion) !== null

    // Flag whether a manual update is available.
    this.manualUpdateAvailable = !this.usesKittenVersioning && (this.currentVersion !== this.latestAvailableCommit)

    if (this.usesKittenVersioning) {
      const currentVersionParts = this.currentVersion.split('.')
      this.currentApiVersion = Number(currentVersionParts[0])
      this.currentAppVersion = Number(currentVersionParts[1])

      const tagsString = await this.git.tag()

      this.tags = tagsString
        .split('\n')
        .slice(0, -1)

      /** @type [number, number][] */
      this.tagsByApiAndAppVersion = this.tags
        .map(x => x.split('.'))
        // @ts-ignore in-place type change (on purpose, converting from string to number in array)
        .map(x => { x[0] = Number(x[0]); x[1] = Number(x[1]); return /** @type [number, number] */ (x) })

      /** @type {Array<number>} */ 
      this.latestVersionByApiVersion = []

      /** @type {Array<string>} */ 
      this.compatibleTags = []

      /** @type {Array<string>} */ 
      this.incompatibleTags = []

      this.tagsByApiAndAppVersion .forEach(apiAndAppVersionTuple => {
        // Keep the static analyser happy.
        if (this.latestVersionByApiVersion === undefined) throw new Error ('this.latestVersionByApiVersion is undefined.')

        const apiVersion = apiAndAppVersionTuple[0]
        const appVersion = apiAndAppVersionTuple[1]
        const versionTag = `${apiVersion}.${appVersion}`

        if (this.currentApiVersion !== undefined && apiVersion <= this.currentApiVersion) {
          this.compatibleTags?.push(versionTag)
        } else {
          this.incompatibleTags?.push(versionTag)
        }

        const currentLatestVersionByApiVersion = this.latestVersionByApiVersion[apiAndAppVersionTuple[0]]
        if (currentLatestVersionByApiVersion === undefined || currentLatestVersionByApiVersion < appVersion) {
          this.latestVersionByApiVersion[apiVersion] = appVersion
        }
      })

      this.latestApiVersion = this.latestVersionByApiVersion.length - 1
      this.latestAppVersion = this.latestVersionByApiVersion[this.latestApiVersion]
      this.latestVersion = `${this.latestApiVersion}.${this.latestAppVersion}`

      this.latestCompatibleAppVersion = this.latestVersionByApiVersion[this.kittenVersion.apiVersion]
      this.latestCompatibleVersion = `${this.currentApiVersion}.${this.latestCompatibleAppVersion}`
    }
  }

  get hasCompatibleAppVersion () {
    return this.latestCompatibleAppVersion !== undefined
  }

  get canBeUpgraded () {
    // For now, don’t support upgrades when not using Kitten versioning.
    // TODO: We should still allow manual updates based on latest commits.
    if (!this.usesKittenVersioning) return false

    if (this.latestVersionByApiVersion === undefined || this.currentAppVersion === undefined || this.currentApiVersion === undefined) {
      throw new Error('Required properties are missing. Please run update() method before accessing.')
    }
    const latestCompatibleAppVersion = this.latestVersionByApiVersion[this.currentApiVersion]
    return latestCompatibleAppVersion > this.currentAppVersion
  }

  get hasNewerApiVersion () {
    // For now, don’t support upgrades when not using Kitten versioning.
    // TODO: We should still allow manual updates based on latest commits.
    if (!this.usesKittenVersioning) return false

    if (this.latestApiVersion === undefined || this.currentApiVersion === undefined) {
      throw new Error('Required properties are missing. Please run update() method before accessing.')
    }
    return this.latestApiVersion > this.currentApiVersion
  }

  /**
    Returns the string 'upgrade' or 'downgrade' based on whether
    going from the current app version to the provided version string
    would be an upgrade or a downgrade.

    If lowercase is false, the string is returned in sentence case.

    Does not handle the versions being include. For that, see the
    isEqualToVersion() method.
  
    @param {string} versionTag
    @returns 'upgrade' | 'downgrade'
  */
  upgradeOrDowngrade (versionTag, lowercase=true) {
    // For now, don’t support upgrades when not using Kitten versioning.
    // TODO: We should still allow manual updates based on latest commits.
    if (!this.usesKittenVersioning) return false

    if (this.currentApiVersion === undefined || this.currentAppVersion === undefined) {
      throw new Error('Required properties are missing. Please run update() method before accessing.')
    }
    const upgrade = lowercase ? 'upgrade' : 'Upgrade'
    const downgrade = lowercase ? 'downgrade' : 'Downgrade'
    const [apiVersion, appVersion] = versionTag.split('.')
    return (Number(apiVersion) <= this.currentApiVersion && Number(appVersion) < this.currentAppVersion) ? downgrade : upgrade
  }

  isEqualToVersion(versionTag) {
    // For now, don’t support upgrades when not using Kitten versioning.
    // TODO: We should still allow manual updates based on latest commits.
    if (!this.usesKittenVersioning) return false

    if (this.currentApiVersion === undefined || this.currentAppVersion === undefined) {
      throw new Error('Required properties are missing. Please run update() method before accessing.')
    }
    const [apiVersion, appVersion] = versionTag.split('.')
    return (Number(apiVersion) === this.currentApiVersion && Number(appVersion) === this.currentAppVersion)
  }

  // TODO: Refactor: based on ManualUpdate.component.js
  UpdateButtonComponent () {
    const buttonLabels = {
      'upgrade': 'Upgrade the app',
      'downgrade': 'Downgrade the app',
      'upgradeSmall': 'Upgrade',
      'downgradeSmall': 'Downgrade',
      'updating': 'Updating…',
      'restarting': 'Restarting…',
      'updatingSmall': 'Updating…',
      'restartingSmall': 'Restarting…'
    }

    return (({type = 'upgrade', version, small = false}) => {
      const isStatusUpdate = type === 'updating' || type === 'restarting'
      return globalThis.kitten.html`
        <button
          id='updateAppButton${version?.replace('.', 'dot')}${small ? 'Small' : ''}'
          ws-send
          confirm='Proceed to ${type} the running app?'
          hx-oob-swap='outerHTML'
          ${isStatusUpdate ? 'disabled' : ''}
          class="${small ? 'smallButton smallStatusButton' : ''}"
        >
          <if ${isStatusUpdate}>
            <${SvgSpinner} ${small ? 'small' : ''} />
          </if>
          ${buttonLabels[type + (small ? 'Small' : '')]}
          <if ${type === 'restarting' && process.env.PRODUCTION}>
            <script>
              // TODO: This is a hack to force the page to reload after a few seconds.
              // Ideally we would inject a mechanism similar to the development-time
              // reload script (or just use that) here instead.
              setTimeout(() => window.location.reload(), 5000)
            </script>
          </if>
        </button>
      `
    }).bind(this)
  }

  VersionComponent ({version, compatible = false, brief = false}) {
    // Format link to version tag in remote repository for git hosts that we support.
    // (Currently, only codeberg.org.)
    if (this.originRemoteUrl?.startsWith('https://codeberg.org/')) {
      return globalThis.kitten.html`
        <div class='version'>
          <a href='${this.originRemoteUrl?.replace('.git', `/commit/${version}`)}' target='_blank'>${version}</a>
          <if ${!brief && compatible === true}>
            <if ${this.isEqualToVersion(version)}>
              <then>
                <em>(current version)</em>
              </then>
              <else>
                <form
                  hx-ext='ws'
                  ws-connect='/🐱/settings/app-upgrade.socket'
                >
                  <input
                    type='hidden'
                    name='version'
                    value='${version}'
                  >
                  <${this.UpdateButtonComponent()}
                    type='${this.upgradeOrDowngrade(version)}'
                    version='${version}'
                    small
                  />
                </form>
              </else>
            </if>
          </if>
        </div>
      `
    } else {
      return version
    }
  }

  CurrentAppVersionComponent () {
    return (() => globalThis.kitten.html`
      <${this.VersionComponent.bind(this)} version='${this.currentVersion}' brief />
    `).bind(this)
  }

  AllAvailableVersionsComponent () {
    if (!this.usesKittenVersioning) return globalThis.kitten.html``
    
    return (() => globalThis.kitten.html`
      <details class='allAvailbleVersionsComponent' >
        <summary><h3>All available app versions</h3></summary>
          <h4>Compatible</h4>
          <ul>
            ${
              this.compatibleTags?.map(tag => globalThis.kitten.html`
                <li>
                  <${this.VersionComponent.bind(this)} version='${tag}' compatible />
                </li>
              `)
            }
          </ul>

          <if ${this.incompatibleTags !== undefined && this.incompatibleTags.length > 0}>
            <then>
              <h4>Incompatible</h4>
              <ul>
                ${
                  this.incompatibleTags?.map(tag => globalThis.kitten.html`
                    <li>
                      <${this.VersionComponent.bind(this)} version='${tag}' />
                    </li>
                  `)
                }
              </ul>
            </then>
          </if>
          <style>
            .allAvailbleVersionsComponent {
              & summary h3 {
                display: inline;
                font-size: 1.15em;
                font-weight: 400;
              }
              & .version {
                display: grid;
                grid-template-columns: 1.5em max-content;
                align-items: center;
              }
              & em {
                font-size: 0.75em;
                /* Give it the same margin so it aligns with the buttons */
                margin-left: 0.25em;
              }
            }
          </style>
      </details>
    `).bind(this)
  }
  
  /**
    Display app version if Kitten is running in production.
  */
  displayAppVersion () {
    console.info(`Latest app version available: ${this.latestAppVersion}`)
  }
}
