/**
  KittenMoji

  Emoji map and methods used to Base256 encode a person’s ed25519
  private key. Emoji is chosen on purpose to make it impossible
  for the person to type it in, thereby necessitating the use of
  copy and paste and a password manager (and hence good security
  hygiene).

  The emoji chosen for the Base256 alphabet are mainly from the
  animals and plants and food groups with some exceptions (to
  avoid common phobias or triggers, etc., and because there are
  just some that have special meaning for me or that I like.) – Aral ;)

  All emoji in this list are surrogate pairs without any
  special modifiers.

  The KittenMoji standard is being frozen in time on Friday,
  November 25th, 2022 and the string created by joining the
  array has the following sha256 hash:

  d9b1533de36a75c9e47f1713aada4ff332918ff0d3cada5b645ca84d73dd50d6
*/

import { sha256 } from '@noble/hashes/sha256'
import { bytesToHex as toHex } from '@noble/hashes/utils'

// Basic assert functionality that follows node:assert interface
// (This is an isomorphic module so we can’t use node-specific modules.)
const assert = {
  /** @param {any} value1; @param {any} value2; @param {string} [message] */
  strictEqual: function (value1, value2, message) {
    assert.true(value1 === value2, message || `Assertion failed: ${value1} === ${value2}`)
  },
  /** @param {any} value1; @param {any} value2; @param {string} [message] */
  notStrictEqual: function (value1, value2, message) {
    assert.true(value1 !== value2, message || `Assertion failed: ${value1} !== ${value2}`)
  },
  /** @param {boolean} condition; @param {string} [message] */
  true: function (condition, message) {
    if (!condition) {
      throw new Error(message || 'Assertion failed');
    }
  }
}

/**
  Intl.Segmenter() does not work in Firefox so here’s our tiny special-purpose
  one for splitting the string of guaranteed 2-byte emoji into an array.
  (See https://github.com/mdn/content/issues/21259)
  Create array of 1-byte fragments then rejoin the high and low bytes to form the emoji.

  @param {string} emojiString
*/
const arrayFromTwoByteEmojiString = emojiString =>
  emojiString
    .split('')
    .reduce(/** @param {string[]} emojiArray */ (emojiArray, currentByte) => {
      emojiArray[emojiArray.length-1] === undefined || emojiArray[emojiArray.length-1].length === 2
        ? emojiArray.push(currentByte) 
        : emojiArray[emojiArray.length-1] += currentByte;
      return emojiArray
    }, [])

// Public.

/**
  Convert secret bytes to emoji string.

  @param {Uint8Array} secret
*/
export function secretToEmojiString (secret) {
  return secret.reduce((emojiString, currentByteValue) => emojiString + kittenMoji[currentByteValue], '')
}

/**
  Convert emoji string to secret bytes.

  @param {string} emojiString
*/
export function emojiStringToSecret (emojiString /* String */) {
  return Uint8Array.from(arrayFromTwoByteEmojiString(emojiString).map(character => emojiIndex[character]))
}

// Private.

const kittenMoji = [
  "🐵", "🐒", "🦍", "🦧", "🐶", "🐕", "🦮", "🐩", "🐺", "🦊", "🦝",
  "🐱", "🐈", "🦁", "🐯", "🐅", "🐆", "🐴", "🧮", "🦄", "🦓", "🦌",
  "🦬", "🐮", "🐂", "🐃", "🐄", "🐷", "🐖", "🐗", "🐽", "🐏", "🐑",
  "🐐", "🐪", "🐫", "🦙", "🦒", "🐘", "🦣", "🦏", "🦛", "🐭", "🐁",
  "🐀", "🐹", "🐰", "🐇", "🎈", "🦫", "🦔", "🦇", "🐻", "🐨", "🐼",
  "🦥", "🦦", "🦨", "🦘", "🦡", "🐾", "🦃", "🎹", "🐓", "🐣", "🐤",
  "🐥", "🐦", "🐧", "💕", "🦅", "🦆", "🦢", "🦉", "🦤", "🪶", "🦩",
  "🦚", "🦜", "🚲", "🐊", "🐢", "🦎", "📚", "🐉", "🦕", "🦖", "🐳",
  "🐋", "🐬", "🦭", "🐟", "🐠", "🐡", "🦈", "🐙", "🐚", "🐌", "🦋",
  "🐛", "🐜", "🐝", "🪲", "🐞", "🦗", "🎭", "🎁", "🧬", "🪱", "🦠",
  "💐", "🌸", "🎠", "🌈", "🌹", "🧣", "🌺", "🌻", "🌼", "🌷", "🌱",
  "🪴", "🌲", "🌳", "🌴", "🌵", "🌾", "🌿", "🎤", "🍀", "🍁", "🪺",
  "👽", "🍇", "🍈", "🍉", "🍊", "🍋", "🍌", "🍍", "🥭", "🍎", "🍏",
  "🍐", "🍑", "🍒", "🍓", "🫐", "🥝", "🍅", "🫒", "🥥", "🥑", "🍆",
  "🥔", "🥕", "🌽", "🧸", "🫑", "🥒", "🥬", "🥦", "🧄", "🧅", "🍄",
  "🥜", "🌰", "🍞", "🥐", "🥖", "💩", "🥨", "🥯", "🥞", "🧇", "🧀",
  "🎶", "🏸", "🎾", "🎨", "🍔", "🔭", "🍕", "🌭", "🥪", "🌮", "🌯",
  "😸", "📷", "🌜", "🥚", "🚂", "🛼", "🚁", "👾", "👻", "🥗", "🍿",
  "🧩", "🖖", "🥫", "🎸", "🍘", "🍙", "🍚", "🃏", "🍜", "🍝", "🍠",
  "🍢", "🍣", "🍤", "🍥", "🥮", "🍡", "🥟", "🥠", "🩰", "🦀", "🦞",
  "🦐", "🦑", "🎡", "🍦", "🍧", "🍨", "🍩", "🍪", "🎂", "🍰", "🧁",
  "🥧", "🍫", "🍬", "🍭", "🍮", "🎓", "🍼", "🎮", "🛹", "🫖", "🌍",
  "🌎", "🌏", "🧭", "🌠", "🪐", "🪀", "🧵", "🧶", "🧋", "🎉", "🪁",
  "🙈", "🙉", "🙊"
]

const kittenMojiHash = toHex(sha256(kittenMoji.join('')))

const emojiIndex = kittenMoji.reduce((object, character, index) => {
  object[character] = index
  return object
}, {})

// Assertions.

assert.strictEqual(kittenMojiHash, 'd9b1533de36a75c9e47f1713aada4ff332918ff0d3cada5b645ca84d73dd50d6')

// In case the list needs to be changed before we go live,
// make sure the following assertions are run. Otherwise,
// the hash, above, is from a well-known correct state.
// Note that changing the list will invalidate secrets that
// are being used and should not be done once the system goes live.

function assertionsToRunOnlyWhenAndIfTheListIsChanged() {
  assert.strictEqual(kittenMoji.length, 256)

  // Ensure no duplicates.
  kittenMoji.forEach((e, i) => {
    kittenMoji.forEach((e2, i2) => {
      if (i != i2) {
        assert.notStrictEqual(e, e2, `${e} (index ${i}) should not be duplicated at index ${i2}`)
      }
    })
  })

  // Test emoji lenghts.

  kittenMoji.forEach((e, _i) => { 
    const codePoint1 = /** @type number */ (e.codePointAt(0))
    const codePoint2 = e.codePointAt(1)
    const codePoint3 = e.codePointAt(2)

    const recreated = String.fromCodePoint(codePoint1)
    const canBeRecreatedFromCodePoint1 = (e == recreated)

    assert.notStrictEqual(codePoint1, undefined)
    assert.notStrictEqual(codePoint2, undefined)
    assert.strictEqual(codePoint3, undefined)
    assert.strictEqual(canBeRecreatedFromCodePoint1, true)
  })

  // Test methods.

  const secretWithAllPossibleByteValues = Uint8Array.from([...Array(256).keys()])
  const emojiString = secretToEmojiString(secretWithAllPossibleByteValues)
  const secretFromEmojiString = emojiStringToSecret(emojiString)

  const secretsAreEqual = secretWithAllPossibleByteValues.reduce((current, previous, index) => current && (previous === secretFromEmojiString[index]), true)
  assert.strictEqual(secretsAreEqual, true)
}
// Reference purposefully unused function to silence unused function warning.
assertionsToRunOnlyWhenAndIfTheListIsChanged
