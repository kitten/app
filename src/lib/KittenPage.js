/**
  Represents a currently active live page. Live pages are open in the client
  and either in the process of being served for the first time, connecting via
  WebSocket, or connected via WebSocket.

  All live pages belong to a session.

  Usage:

  When authoring, you can export your page route as a subclass of KittenPage using the global reference to it at `kitten.Page`.

  Helpers:

  page.send(): Sends message to connected page (calls page.socket.send()).
  page.everyoneElse.send(): Sends message to everyone connected to this page but the current person.
  page.everyone.send(): Send message to everyone connected to this page, including the current person.
*/

/**
  @typedef {import('ws').WebSocket & {isAlive: boolean}} WebSocketWithIsAlive
  @typedef {import('polka').Request & {ws: function}} RequestWithWebSocket

  @typedef {import('polka').Request & {
    ws: function,
    session: import('../Sessions.js').Session,
  }} KittenRequest

  @typedef {import('polka').Response & {
    get (location:string):void
    redirect (location:string):void
    permanentRedirect (location:string):void
    badRequest (body?:string):void
    unauthenticated (body?:string):void
    forbidden (body?:string):void
    notFound (body?:string):void
    error (body?:string):void
  }} KittenResponse

  @typedef {
    { before?: string } |
    { after?: string } |
    { asFirstChildOf?: string } |
    { asLastChildOf?: string }
  } SwapTarget
*/

import KittenComponent from './KittenComponent.js' 

const colonAndAnythingFollowingIt = /:.*$/

/**
  Transform event name into event handler name.

  @param {string} eventName
*/
function eventNameToEventHandlerName (eventName) {
  return `on${eventName[0].toUpperCase()}${eventName.slice(1)}`
}

/**
  Converts any key paths found in the keys of
  passed data into an actual object structure.

  This enables you to specify object structure in
  the names of your connected elements and makes it
  easier to persist them on the server.

  @param {Object<string, any>} data
  @returns {Object<string, any>}
*/
function covertKeypathsToObjects (data) {
  const result = {}

  for (const key in data) {
    const value = data[key]
    const keys = key.split('.')
    
    let currentLevel = result
    for (let i = 0; i < keys.length; i++) {
      if (i === keys.length - 1) {
        currentLevel[keys[i]] = value
      } else {
        if (!currentLevel[keys[i]]) {
          currentLevel[keys[i]] = {}
        }
        currentLevel = currentLevel[keys[i]]
      }
    }
  }

  return result
}

/** Map of our swap target syntactic sugar to htmx’s attributes. */
const swapTargetMap = {
  before: 'beforebegin',
  after: 'afterend',
  asFirstChildOf: 'afterbegin',
  asLastChildOf: 'beforeend'
}
  
/**
  Add message envelope used when inserting elements into the
  DOM relative to another element using htmx’s out-of-band swaps
  (oob-swaps).

  If a swap target is not provided, the message is returned unchanged.

  @param {BufferLike} message
  @param {SwapTarget} swapTarget
*/
function addMessageEnvelopeIfNecessary (message, swapTarget) {
  const keys = Object.keys(swapTarget)
  if (keys.length !== 0) {
    if (keys.length === 1) {
      const key = keys[0]
      const htmxSwapAttribute = swapTargetMap[key]
      if (htmxSwapAttribute !== undefined) {
        // TODO: Look into this.
        if (/** @type {string} */ (message).startsWith('<tr') && !/** @type {string} */ (message).startsWith('<track')) {
          // Table rows must be wrapped in a tbody envelope.
          // Ensure your tables have a tbody defined when authoring to avoid heartache.
          message = kitten.html`
            <tbody swap-target='${htmxSwapAttribute}:${swapTarget[key]}'>
              ${[message]}
            </tbody>
          `
        } else {
          // It should be safe to wrap anything else in a div-based envelope.
          message = kitten.html`
            <div swap-target='${htmxSwapAttribute}:${swapTarget[key]}'>
              ${[message]}
            </div>
          `
        }
        console.debug('Added swap target to message', message)
      } else {
        throw new Error(`Swap target (${key}) is invalid (use one of: before, after, asFirstChildOf, or asLastChildOf). Ignoring.`)
      }
    } else {
      throw new Error(`Too many swap targets (${keys}), there should only be one (before, after, asFirstChildOf, or asLastChildOf).`)
    }
  }

  return message
}


export default class KittenPage extends KittenComponent {
  /**
    Every page has a unique ID (inherited from KittenComponent) and can hold
    ephemeral page-level data. e.g., the states of components when
    using the Streaming HTML workfow. (Page storage is ephemeral is that
    it only lasts for the lifetime of a page in the browser and is destroyed
    when the page is closed or reloaded.)

    If you need greater persistence, use session storage (request.session) or
    the built-in JSDB database (kitten.db).

    A page is an authored page if the author of the Kitten app/site wrote
    and exported a KittenPage subclass in the route (as opposed to exporting a
    simple function and function-based event handlers that then resulted
    in a generic page being created by Kitten’s PageRoute class). Keeping
    track of this is an optimisation that enabled the PageSocketRoute to
    not have to import the source file again if the page was authored
    as a page instance and thus already contains its event handlers (as
    opposed to the event handlers being exported as separate functions that
    have be read in and mixed into the generic KittenPage instance by the PageSocketRoute).

    @param {KittenRequest} request
    @param {KittenResponse} response
    @param {boolean} isAuthoredPage
  */
  constructor (request, response, isAuthoredPage = false) {
    super()

    // A page is a specialised kitten.Component that is the
    // root of the Kitten component hierarchy. Every kitten.Component
    // has a page reference that refers to the page it belongs to.
    // In this case, since we are the page, our page is, well, us.
    this.page = this

    this.request = request
    this.session = request.session
    this.response = response
    this.isAuthoredPage = isAuthoredPage
    this.data = {}

    // Register the pages in the global pages collection.
    globalThis.kitten.pages[this.id] = this

    // Broadcast page creation event.
    kitten.events.emit('kittenPageCreate', {
      page: this
    })
  }
  
  /**
    The page has connected to its WebSocket. The list of sockets and the
    specific socket for this page are passed for storage on the page and the
    list of event handlers imported from the page (when using function-based
    page routes), if any, to be mixed into this instance as methods.

    @param {WebSocketWithIsAlive} socket
    @param {[WebSocketWithIsAlive]} sockets
    @param {Object.<string, import('./KittenComponent.js').JavaScriptFunction>} eventHandlers
  */
  connect (socket, sockets, eventHandlers = {}) {
    this.socket = socket
    this.sockets = sockets
    // @ts-ignore Mixed-in property.
    this.id = this.socket.id

    // If the author is using function-based routes, we will be passed
    // an object with event handlers to mix into outselves.
    Object.entries(eventHandlers).forEach(([eventHandlerName, eventHandler]) => this[eventHandlerName] = eventHandler)

    // Handle messages (map them to event handler methods on ourself).
    this.socketMessageHandler = this._messageHandler.bind(this)
    socket.addEventListener('message', this.socketMessageHandler)

    // Create broadcast endpoints.
    this.everyone = new MessageSender({socket, sockets, includeSelf: true}) 
    this.everyoneElse = new MessageSender({socket, sockets, includeSelf: false})

    this.bubbleEvent('_onConnect')
    
    // Broadcast page connection events.
    kitten.events.emit('kittenPageConnect', { page: this })
    kitten.events.emit(`kittenPageConnect-${this.id}`, { page: this })
  }

  /**
    Remove the socket event listener on disconnect.
  */
  _onDisconnect () {
    this.socket.removeEventListener('message', this.socketMessageHandler)
    super._onDisconnect()
  }

  /**
    Dynamically add specified event handler for specified event name.

    TODO: Is this still required with the new component model?
    If we find that it doesn’t fit any use cases, we should remove this.

    @param {string} eventName
    @param {() => void} eventHandler

    @deprecated Instead of `on(eventName, handler)`, export `onEventName()` from your page (or add `onEventName()` method to your `kitten.Page` subclass if using class-based page routes.)
  */
  on (eventName, eventHandler) {
    // Only re-enable the deprecation notice once the class-based component model
    // with implict event handlers (implic-event-handlers branch) has been
    // full implemented and merged.
    // 
    console.warn(`[Deprecated] on(): Instead of on('${eventName}', handler), export on${eventName[0].toUpperCase()}${eventName.substring(1)}() from your page.`)
    this[eventNameToEventHandlerName(eventName)] = eventHandler
  }

  /**
    @typedef {
      | string
      | Buffer
      | DataView
      | number
      | ArrayBufferView
      | Uint8Array
      | ArrayBuffer
      | SharedArrayBuffer
      | ReadonlyArray<any>
      | ReadonlyArray<number>
      | { valueOf(): ArrayBuffer }
      | { valueOf(): SharedArrayBuffer }
      | { valueOf(): Uint8Array }
      | { valueOf(): ReadonlyArray<number> }
      | { valueOf(): string }
      | { [Symbol.toPrimitive](hint: string): string }
    } BufferLike
  */

  /**
    Send specified message to just this page’s socket.

    You can specify an optional swap target that intelligently
    wraps what you’re sending with the necessary envelope tag.
    This is normally rather confusing with htmx’s oob swaps,
    especially when inserting table rows. See:
    https://htmx.org/attributes/hx-swap-oob/#using-alternate-swap-strategies

    @param {BufferLike} message
    @param {SwapTarget} swapTarget?
  */

  send (message, swapTarget = {}) {
    if (this.socket !== undefined) {
      // Wrap message in htmx swap-oob envelope
      // if it has a insertion swap target.
      message = addMessageEnvelopeIfNecessary(message, swapTarget)
      this.socket.send(message)
    } else {
      // This should not happen but, just in case.
      console.warn('Warning: Streaming HTML message not delivered as socket is undefined on page (page might be reloading)', this.id, message)
    }
  }

  /**
    The convention for page sockets is to map the name of the DOM element
    that triggered the HTMX request to the name of the event handler so
    that <element connect name='eventName' /> maps to onEventName() on
    the page.

    @param {{ data: string }} event
  */
  async _messageHandler (event) {
    let data
    try {
      data = JSON.parse(event.data)
    } catch (error) {
      // Ignore if data is not valid JSON.
      return
    }

    if (data.HEADERS === undefined) {
      console.warn('[PageSocket] No headers found in htmx WebSocket data, cannot route call.\n[PageSocket] Data:', event.data)
      return
    }

    // The event name is the name of the element that triggered the request
    // but anything after a colon (:) in the event name is ignored.
    // This is for two reasons:
    //   - <details> tags are grouped by name and only one element may be open in a group.
    //     If you want more than more element to be open, you must give each element a 
    //     unique name. This allows you to do that and keep the event name the same.
    //     e.g., <details name='myEvent:unique-suffix'>…</details> where :unique-suffix
    //     will be ignored.
    // 
    //   - HTML validation issues. Some HTML validators might be overly restrictive and,
    //     e.g., require form controls to have a unique name. (e.g., see 
    //     https://html-validate.org/rules/form-dup-name.html). While this is stricly
    //     an issue with the validator, this is your escape hatch should you need it.
    let triggerName = data.HEADERS['HX-Trigger-Name']

    if (triggerName === null) {
      // OK, the element that triggered this event did not have a name
      // attribute. Let’s try and fall back to using its id attribute
      // instead, if there is one.
      const triggerId = data.HEADERS['HX-Trigger']
      if (triggerId === null) {
        // The author forgot to add either a `name` or `id` attribute
        // to the element triggering the event (the one with the `connect`
        // attribute). Let’s raise a warning and proceed no further.
        const errorRoute = data.HEADERS['HX-Current-URL'].replace(`https://${kitten.domain}`, '')
        const triggerEvent = data.HEADERS['HX-Trigger-Event']
        console.error(`❌ Kitten Page Messaging Error on ${errorRoute}: ${triggerEvent} event triggered on element without \`name\` or \`id\` attributes.\n\n   Cannot route to event handler.\n\n   Please see the Streaming HTML tutorial for information on how automatic event mapping works in Kitten:\n   https://kitten.small-web.org/tutorials/streaming-html/`)
        return
      } else {
        // Fall back to using the `id` attribute, instead of `name`.
        triggerName = triggerId
      }
    }
    
    const eventName = triggerName.replace(colonAndAnythingFollowingIt, '')
    const eventHandlerName = eventNameToEventHandlerName(eventName)

    // Save a copy of the headers so we can pass it with the event object
    // but don’t include it in the data (as it is, technically, metadata).
    // This makes it easier to just pop client-side validated data in the
    // database for authenticated routes (do not do that for non-authenticated
    // routes. Not that you should be accepting data from non-authenticated
    // routes into your database in Small Web apps to being with, but still.)
    const headers = data.HEADERS
    delete data.HEADERS

    // If there are any key paths in the data, convert them to
    // an object hierarchy to make it easier to persist, etc.
    data = covertKeypathsToObjects(data)

    // Post the event on the global events object so any part of the app
    // that wan’t to generically listen for incoming events (e.g., for debugging
    // etc., can do so.)
    // TODO: Strongly type this.
    const kittenPageEvent = {
      page: this,
      name: eventName,
      data,
      headers,
      event
    }
    kitten.events.emit('kittenPageEvent', kittenPageEvent)

    // Also emit a page-id-suffixed version so that it can be listened for
    // when using function-based event handlers.
    kitten.events.emit(`kittenPageEvent-${this.id}`, kittenPageEvent)

    console.debug(`[Page] About to bubble ${eventHandlerName} with`, data)
    await this.bubbleEvent(eventHandlerName, data, event)
  }
}

/**
  Provides a namespaced send() method for use in Page instances.
*/
class MessageSender {
  constructor ({ socket, sockets, includeSelf=false }) {
    this.socket = socket
    this.sockets = sockets
    this.includeSelf = includeSelf
  }

  /**
    Sends message either to all connections or to all connections
    excluding the current one.

    You can specify an optional swap target that intelligently
    wraps what you’re sending with the necessary envelope tag.
    This is normally rather confusing with htmx’s oob swaps,
    especially when inserting table rows. See:
    https://htmx.org/attributes/hx-swap-oob/#using-alternate-swap-strategies

    @param {BufferLike} message
    @param {SwapTarget} swapTarget?
  */
  send (message, swapTarget = {}) {
    this.sockets.forEach((/** @type {WebSocketWithIsAlive} */ socket) => {
      if (this.includeSelf || (!this.includeSelf && socket !== this.socket)) {
        message = addMessageEnvelopeIfNecessary(message, swapTarget)
        socket.send(message)
      }
    })
    // Return the number of connections sent to.
    return this.sockets.length - (this.includeSelf ? 0 : 1)
  }
}
