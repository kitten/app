/**
  Kitten Crypto API

  Runs in browsers (packaged via esbuild) and in Node.

  Exposes high-level cryptographic functions to enable generation
  of secret key material as well as Diffie-Hellman key exchange (shared secrets)
  and signing and encryption for the Small Web Protocol.

  When used in browsers, does so in the person’s own browser without ever
  hitting the server and, from there, generates the ed25519, ssh, and pgp
  public keys that are sent to the server for storage and discovery.

  Copyright © 2022-present, Aral Balkan
  Small Technology Foundation.
  Released under GNU AGPLv3.
*/

import ssh from 'ed25519-keygen/ssh'
import * as ed from '@noble/ed25519'

import { secretToEmojiString, emojiStringToSecret } from './KittenMoji.js'
import { getSharedSecret, sign, verify } from '@noble/ed25519'
import { encrypt, decrypt } from 'micro-aes-gcm'

// Note: randomBytes from Noble is polymorphic and bails if
// cryptographically-secure random number generator is not
// present in the environment (Node/Browser/Deno/fridge, etc.)
import { randomBytes } from 'ed25519-keygen/utils'

// General utilities for converting between bytes and hex.
import { bytesToHex, hexToBytes } from '@noble/hashes/utils'

export {
  secretToEmojiString,
  emojiStringToSecret,
  getSharedSecret,
  encrypt,
  decrypt,
  sign,
  verify,
  randomBytes,
  bytesToHex,
  hexToBytes
}

//
// Secret tokens.
//

/**
  Generates cryptographically random 32-byte token and coverts it to hexadecimal representation.

  Use anywhere you need a secret token (domain token, webhook secret, etc.)
*/
export function random32ByteTokenInHex () {
  return bytesToHex(randomBytes(32))
}

//
// Encryption/decryption.
//

// Cache shared secrets for different domains as they’re
// expensive to calculate.
const sharedSecrets = {}

const textDecoder = new TextDecoder() // default: 'utf-8'

/**
  Calculates the shared secret for a domain using the
  domain’s public key and our private key.

  @param {string} domain
  @param {string} ourPrivateKey
*/
async function sharedSecretForDomain (domain, ourPrivateKey) {
  // console.time('sharedSecretForDomain')

  if (sharedSecrets[domain] === undefined) {
    // We don’t have a shared secret yet. Attempt to calculate one
    // by getting the other domain’s ed25519 public key.
    const domainToContact = `https://${domain}/💕/id/`
    const theirPublicKeyResponse = await fetch(domainToContact)
    const theirPublicKeyHex = await theirPublicKeyResponse.text()
    const ourPrivateKeyHex = bytesToHex(emojiStringToSecret(ourPrivateKey))
    sharedSecrets[domain] = await getSharedSecret(ourPrivateKeyHex, theirPublicKeyHex)
  }

  // console.timeEnd('sharedSecretForDomain')

  return sharedSecrets[domain]
}

/**
  Encrypts a message for a domain.

  @param {string} message
  @param {string} ourPrivateKey - The emoji-encoded private key
  @param {string} domain

  @returns {Promise<string>} Hex-encoded encrypted message.
*/
export async function encryptMessageForDomain (message, ourPrivateKey, domain) {
  // console.time('encryptMessageForDomain')

  const sharedSecret = await sharedSecretForDomain(domain, ourPrivateKey)
  const encryptedMessageBytes = await encrypt(sharedSecret, message)
  const encryptedMessage = bytesToHex(encryptedMessageBytes)

  // console.timeEnd('encryptMessageForDomain')

  return encryptedMessage
}

/**
  Decrypts message from domain.

  @param {string} encryptedMessage - Hex-encoded encrypted message.
  @param {string} ourPrivateKey - Emoji-encoded private key string.
  @param {string} domain

  @returns {Promise<string>} Unencrypted message as UTF8 string.
*/
export async function decryptMessageFromDomain (encryptedMessage, ourPrivateKey, domain) {
  // console.time('decryptMessageFromDomain')

  const sharedSecret = await sharedSecretForDomain(domain, ourPrivateKey)
  const decryptedMessageUInt8Array = await decrypt(sharedSecret, hexToBytes(encryptedMessage))
  const decryptedMessageUtf8String = textDecoder.decode(decryptedMessageUInt8Array)

  // console.timeEnd('decryptMessageFromDomain')

  return decryptedMessageUtf8String
}

//
// Key generation.
//

/**
  Creates keys for domain.

  @param {string} domain
*/
export async function createKeys (domain) {
  // console.time('createKeys')

  // console.time('Create ed25519 random private key')
  const privateKey = ed.utils.randomPrivateKey()
  // console.timeEnd('Create ed25519 random private key')

  const keys = await deriveKeysFromSecretKeyForDomain(privateKey, domain)
  keys.private.ed25519 = {
    asBytes: privateKey,
    asString: secretToEmojiString(privateKey)
  }

  // console.timeEnd('createKeys')

  return keys
}


/**
  Derive keys from secret key for domain.

  @param {Uint8Array} secretKey
  @param {string} domain

  @returns Promise
*/
export async function deriveKeysFromSecretKeyForDomain (secretKey, domain) {
  // console.time('deriveKeysFromSecretKeyForDomain')

  // console.time('Get ed25519 public key from secret key')
  const ed25519PublicKey = await ed.getPublicKey(secretKey)
  // console.timeEnd('Get ed25519 public key from secret key')

  // console.time('Derive ssh key from secret')
  const sshKeys = await ssh(secretKey, /* comment */ `Kitten: person@${domain}`)
  // console.timeEnd('Derive ssh key from secret')

  // console.timeEnd('deriveKeysFromSecretKeyForDomain')

  // Separate the key material based on whether it is private or public.
  // Private key material should never be stored on the server and be
  // recreated as necessary from the ed25519 private key (the secret).

  return {
    private: {
      ssh: {
        asString: sshKeys.privateKey
      }
    },

    public: {
      ed25519: {
        asString: ed.utils.bytesToHex(ed25519PublicKey),
        asBytes: ed25519PublicKey
      },

      ssh: {
        asString: sshKeys.publicKey,
        asBytes: sshKeys.publicKeyBytes,
        fingerprint: sshKeys.fingerprint
      }
    }
  }
}
