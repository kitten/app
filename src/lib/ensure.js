/**
  Kitten critical assertions.

  Copyright ⓒ 2022-present, Aral Balkan (https://ar.al)
  Small Technology Foundation (https://small-tech.org).

  License: AGPL version 3.0.
*/

import childProcess from 'child_process'
import chalk from 'chalk'
import tcpPortUsed from 'tcp-port-used'

import serviceStatus from './service-status.js'

const functions = {
  commandExists, systemctl, journalctl, serviceNotActive, portsAreFree, canEnableService
}
export default functions

/**
  Does the command with the passed name exist?

  @param {string} command
*/
export function commandExists (command) {
  try {
    childProcess.execFileSync('which', [command], {env: process.env})
    return true
  } catch (error) {
    return false
  }
}

// Ensure systemctl exists.
export function systemctl () {
  if (!commandExists('systemctl')) {
    throw new Error('systemd not found (systemctl is missing).')
  }
}


// Ensure journalctl exists.
export function journalctl () {
  if (!commandExists('journalctl')) {
    throw new Error('systemd not found (journalctl is missing).')
  }
}

export async function portsAreFree () {
  if (await tcpPortUsed.check(80)) {
    throw new Error('required port (80) is in use.')
  } 
  
  if (await tcpPortUsed.check(443)) {
    throw new Error('required port (443) is in use.')
  }
}

export async function canEnableService () {
  // Ensure systemd exists.
  systemctl()
  journalctl()
}

// Ensures that the server daemon is not currently active.
export function serviceNotActive () {
  // Ensure systemctl exists as it is required for getStatus().
  // We cannot check in the function itself as it would create
  // a circular dependency.
  systemctl()
  const { isActive } = serviceStatus()

  if (isActive) {
    throw new Error(`Kitten Daemon is already running.\n\n${chalk.yellowBright('Please stop it before retrying using:')} kitten ${chalk.greenBright('stop')}\n`)
  }
}
