// @ts-check

/**
  Kitten Component.
  
  A class that makes it easier to author hierarchies of connected components.

  Handles wiring up and disposal of event listeners for you and gives you an
  ergonomic, class-based way of writing maintainable applications that take
  advantage of Kitten’s Streaming HTML workflow while working with well-encapsulated,
  self-contained components in a clear hierarchy. 

  Extend and instantiate this class to create your own components (components, fragments,
  layout components, and pages) and provide at least an override for the `html()` method,
  which is just a function that returns `kitten.html`.

  @example

  The following will update the displayed date whenever the button is pressed.

  // index.page.js:
  class CurrentDate extends kitten.Component {
    html () {
      return kitten.html`
        <h1 id='current-date' morph>${new Date()}</h1>
      `
    }

    // The updateCurrentDate event will bubble up from the page to
    // this component and get handled here.
    onUpdateCurrentDate () {
      // Just stream an updated version of ourselves to the page.
      this.update()
    }
  }

  export default class MyPage extends kitten.Page {
    html () {
      return kitten.html`
        <${CurrentDate.connectedTo(this)} />
        <button 
          name='updateCurrentDate'
          connect
        >Update date</button>
      `
    }
  }
*/
import crypto from 'node:crypto'

/**
  Type definitions. First one required due to following shenanigans:
  https://github.com/Microsoft/TypeScript/issues/20007#issuecomment-2255964704

  @typedef {(...args: any[]) => any} JavaScriptFunction
  @typedef {Object.<string, JavaScriptFunction>} FunctionMap
*/

class Listener {
  /**
    Create EventEmitter listener.
    
    @param {object} target
    @param {string} eventName
    @param {JavaScriptFunction} eventHandler
  */
  constructor (target, eventName, eventHandler) {
    this.target = target
    this.eventName = eventName
    this.eventHandler = eventHandler

    this.target.on(this.eventName, this.eventHandler)
  }

  remove () {
    this.target.off(this.eventName, this.eventHandler)
  }
}

/** @param {string} eventName */
function eventNameToEventHandlerName (eventName) {
  return `on${eventName[0].toLowerCase()}${eventName.slice(1)}`
}

export default class KittenComponent {
  isAttached = false
  isConnected = false

  /** @type Array<Listener> */
  listeners

  /** @type Array<KittenComponent> */
  children

  /** @type import('./KittenPage.js').default */
  page

  /** @type Record<string, any> */
  data

  /**
    Factory method: Creates an instance of the Kitten component,
    connects it to the passed parent, and returns a bound reference
    to its component function, ready to be rendered in kitten.html.

    @example

    ```
    <!-- In your page (`this` refers to the page). -->
    <${MyComponent.connectedTo(this)} />
    ```

    @param {KittenComponent} parent
    @param {Record<string, any>} data?
  */
  static connectedTo (parent, data = {}) {
    const instance = new this(data)
    parent.addChild(instance)
    return instance.component
  }

  /**
    Use the static `.connectedTo()` factory method instead of
    creating an instance directly via `new`.

    The only time you should be calling this constructor directly
    is via `super()` if you override it in your custom component.

    @param {Record<string, any>} data?
  */
  constructor (data = {}) {
    // Note: we add the kid- prefix (for Kitten ID), to ensure
    // the ID starts with a letter.
    // console.log(this.constructor.name, 'data', Object.keys(data))
    this.id  = `kid-${crypto.randomUUID()}`
    this.children = []
    this.listeners = []
    this.data = data
  }

  /**
    Overridable (hook) methods.

    Required:

    - html() – the component function that outputs kitten.html

    Optional:

    - onConnect() - called when the page this component is on
                   (or is) connects.

    - onDisconnect() - called when the page this component is on
                      (or is) disconnects.
  */

  /**
    Override this function with your own function that returns kitten.html.
  */
  html () {
    throw new Error(`Required <code>html()</code> render method missing in <code>${this.constructor.name}</code> class. <p style='font-size: 1.25rem; font-weight: normal;'>Please add an <code>html()</code> method to your class and have it return the <code>kitten.html\`\`</code> you want to render. e.g.,</p>
      ${kitten.md.render(`
\`\`\`js
class ${this.constructor.name} extends kitten.Page {
  html () {
    return kitten.html\`<h1>Hello, world!</h1>\`
  }
}
\`\`\`
      `)}

      <style>h2 { word-break: normal; } h2 :not(pre) code, h2 pre { font-size: 1rem; } h2 code.hljs { padding:0; margin-top: 1.5em; margin-bottom: 2em; }</style>`)
  }

  /**
    These event handler methods (optional hooks) may be overriden by subclasses
    to provide custom logic.

    ⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄
  */

  /**
    Optional hook: override this method to run custom logic when the component
    has been loaded (attached to its parent and, thus, to the component hiearchy).

    At this point it will have a reference to its parent component, to any data that
    may have been passed to its `connectedTo()` factory method in the `kitten.html`, and
    to the page it is on (at `this.parent`, `this.data`, and `this.page`, respectively.)

    However, there is no guarantee that the page this component is attached to has
    connected to the client via its automatic WebSocket. For that, rely on the
    `onConnect()` handler instead.
  */
  onLoad() {}

  /**
    Optional hook: override this method to provide custom logic for your app
    to be run when the page this component is on connects to the client via its WebSocket.

    This hook will get called not just for initially-rendered components when the page
    first connects but also for any components dynamically added to an already-connected
    page/component hierarchy after the fact.

    (So you can be sure that this handler will be called once when a component is fully
    initialised on a connected page. This is a good place to add event handlers or to
    start streaming updates to the client.)
  */
  onConnect(_parameterObject={}) {}

  /**
    Optional hook: override this method to run custom logic when the page
    this component is on disconnects from the client via its WebSocket.
    (This usually means the page the about to be unloaded, either because the
    person is nagivating away from it or reloading it.)
  */
  onDisconnect(_parameterObject={}) {}

  /**
    ⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄
  */

  /**
    Returns a bound version of the render function that can
    refer to the component itself as this when calling
    `connectedTo()` on child components.
  */
  get component () {
    return this.html.bind(this)
  }

  /**
    Adds child component to this one.

    Child components are entered into the event bubbling hierarchy and
    contain a reference to the page that they’re on.

    @param {KittenComponent} component
  */
  addChild (component) {
    this.children.push(component)
    component.page = this.page
    component.isAttached = true

    // Signal to instance that it has been attached to
    // the component hierarchy. (This is a good place
    // to add event handlers, etc.)
    component.onLoad()

    // If the component was attached to a connected page,
    // also ensure its onConnect() handler will get called.
    if (this.page.isConnected) {
      component._onConnect()
    }
  }

  /**
    Add an event handler. Event listening and listener clean-up
    are automatically handled so the author doesn’t have to worry
    about implementing this finickety aspect manually.

    @param {object} target
    @param {string} eventName
    @param {JavaScriptFunction} eventHandler
  */
  addEventHandler (target, eventName, eventHandler) {
    this.listeners.push(new Listener(target, eventName, eventHandler.bind(this)))
  }

  /**
    A helper for adding an event handler and streaming an updated
    version of the computer to the client (this is a common pattern).

    TODO: Is this still required in the improved component model?

    @param {string} eventName
  */
  updateOnEvent (eventName) {
    this[eventNameToEventHandlerName(eventName)] = this.update.bind(this)
  }

  /**
    Helper for sending an updated version of this component to the page.

    Also handles removal of event listeners for itself and all its
    children so we don’t have any leaks.
  */
  async update () {
    for await (const child of this.children) {
      await child.bubbleEvent('_onDisconnect')
    }
    this.page.send(globalThis.kitten.html`<${this.component} />`)
  }

  /**
    Helper for removing this component from the live component hierachy.

    Also handles removal of event listeners for itself and all its
    children so we don’t have any leaks.
  */
  remove () {
    this.bubbleEvent('_onDisconnect')
    this.page.send(kitten.html`<div id=${this.id} swap-target='delete' />`)
  }

  /**
    Bubbles an event up to all children.

    @param {string} eventHandlerName
    @param {any} data
    @param {any} event
  */
  async bubbleEvent (eventHandlerName, data = undefined, event = undefined) {
    console.debug(`[KittenComponent] Bubbled ${eventHandlerName} to`, this.id)
    if (typeof this[eventHandlerName] === 'function') {
      console.debug(`[KittenComponent] Running ${eventHandlerName} on`, this.id)
      try {
        await this[eventHandlerName](data, event)
      } catch (error) {
        // TODO: Stream this error to the client also and make it appear in a modal.
        console.error(error)
      }
    } else {
      console.debug(`[KittenComponent] No handler (${eventHandlerName}) on`, this.id)
    }
    for (const child of this.children) {
      await child.bubbleEvent(eventHandlerName, data, event)
    }
  }

  /**
    The internal onConnect handler that gets called by Kitten.
    We use this to connect event handlers, listen for events,
    and inform connected components to do the same.
  */
  _onConnect () {
    // Flag that we’re connected.
    this.isConnected = true

    // If the application author has provided a custom onConnect
    // function, call that so they can carry out custom behaviour.
    // We pass the parameter object for compatibility with earlier
    // version of the component model.
    this.onConnect({page: this.page, request: this.page.request, response: this.page.response })
  }

  /**
    The internal onDisconnect handler that gets called by Kitten.
    We use this to remove event listeners and and inform connected
    components to do the same.
  */
  _onDisconnect () {
    if (this.page.id === undefined) {
      // This can happen if we, for example, hit a 404 page and then hit back
      // during development. Ignore it.
      return
    }

    // Remove all listeners
    this.listeners.forEach(listener => listener.remove())
    this.listeners = []

    // Flag that we’re disconnected.
    this.isConnected = false

    // Finally, if the application author has provided a custom onDisconnect
    // function, call that so they can carry out custom behaviour.
    // We pass the parameter object for compatibility with earlier
    // version of the component model.
    this.onDisconnect({page: this.page, request: this.page.request, response: this.page.response })
  }
}
