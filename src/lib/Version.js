/**
  Deserialises version information from the version.json
  file created by the Kitten build process.

  Singleton.
*/

import fs from 'node:fs'
import path from 'node:path'
import paths from './paths.js'

import chalk from 'chalk'

export default class Version {
  /** @type {Version} */ static instance

  static getInstance () {
    if (this.instance === undefined) {
      this.instance = new this()
    }
    return this.instance
  }

  constructor () {
    /**
      @type {{
        date: string,
        versionStamp: number,
        gitHash: string,
        apiVersion: number,
        nodeVersion: string,
      }}
    */
    const version = JSON.parse(fs.readFileSync(path.join(paths.KITTEN_APP_DIRECTORY_PATH, 'version.json'), 'utf-8'))

    this.date = new Date(version.date)
    this.versionStamp = version.versionStamp
    this.gitHash = version.gitHash
    this.apiVersion = version.apiVersion
    this.nodeVersion = version.nodeVersion
    this.exactVersion = `${version.apiVersion}-${version.gitHash}-${version.nodeVersion}-${version.versionStamp}`
  }

  /**
    @param {Date} date
  */
  set date (date) {
    this._date = date

    this.year = date.getUTCFullYear()
    this.month = date.getUTCMonth() + 1
    this.day = date.getUTCDate()

    this.paddedMonth = this.month.toString().padStart(2, '0')
    this.paddedDay = this.day.toString().padStart(2, '0')

    this.hour = date.getUTCHours()
    this.minute = date.getUTCMinutes()
    this.second = date.getUTCSeconds()

    this.paddedHour = this.hour.toString().padStart(2, '0')
    this.paddedMinute = this.minute.toString().padStart(2, '0')
    this.paddedSecond = this.second.toString().padStart(2, '0')

    this.formattedDate = `${this.year}/${this.paddedMonth}/${this.paddedDay}`
    this.formattedTime = `${this.paddedHour}:${this.paddedMinute}:${this.paddedSecond}`
  }

  get date () {
    if (this._date === undefined) throw new Error('Date is undefined')
    return this._date
  }

  birthday () {
    return `${this.formattedDate} at ${this.formattedTime} UTC (${this.starSign})`
  }

  /**
    Get star sign based on passed month and day.

    Based on https://gist.github.com/vyach-vasiliev/175758c7633fda9587f9dabf850507c0
    @returns {string} Star sign.
  */
  get starSign () {
    var zodiac = ['', 'Capricorn', 'Aquarius', 'Pisces', 'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn']
    var lastDay = [0, 19, 18, 20, 20, 21, 21, 22, 22, 21, 22, 21, 20, 19]
    if (this.day === undefined || this.month === undefined) throw new Error('Day or month is undefined.')
    return (this.day > lastDay[this.month]) ? zodiac[this.month + 1] : zodiac[this.month]
  }

  get Component () {
    return this.html.bind(this)
  }

  /**
    HTML component to display version information.
  */
  html () {
    const gitHashAsCSSHexColourString = `#${this.gitHash}`
    return globalThis.kitten.html`
      <dl class='nameValueList'>
        <dt>Version</dt>
        <dd>${this.apiVersion}-${this.gitHash}-${this.nodeVersion}-${this.versionStamp}</dd>
        <dt>Born</dt>
        <dd>${this.birthday()}</dd>
        <dt>Favourite colour</dt>
        <dd id='favouriteColour'>${gitHashAsCSSHexColourString}</dd>
        <dt>API version</dt>
        <dd class='${this.apiVersion === 0 ? "prerelease" : ""}' }>${this.apiVersion}<if ${this.apiVersion === 0}> (prelease, expect breaking changes)</if></dd>
        <dt>Runtime</dt>
        <dd>Node.js ${this.nodeVersion}</dd>
        <style>
          dl {
            display: grid;
            grid-template-columns: auto 1fr;
          }
          dt {
            font-weight: bold;
          }
          dd.prerelease {
            background-color: var(--background-alt);
            font-weight: 600;
            font-style: italic;
          }
          #favouriteColour::before {
            content: '█ ';
            color: ${gitHashAsCSSHexColourString};
          }
        </style>
      </dl>
    `
  }

  /**
    Output version information to the console.
  */
  printToConsole () {
    const gitHashAsCSSHexColourString = `#${this.gitHash}`
    const favouriteColour = chalk.hex(gitHashAsCSSHexColourString)
    console.info(chalk.magentaBright.bold('\n   Version'), chalk.magentaBright.italic(`${this.apiVersion}-${this.gitHash}-${this.nodeVersion}-${this.versionStamp}`))
    console.info(chalk.magentaBright.bold('   Born'), chalk.magentaBright.italic(this.birthday()))
    console.info(chalk.magentaBright.bold('   Fav. colour'), chalk.magentaBright(gitHashAsCSSHexColourString), favouriteColour('███'))
    console.info(chalk.magentaBright.bold('   API version'), chalk.magentaBright(this.apiVersion))
    console.info(chalk.magentaBright.bold('   Runtime'), chalk.magentaBright(`Node.js ${this.nodeVersion}`))
  }
}
