import fs from 'node:fs'
import path from 'node:path'

import WebSocket from 'ws'
import { html, hyperscriptToHtmlString } from './html.js'
import sanitiseHtml from 'sanitize-html'
import { EventEmitter } from 'node:events'
import paths from './paths.js'
import * as utils from '../Utils.js'
import deploy from './deploy.js'
import markdownIt, { slugify } from './markdown.js'
import yaml from 'yaml'
import * as crypto from './crypto.js'
import KittenComponent from './KittenComponent.js'
import KittenPage from './KittenPage.js'
import { Upload } from '../routes/PostRoute.js'

/**
  Creates and populates the Kitten global namespace (kitten).
*/
export default class Globals {
  static initialised = false

  /**
    Initialise Kitten globals (unique for each domain/port/project directory combination).

    @param {string} domain
    @param {number} port
  */
  static initialise(domain, port) {
    if (Globals.initialised) {
      console.info('ℹ Global kitten namespace already initialised, not re-initialising.')
      return
    }

    // Set up data directory for app using a unique identifier based on the absolute path
    // the project is in. This should enable all projects to have unique databases.
    // If running on localhost, also add localhost.<port> to the project identifier
    // so we can run several instances of the same app to test peer-to-peer
    // functionality locally.
    const projectIdentifier = utils.getProjectIdentifierForDomain(domain, port)
    const appDataDirectory = path.join(paths.KITTEN_DATA_DIRECTORY, projectIdentifier)
    const appUploadsDirectory = path.join(appDataDirectory, 'uploads')
    const appReplDirectory = path.join(appDataDirectory, 'repl')

    // Ensure main data directory exists.
    // The other directories will be created where used.
    fs.mkdirSync(appDataDirectory, {recursive: true})

    const kitten = globalThis.kitten

    kitten.Component = KittenComponent
    kitten.Page = KittenPage
    kitten.Upload = Upload
    kitten.slugify = slugify
    kitten.yaml = yaml
    kitten.deprecationWarnings = globalThis.deprecationWarnings
    kitten.pages = {}
    kitten.utils = utils

    const packageJsonFilePath = path.join(/** @type {string} */ (process.env.basePath), 'package.json')

    // Create object that holds information about the Kitten app.
    kitten.app = {}

    // The unique identifier to the current app/project.
    kitten.projectIdentifier = projectIdentifier

    // Enable Kitten apps (like Kitten’s own web app, for example)
    // to access Kitten’s paths from the global kitten object.
    kitten.paths = paths

    // Add additional paths calculated based on the project identifier.
    kitten.paths.APP_DATA_DIRECTORY = appDataDirectory
    kitten.paths.APP_UPLOADS_DIRECTORY = appUploadsDirectory
    kitten.paths.APP_REPL_DIRECTORY = appReplDirectory

    // If a package.json file exists for the app, load its contents.
    if (fs.existsSync(packageJsonFilePath)) {
      console.debug('⚙️ Found package.json file in app, loading it.')
      // Copy package.json fields to the Kitten app object.
      kitten.app.package = JSON.parse(fs.readFileSync(packageJsonFilePath, 'utf-8'))
    }


    // Expose the deployment function so it can be used by the Kitten Web app.
    kitten.deploy = deploy

    // Add global WebSocket reference.
    // @ts-ignore WebSocket type definition bug.
    // (see https://github.com/DefinitelyTyped/DefinitelyTyped/issues/36269)
    kitten.WebSocket = WebSocket

    // Add global html function.
    kitten.html = html

    // Kitten’s cryptographical library with higher-level functions.
    // Included here for consistency to encourage the same functions
    // to be used everywhere.
    kitten.crypto = crypto

    // Add constants for well-known/supported client-side libraries
    // to global scope so they can be referenced in page routes.
    kitten.libraries = {
      htmx: process.env.PRODUCTION ? 'htmx-2.min.js.gz' : 'htmx-2.js',
      htmxIdiomorph: process.env.PRODUCTION ? 'htmx-idiomorph-0.3.min.js' : 'htmx-idiomorph-0.3.js',
      htmxWebSocket: 'htmx-ws-2.js',
      alpineJs: process.env.PRODUCTION ? 'alpinejs-3.min.js.gz' : 'alpinejs-3.js',
      water: process.env.PRODUCTION ? 'water-2.min.css' : 'water-2.css'
    }

    // Special page areas; targets of <content for='…'> tags.
    kitten.page = {
      html: 'HTML',
      head: 'HEAD',
      startOfBody: 'START_OF_BODY',
      beforeLibraries: 'BEFORE_LIBRARIES',
      afterLibraries: 'AFTER_LIBRARIES',
      endOfBody: 'END_OF_BODY'
    }
    // Add common aliases/miscapitalisations to be more forgiving during authoring.
    kitten.page.HTML = kitten.page.Html = kitten.page.html
    kitten.page.HEAD = kitten.page.Head = kitten.page.head
    kitten.page.START_OF_BODY = kitten.page.StartOfBody = kitten.page.startofbody = kitten.page.startOfBody
    kitten.page.AFTER_LIBRARIES = kitten.page.AfterLibraries = kitten.page.afterlibraries = kitten.page.afterLibraries
    kitten.page.END_OF_BODY = kitten.page.EndOfBody = kitten.page.endofbody = kitten.page.endOfBody

    // Global event emitter.
    
    // TODO: Add more semantics around remote messages. e.g. Any route can
    // subscribe to 'message' events on this to be informed when
    // any message comes in. Also, you can listen for 'message.type'
    // events to be notified only when a message of type <type> is
    // received. While type may be any string, it is recommended to/
    // use a URL to a resource your control (e.g., a page documenting
    // the format) for the following two reasons:
    //
    // - Namespacing (to avoid message type collisions in the future)
    // - Self-documenting formats
    // TODO: Make this a custom class and carry out more validation on messages.
    kitten.events = new EventEmitter()
    kitten.events.setMaxListeners(Infinity)

    /**
      CSS tagged template.

      Just does a default merge of the strings and interpolations arrays
      and wraps them within style tags for now. Not using the HTML tagged template
      from htm/vhtml as it escapes angular brackets, etc., and also so we don’t
      have to wrap CSS in a <style></style> tag manually (so we can also keep them
      in valid external CSS files.)

      @param {string[]} strings - Static strings.
      @param {[]} interpolations - Interpolated values.
    */
    kitten.css = (strings, ...interpolations) => {
      const styles = interpolations.reduce((previous, current, index) => previous + current + strings[index+1], strings[0])
      return `<style>${styles}</style>`
    }

    /**
      JS tagged template. This is a basic function that simply attaches its input to the page
      without any escaping. It can be used to get language intelligence/syntax highlighting
      for Alpine.js snippets in your editor (if your editor and/or language server understands
      to display kitten.js`` tagged templates as JavaScript).

      Be careful if using this as you will have to escape backticks in your code and it
      can make your code harder to read and understand for other people.

      Use for simple things only.

      @param {string[]} strings - Static strings.
      @param {[]} interpolations - Interpolated values.
    */
    kitten.js = (strings, ...interpolations) => hyperscriptToHtmlString(null, {
      dangerouslySetInnerHTML: { __html: 
        interpolations.reduce((previous, current, index) => previous + current + strings[index+1], strings[0])
      }
    })

    /**
      Markdown wrapper.

      Results in exactly the same thing as kitten.html`<markdown>…</markdown>` but without
      requiring you to add the markdown tags. In a lot of ways more limited than just
      using kitten.html`` with markdown tags inside it but might be nicer semantically in
      certain cases.

      @param {string[]} originalStrings - Static strings.
      @param {[]} interpolations - Interpolated values.
    */
    kitten.markdown = (originalStrings, ...interpolations) => {
      const copyOfStrings = Array.from(originalStrings)
      const lastStringIndex = copyOfStrings.length - 1
      copyOfStrings[0] = '<markdown>' + copyOfStrings[0]
      copyOfStrings[lastStringIndex] = copyOfStrings[lastStringIndex] + '</markdown>' 
      return html(copyOfStrings, ...interpolations)
    }

    // You can’t interpolate markdown into markdown blocks. Instead, you can use
    // the kitten.md.render() function if you need more flexibility.
    // TODO: Decide if we actually need the markdown template function or whether
    // it’s cleaner just to expose this as kitten.markdown so folks can use
    // it if they need to do more advanced stuff.
    
    kitten.md = markdownIt
    
    /**
      Use this method to have untrusted markup (e.g., list of posts from your
      Mastodon instance’s API) added to your page safely. You can customise the
      list of allowed tags by providing a list of ones to add to them (e.g., ['img'])
      or by replacing them altogether with your own list by passing `false` as the
      last argument.

      Kitten does not give you direct access to dangerouslySetInnerHTML as you should
      never need to use that for interpolated strings at this high-level of authoring.
      If you don’t do anything, interpolated content will be escaped and so you are 
      safe by default from injection attacks (if you’re hugely determined to use it,
      I‘m sure you can figure out how to by looking at the code in this function but I
      highly recommend that you don’t unless you know exactly what you’re doing.)

      @param {string} untrustedContent
      @param {string[]} allowedTags
      @param {boolean} concat
    */
    kitten.safelyAddHtml = (untrustedContent, allowedTags = [], concat = true) => hyperscriptToHtmlString(null, {
      dangerouslySetInnerHTML: { __html: sanitise(untrustedContent, allowedTags, concat) }
    })

    /**
      This is a wrapper around the sanitize-html function that you can use
      to only allow whitelisted HTML in strings.

      Note: since the html-sanitize module writes out xhtml-style tags by default,
      the global replace at the end transforms those to html 5 tags so they do
      not trigger our html validation rules.
      See: https://github.com/apostrophecms/sanitize-html/issues/51

      @param {string} untrustedContent
      @param {string[]} allowedTags
      @param {boolean} concat
    */
    const sanitise = (untrustedContent, allowedTags = [], concat = true) => sanitiseHtml(
      untrustedContent,
      {
        allowedTags: concat ? sanitiseHtml.defaults.allowedTags.concat(allowedTags) : allowedTags
      }
    ).replace(/\s?\/>/g, '>')

    kitten.sanitise = sanitise

    // Flag that globals have been initialised.
    Globals.initialised = true
  }
}
