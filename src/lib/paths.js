// Binary and configuration path usage adheres to 
// freedesktop.org XDG Base Directory Specification
// https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
import path from 'node:path'

const HOME = process.env.HOME
if (HOME === undefined) throw new Error(`process.env.HOME is undefined. Cannot continue.`)

const BINARY_HOME = path.join(HOME, '.local', 'bin')
const KITTEN_BINARY_PATH = path.join(BINARY_HOME, 'kitten')

const DATA_HOME = process.env.XDG_DATA_HOME || path.join(HOME, '.local', 'share')
const SMALL_TECH_DATA_HOME = path.join(DATA_HOME, 'small-tech.org')

const KITTEN_DATA_HOME = path.join(SMALL_TECH_DATA_HOME, 'kitten')
const KITTEN_APP_DIRECTORY_PATH = path.join(KITTEN_DATA_HOME, 'app')
const KITTEN_WEB_APP_DIRECTORY_PATH = path.join(KITTEN_APP_DIRECTORY_PATH, 'web')

const KITTEN_TLS_DIRECTORY = path.join(KITTEN_DATA_HOME, 'tls')
const KITTEN_TLS_LOCAL_CERTIFICATE_DIRECTORY = path.join(KITTEN_TLS_DIRECTORY, 'local')
const KITTEN_TLS_GLOBAL_CERTIFICATE_DIRECTORY = path.join(KITTEN_TLS_DIRECTORY, 'global')

const KITTEN_TEMP_DIRECTORY = path.join(path.sep, 'tmp', 'small-tech.org', 'kitten')
const KITTEN_TEMP_RUNTIME_ARCHIVE_PATH = path.join(KITTEN_TEMP_DIRECTORY, 'runtime.tar.xz')
// Note: The same-device temp directory is guaranteed to be on the same device
// as the rest of Kitten’s data so you can use, for example, fs.rename on it while
// you may not be able to do so with KITTEN_TEMP_DIRECTORY, which might be on
// a different partition.
const KITTEN_SAME_DEVICE_TEMP_DIRECTORY = path.join(KITTEN_DATA_HOME, 'tmp')

const KITTEN_RUNTIME_DIRECTORY = path.join(KITTEN_DATA_HOME, 'runtime')
const KITTEN_RUNTIME_BIN_DIRECTORY = path.join(KITTEN_RUNTIME_DIRECTORY, 'bin')
const KITTEN_DATA_DIRECTORY = path.join(KITTEN_DATA_HOME, 'data')
const KITTEN_GLOBAL_INTERNAL_DATABASE_DIRECTORY = path.join(KITTEN_DATA_DIRECTORY, '__db')
const KITTEN_DEPLOYMENTS_DIRECTORY = path.join(KITTEN_DATA_HOME, 'deployments')
const KITTEN_CLONES_DIRECTORY = path.join(KITTEN_DATA_HOME, 'clones')

const CONFIG_HOME = path.join(process.env.XDG_CONFIG_HOME || HOME, '.config')
const SYSTEMD_USER_DIRECTORY = path.join(CONFIG_HOME, 'systemd', 'user')
const KITTEN_SYSTEMD_UNIT_PATH = path.join(SYSTEMD_USER_DIRECTORY, 'kitten.service')

const paths = {
  BINARY_HOME,
  KITTEN_BINARY_PATH,

  DATA_HOME,
  SMALL_TECH_DATA_HOME,

  KITTEN_DATA_HOME,
  KITTEN_APP_DIRECTORY_PATH,
  KITTEN_WEB_APP_DIRECTORY_PATH,

  KITTEN_TLS_DIRECTORY,
  KITTEN_TLS_LOCAL_CERTIFICATE_DIRECTORY,
  KITTEN_TLS_GLOBAL_CERTIFICATE_DIRECTORY,
  
  KITTEN_TEMP_DIRECTORY,
  KITTEN_TEMP_RUNTIME_ARCHIVE_PATH,
  KITTEN_SAME_DEVICE_TEMP_DIRECTORY,

  KITTEN_RUNTIME_DIRECTORY,
  KITTEN_RUNTIME_BIN_DIRECTORY,
  KITTEN_DATA_DIRECTORY,
  KITTEN_GLOBAL_INTERNAL_DATABASE_DIRECTORY,
  KITTEN_DEPLOYMENTS_DIRECTORY,
  KITTEN_CLONES_DIRECTORY,

  CONFIG_HOME,
  SYSTEMD_USER_DIRECTORY,
  KITTEN_SYSTEMD_UNIT_PATH
}

export default paths
