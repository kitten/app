export const developmentTimeHotReloadScript = `
    <script>
      // Kitten development time hot reload script.
      let __kittenConnectionCount = 0
      let __kittenDevelopmentSocket
      let __kittenDevelopmentSocketReconnectionAttemptTimeoutId = null

      function __kittenConnectToDevelopmentSocket () {
        console.info('Attempting to connect to Kitten development socket.')
        __kittenDevelopmentSocket = new WebSocket(\`wss://\${location.host}/🐱/development-socket/\`)

        const socketOpenHandler = () => {
          clearInterval(__kittenDevelopmentSocketReconnectionAttemptTimeoutId)
          __kittenConnectionCount++
          if (__kittenConnectionCount === 2) {
            // Reconnected after Kitten restart. Ask for page reload.
            window.location.reload()
          } else {
            console.info('🐱 Kitten: connected to development-time hot reload socket.')
          }
        }

        const socketCloseHandler = () => {
          clearInterval(__kittenDevelopmentSocketReconnectionAttemptTimeoutId)
          __kittenDevelopmentSocketReconnectionAttemptTimeoutId = setTimeout(__kittenConnectToDevelopmentSocket, 10)
        }

        __kittenDevelopmentSocket.addEventListener('open', socketOpenHandler)
        __kittenDevelopmentSocket.addEventListener('close', socketCloseHandler)

        // This is mainly a fix for Firefox, which is the only browser that results in
        // the closing of the WebSocket connection at the start of an HTTP load request
        // (e.g., form POST) and subsequent reconnection, thereby firing of a premature
        // GET request to the server.
        // See: https://mastodon.ar.al/@aral/109104595295584049
        window.addEventListener('beforeunload', () => {
          clearInterval(__kittenDevelopmentSocketReconnectionAttemptTimeoutId)
          __kittenDevelopmentSocket.removeEventListener('open', socketOpenHandler)
          __kittenDevelopmentSocket.removeEventListener('close', socketCloseHandler)
          __kittenDevelopmentSocket.close()
        }) 
      }

      // In case the developer is using htmx to update the body of the page,
      // make sure the development script runs only once.
      if (!globalThis.kittenDevelopmentTimeHotReloadScriptAlreadyInitialised) {
        globalThis.kittenDevelopmentTimeHotReloadScriptAlreadyInitialised = true
        __kittenConnectToDevelopmentSocket()
      }
    </script>
  `
