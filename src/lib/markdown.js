/**
  Kitten’s Markdown support, based on MarkdownIt and its plugins.

  This library is used by both the ES Module Loader and main process.

  Copyright ⓒ 2024-present, Aral Balkan, Small Technology Foundation.
  Released under the AGPL 3.0 license.
*/

import MarkdownIt from 'markdown-it'
import markdownItAnchor from 'markdown-it-anchor'
import markdownItFootNote from 'markdown-it-footnote'
import markdownItImageFigures from 'markdown-it-image-figures'
import markdownItIns from 'markdown-it-ins'
import markdownItMark from 'markdown-it-mark'
import markdownItSub from 'markdown-it-sub'
import markdownItSup from 'markdown-it-sup'
import markdownItHighlightJS from 'markdown-it-highlightjs'
import markdownItTOC from 'markdown-it-toc-done-right'
import markdownItTaskCheckbox from 'markdown-it-task-checkbox'
import slugify from '@sindresorhus/slugify'

const markdownIt = new MarkdownIt({
  html: true,
  typographer: true,
  linkify: true
})
.use(markdownItAnchor, {
  // TODO: Make permalinks an option people can specify on the page. We don’t
  // have a page options feature yet so this is currently commented out.
  // permalink: markdownItAnchor.permalink.headerLink({ safariReaderFix: true })
  // TODO: Now that Markdown is rendered here, how would we set this?

  slugify: s => slugify(s)
})
.use(markdownItFootNote)
.use(markdownItImageFigures, {
  figcaption: "title"
})
.use(markdownItIns)
.use(markdownItMark)
.use(markdownItSub)
.use(markdownItSup)
.use(markdownItHighlightJS)
.use(markdownItTOC, {
  level: [2, 3],
  slugify: s => slugify(s)
})
.use(markdownItTaskCheckbox, {
  disabled: false,
  divWrap: true,
  divClass: 'task-checkbox',
  idPrefix: 'task-checkbox-',
  ulClass: 'task-list',
  liClass: 'task-list-item'
})

export default markdownIt
export { slugify }
