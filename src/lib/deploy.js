/**
  Deploys an app from a Git repository as a
  systemd --user service.

  Copyright ⓒ 2021-present, Aral Balkan
  Small Technology Foundation

  License: AGPL version 3.0.
*/

import os from 'node:os'
import fs from 'node:fs'
import path from 'node:path'
import childProcess from 'node:child_process'

import chalk from 'chalk'
import simpleGit from 'simple-git'

import paths from './paths.js'
import ensure from './ensure.js'
import GlobalInternalDatabase from '../GlobalInternalDatabase.js'

const git = simpleGit()

/**
  Format error message with optional details.

  @param {{
    message: string,
    details?: string,
    context?: 'cli' | 'web'
  }} parameters
*/
function withMessageAndDetails({ message, details, context = 'cli' }) {
  if (context === 'cli') {
    // Format for CLI.
    return `${chalk.bold.red('❌ Error:')} ${message}${details ? `\n${chalk.blue(details)}` : ''}`
  } else {
    // Format for web.
    return `<strong>${message}</strong> ${details || ''}`
  }
}

/**
  @param {string} gitHttpsCloneUrl - git clone url (HTTPS)
  @param {Object} options
*/
export default async function deploy (gitHttpsCloneUrl, options) {
  console.info('Deploying service.')
  console.info()

  // Create options for serve command to write into systemd unit.
  const hostname = os.hostname()
  const domain = options.domain || hostname
  const domainOption = `--domain=${domain}`
  const aliasesOption = options.aliases ? `--aliases=${options.aliases}` : ''
    
  // Check if Small Web host and domain token are provided. If they are,
  // we store them and also communicate progress to the Small Web host.
  const smallWebHostDomain = options['small-web-host-domain']? options['small-web-host-domain'] : false
  const progressDelegate = options['progress']? options['progress'] : false
  const domainToken = options['domain-token'] ? options['domain-token']: false
  const context = options['context']
  const smallWebHostDomainOption = smallWebHostDomain ? `--small-web-host-domain=${smallWebHostDomain}` : ''
  const domainTokenOption = domainToken ? `--domain-token=${domainToken}` : ''

  if (smallWebHostDomain && !domainToken) {
    // If a Small Web Host domain has been specified, a domain token MUST also be
    // specified but it is not. Exit with error as we cannot continue.
    throw new Error(withMessageAndDetails({
      message: '--small-web-host-domain specified but --domain-token not specified. Invalid configuration. Cannot continue.'
    }))
  }

  // If we’re being deployed by a Small Web Host (Domain instance), update it on our progress.
  const __db = await GlobalInternalDatabase.initialise()

  // A Small Web Host deploys Kitten twice: once for the prewarmed server and
  // once for the actual app that’s being deployed. In the first case, we will
  // get an option, --is-being-deployed-by-domain passed to us. In the second,
  // the /💕/deploy/ route is called. That route signals that we’re being deployed
  // by domain by setting __db.state.isBeingDeployedByDomain. In both cases, we
  // set that flag in the internal database as the Server itself will need to check
  // it to decide whether or not it needs to notify the Small Web Host when it
  // has successfully starting up.
  if (options['is-being-deployed-by-domain']) {
    console.info('🚚 Pre-warmed server is being deployed by Domain.')
    __db.state.isBeingDeployedByDomain = true
  }
  // Read the state as it might have been set by the /💕/deploy route.
  const beingDeployedByDomain = __db.state.isBeingDeployedByDomain

  let dispatchProgressUpdate = async (/** @type {string} */ _message) => {}

  if (beingDeployedByDomain || progressDelegate) {
    dispatchProgressUpdate = async (/** @type {string} */ message) => {
      if (beingDeployedByDomain) {
        const url = `https://${smallWebHostDomain}/domain/${domainToken}/status/${message}`
        let response
        try {
          response = await fetch(url)
        } catch (error) {
          throw new Error(withMessageAndDetails({
            message: 'Could not notify Domain of status update:',
            details: error,
            context
          }))
        }
      }
      if (progressDelegate) {
        progressDelegate(message)
      }
    }
  }

  try {
    await ensure.canEnableService()
  } catch (error) {
    await dispatchProgressUpdate('error-cannot-enable-service')
    throw new Error(withMessageAndDetails({
      message: `Cannot enable systemd service: ${error.message}`,
      context
    }))
  }

  const advice = 'To avoid errors, copy the HTTPS URL from your repository host’s web interface to ensure it’s correct.'
  if (!gitHttpsCloneUrl.startsWith('https://') || !gitHttpsCloneUrl.endsWith('.git')) {
    if (gitHttpsCloneUrl.startsWith('git@')) {
      await dispatchProgressUpdate('error-ssh-git-clone-url')
      throw new Error(withMessageAndDetails({
        message: 'Please provide an HTTPS protocol git clone URL, not SSH.',
        details: `It should start with https://, not git@. ${advice}`,
        context
      }))
    } else {
      await dispatchProgressUpdate('error-invalid-git-clone-url')
      throw new Error(withMessageAndDetails({
        message: 'Invalid git clone URL',
        details: `Must begin with https:// and end with .git. ${advice}`,
        context
      }))
    }
  }

  // Ensure that the hostname matches the domain and update it if it doesn’t.
  if (hostname !== domain) {
    console.info(`  • Hostname (${hostname}) does not match requested domain (${domain}), updating hostname to match.`) 
    await dispatchProgressUpdate('updating-hostname-to-match-domain')
    childProcess.execSync(`sudo hostnamectl set-hostname ${domain}`)
  }

  // Generate working copy path based on clone URL.
  // We prefix it with a timestamp of the current date (based on Number.MAX_SAFE_INTEGER,
  // the timestamps will not overflow for another 280,000 years or so, so we’re safe to
  // use this).
  const deploymentDirectoryName = Date.now() + '-' + gitHttpsCloneUrl.replace('https://', '').replace('.git', '').replace(/\//g, '.')
  const deploymentPath = path.join(paths.KITTEN_DEPLOYMENTS_DIRECTORY, deploymentDirectoryName)
  
  // Ensure main deployments directory exists.
  fs.mkdirSync(paths.KITTEN_DEPLOYMENTS_DIRECTORY, { recursive: true })
  
  // Make sure we start with a fresh deployment directory in case this app was deployed previously.
  fs.rmSync(deploymentPath, {recursive: true, force: true})

  console.info(`  • Cloning repository ${chalk.italic(`(${gitHttpsCloneUrl})`)}`)
  await dispatchProgressUpdate('cloning-app-repository')

  await git.clone(gitHttpsCloneUrl, deploymentPath)

  await dispatchProgressUpdate('app-repository-cloned')

  // TODO: Should we check here if deployed app’s version matches Kitten API version?

  await dispatchProgressUpdate('writing-systemd-service-unit')

  console.info(`  • Writing systemd service unit ${chalk.italic(`(${paths.KITTEN_SYSTEMD_UNIT_PATH})`)}`)

  const unit = `
  [Unit]
  Description=Kitten Service
  Documentation=https://codeberg.org/kitten/app/src/README.md

  [Service]
  Type=simple
  RestartSec=1s
  Restart=on-failure
  WorkingDirectory=${deploymentPath}
  Environment="PRODUCTION=true"
  # Environment="VERBOSE=true"
  ExecStart=${paths.KITTEN_BINARY_PATH} ${deploymentPath} ${domainOption} ${aliasesOption} ${smallWebHostDomainOption} ${domainTokenOption}

  [Install]
  WantedBy=default.target
  `

  // On some server operating systems (e.g., Fedora-based ones) the systemd “user” directory does not exist.
  // Make sure it does before trying to save the unit file so we don’t get a runtime error here.
  // See: https://codeberg.org/kitten/app/issues/44
  fs.mkdirSync(paths.SYSTEMD_USER_DIRECTORY, { recursive: true })
  fs.writeFileSync(paths.KITTEN_SYSTEMD_UNIT_PATH, unit, {encoding: 'utf-8'})

  await dispatchProgressUpdate('starting-kitten-service')

  console.info('  • Starting Kitten service.')

  // Just in case this app (or some other) was already deployed,
  // let’s be safe and reload existing daemons so we’re sure
  // the one just deployed is the one that’s running.
  // (In these cases, systemd will show the following warning:
  // Warning: The unit file, source configuration file or drop-ins of kitten.service
  // changed on disk. Run 'systemctl --user daemon-reload' to reload units.
  childProcess.execSync('systemctl --user daemon-reload')

  childProcess.execSync('systemctl restart --user kitten')

  await dispatchProgressUpdate('kitten-service-started')

  console.info('  • Installing Kitten service to launch automatically at boot, restart on errors, etc.')

  await dispatchProgressUpdate('installing-kitten-service')

  childProcess.execSync('systemctl enable --user kitten')

  await dispatchProgressUpdate('kitten-service-installed')

  console.info("\nDone.")
}
