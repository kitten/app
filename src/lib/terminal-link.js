/**
  Uses OSC 8 (https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda)
  to create clickable hyperlinks with custom text in terminal.

  Additionally, styles them with a dotted underline manually as some terminals
  don’t do this (yet, at least).

  e.g., See https://github.com/wez/wezterm/issues/4077#issuecomment-2324401900

  Terminal support for this feature appears to be good:
  https://github.com/Alhadis/OSC8-Adoption

  Based on:
  https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda?permalink_comment_id=5098507#gistcomment-5098507

  For sequence code documentation, please see
  https://gist.github.com/ConnerWill/d4b6c776b509add763e17f9f113fd25b#sequences
*/

const ESC = "\u001B"
const CSI = `${ESC}[`
const OSC = `${ESC}]`
const SEP = ";"
const BEL = "\u0007"
const TO = BEL
const HYPERLINK=`${OSC}8${SEP}${SEP}`
const HYPERLINK_END = `${HYPERLINK}${BEL}`
const DOTTED_UNDERLINE = `${CSI}4:4m`
const RESET_STYLE = `${CSI}0m`

/**
  @param {string} text
  @param {string} url
*/
export const link = (text, url) =>
  [DOTTED_UNDERLINE, HYPERLINK, url, TO, text, HYPERLINK_END, RESET_STYLE].join("");

export default link
