/**
  Kitten Package.
*/

import { execSync, execFileSync } from 'node:child_process'
import Version from './Version.js'

/**
  @typedef {{
    versionStamp: number,
    gitHash: string,
    apiVersion: number,
    nodeVersion: string,
    exactVersion: string,
    buildDate: Date,
    releaseDate: Date,
    _buildDate:string,
    _releaseDate:string
  }} RemoteVersionDetails
*/

/**
  Returns true if command exists on system, false otherwise.

  Cross-platform.

  @param {string} command
*/
function commandExists (command) {
  try {
    const commandToUse = (process.platform === 'win32') ? 'where.exe /Q' : 'which'
    execFileSync(commandToUse, [command], {env: process.env})
    return true
  } catch (error) {
    return false
  }
}

/**
  Represents the currently-installed Kitten package.

  Provides information about and manages updates of the
  package by communicating with Kitten’s deployment site
  (https://kittens.small-web.org).

  _Note that this class does NOT manage automatic updates.
  For that, please see the AutomaticUpdates class._
*/
export default class KittenPackage {
  /** @type {KittenPackage} */ static instance

  static getInstance () {
    if (this.instance === undefined) {
      this.instance = new this()
    }
    return this.instance
  }

  constructor () {
    this.currentVersion = Version.getInstance()
    this.apiVersion = this.currentVersion.apiVersion

    this.limitByApiVersion = this.apiVersion !== undefined
    this.versionCheckUrl = `https://kittens.small-web.org/latest/${this.apiVersion || ''}`

  }

  get hasCompatibleVersion () {
    return this.apiVersion !== undefined && this.compatibleVersion !== undefined
  }

  get isLatestReleaseVersion () {
    if (this.latestRecommendedVersion === undefined) return false
    return this.latestRecommendedVersion.versionStamp === this.currentVersion.versionStamp
  }

  get isMoreRecentThanReleaseVersion () {
    if (this.latestRecommendedVersion === undefined) return false
    return this.latestRecommendedVersion.versionStamp < this.currentVersion.versionStamp
  }

  get canBeUpgraded () {
    return this.latestRecommendedVersion !== undefined && this.currentVersion.versionStamp < this.latestRecommendedVersion.versionStamp
  }

  /**
    Upgrades the Kitten package to the latest recommended version.

    Note that if you’re running a development version, this will actually result
    in a downgrade to the latest available published package. Make sure your
    interface detects this state and prompts before downgrading.

    @param {number | undefined} apiVersion

    @throws
  */
  upgrade (apiVersion) {
    const _apiVersion = apiVersion === undefined ? this.latestRecommendedVersion?.apiVersion : apiVersion
    const installSpecificApiVersion = this.limitByApiVersion || apiVersion !== undefined

    const bashArgumentsIfAny = installSpecificApiVersion ? ` -s -- ${_apiVersion}` : ''
    const apiVersionRouteIfAny = installSpecificApiVersion ? `${_apiVersion}/` : ''

    const downloadUrl = `https://kittens.small-web.org/install/${apiVersionRouteIfAny}`
    const bashCommand = `bash${bashArgumentsIfAny}`

    let downloadCommand = ''
    switch (true) {
      case commandExists('curl'):
        downloadCommand = `curl -sL ${downloadUrl} | ${bashCommand}`
        break
      case commandExists('wget'):
        downloadCommand = `wget -qO- ${downloadUrl} | ${bashCommand}`
        break
      default:
        console.error(`Cannot find curl or wget on the system using 'which'. Please install one or the other and rerun this command to update Kitten.`)
        process.exit(1)
    }

    // This can throw.
    execSync(downloadCommand)
  }

  /**
    Fetches the latest version information from kittens.small-web.org.
  */
  async update () {
    /**
      @type {{
        latest: RemoteVersionDetails,
        compatible: RemoteVersionDetails | undefined
      }}
    */
    let versions

    try {
      versions = await(await fetch(this.versionCheckUrl)).json()
    } catch (error) {
      if (error.cause.code === 'ENOTFOUND') {
        // No idea why yet but on my machine, at least, Node (20.7.0, at least)
        // periodically fails to resolve DNS for kittens.small-web.org.
        throw new Error(`DNS resolution failed for kittens.small-web.org. Please try again later.`) 
      } else {
        throw new Error(`Could not reach kittens.small-web.org\n${error}`)
      }
    }

    this.latestVersion = versions.latest
    this.latestVersion.buildDate = new Date(this.latestVersion._buildDate)
    this.latestVersion.releaseDate = new Date(this.latestVersion._releaseDate)
    this.compatibleVersion = versions.compatible
    if (this.compatibleVersion !== undefined) {
      this.compatibleVersion.buildDate = new Date(this.compatibleVersion._buildDate)
      this.compatibleVersion.releaseDate = new Date(this.compatibleVersion._releaseDate)
    }

    this.latestRecommendedVersion = this.limitByApiVersion ? versions.compatible : versions.latest
  }
}
