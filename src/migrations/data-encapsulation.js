import fs from 'node:fs'
import path from 'node:path'
import paths from '../lib/paths.js'

export default function migration () {
  console.info('🧳 Migrating internal data structure to post 2024/07/14 19:09:13 state…')

  const KITTEN_DATA_HOME = paths.KITTEN_DATA_HOME
  const OLD_KITTEN_DATABASES_DIRECTORY = path.join(KITTEN_DATA_HOME, 'databases')
  const OLD_KITTEN_UPLOADS_DIRECTORY = path.join(KITTEN_DATA_HOME, 'uploads')
  const OLD_KITTEN_REPL_DIRECTORY = path.join(KITTEN_DATA_HOME, 'repl')

  // Create new data directory.
  const NEW_KITTEN_DATA_DIRECTORY = paths.KITTEN_DATA_DIRECTORY
  fs.mkdirSync(NEW_KITTEN_DATA_DIRECTORY, { recursive: true })

  // Copy all databases to new data directory.
  const databasePaths = fs.readdirSync(OLD_KITTEN_DATABASES_DIRECTORY)

  databasePaths.forEach(databasePath => {
    const sourcePath = path.join(OLD_KITTEN_DATABASES_DIRECTORY, databasePath)
    const destinationPath = path.join(NEW_KITTEN_DATA_DIRECTORY, databasePath)
    fs.cpSync(sourcePath, destinationPath, { recursive: true })
  })

  // Next, copy the uploads.
  const uploadsPaths = fs.readdirSync(OLD_KITTEN_UPLOADS_DIRECTORY)

  uploadsPaths.forEach(uploadPath => {
    const sourcePath = path.join(OLD_KITTEN_UPLOADS_DIRECTORY, uploadPath)
    const destinationPath = path.join(NEW_KITTEN_DATA_DIRECTORY, uploadPath, 'uploads')
    fs.cpSync(sourcePath, destinationPath, { recursive: true })
  })

  // Finally, copy the REPL history data.
  const replPaths = fs.readdirSync(OLD_KITTEN_REPL_DIRECTORY)

  replPaths.forEach(replPath => {
    const sourcePath = path.join(OLD_KITTEN_REPL_DIRECTORY, replPath)
    const destinationPath = path.join(NEW_KITTEN_DATA_DIRECTORY, replPath, 'repl')
    fs.cpSync(sourcePath, destinationPath, { recursive: true })
  })

  // Right, all data has been copied over, now remove the old directories.
  fs.rmSync(OLD_KITTEN_DATABASES_DIRECTORY, { force: true, recursive: true })
  fs.rmSync(OLD_KITTEN_UPLOADS_DIRECTORY, { force: true, recursive: true })
  fs.rmSync(OLD_KITTEN_REPL_DIRECTORY, { force: true, recursive: true })

  console.info('🧳 Migration complete.')
  console.info()
}
