### Special thanks to

This list is loosely based on the [All Contributors](https://allcontributors.org/) idea and curated by hand to say a personal thank-you to the people who have contributed to Kitten in substantial ways. 🐱💕

<table>
  <tr>
  <th></th><th>Name</th><th><a href='#contribution-key' title='Contribution emoji (jump to key)'>🔑</a></th><th>Details</th>
  </tr>
  <tr>
    <td><img class='avatar' src='https://codeberg.org/avatars/7c559f0dfaa0c30e04b9f7ccda5ec888' alt='Aral Balkan'></td>
    <td><a href='https://codeberg.org/aral'>Aral Balkan</a></td>
    <td>😻</td>
    <td><p> Chief meow.</p></td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://avatars.githubusercontent.com/u/300067?v=4' alt='A charcoal drawn triangular spiral with alternating white and black rings on a black background'>
    </td>
    <td><a href='https://github.com/dy'>Dmitry Ivanov</a></td>
    <td class='cemoji'><a href='https://github.com/dy/xhtm' title='Code'>💻</a></td>
    <td>
      <p><a href='https://codeberg.org/kitten/app/src/branch/main/src/lib/html.js'>Kitten’s template renderer</a> inlines and extends Dmitry’s <a href='https://github.com/dy/xhtm'>xhtm</a> library.</p>
      <p>Dmitry worked with me (Aral) over several days through <a href='https://github.com/dy/xhtm/issues?q=is%3Aissue+author%3Aaral'>more than a dozen issues</a> to improve xhtm and thus also Kitten’s HTML rendering. His help has been invaluable in ensuring one of Kitten’s core components works reliably.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://avatars.githubusercontent.com/u/105127?v=4' alt='A man with a beard looking at the camera, in front of an electronic drum set.'>
    </td>
    <td><a href='https://github.com/developit'>Jason Miller</a></td>
    <td class='cemoji'><a href='https://github.com/developit/htm' title='Code'>💻</a> </td>
    <td>
      <p>Jason’s <a href='https://github.com/developit/htm'>htm</a> is what Dmitry’s xhtm is based on and Jason’s <a href='https://github.com/developit/vhtml'>vhtml</a>, like xhtm, is now inlined into Kitten’s template renderer and extended with functionality like Kitten’s Markdown support.</p>
      <p>For eight months or so of its development, Kitten was using htm and vhtml verbatim as dependencies and could not have gotten to where it is today without them.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://avatars.githubusercontent.com/u/20725046?v=4' alt='Extreme close-up of an illustration of a yellow lion-like creature'>
    </td>
    <td><a href='https://github.com/Renerick'>Denis Palashevskii</a></td>
    <td class='cemoji'><a href='https://github.com/bigskysoftware/htmx/pull/2418/files' title='Code'>💻</a> </td>
    <td>
      <p>Denis was hugely helpful in tracking down and fixing <a href='https://discord.com/channels/725789699527933952/1046573806547910677/1219379104013488239'>a regression in HTMX 2 that was affecting the WebSocket extension</a> that Kitten uses for its Streaming HTML workflow, thereby helping us adopt HTMX 2 while still in pre-release.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://avatars.githubusercontent.com/u/65089?v=4' alt='A man with dark brown/black hair and glasses looking at the mirror while taking a selfie.'>
    </td>
    <td><a href='https://github.com/jeffturcotte'>Jeff Turcotte</a></td>
    <td class='cemoji'><a href='https://github.com/bigskysoftware/htmx/pull/2367' title='Code'>💻</a> </td>
    <td>
      <p>Jeff contributed <a href='https://github.com/bigskysoftware/htmx/pull/2367'>the Swap API normalisation code</a> to the HTMX 2 codebase, which we’re using in our fork of HTMX 2 in Kitten. This enables us to use <code>settle</code>, <code>swap</code> etc., attributes in out-of-band swaps (as used in Kitten’s Streaming HTML workflow). Jeff’s work, along with Denis’s, allowed us to adopt HTMX 2 in Kitten while it is still in pre-release.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://codeberg.org/avatars/4cf065a928bc23cca8222e3e3362932c' alt='A pencil drawing of a bee'>
    </td>
    <td><a href='https://codeberg.org/nonetoohappy'>Andrew Chou</a></td>
    <td class='cemoji'><a href='https://codeberg.org/kitten/app/issues/81' title='Ideas'>🤔</a></td>
    <td>
      <p>Thanks to Andrew’s idea, Kitten files have compound extensions (so what used to be <em>.page</em> is now <em>.page.js</em>, etc.)</p>
      <p>This means Kitten no longer needs any specialised tooling and Kitten files automatically get syntax highlighted in source code repositories. We were also able to remove the ES Module Loader, simplifying Kitten further.</p>
    </td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://codeberg.org/avatars/b9e7feda9abb69d709c8c5459d9c24cf' alt='Close-up of a white man with brown hair wearing glasses standing in front of trees with a white building in the background.'>
    </td>
    <td><a href='https://codeberg.org/sexybiggetje'>Martijn de boer</a></td>
    <td class='cemoji'><a href='https://codeberg.org/kitten/app/issues/60' title='Bug report'>🐛</a></td>
    <td><p> Martijn’s bug report led to the implementation of cross-platform support for Kitten.</p></td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://codeberg.org/avatars/f12b79db02a257cf6bbd48e22e49c05d' alt='Generic avatar: A white square with geometric patterns in green inside it'>
    </td>
    <td><a href='https://codeberg.org/mabu'>mabu</a></td>
    <td class='cemoji'><a href='https://codeberg.org/kitten/app/issues/56' title='Bug report'>🐛</a></td>
    <td><p>mabu’s bug report led to an eventual rewrite of <a href='https://codeberg.org/small-tech/auto-encrypt-localhost'>Auto Encrypt Localhost</a> in 100% JavaScript.</p></td>
  </tr>
  <tr>
    <td>
      <img class='avatar' src='https://codeberg.org/avatars/15694406ba7ebd0fee1596fc8ed208ce' alt='Generic avatar: A purple square with pink squares and rectangles in it'>
    </td>
    <td class='cemoji'><a href='https://codeberg.org/sorenmat'>sorenmat</a></td>
    <td><a href='https://codeberg.org/kitten/app/commits/branch/main/search?q=author%3Asorenmat' title='Documentation'>📖</a></td>
    <td><p>Kitten’s first pull request 😽</p></td>
  </tr>
</table>

### Contribution Key

♿️ accessibility | 💬 answering questions | 🐛 bug reports | 💻 code | 📖 documentation | 💡 examples | 📋 event organisation | 🔍 finding funding | 🤔 ideas | 🚇 infrastructure (hosting, etc.) | 🚧 maintenance | 🧑‍🏫mentoring | 📣 promotion |  👀 review | 🛡️ security | 🌍 translation | ⚠️ testing | ✅ tutorials | 📢 talks | 📹 videos 

